
$(document).ready(function(){

    /*$(document).ready(function(){
         $(".owl-home-carousel").owlCarousel({
         items: 1,
         loop: true,
         dots: true,
         nav: true,
         //navText: ["<i class='fa fa-angle-left' aria-hidden='true'>","<i class='fa fa-angle-right' aria-hidden='true'>"],
         navText: ["", ""],
         autoplay: true,
         smartSpeed: 1000,
         //animateIn: 'fadeIn',
         //animateOut: 'fadeOut',
         //mouseDrag: false,
         });
         });
     */

    $(".owl-home-carousel").owlCarousel({
        items: 1,
        loop: true,
        dots: true,
        nav: true,
        navSpeed: 1500,
        video:true,
        lazyLoad:true,
        navText: ["<i class='fa fa-angle-left' aria-hidden='true'>","<i class='fa fa-angle-right' aria-hidden='true'>"],
        //navText: ["", ""],
        autoplay: false,
        //smartSpeed: 100,
    });

    //controllo grandezza finestra browser e numero di elementi del carousel per gestire la visualizzazione o meno dei dots e del loop
    var related_item_count = parseInt($(this).find('.ps-product-rel').length);
    var like_item_count = parseInt($(this).find('.ps-product-like').length);
    var window_width = $( window  ).width();

    //carousel prodotti correlati
    $(".owl-slider-related").owlCarousel({
        items: 4,
        dots: true,
        nav: false,
        lazyLoad:true,
        autoplay: false,
        smartSpeed: 100,
        responsive : {
            // breakpoint from 0 up
            0 : {
                items : 1,
                slideBy: 1
            },
            // breakpoint from 768 up
            768 : {
                items : 2,
                slideBy: 2
            },
            1024 : {
                items : 4,
                slideBy: 4
            }
        },

        /*items: 4,
        loop: true,
        dots: true,
        nav: false,
        lazyLoad:true,
        //navText: ["<i class='fa fa-angle-left' aria-hidden='true'>","<i class='fa fa-angle-right' aria-hidden='true'>"],
        autoplay: false,
        smartSpeed: 100,
        responsive : {
            // breakpoint from 0 up
            0 : {
                items : 1,
                slideBy: 1
            },
            // breakpoint from 768 up
            768 : {
                items : 2,
                slideBy: 2
            },
            1024 : {
                items : 4,
                slideBy: 4
            }
        },
        onInitialize: function(event) {

            // controllo se sono sopra i 1024px e se ho meno di 4 item oppure sopra i 768 e se ho meno di 2 item oppure sotto i 768 e se ho 1 item
            if (window_width >= '1024' && related_item_count <= 4) {
                this.settings.loop = false;
                this.settings.nav = false;
                this.settings.dots = false;
            }else if(window_width >= '768' && related_item_count <= 2){
                this.settings.loop = false;
                this.settings.nav = false;
                this.settings.dots = false;
            }else if(window_width >= '0' && related_item_count == 1 ){
                this.settings.loop = false;
                this.settings.nav = false;
                this.settings.dots = false;
            }else {
                this.settings.loop = true;
                this.settings.nav = false;
                this.settings.dots = true;
            }
        },*/
    });
    $('.owl-slider-related .owl-nav').remove();

    //carousel prodotti potrebbero piacere
    $(".owl-slider-like").owlCarousel({
        items: 4,
        loop: false,
        dots: true,
        nav: false,
        lazyLoad:true,
        //navText: ["<i class='fa fa-angle-left' aria-hidden='true'>","<i class='fa fa-angle-right' aria-hidden='true'>"],
        autoplay: false,
        smartSpeed: 100,
        responsive : {
            // breakpoint from 0 up
            0 : {
                items : 1,
                slideBy: 2
            },
            // breakpoint from 768 up
            768 : {
                items : 2,
                slideBy: 2
            },
            1024 : {
                items : 4,
                slideBy: 4
            }
        },
        onInitialize: function(event) {

            // controllo se sono sopra i 1024px e se ho meno di 4 item oppure sopra i 768 e se ho meno di 2 item oppure sotto i 768 e se ho 1 item
            /*if (window_width >= '1024' && like_item_count <= 4) {
                this.settings.loop = false;
                this.settings.nav = false;
                this.settings.dots = false;
            }else if(window_width >= '768' && like_item_count <= 2){
                this.settings.loop = false;
                this.settings.nav = false;
                this.settings.dots = false;
            }else if(window_width >= '0' && like_item_count == 1 ){
                this.settings.loop = false;
                this.settings.nav = false;
                this.settings.dots = false;
            }else {
                this.settings.loop = false;
                this.settings.nav = false;
                this.settings.dots = true;
            }*/
            if (window_width >= '1024' && like_item_count <= 4) {
                this.settings.dots = false;
            }else if(window_width >= '768' && like_item_count <= 2){
                this.settings.dots = false;
            }else if(window_width >= '0' && like_item_count == 1 ){
                this.settings.dots = false;
            }else {
                this.settings.dots = true;
            }
        },
    });
    $('.owl-slider-like .owl-nav').remove();

    $(".owl-slider-img-mobile").owlCarousel({
        items: 1,
        loop: false,
        dots: false,
        nav: false,
        autoplay: true,
        autoplayTimeout: 2000,
        autoplayHoverPause: true,
        smartSpeed: 2000,
        /*responsive: {
            0: {
                items: 1,
                nav: false,
                loop: false
            },
        }*/
    });


    $('.owl-slider-img-mobile .owl-nav').remove();

    $(".ps-cart--sidebar .ps-cart__header .ps-btn--close.close-sidebar").on("click", function (e) {
        e.preventDefault();
        console.log('cliccato');
        /*$(".ps-cart--sidebar").removeClass("active");*/
        $("#sidebar-cart").removeClass("active");
        $(".ps-site-overlay").removeClass("active");
    });

    /* Inizializzazione secondo search visibile sulla barra in mobile(conflitto con quello del menu) */
    /*
    $(".ps-search-btn-2").on("click", function (e) {
        e.preventDefault();
        $(".ps-search-2").addClass("active");
    });
    $(".ps-search-2").find(".ps-btn--close").on("click", function (e) {
        e.preventDefault();
        $(".ps-search-2").removeClass("active");
    })*/
});