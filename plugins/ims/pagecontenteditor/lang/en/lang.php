<?php return [
    'plugin' => [
        'name' => 'page Content Editor',
        'description' => ''
    ],
    'pagecontentcontroller' => [
        'page' => 'Page Link',
        'widget' => 'Widget/Section',
        'order' => 'Order no.',
        'date' => 'Date',
        'title' => 'Title',
        'subtitle' => 'Subtitle',
        'content_intro' => 'Intro',
        'content' => 'Content',
        'published' => 'Published',
        'main_image' => 'Main image',
        'gallery' => 'Gallery images',
        'media_file' => 'File',
        'colorder' => 'Order',
        'colpage' => 'Page',
        'coltitle' => 'Title',
        'colpublished' => 'Published',
        'colupdated_at' => 'Updated at',
        'colwidget' => 'Widget',
        'plugin_name' => 'Page Content'
    ],

];