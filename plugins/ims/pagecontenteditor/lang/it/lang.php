<?php return [
    'plugin' => [
        'name' => 'page Content Editor',
        'description' => ''
    ],
    'pagecontentcontroller' => [
        'page' => 'Pagina di riferimento',
        'widget' => 'Widget/Sezione',
        'order' => 'Ordine.',
        'date' => 'Data',
        'title' => 'Titolo',
        'subtitle' => 'Sottotitolo',
        'content_intro' => 'Introduzione',
        'content' => 'Contenuto',
        'published' => 'Pubblicato',
        'main_image' => 'Immagine principale',
        'gallery' => 'Galleria immagini',
        'media_file' => 'Documenti',
        'colorder' => 'Ordine',
        'colpage' => 'Pagina',
        'coltitle' => 'Titolo',
        'colpublished' => 'Pubblicato',
        'colupdated_at' => 'Aggiornato',
        'colwidget' => 'Widget/Sezione',
        'plugin_name' => 'Gestore contenuti'
    ]
];