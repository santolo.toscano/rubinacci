<?php namespace Ims\PageContentEditor\Models;

use Cms\Classes\Page;
use Cms\Classes\Theme;
use Model;

/**
 * Model
 */
class PageContent extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Validation
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'ims_pagecontenteditor_contents';

    public $implement = ['RainLab.Translate.Behaviors.TranslatableModel'];

    public $translatable = [
        'title',
        'content',
        'content_intro',
        'subtitle',
        'advanced_content'
    ];

    /**
     * @var string Fake var for wysiwyg editing
     */
    public $wysiwyg = '';

    public $attachOne = [
        'mediaFile' => ['System\Models\File'],
    ];
	public $attachMany = [
        'gallery' => ['System\Models\File', 'order' => 'sort_order asc'],
        
    ];


    public function getPageOptions()
    {
        $currentTheme = Theme::getEditTheme();
        $allPages = Page::listInTheme($currentTheme, true);

        $pages = $allPages->lists('title','fileName');

        return array_merge($pages, [ '' => ' - No Page- ']);
    }

}