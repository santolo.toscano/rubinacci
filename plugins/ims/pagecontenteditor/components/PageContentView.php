<?php namespace Ims\PageContentEditor\Components;

use Cms\Classes\ComponentBase;
use Ims\PageContentEditor\Models\PageContent;

class PageContentView extends ComponentBase
{

    public function componentDetails()
    {
        return [
            'name'        => 'Page Content View',
            'description' => 'Inject content in pages'
        ];
    }

    public function onRun()
    {
		$page = $this->controller->getPage()->getFileName();
        
			
		$contents = PageContent::where('page', $page)->where('widget', '=', '')->orderBy('order')->get();	
		$contentsWidget = PageContent::where('page', $page)->where('widget', '<>', '')->orderBy('order')->get();
		
		$this->page['pageContents'] = $contents;
		
		$this->page['pageContentsAll'] = PageContent::orderBy('order')->get();
		
		//$contentsEmptyWidget = $contents->
		
		$this->page['widget'] = $contentsWidget->keyBy('widget');

        $allcontents = PageContent::orderBy('order')->get();
        $this->page['allWidget'] = $allcontents->keyBy('widget');
		
		if ( $contents->count() > 0 )
		{
			$this->page['pageContent'] = $contents[0];
		}
    }

    public function defineProperties()
    {
        return [];
    }

}