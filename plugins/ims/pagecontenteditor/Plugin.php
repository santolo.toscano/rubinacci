<?php namespace Ims\PageContentEditor;

use System\Classes\PluginBase;
use Backend;


class Plugin extends PluginBase
{
    public function registerComponents()
    {
        return [
            'Ims\PageContentEditor\Components\PageContentView' => 'pageContentView'
        ];
    }
    public function registerSettings()
    {
    }
	
	public function registerNavigation()
    {
        return [
            'pagecontentcontroller' => [
                'label' => 'ims.pagecontenteditor::lang.pagecontentcontroller.plugin_name',
                'url'   => Backend::url('ims/pagecontenteditor/pagecontentcontroller'),
                'icon'        => 'icon-pencil',
                'permissions' => ['ims.pagecontenteditor.*'],
                'order'       => 105,
            ],
        ];
    }
	
	public function registerMarkupTags()
	{
		return [
			'filters' => [
				'die_dump' => [$this, 'die_dump']
			]
		];
	}
	
	public function die_dump($var)
	{
		return dd($var);
	}
	
	public function registerPermissions()
    {
        return [
            'ims.pagecontenteditor.*' => ['tab' => 'ims.pagecontenteditor::lang.plugin.name', 'label' => 'PageContentEditor']
        ];
    }
}
