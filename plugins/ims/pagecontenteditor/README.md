## Cms Page Editor Plugin
Require Wysiwyg Plugin

### List of wysiwyg editors tested in plugin
1. CKEditor

### Configuration (REQUIRED)
To configure this plugin go to backend *Settings* then find *CMS* in left side bar, then click on *Wysiwyg Editors*, select CKEDITOR

## FAQ
1. Who can use this plugin?
> Any one can use this plugin for editing content.
2. Where images / files are uploaded?
> You can find uploaded images / files in __storage/app/media__ directory.
