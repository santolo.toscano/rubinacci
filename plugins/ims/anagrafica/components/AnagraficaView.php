<?php namespace Ims\Anagrafica\Components;

use Cms\Classes\ComponentBase;
use Ims\Anagrafica\Models\Anagrafica;

class AnagraficaView extends ComponentBase
{
    //
    // Properties
    //
    public function componentDetails()
    {
        return [
            'name'        => 'Anagrafica View',
            'description' => 'Visualizza informazione dell anagrafica',
			'icon' => 'bar-chart'
        ];
    }

    public function onRun()
    {
        $this->page['info'] = Anagrafica::first();
    }

	
	
}
