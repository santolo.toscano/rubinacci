<?php namespace Ims\Anagrafica;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
        return [
            'Ims\Anagrafica\Components\AnagraficaView' => 'AnagraficaView'
            ];
    }

    public function registerSettings()
    {
    }
}
