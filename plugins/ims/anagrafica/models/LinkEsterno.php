<?php namespace Ims\Anagrafica\Models;

use Model;

/**
 * Model
 */
class LinkEsterno extends Model
{
    use \October\Rain\Database\Traits\Validation;
    

    /**
     * @var string The database table used by the model.
     */
    public $table = 'ims_anagrafica_external_link';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
	
	public $belongsTo = [
		'anagrafica' => Anagrafica::class
	];

    public function link_type_name(){
        $link_type = Anagrafica::pluck('link_type');

        $link_type_nameOptions = [''];
        foreach($link_type as $item)  {

            // json decoding
            $decoded = json_decode($item, true);

            if (count($decoded)>0)
                foreach($decoded as $itemdec) {
                    $link_type_nameOptions[$itemdec] = $itemdec;
                }
            else
                return [];

        }

        return $link_type_nameOptions;
    }
}
