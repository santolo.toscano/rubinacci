<?php namespace Ims\Anagrafica\Models;

use Model;

/**
 * Model
 */
class Anagrafica extends Model
{
    use \October\Rain\Database\Traits\Validation;
    

    /**
     * @var string The database table used by the model.
     */
    public $table = 'ims_anagrafica_info';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $hasMany = [
        'links' => LinkEsterno::class,
    ];

    public $jsonable = ['link_type'];
}
