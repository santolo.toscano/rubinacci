/*
Navicat MySQL Data Transfer

Source Server         : IMSOL2012
Source Server Version : 50717
Source Host           : 192.168.10.251:3306
Source Database       : developmentdb

Target Server Type    : MYSQL
Target Server Version : 50717
File Encoding         : 65001

Date: 2019-12-10 17:56:47
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for ims_anagrafica_external_link
-- ----------------------------
DROP TABLE IF EXISTS `ims_anagrafica_external_link`;
CREATE TABLE `ims_anagrafica_external_link` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `anagrafica_id` int(11) NOT NULL,
  `link_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link_address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
SET FOREIGN_KEY_CHECKS=1;
