/*
Navicat MySQL Data Transfer

Source Server         : IMSOL2012
Source Server Version : 50717
Source Host           : 192.168.10.251:3306
Source Database       : developmentdb

Target Server Type    : MYSQL
Target Server Version : 50717
File Encoding         : 65001

Date: 2019-12-10 17:56:34
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for ims_anagrafica_info
-- ----------------------------
DROP TABLE IF EXISTS `ims_anagrafica_info`;
CREATE TABLE `ims_anagrafica_info` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `business_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `p_iva` varchar(13) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_2` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `booking_engine` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `struttura_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link_type` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `gmaps_url` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_code` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `has_whatsapp` tinyint(1) NOT NULL,
  `region` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
SET FOREIGN_KEY_CHECKS=1;
