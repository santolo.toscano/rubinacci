<?php

Route::get('api/reportmanager/schema/{id}', array('as' => 'schemaReport', 'uses' => 'Ims\ReportManager\Controllers\ReportApiController@schema'));
Route::get('api/reportmanager/data/{id}', array('as' => 'dataReport', 'uses' => 'Ims\ReportManager\Controllers\ReportApiController@data'));
Route::get('api/reportmanager/data/{id}/{record}', array('as' => 'dataReport', 'uses' => 'Ims\ReportManager\Controllers\ReportApiController@data'));
Route::get('api/reportmanager/definition/{id}', array('as' => 'definitionReport', 'uses' => 'Ims\ReportManager\Controllers\ReportApiController@definition'));

Route::get('api/reportmanager/test/{id}', array('as' => 'testReport', 'uses' => 'Ims\ReportManager\Controllers\ReportApiController@test'));