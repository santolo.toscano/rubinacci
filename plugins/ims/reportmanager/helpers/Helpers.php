<?php namespace Ims\ReportManager\Helpers;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
//use Ims\Management\Models\ViewReportCantiere;
use October\Rain\Support\Facades\Schema;

Class Helpers {

	public function apiArrayResponseBuilder($statusCode = null, $message = null, $data = [])
	{
		$arr = [
			'status_code' => (isset($statusCode)) ? $statusCode : 500,
			'message' => (isset($message)) ? $message : 'error'
		];
		if (count($data) > 0) {
			$arr['data'] = $data;
		}

		return response()->json($arr, $arr['status_code']);
		//return $arr;
		
	}

    public function apiArrayResponseBuilderWithCollections($statusCode = null, $message = null, $data = [], $collections = [])
    {
        $arr = [
            'status_code' => (isset($statusCode)) ? $statusCode : 500,
            'message' => (isset($message)) ? $message : 'error'
        ];
        if (count($data) > 0) {
            $arr['data'] = $data;
        }

        $arr['collections'] = $collections;

        return response()->json($arr, $arr['status_code']);
        //return $arr;
    }


    /**
     * Ritorna la rappresentazione pulita del model
     * @param Model $model
     * @return \Illuminate\Http\JsonResponse
     */
    public function returnRawDataFromModel($model)
    {
        $data = $model::all();
        return response()->json($data);
    }
	
	 /**
     * Ritorna la rappresentazione pulita del model
     * @param $tableName
     * @param null $record
     * @return \Illuminate\Http\JsonResponse
     * @internal param Model $model
     */
    public function returnRawData($data, $tableName)
    {
        $columns = Schema::getColumnListing($tableName);

        // Legge tutti i campi di formato data
        $date_columns = [];
        for ($i = 0; $i < count($columns); $i++) {
            $type = DB::getSchemaBuilder()->getColumnType($tableName, $columns[$i]);
            if ( $type == 'datetime' )
            {
                array_push($date_columns, $columns[$i]);
            }
        }

        // Cicla tutti i record e cambia il valore dei campi data
        for ($i = 0; $i < count($data); $i++) {

            for ($c = 0; $c < count($date_columns); $c++) {
                $column_name = $date_columns[$c];
                if ( $data[$i]->$column_name ) {
                    $newdate = Carbon::createFromFormat('Y-m-d H:i:s', $data[$i]->$column_name)->setTimezone('Europe/Rome')->format('Y/m/d H:i:s');
                    $data[$i]->$column_name = $newdate;
                }
            }

        }

        //$date = Carbon::createFromFormat('Y-m-d H:i:s', $data[0]->start_date)->format('d/m/Y');
        //dd($date);
        //$data[0]->start_date = $data[0]->start_date;

        return response()->json($data);
    }

    /**
     * Ritorna la rappresentazione pulita del model
     * @param $tableName
     * @param null $record
     * @return \Illuminate\Http\JsonResponse
     * @internal param Model $model
     */
    public function returnRawDataFromTable($tableName, $record = null)
    {
        if ( $record )
        {
            $data = \DB::table($tableName)->where('id', $record)->get();
        }
        else
        {
            $data = \DB::table($tableName)->get();
        }

        $columns = Schema::getColumnListing($tableName);

        // Legge tutti i campi di formato data
        $date_columns = [];
        for ($i = 0; $i < count($columns); $i++) {
            $type = DB::getSchemaBuilder()->getColumnType($tableName, $columns[$i]);
            if ( $type == 'datetime' || $type == 'date' )
            {
                array_push($date_columns, $columns[$i]);
            }

        }

        // Cicla tutti i record e cambia il valore dei campi data
        for ($i = 0; $i < count($data); $i++) {

            for ($c = 0; $c < count($date_columns); $c++) {
                $column_name = $date_columns[$c];
                if ( $data[$i]->$column_name ) {
					// verifica se date o datetime
					if ( strlen($data[$i]->$column_name) > 10 )
					{
						//$newdate = Carbon::createFromFormat('Y-m-d H:i:s', $data[$i]->$column_name)->setTimezone('Europe/Rome')->format('m/d/Y H:i:s');
						$newdate = Carbon::createFromFormat('Y-m-d H:i:s', $data[$i]->$column_name)->setTimezone('Europe/Rome')->toDateTimeString();
					}
					else
					{
						//$newdate = Carbon::createFromFormat('Y-m-d', $data[$i]->$column_name)->setTimezone('Europe/Rome')->format('m/d/Y');
						$newdate = Carbon::createFromFormat('Y-m-d', $data[$i]->$column_name)->setTimezone('Europe/Rome')->toDateString(); // 1975-12-25
					}
					$data[$i]->$column_name = $newdate;

                }
            }

        }

        //$date = Carbon::createFromFormat('Y-m-d H:i:s', $data[0]->start_date)->format('d/m/Y');
        //dd($date);
        //$data[0]->start_date = $data[0]->start_date;

        return response()->json($data);
    }


    /**
     * @param Model $model
     */
    public function returnSchemaFromModel($model)
    {
        ["field" => ['name' => "cantiere_nome", 'type' => "text"]];
    }


    public function getTableSchema($tableName, $exclude = [])
    {
        $columns = Schema::getColumnListing($tableName); // users table

        $array = array(count($columns));

        for ($i = 0; $i < count($columns); $i++) {

            if (in_array($columns[$i], $exclude))
            {
                continue;
            }

            $type = DB::getSchemaBuilder()->getColumnType($tableName, $columns[$i]);
            $dateFormat = '';
            if( $type == 'integer' || $type == 'smallint' || $type == 'decimal' )
                $jsonType = 'number';
            elseif ($type == 'string' || $type == 'text')
                $jsonType = 'text';
            elseif ($type == 'tinyint')
                $jsonType = 'bolean';
            elseif ($type == 'datetime') {
                $jsonType = 'date';
                $dateFormat = "YYYY-M-D h:mm:s";
            }
            elseif ($type == 'date') {
                $jsonType = 'date';
                $dateFormat = "YYYY-M-D";
				//$dateFormat = "YYYY-M-D";
            }			
            else {
                $jsonType = 'text';
            }

            $definition = ['name' => $columns[$i], 'type' => $jsonType];
            if ( $dateFormat )
            {
                $definition["dateFormat"] = $dateFormat;
            }
            $array[$i] = $definition;

        }

        return $array;
    }

    public function reportSchema($tableName)
    {
        //$table_name = 'view_report_cantiere';
        $arraySchema = $this->getTableSchema($tableName);

        $data = ["fields" => $arraySchema];

        return response()->json($data);
    }

    /*
    public function reportSingoloCantiereData($id)
    {
        $data = ViewReportCantiere::where('cantiere_id', $id)->get();
        return response()->json($data);
    }

    public function reportCantieriData()
    {
        $data = ViewReportCantiere::get();
        return response()->json($data);
    }
    */


}