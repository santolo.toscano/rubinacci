<?php namespace Ims\ReportManager;

use System\Classes\PluginBase;
use Backend;

class Plugin extends PluginBase
{
    public function pluginDetails()
    {
        return [
            'name'        => 'Report Manager',
            'description' => 'Implementing Reports',
            'author'      => 'Ims',
            'icon'        => 'icon-file-text-o'
        ];
    }

    public function registerComponents()
    {
    }

    /*
    public function boot()
    {

        Event::listen('backend.menu.extendItems', function($manager) {
            $manager->addMainMenuItems('Ims.Schedulerstatics', [
                'schedulerstatics'        => [
                    'label'       => 'Report',
                    'icon'        => 'icon-file-text-o',
                    'url'         => Backend::url('ims/scheduler/reportcontroller'),
                    'permissions' => ['ims.scheduler.*'],
                    'order'       => 8,
                    'sideMenu'    => [
                        'report' =>[
                            'label'       => 'Report',
                            'icon'        => 'icon-file-text-o',
                            'url'         => Backend::url('ims/scheduler/reportcontroller'),
                        ],
                        'reportmanager' => [
                            'label'          => 'Report Manager',
                            'url'            => Backend::url('ims/reportmanager/reportdefinitioncontroller'),
                            'icon'        => 'icon-file-text-o',
                        ],
                    ]

                ]
            ]);
        });
    }
    */

    public function registerSettings()
    {
    }


    public function registerNavigation()
    {
        return [
            'reportmanager' => [
                'label'          => 'Report Manager',
                'url'            => Backend::url('ims/reportmanager/reportdefinitioncontroller'),
                'icon'        => 'icon-file-text-o',
				'permissions'  => ['ims.reportmanager.*'],
                'order'       => 102
            ],
        ];
    }


    public function registerFormWidgets()
    {
        return [
            'Ims\ReportManager\Widgets\Report' => 'report',
        ];
    }
	

	public function registerPermissions()
    {
        return [
            'ims.reportmanager.*' => [
				'tab' => 'ReportManager',
				'label' => 'Manage reportmanager'
			],
        ];
    }


}
