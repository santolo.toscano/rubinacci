<?php namespace Ims\ReportManager\Models;

use Model;

/**
 * Work Model
 */
class ViewReportCantiere extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'view_report_cantiere';

}
