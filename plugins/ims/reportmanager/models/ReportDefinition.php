<?php namespace Ims\ReportManager\Models;

use Model;

/**
 * Model
 */
class ReportDefinition extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'ims_reportmanager_reportdesigner';

    use \October\Rain\Database\Traits\Validation;



    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
	
		
	public function getReportTypeOptions()
	{
		return ['' => 'Report di sistema', 'user' => 'Report utente', 'id' => 'Documento'];
	}

	public function getParametersTypeOptions()
	{
		return ['' => 'nessun filtro', 'id' => 'id', 'date' => 'data singola', 'mindate' => 'data maggiore', 'range' => 'intervallo di date', 'value' => 'valore'];
	}
	
	public function getParametersFieldOptions()
	{
		return ['' => 'nessun filtro', 'id' => 'id', 'id' => 'data singola', 'range' => 'intervallo di date', 'value' => 'valore'];
	}


}
