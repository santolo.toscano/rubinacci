<?php

namespace Ims\ReportManager\Controllers;

use Backend\Classes\Controller;
use Illuminate\Support\Facades\App;
use Ims\ReportManager\Helpers\Helpers;
use Ims\ReportManager\Models\ReportDefinition;

class ReportApiController  extends Controller
{

    protected $helpers;

    /**
     * ReportApiController constructor.
     * @param $helpers
     */
    public function __construct(Helpers $helpers)
    {
        $this->helpers = $helpers;
    }

    public function definition($id)
    {
        return response()->json(ReportDefinition::find($id)->report_definition_json);
    }

    public function schema($id)
    {
        $report = ReportDefinition::find($id);
        if ( $report )
        {
            if ( $report->table_view_name ) {
                $tableName = ReportDefinition::find($id)->table_view_name;
                return $this->helpers->reportSchema($tableName);
            }
            else if ( $report->model_name )
            {
                $modelName = $report->model_name;
                $model = App::make($modelName);
                $arraySchema = $model->getModelSchema();
                $data = ["fields" => $arraySchema];
                return response()->json($data);
            }
        }
        return response()->json([]);
    }


    /**
     * Funzione utilizzata sia per tutti i record che per singolo record
     * @param $id
     * @param null $record
     * @return \Illuminate\Http\JsonResponse
     */
    public function data($id, $record = null)
    {
        
		$report = ReportDefinition::find($id);
        if ( $report )
        {
            if ( $report->table_view_name ) {
				// verifica se applicare filtri
				if ( $report->parameters_type && $record )
				{
					switch ( $report->parameters_type )
					{
						case 'mindate':
							// la data arriva in formato europe/rome su db stanno sempre UTC
							$carbon = \Carbon\Carbon::createFromFormat("d-m-Y  H:i:s", $record.' 00:00:00', 'Europe/Rome');
							$data = \DB::table($report->table_view_name)->where($report->parameters_field, '>=', $carbon->setTimezone('UTC'))->get();
							break;
                        case 'id':
                            $data = \DB::table($report->table_view_name)->where($report->parameters_field, '=', $record)->get();
                            break;
                        case 'range':
                            $records = explode('|', $record);
                            $carbonFrom = \Carbon\Carbon::createFromFormat("d-m-Y  H:i:s", $records[0].' 00:00:00', 'Europe/Rome');
                            $data = \DB::table($report->table_view_name)->where($report->parameters_field, '>=', $carbonFrom->setTimezone('UTC'));
                            if ( count($records) > 1 )
                            {
                                $carbonTo = \Carbon\Carbon::createFromFormat("d-m-Y  H:i:s", $records[1].' 00:00:00', 'Europe/Rome')->addDays(1);
                                $data = $data->where($report->parameters_field, '<=', $carbonTo->setTimezone('UTC'));
                            }
                            $data = $data->get();
                            break;
					}
					
					return $this->helpers->returnRawData($data, $report->table_view_name);
				}
				else
				{
	                return $this->helpers->returnRawDataFromTable($report->table_view_name, $record);
				}

            }
            else if ( $report->model_name )
            {
				if ( $report->parameters_type && $record )
				{
					if ( $record != '_definition_' )
					{
						switch ( $report->parameters_type )
						{
							case 'id':
								$modelName = $report->model_name;
								if ( $report->parameters_field == 'id' )
								{
									$model = $modelName::find($record);
									$records = $model ? $model->getModelData() : [];
								}
								else
								{
									//$modelData = app($report->model_name)->where($report->parameters_field, '>=', $carbon);
									//$records = app($report->model_name)->getModelData($modelData);
								}
								break;
							case 'mindate':
								$carbon = \Carbon\Carbon::createFromFormat("d-m-Y  H:i:s", $record.' 00:00:00', 'Europe/Rome');
								$modelData = app($report->model_name)->where($report->parameters_field, '>=', $carbon->setTimezone('UTC'));
								$records = app($report->model_name)->getModelData($modelData);
								break;
							case 'range':
								$records = explode('|', $record);
								$carbonFrom = \Carbon\Carbon::createFromFormat("d-m-Y  H:i:s", $records[0].' 00:00:00', 'Europe/Rome');
								$modelData = app($report->model_name)->where($report->parameters_field, '>=', $carbonFrom->setTimezone('UTC'));
								if ( count($records) > 1 )
								{
									$carbonTo = \Carbon\Carbon::createFromFormat("d-m-Y  H:i:s", $records[1].' 00:00:00', 'Europe/Rome')->addDays(1);
									$modelData = $modelData->where($report->parameters_field, '<=', $carbonTo->setTimezone('UTC'));
								}
								$records = app($report->model_name)->getModelData($modelData);
								break;
							default:
								$records = [];
						}
					}
					else
					{
						// designmode -> solo i primi 10 records
						$model = app($report->model_name)::firstOrNew([]);
						$records = $model->getModelData()->take(10);
					}
                }
                else
                {
                    // nessun filtro
					$model = app($report->model_name)::firstOrNew([]);
                    $records = $model->getModelData();
                }
				
				
				

                return response()->json($records);

                //$data = ["fields" => $records];
            }
        }

        return response()->json([]);
    }

    public function test($id)
    {

    }
}
