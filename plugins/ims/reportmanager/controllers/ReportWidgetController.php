<?php namespace Ims\ReportManager\Controllers;

use App;
use BackendMenu;
use Backend\Classes\Controller;
use Config;
use File;
use Illuminate\Http\Request;
use Ims\ReportManager\Models\ReportDefinition;
use Ims\ReportManager\Widgets\Report;
use Lang;
use Yaml;

class ReportWidgetController extends Controller
{
    protected function getWidget()
    {
        $widget  = new Report($this);
        $widget->alias = "report";
        $widget->bindToController();

        return $widget;
    }

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Ims.ReportManager', 'reportmanager', 'report');

        $widget  = $this->getWidget();
    }

    /**
     * Controller index action.
     */
    public function index()
    {
        $this->pageTitle = "Report Demo";

        // Necesasria per poter far il rendering
        $widget = $this->getWidget();
        $widget->reportMode = 'index';
        return $widget->render();
    }

    /**
     * Lancia il widget di report editor
     * @param $id
     * @return mixed|string
     */
    public function design($id)
    {
        // Necessaria per poter far il rendering
        $widget = $this->getWidget();
        $widget->reportDefinitonID = $id;
        $widget->reportMode = "edit";
        $this->pageTitle = "Report"; //Lang::get('vojtasvoboda.reservationscalendar::lang.page_title');
        return $widget->render();
    }

    /**
     * Lancia il widget di report editor
     * @param $id
     * @return mixed|string
     */
    public function view($id)
    {
        // Necessaria per poter far il rendering
        $widget = $this->getWidget();
        $widget->reportDefinitonID = $id;
        $widget->reportMode = "view";
        $this->pageTitle = " Visualizzazione Report";
        return $widget->render();
    }

    /**
     * Lancia il widget di report editor
     * @param $id
     * @param $rid
     * @return mixed|string
     */
    public function document($id, $rid)
    {
        // Necessaria per poter far il rendering
        $widget = $this->getWidget();
        $widget->reportDefinitonID = $id;
        $widget->reportMode = "viewer";
        $widget->recordID = $rid;
        $this->pageTitle = " Visualizzazione Report";
        return $widget->render();
    }

    /**
     * Lancia il widget di report editor
     * @param $id
     * @return mixed|string
     */
    public function open($id, $rid = null)
    {
        // Necessaria per poter far il rendering
        $widget = $this->getWidget();
        $widget->reportDefinitonID = $id;
        $widget->recordID = $rid;
        $widget->reportMode = 'view';
        $this->pageTitle = "Report";
        return $widget->render();
    }

    public function onQuickSave()
    {
        $val = \Request::all();
        $data = post();
        $id = $data['id'];

        $reportDefinition = ReportDefinition::find($id);
        $reportDefinition->report_definition_json = $data['report_definition'];
        $reportDefinition->save();

        return \Response::json(['operation' => 'updated' , 'id' => $id]);
    }

}
