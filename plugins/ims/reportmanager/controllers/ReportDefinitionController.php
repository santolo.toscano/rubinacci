<?php namespace Ims\ReportManager\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use Ims\ReportManager\Widgets\Report;
use App;
use Config;
use File;
use Lang;
use Yaml;

class ReportDefinitionController extends Controller
{
    public $implement = [
        'Backend\Behaviors\ListController',
        'Backend\Behaviors\FormController',
    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Ims.ReportManager', 'reportdefinition', 'reportdefinition');
    }

}
