<?php namespace Ims\ReportManager\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateImsReportmanagerReportdesigner2 extends Migration
{
    public function up()
    {
        Schema::table('ims_reportmanager_reportdesigner', function($table)
        {
            $table->renameColumn('nome', 'name');
        });
    }
    
    public function down()
    {
        Schema::table('ims_reportmanager_reportdesigner', function($table)
        {
            $table->renameColumn('name', 'nome');
        });
    }
}
