<?php namespace Ims\ReportManager\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateImsReportmanagerReportdesigner extends Migration
{
    public function up()
    {
        Schema::table('ims_reportmanager_reportdesigner', function($table)
        {
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('ims_reportmanager_reportdesigner', function($table)
        {
            $table->dropColumn('created_at');
            $table->dropColumn('updated_at');
        });
    }
}
