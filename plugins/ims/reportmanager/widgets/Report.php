<?php namespace Ims\ReportManager\Widgets;

use App;
use Backend;
use Backend\Classes\WidgetBase;
use Carbon\Carbon;
use Config;
use Event;
use Ims\ReportManager\Models\ReportDefinition;
use Lang;
use Input;
use October\Rain\Database\Collection;
use October\Rain\Exception\AjaxException;
use Request;
use Ims\Scheduler\Models\Booking;

class Report extends WidgetBase
{
    protected $defaultAlias = 'report';
    public $reportDefinitonID;
    public $recordID;
    public $reportMode;


    public function widgetDetails()
    {
        return [
            'name' => 'Report engine',
            'description' => 'Report engine platform.',
        ];
    }

    public function render()
    {
        $libPath = '/plugins/ims/reportmanager/widgets/report/assets/jsreports-all';

        $this->addJs($libPath.'/jsreports-all.min.js');
        $this->addCss($libPath.'/css/jsreports-october.css');

        $reportDefinition = ReportDefinition::find($this->reportDefinitonID);
        $reportDataUrl = '/backend/ims/reportmanager/reportapicontroller/data/'.$this->reportDefinitonID;
        if ( $this->recordID )
        {
            $reportDataUrl = $reportDataUrl.'/'.$this->recordID;
        }

        $params['params']['libpath'] = $libPath;
        $params['params']['report_name'] = $reportDefinition->name;
        $params['params']['report_data_id'] = $reportDefinition->id;
		$params['params']['parameters_type'] = $reportDefinition->parameters_type;
        $params['params']['record_id'] = $this->recordID;
        $params['params']['report_definition_url'] = '/backend/ims/reportmanager/reportapicontroller/definition/'.$this->reportDefinitonID;
        $params['params']['report_save_url'] = '/backend/ims/reportmanager/reportapicontroller/save/'.$this->reportDefinitonID;
        $params['params']['report_schema_url'] = '/backend/ims/reportmanager/reportapicontroller/schema/'.$this->reportDefinitonID;
        $params['params']['report_data_url'] = $reportDataUrl;



        /*
        if ( $this->reportName == 'invoice' )
        {
            $booking = Booking::find($this->reportId);
            $invoice_header = [
                'addressType' => 'Ricevuta num '.$booking->id,
                'name' => $booking->last_name,
                'street1' => $booking->portal->name,
                'city' => 'Sorrento',
                'state' => 'NA',
                'zipcode' => '80067',
                'country' => 'Italy'
            ];

            $params['params']['report_header_jsondata'] = json_encode($invoice_header);
            return $this->makePartial('reportview', $params);
        }
        else
        {

        }
        */


        if ( $this->reportMode == 'view' )
        {
            return $this->makePartial('reportviewer', $params);
        }
		else if ( $this->reportMode == 'viewer' )
		{
            return $this->makePartial('reportview', $params);
		}
        else
        {
            return $this->makePartial('reportdesigner', $params);
			
        }

    }

}
