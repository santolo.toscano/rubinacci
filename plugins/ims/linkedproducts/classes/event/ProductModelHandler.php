<?php namespace IMS\LinkedProducts\Classes\Event;

use Lovata\Toolbox\Classes\Event\ModelHandler;

use Lovata\Shopaholic\Models\Product;
use Lovata\Shopaholic\Classes\Item\ProductItem;

use Lovata\Shopaholic\Classes\Collection\ProductCollection;
use IMS\LinkedProducts\Classes\Store\Product\LinkedProductListStore;

/**
 * Class ProductModelHandler
 * @author  Andrey Kharanenka, a.khoronenko@lovata.com, LOVATA Group
 */
class ProductModelHandler extends ModelHandler
{
    /** @var  Product */
    protected $obElement;

    /**
     * Add listeners
     * @param \Illuminate\Events\Dispatcher $obEvent
     */
    public function subscribe($obEvent)
    {
        parent::subscribe($obEvent);

        Product::extend(function ($obElement) {
            $this->extendProductModel($obElement);
        });

        ProductItem::extend(function ($obItem) {
            $this->extendProductItem($obItem);
        });
    }

    /**
     * After save event handler
     */
    protected function afterSave()
    {
        LinkedProductListStore::instance()->clear($this->obElement->id);
    }

    /**
     * After delete event handler
     */
    protected function afterDelete()
    {
        LinkedProductListStore::instance()->clear($this->obElement->id);
    }

    /**
     * Extend product model
     * @param Product $obElement
     */
    protected function extendProductModel($obElement)
    {
        $obElement->belongsToMany['linked'] = [
            Product::class,
            'table'    => 'ims_linked_products_link',
            'key'      => 'product_id',
            'otherKey' => 'linked_id',
        ];
    }

    /**
     * Extend product item
     * @param ProductItem $obItem
     */
    protected function extendProductItem($obItem)
    {
        $obItem->addDynamicMethod('getLinkedAttribute', function ($obItem) {
            /** @var ProductItem $obItem */
            //Get product ID list
            $arProductIDList = LinkedProductListStore::instance()->get($obItem->id);
            $obProductCollection = ProductCollection::make()->intersect($arProductIDList);

            return $obProductCollection;
        });
    }

    /**
     * Get model class name
     * @return string
     */
    protected function getModelClass()
    {
        return Product::class;
    }

    /**
     * Get item class name
     * @return string
     */
    protected function getItemClass()
    {
        return ProductItem::class;
    }
}
