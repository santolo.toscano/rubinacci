<?php namespace IMS\LinkedProducts\Classes\Store\Product;

use DB;
use Lovata\Toolbox\Classes\Store\AbstractStoreWithParam;

/**
 * Class LinkedProductListStore
  * @author  Andrey Kharanenka, a.khoronenko@lovata.com, LOVATA Group
 */
class LinkedProductListStore extends AbstractStoreWithParam
{
    protected static $instance;

    /**
     * Get ID list from database
     * @return array
     */
    protected function getIDListFromDB() : array
    {
        $arElementIDList = (array) DB::table('ims_linked_products_link')->where('product_id', $this->sValue)->lists('linked_id');

        return $arElementIDList;
    }
}
