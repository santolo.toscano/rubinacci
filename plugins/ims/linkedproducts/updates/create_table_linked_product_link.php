<?php namespace IMS\LinkedProducts\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

/**
 * Class CreateTableLinkedProductLink
 * @package Lovata\RelatedProductsShopaholic\Updates
 */
class CreateTableLinkedProductLink extends Migration
{
    /**
     * Apply migration
     */
    public function up()
    {
        if (Schema::hasTable('ims_linked_products_link')) {
            return;
        }
        
        Schema::create('ims_linked_products_link', function(Blueprint $obTable)
        {
            $obTable->engine = 'InnoDB';
            $obTable->integer('product_id')->unsigned();
            $obTable->integer('linked_id')->unsigned();
            $obTable->primary(['product_id', 'linked_id'], 'linked_product_link');

            $obTable->index('product_id');
        });
    }

    /**
     * Rollback migration
     */
    public function down()
    {
        Schema::dropIfExists('ims_linked_products_link');
    }
}
