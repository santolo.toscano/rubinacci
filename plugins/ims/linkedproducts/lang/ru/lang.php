<?php return [
    'plugin'    => [
        'name'        => 'IMS Linked products',
        'description' => 'Позволяет привязавать к товару связанные товары',
    ],
    'tab'       => [
        'linked' => 'Связанные товары',
    ],
];