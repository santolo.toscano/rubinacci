<?php return [
    'plugin'    => [
        'name'        => 'IMS Linked Products',
        'description' => 'Allows you link products to product',
    ],
    'tab'       => [
        'linked' => 'Linked products',
    ],
];