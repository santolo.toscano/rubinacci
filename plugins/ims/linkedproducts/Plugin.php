<?php namespace IMS\LinkedProducts;

use Event;
use System\Classes\PluginBase;

use IMS\LinkedProducts\Classes\Event\ProductControllerHandler;
use IMS\LinkedProducts\Classes\Event\ProductModelHandler;

/**
 * Class Plugin
 * @author Andrey Kharanenka, a.khoronenko@lovata.com, LOVATA Group
 */
class Plugin extends PluginBase
{
    /** @var array Plugin dependencies */
    public $require = ['Lovata.Shopaholic', 'Lovata.Toolbox'];

    /**
     * Plugin boot method
     */
    public function boot()
    {
        $this->addEventListener();
    }

    /**
     * Add event listeners
     */
    protected function addEventListener()
    {
        Event::subscribe(ProductControllerHandler::class);
        Event::subscribe(ProductModelHandler::class);
    }
}
