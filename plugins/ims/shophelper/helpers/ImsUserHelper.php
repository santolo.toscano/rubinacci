<?php
/**
 * Created by PhpStorm.
 * User: marco
 * Date: 05/11/2020
 * Time: 15:24
 */

namespace Ims\Shophelper\Helpers;


use Carbon\Carbon;
use Lovata\Buddies\Classes\Item\UserItem;

class ImsUserHelper
{
    /**
     * Prepara i dati utente leggendoli dal property
     * @param UserItem $obUser
     * @return array
     */
    public static function parseUserData($obUser)
    {
        // Data di nascita
        // $birthDate = $obUser->getObject()->property['birthdate'];

        try {
            $birthDate = str_replace('/', '-', $birthDate);
            $birthDate = str_before($birthDate, ' ');
            $birthDate = Carbon::createFromFormat('Y-m-d', $birthDate);
        }
        catch ( \Exception $ex )
        {
            $birthDate = null;
        }

        //dd($obUser->getObject(), $birthDate);

        // TODO: Approfondire se li mette prima o dopo
        try {
            $arrayTelefono = explode(',', $obUser->phone);
            $phone = last($arrayTelefono);
        }
        catch ( \Exception $ex )
        {
            $phone = 'ERROR';
        }

        $shipping_state = static::getUserProperty($obUser, 'shipping_state');
        $prefixInternational = static::getUserProperty($obUser, 'prefix');
        if ( !$prefixInternational )
        {
            $prefixInternational = static::getInternationalCode($shipping_state);
        }

        // NB errore impostazione shipping_country
        $shipping_region =  static::getUserProperty($obUser, 'shipping_country');

        $isCompany = static::getUserProperty($obUser, 'company_name') || static::getUserProperty($obUser, 'p_iva');
        $isAlternateAddress = ImsUserHelper::getUserProperty($obUser,'shipping_name') != ImsUserHelper::getUserProperty($obUser,'billing_name') || ImsUserHelper::getUserProperty($obUser,'shipping_address1') != ImsUserHelper::getUserProperty($obUser,'billing_address') || ImsUserHelper::getUserProperty($obUser,'shipping_city') != ImsUserHelper::getUserProperty($obUser,'billing_city');

        $userData = [
            'is_company' => $isCompany,
            'is_other_address' => $isAlternateAddress,
            'birth_date' => $birthDate ? $birthDate->format('Y-m-d') : '',
            'fiscal_code' => ImsUserHelper::getUserProperty($obUser, 'cf'),
            'prefix' => $prefixInternational,
            'phone' => $phone,
            'shipping_name' => ImsUserHelper::getUserProperty($obUser,'shipping_name'),
            'shipping_last_name' => ImsUserHelper::getUserProperty($obUser,'shipping_last_name'),
            'shipping_address' => ImsUserHelper::getUserProperty($obUser,'shipping_address1'),
            'shipping_city' => ImsUserHelper::getUserProperty($obUser,'shipping_city'),
            'shipping_state' => $shipping_state,
            'shipping_region' =>  $shipping_region,
            // deve salvare il campo C/O
        ];

        return $userData;
    }

    /**
     * @param UserItem $obUser
     * @param string $propertyName
     * @return string
     */
    public static function getUserProperty($obUser, $propertyName)
    {
        try
        {
            if ( !array_key_exists($propertyName, $obUser->property) )
            {
                return "";
            }

            return $obUser->property[$propertyName];
        }
        catch ( \Exception $ex )
        {
            return 'error';
        }
    }


    public static function getInternationalCode($stateCode)
    {
        $stateCode = strtolower($stateCode);
        switch ( $stateCode )
        {
            case 'it':
                //break;
                return '39';
            case 'us':
                return '1';
            case 'de':
                return '49';
            case 'fr':
                return '33';
            case 'gb':
                return '44';
            default:
                return '';
        }
    }

}