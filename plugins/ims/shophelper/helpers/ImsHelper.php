<?php
/**
 * Created by PhpStorm.
 * User: marco
 * Date: 17/10/2020
 * Time: 18:43
 */

namespace Ims\Shophelper\Helpers;

use Carbon\Carbon;
use Lovata\Shopaholic\Classes\Collection\ProductCollection;
use Lovata\Shopaholic\Models\Product;
use RainLab\Translate\Classes\Translator;

class ImsHelper
{

    /**
     * @return string
     */
    public static function getLocale()
    {
        $translator = Translator::instance();
        return $translator->getLocale();
    }

    public static function manageCustomizedContent($cust_cont, $param){
        $result = null;
        if (isset($cust_cont)) {
            if (is_array(json_decode($cust_cont, true))) {
                $result = json_decode($cust_cont, true);

                if ($param == 'it') {
                    $polsi = 'Polsi';
                    $dettB = 'Dettagli bianchi';
                } else {
                    $polsi = 'Cuffs';
                    $dettB = 'White details';
                }

                $element = '<span>
                                ' . $polsi . ': ' . $result['polsi'] . '<br>
                                ' . $dettB . ': ' . $result['dett-bianchi'] . '
                            </span>';
            }else if($cust_cont == '#bespoke#'){
                $result = $cust_cont;
                if ($param == 'it') {
                    $option = 'Tessuto per capo su misura';
                } else {
                    $option = 'Fabric for bespoke';
                }

                $element = '<span>
                                ' . $option . '
                            </span>';
            }else{
                $result = $cust_cont;
                if ($param == 'it') {
                    $option = 'Iniziali';
                } else {
                    $option = 'Initials';
                }

                $element = '<span>
                                ' . $option . ': ' . $result . '
                            </span>';
            }
        }

        if ($result != null) {
            return $element;
        } else{
            return $result;
        }
    }

    /**
     * @param Product $original
     * @return mixed
     */
    public static function cloneProduct($original)
    {
        $product = Product::all()->last();
        $newId = $product->id+1;

        $locale = static::getLocale();

        // Setto il model originale per italiano
        $original->translateContext('it');


        //duplico il prodotto e rendo univoci i campi name e slug
        $clone = $original->replicate();
        $clone->name = $clone->name.'-'.$newId;
        $clone->slug = $clone->slug.'-'.$newId;

        // setto i campi in lingua
        $lang = 'en';
        $original->translateContext($lang);
        $clone->setAttributeTranslated('name', $original->name.'-'.$newId, $lang);
        $clone->setAttributeTranslated('slug', $original->slug.'-'.$newId, $lang);
        $arrayCampi = ['preview_text','description'];
        for($i = 0; $i < count($arrayCampi); $i++){
            $campo = $arrayCampi[$i];
            $clone->setAttributeTranslated($campo, $original->$campo, $lang);
        }

        //aggiungo le categorie aggiuntive che non inserisce il replicate(inserisce solo la principale)
        $clone->additional_category = $original->additional_category;
        $clone->save();

        //duplico le offer
        $offers = $original->offer;
        foreach ( $offers as $offer ) {
            // Setto il model originale per italiano
            $offer->translateContext('it');
            // duplico l'offer è setto il product_id del nuovo prodotto
            $cloneOffer = $offer->replicate();
            $cloneOffer->product_id = $clone->id;
            // rendo univoci i campi necessari
            $cloneOffer->name = $cloneOffer->name;
            $cloneOffer->code = $cloneOffer->code.'-'.$newId;
            $cloneOffer->external_id = $cloneOffer->external_id.'-'.$newId;
            // setto i campi in lingua
            $lang = 'en';
            $offer->translateContext($lang);
            $cloneOffer->setAttributeTranslated('name', $offer->name, $lang);

            $cloneOffer->save();

            //dopo aver dupolicato l'offer ed averla salvata inserisco a mano i due prezzi e le property
            $cloneOffer->main_price->price = $offer->main_price->price;
            $cloneOffer->main_price->old_price = $offer->main_price->old_price;
            $cloneOffer->property = $offer->property;
            $cloneOffer->save();
        }

        return $clone;

        // copio le traduzioni dei campi del prodotto e delle offerte
        /*$prodTrads = Attribute::where('model_id', $original->id)->get();
        foreach ($prodTrads as $trad) {
            $cloneTrad = $trad->replicate(['updated_at', 'created_at']);//esclude i timestamps poicha la tabelle delle traduzioni non ha i relativi campi ed andava in errore - stessa cosa sotto per le offer
            $cloneTrad->model_id = $clone->id;
            //rendo univoci nome e slug della traduzione del prodotto
            if ($cloneTrad->model_type == 'Lovata\Shopaholic\Models\Product'){
                $temp = json_decode($cloneTrad->attribute_data, true);
                $temp['name'] = $temp['name'] . ' (Copy)';
                $temp['slug'] = $temp['slug'] .'-'. str_replace([' ', ':', '-'], '', Carbon::now());
                $cloneTrad->attribute_data = json_encode($temp);
            }
            $cloneTrad->timestamps = false;
            $cloneTrad->save();
        }*/

    }


}