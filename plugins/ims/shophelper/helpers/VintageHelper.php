<?php


namespace Ims\Shophelper\Helpers;


use Lovata\Shopaholic\Classes\Item\ProductItem;
use Lovata\Shopaholic\Models\Category;

class VintageHelper
{
    /**
     * Controlla se si tratta di un tessuto
     * TODO: Fare il check sul tessuto
     * @param $product
     * @return bool
     */
    public static function containsVintageCat($product) {
        try{
            //recupero la cat vintage per poi usare l'id nel controllo
            $vintageCat = Category::where('id', 108)->first();
            //controllo che nelle categorie aggiuntive sia presente o meno la categoria vintage e setto di conseguenza la variabile check, inizialmente settata a false poichè le additional_category potrebbero essere vuote
            $checkAdditionalCat = false;
            if(isset($product->additional_category)){
                $checkAdditionalCat = $product->additional_category->where('id', $vintageCat->id)->count() > 0 ? true : false;
            }

            //controllo effettivamente se la cat principale o le aggiuntive siano vintage
            if($product->category_id == $vintageCat->id || $checkAdditionalCat){
                return true;
            }else{
                return false;
            }
        }catch( \Exception $ex ) {
            return false;
        }
    }

    /**
     * @param ProductItem $obProduct
     */
    public static function isTessuto($obProduct)
    {
        //$idCategoriaTessuto = config('rubinacci.vintage.categoria_tessuti_id', 0);

        $isTessuto = strtolower($obProduct->category->code) == 'tessuti' || strtolower($obProduct->category->slug) == 'tessuti' || strtolower($obProduct->category->slug) == 'fabrics';
        return $isTessuto;

    }

    public static function getOfferTessuto($obProduct)
    {
        if ($obProduct->offer->count() > 0) {// cambiato il controllo da >1 a >0 poichè anche se solo un'offer bisogna controllare se non è la custom - logica in parte errata perchè prima gestivamo male il decremento del tessuto, eventualmente da migliorare
            foreach($obProduct->offer as $offer){
                if ($offer->price != 0){
                    $offerteAttive++;
                    if ( !$offertaTessuto ) {
                        $offertaTessuto = $offer;
                    }
                }elseif (strtolower($offer->name) == 'soldout'){//altrimenti non risultano offerte attive nel controllo della pagina, che in caso negativo reindirizza alla 404
                    $offerteAttive++;
                }
            }
        }
    }
}