<?php namespace Ims\Shophelper\Helpers;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use October\Rain\Support\Facades\Schema;

Class Helpers {

    public function printStartMessage($command) {
        $now = date("Y-m-d_H:i:s");
        $padSize = strlen($command) + strlen($now);
        $padSpaces = str_repeat(" ", $padSize);
        $padStars = str_repeat("*", $padSize);
        print_r("**$padStars***********\r\n");
        print_r("* $padSpaces          *\r\n");
        print_r("* $now - Start $command *\r\n");
        print_r("* $padSpaces          *\r\n");
        print_r("**$padStars***********\r\n");
    }

    public function apiArrayResponseBuilder($statusCode = null, $message = null, $data = [])
	{
		$arr = [
			'status_code' => (isset($statusCode)) ? $statusCode : 500,
			'message' => (isset($message)) ? $message : 'error'
		];
		if (count($data) > 0) {
			$arr['data'] = $data;
		}

		return response()->json($arr, $arr['status_code']);
		//return $arr;

	}

    public function apiArrayResponseBuilderWithCollections($statusCode = null, $message = null, $data = [], $collections = [])
    {
        $arr = [
            'status_code' => (isset($statusCode)) ? $statusCode : 500,
            'message' => (isset($message)) ? $message : 'error'
        ];
        if (count($data) > 0) {
            $arr['data'] = $data;
        }

        $arr['collections'] = $collections;

        return response()->json($arr, $arr['status_code']);
        //return $arr;
    }


    /**
     * Ritorna la rappresentazione pulita del model
     * @param Model $model
     * @return \Illuminate\Http\JsonResponse
     */
    public function returnRawDataFromModel($model)
    {
        $data = $model::all();
        return response()->json($data);
    }





}