<?php
/**
 * Created by PhpStorm.
 * User: marco
 * Date: 17/10/2020
 * Time: 18:43
 */

namespace Ims\Shophelper\Helpers;

use Exception;
use Ims\Shophelper\Models\AvailableEmailProduct;
use Lovata\Buddies\Models\User;
use Lovata\OrdersShopaholic\Classes\Item\OrderItem;
use Lovata\OrdersShopaholic\Classes\Processor\OrderProcessor;
use Lovata\Toolbox\Classes\Helper\SendMailHelper;
use Lovata\Toolbox\Classes\Helper\UserHelper;
use RainLab\Translate\Classes\Translator;
use Lovata\Shopaholic\Models\Settings;

class EmailHelper
{
    /**
     * @param OrderItem $orderItem
     * @param String $locale
     */
    public static function sendEmailUser($orderItem, $locale)
    {
        static::sendUserEmailAfterCreating($orderItem, $locale);
    }

    /**
     * @param OrderItem $orderItem
     */
    public static function sendEmailManager($orderItem)
    {
        static::sendManagerEmailAfterCreating($orderItem); // Manda sempre in italiano al manager
    }
    /**
     * @param OrderItem $orderItem
     */
    public static function sendEmailUkPosition($order)
    {
        static::sendManagerEmailAfterCreatingOrderWithUkOffer($order); // Manda sempre in italiano al manager
    }

    public static function sendGenericEmail($item, $templateName)
    {

    }

    /**
     * @param User $obUser
     */
    public static function sendUserRegistrationEmail($obUser)
    {
        $sMailTemplate = "user_registration_confirmation";
        if($obUser instanceof User){
            $sEmail = $obUser->email;
        }else {
            $sEmail = $obUser;
            $obUser = UserHelper::instance()->findUserByEmail($sEmail);
        }

        // Elenco dei manager
        /*$sEmailList = Settings::getValue('creating_order_manager_email_list');
        if ( !empty($sEmailList)) {
            $sEmail = $sEmail.','.$sEmailList;
        }*/

        //Get mail data
        // prepare array data for template
        $arMailData = [
            'user'        => $obUser,
            'site_url'     => config('app.url'),
        ];

        $translator = Translator::instance();
        $locale = $translator->getLocale();
        if ( $locale != $translator->getDefaultLocale() )
        {
            $sMailTemplate = $sMailTemplate.'_'.$locale;
        }

        $obSendMailHelper = SendMailHelper::instance();
        $obSendMailHelper->send(
            $sMailTemplate,
            $sEmail,
            $arMailData,
            null, // Fa scattare eventuali eventi
            true);
    }

    /**
     * @param User $email
     */
    public static function sendUserEmailWelcomeConfirmation($email)
    {
        $sMailTemplate = "user_welcome_confirmation";

        $sEmail = $email;
        $obUser = UserHelper::instance()->findUserByEmail($sEmail);

        //Get mail data
        // prepare array data for template
        $arMailData = [
            'user'        => $obUser,
            'site_url'     => config('app.url'),
        ];

        $translator = Translator::instance();
        $locale = $translator->getLocale();
        if ( $locale != $translator->getDefaultLocale() )
        {
            $sMailTemplate = $sMailTemplate.'_'.$locale;
        }

        $obSendMailHelper = SendMailHelper::instance();
        $obSendMailHelper->send(
            $sMailTemplate,
            $sEmail,
            $arMailData,
            null, // Fa scattare eventuali eventi
            true);


    }

    public static function sendNewsletterConfirmation($email)
    {
        $sMailTemplate = "newsletter_registration";
        $sEmail = $email;

        // Elenco dei manager
        $sEmailList = Settings::getValue('creating_order_manager_email_list');
        if ( !empty($sEmailList)) {
            $sEmail = $sEmail.','.$sEmailList;
        }

        //Get mail data
        // prepare array data for template
        $arMailData = [
            'email'        => $email,
            'site_url'     => config('app.url'),
        ];

        $translator = Translator::instance();
        $locale = $translator->getLocale();
        if ( $locale != $translator->getDefaultLocale() )
        {
            $sMailTemplate = $sMailTemplate.'_'.$locale;
        }

        $obSendMailHelper = SendMailHelper::instance();
        $obSendMailHelper->send(
            $sMailTemplate,
            $sEmail,
            $arMailData,
            null, // Fa scattare eventuali eventi
            true);
    }

    /**
     * check if input email is a fake one
     * @param string $email
     */
    private static function isFakeEmail($email) {
        return preg_match('%^fake.*@fake\.com$%', $email);
    }

    /**
     * @param AvailableEmailProduct $email_product
     * @return boolean
     */
    public static function safeSendEmailProductAvailable($email_product)
    {
        try {
            self::sendEmailProductAvailable($email_product);
            return true;
        } catch(Exception $e) {
            return false;
        }
    }

    /**
     * @param AvailableEmailProduct $email_product
     */
    public static function sendEmailProductAvailable($email_product)
    {
        //check user email
        if (static::isFakeEmail($email_product->email_user)) {
            return;
        }

        $product = $email_product->product;
        //Prepare array data for template
        $arMailData = [
            'email'        => $email_product->email_user,
            'product_slug' => $product->slug,
            'product_img'  => $product->preview_image->path,
        ];

        SendMailHelper::instance()->send(
            "available_size_template", //template
            $email_product->email_user, //email to
            $arMailData //data
        );
    }

    /**
     * Invia email con supporto in lingua,
     * NB!!!!!!! va disattivato nei setting invio dell'email ma i parametri vengono comunque tenuti
     * Send email to user, after order creating
     * @param OrderItem $obElement
     * @param String $locale
     */
    protected static function sendUserEmailAfterCreating($obElement, $locale)
    {
        //Get user email
        $arOrderPropertyList = $obElement->property;
        if (empty($arOrderPropertyList) || !isset($arOrderPropertyList['email']) || empty($arOrderPropertyList['email'])) {
            return;
        }

        $sEmail = $arOrderPropertyList['email'];
        if (static::isFakeEmail($sEmail)) {
            return;
        }

        //Get mail data
        // prepare array data for template
        $arMailData = [
            'order'        => $obElement,
            'order_number' => $obElement->order_number,
            'site_url'     => config('app.url'),
        ];

        //Get mail template
        $sMailTemplate = Settings::getValue('creating_order_mail_template', 'lovata.ordersshopaholic::mail.create_order_user');
        $bespokeTemplate = 'bespoke_template';

        // Aggoimge la lingua al template email solo se diversa dalla lingua predefinita
        $translator = Translator::instance();
        if ( $locale != $translator->getDefaultLocale() )
        {
            $sMailTemplate = $sMailTemplate.'_'.$locale;
            $bespokeTemplate = $bespokeTemplate.'_'.$locale;
        }

        $obSendMailHelper = SendMailHelper::instance();
        $obSendMailHelper->send(
            $sMailTemplate,
            $sEmail,
            $arMailData,
            OrderProcessor::EVENT_ORDER_CREATED_USER_MAIL_DATA,
            true);


        // Luca: gestione email supplementare in caso di prenotazione tessuti
        $bespokeProdCount = 0;
        foreach($obElement->order_position as $cartPositionElement){
            if($cartPositionElement->customized_content == '#bespoke#'){
                $bespokeProdCount++;
            }
        }
        if($bespokeProdCount > 0){
            $obSendMailHelper->send(
                $bespokeTemplate,
                $sEmail,
                $arMailData,
                null,
                true);
        }
    }

    /**
     * Send email to manager, after order creating
     * MODIFICATO RICHIEDE ORDERITEM
     * @param OrderItem $obOrder
     */
    protected static function sendManagerEmailAfterCreating($obOrder)
    {
        //Get email list
        $sEmailList = Settings::getValue('creating_order_manager_email_list');
        if (empty($sEmailList)) {
            return;
        }

        // prepare array data for template
        $arMailData = [
            'order'        => $obOrder,
            'order_number' => $obOrder->order_number,
            'site_url'     => config('app.url'),
        ];

        //Get mail template
        $sMailTemplate = Settings::getValue('creating_order_manager_mail_template', 'lovata.ordersshopaholic::mail.create_order_manager');

        $obSendMailHelper = SendMailHelper::instance();
        $obSendMailHelper->send(
            $sMailTemplate,
            $sEmailList,
            $arMailData,
            OrderProcessor::EVENT_ORDER_CREATED_MANAGER_MAIL_DATA,
            true);
    }

    /**
     * Send email to manager, after order creating whit uk offer in quantity
     * @param OrderItem $obOrder
     * @param OrderItemPositions $obOrderUk
     */
    protected static function sendManagerEmailAfterCreatingOrderWithUkOffer($obOrder)
    {
        //Get email list
        // $sEmailList = Settings::getValue('creating_order_manager_email_list');
        $sEmailList = "London@marianorubinacci.com";
        if (empty($sEmailList)) {
            return;
        }

        // prepare array data for template
        $arMailData = [
            'order'             => $obOrder,
            'order_number'      => $obOrder->order_number,
            'site_url'          => config('app.url'),
        ];

        //Get mail template
        $sMailTemplate = Settings::getValue('lovata.ordersshopaholic::mail.create_request_offer_from_uk', 'lovata.ordersshopaholic::mail.create_request_offer_from_uk');

        $obSendMailHelper = SendMailHelper::instance();
        $obSendMailHelper->send(
            $sMailTemplate,
            $sEmailList,
            $arMailData,
            OrderProcessor::EVENT_ORDER_CREATED_MANAGER_MAIL_DATA,
            true,
            true
        );
    }

    /**
     * @param OrderItem $obOrder
     * @param String $tracking_url
     */
    public static function sendShippingEmail($obOrder, $tracking_url)
    {
        $sEmail = $obOrder->getObject()->user->email;

        if (static::isFakeEmail($sEmail)) {
            return;
        }

        // Elenco dei manager
        $sEmailList = Settings::getValue('creating_order_manager_email_list');
        if ( !empty($sEmailList)) {
            $sEmail = $sEmail.','.$sEmailList;
        }

        //Get mail data
        // prepare array data for template
        $arMailData = [
            'order'        => $obOrder,
            'shipping_tracking' => $obOrder->getObject()->shipping_tracking,
            'tracking_url' => $tracking_url,
            //'file_awb' => $obOrder->getObject()->file_dhl,
            'site_url'     => config('app.url'),
        ];

        //Get mail template
        $sMailTemplate = "beepro::shipping_confirmation";


        // Aggoimge la lingua al template email solo se diversa dalla lingua predefinita
        $translator = Translator::instance();
        $locale = $obOrder->property['locale'];
        if ( $locale != $translator->getDefaultLocale() )
        {
            $sMailTemplate = $sMailTemplate.'_'.$locale;
        }

        $obSendMailHelper = SendMailHelper::instance();
        $obSendMailHelper->send(
            $sMailTemplate,
            $sEmail,
            $arMailData,
            null,
            true);
    }

}