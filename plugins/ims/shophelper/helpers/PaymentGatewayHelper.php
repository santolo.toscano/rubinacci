<?php
/**
 * Created by PhpStorm.
 * User: marco
 * Date: 17/10/2020
 * Time: 18:43
 */

namespace Ims\Shophelper\Helpers;


use Illuminate\Support\Facades\DB;
use Lovata\CouponsShopaholic\Models\Coupon;
use Lovata\OrdersShopaholic\Classes\Item\OrderItem;
use Lovata\OrdersShopaholic\Classes\Processor\CartProcessor;
use Lovata\OrdersShopaholic\Classes\Processor\OfferCartPositionProcessor;
use Lovata\OrdersShopaholic\Models\Order;
use Lovata\OrdersShopaholic\Models\PaymentMethod;
use Lovata\Shopaholic\Classes\Item\OfferItem;
use Lovata\Shopaholic\Classes\Item\ProductItem;
use Lovata\Shopaholic\Models\Category;
use Lovata\Shopaholic\Models\Offer;

class PaymentGatewayHelper
{
    /**
     * @param Order $order
     * @param PaymentMethod $payment
     * @return string
     */
    public static function getRedirectUrlForSuccess($order, $payment)
    {
        $payment = $payment->code;
        $secret_key = $order->secret_key;

        // Invia email all'utente per pagamento completato
        $orderItem = OrderItem::make($order->id);

        $locale = array_key_exists('locale', $order->property) ? $order->property['locale'] : "";

        EmailHelper::sendEmailUser($orderItem, $locale);
        EmailHelper::sendEmailManager($orderItem, 'it');

        return "/order-success?order=$secret_key&payment=$payment";
    }

    /**
     * @param Order $order
     * @param PaymentMethod $payment
     * @return string
     */
    public static function getRedirectUrlForCancel($order, $payment)
    {
        $payment = $payment->code;
        $secret_key = $order->secret_key;

        return "/order-success?order=$secret_key&fail=1";
    }

    /**
     * @param Order $order
     */
    public static function cancelOrder($order)
    {
        $obPaymentMethod = $order->payment_method;

        $obStatus = $order->payment_method->cancel_status;
        if (empty($obStatus)) {
            return;
        }

        $order->status_id = $obStatus->id;
        $order->save();

        if ($obPaymentMethod->restore_cart) {
            $orderItem = OrderItem::make($order->id);

            $cartProcessor = CartProcessor::instance();
            $cartProcessor->restoreFromOrder($order, OfferCartPositionProcessor::class);

            // Ricarica il promo code
            $promoCode = $orderItem->coupon_list ? $orderItem->coupon_list[0] : '';
            if ( $promoCode )
            {
                $obCoupon = Coupon::getByCode($promoCode)->first();
                $obCart = $cartProcessor->getCartObject();
                $obCart->coupon()->add($obCoupon);

                // Rimuove il coupon dall'ordine fallito
                DB::table('lovata_coupons_shopaholic_order_coupon')->where('order_id', $order->id)->delete();
            }

            static::restoreInventory($order);
        }
    }

    /**
     * Controlla se si tratta di un tessuto
     * TODO: Fare il check sul tessuto
     * @param $product
     * @return bool
     */
    public static function containsVintageCat($product) {
        try{
            //recupero la cat vintage per poi usare l'id nel controllo - modificato l'id da 108 a 110 per intercettare solo i tessuti del vintage
            $vintageCat = Category::where('id', 110)->first();
            //controllo che nelle categorie aggiuntive sia presente o meno la categoria vintage e setto di conseguenza la variabile check, inizialmente settata a false poichè le additional_category potrebbero essere vuote
            $checkAdditionalCat = false;
            if(isset($product->additional_category)){
                $checkAdditionalCat = $product->additional_category->where('id', $vintageCat->id)->count() > 0 ? true : false;
            }

            //controllo effettivamente se la cat principale o le aggiuntive siano vintage
            if($product->category_id == $vintageCat->id || $checkAdditionalCat){
                return true;
            }else{
                return false;
            }
        }catch(\Exception $ex ){
            return false;
        }

    }


    /**
     * @param ProductItem $product
     */
    public static function restoreFabricAvailability($product)
    {
        // Cancella il soldout e ripristina gli altri record a 1
        //per le offer esistenti imposto la qnt a 0 e le disattivo
        foreach ($product->offer as $offer) {
            // Se non è soldout
            if ( $offer->name != 'SOLDOUT' ) {
                $offer->quantity = 1;
                $offer->active = 1;
                $offer->save();
            }
            else
            {
                $soldOutOffer = $offer;
                $soldOutOffer->quantity = 0;
                $soldOutOffer->active = 0;
                $soldOutOffer->save();
            }
        }
    }

    /**
     * Ripristina la giacenza per gli articoli dell'ordine annullato
     * @param Order $order
     */
    public static function restoreInventory($order)
    {
        //Get order positions
        $obOrderPositionList = $order->order_position;
        if ($obOrderPositionList->isEmpty()) {
            return;
        }

        //Create cart positions from order positions
        foreach ($obOrderPositionList as $obOrderPosition) {
            $offerId = $obOrderPosition->item_id;
            $obOffer = OfferItem::make($offerId);

            /** @var Offer $offer */
            $offer = $obOffer->getObject();
            if ( !static::containsVintageCat($offer->product) )
            {
                $offer->quantity = $offer->quantity + $obOrderPosition->quantity;
                $offer->save();
            }
            else
            {
                // ripristina disponibilitù del tessuto
                static::restoreFabricAvailability($offer->product);
            }
        }

    }

}