/**
 * Created by marco on 14/10/2020.
 */
var  lastVal ="";
var  lastQuantity ="";

function  manualInventoryChange(control) {
    $(control).css("color", "red");

    $.request('onChangeIsManualInventory', {
        data: {id: $(control).attr("data-request-data"), value: $(control).val()},
        flash: 1,
        handleFlashMessage: function (message, type) {
            $.oc.flashMsg({text: message, class: type})
        }
    });
}












function quantityFocus(control)
{
    lastQuantity = $(control).val();
}

function quantityFocusOut(control)
{
    if (lastQuantity != $(control).val())
    {
        $(control).css("color", "red");
        console.log($(control).val());

        $.request('onChangeQuantity', {
            data: {id: $(control).attr("data-request-data"), value: $(control).val()},
            flash: 1,
            handleFlashMessage: function(message, type) {
                $.oc.flashMsg({ text: message, class: type })
            },
            complete: function(response) {
                if ( response.status ) {
                    $(control).css("color", "green");
                }
            }
        });
    }
}

function codeFocus(control)
{
    lastVal = $(control).val();
}

function codeFocusOut(control)
{
    if (lastVal != $(control).val())
    {
        $(control).css("color", "red");
        console.log($(control).val());

        $.request('onChangeCode', {
            data: {id: $(control).attr("data-request-data"), value: $(control).val()},
            flash: 1,
            handleFlashMessage: function(message, type) {
                $.oc.flashMsg({ text: message, class: type })
            },
            complete: function(response) {
                if ( response.status ) {
                    $(control).css("color", "green");
                }
            }
        });
    }
}

function barcodeFocus(control)
{
    lastVal = $(control).val();
}

function barcodeFocusOut(control)
{
    if (lastVal != $(control).val())
    {
        $(control).css("color", "red");
        console.log($(control).val());

        $.request('onChangeBarcode', {
            data: {id: $(control).attr("data-request-data"), value: $(control).val()},
            flash: 1,
            handleFlashMessage: function(message, type) {
                $.oc.flashMsg({ text: message, class: type })
            },
            complete: function(response) {
                if ( response.status ) {
                    $(control).css("color", "green");
                }
            }
        });
    }
}

function registerCustomHandler()
{
    /*
    $(".quantity" )
        .focus(function () {
            lastVal = $(this).val();

        }).focusout(function() {
        if (lastVal != $(this).val())
        {
            $(this).css("color", "red");
            console.log($(this).val());

            $.request('onChangeQuantity', {
                data: {id: $(this).attr("data-request-data"), value: $(this).val()},
                flash: 1,
                handleFlashMessage: function(message, type) {
                    $.oc.flashMsg({ text: message, class: type })
                },
                complete: function() {
                    registerCustomHandler();
                },
            });
        }
    });

    $(".code" )
        .focus(function () {
            lastVal = $(this).val();

        }).focusout(function() {
        if (lastVal != $(this).val())
        {
            $(this).css("color", "red");
            console.log($(this).val());

            $.request('onChangeCode', {
                data: {id: $(this).attr("data-request-data"), value: $(this).val()},
                flash: 1,
                handleFlashMessage: function(message, type) {
                    $.oc.flashMsg({ text: message, class: type })
                },
                complete: function() {
                    registerCustomHandler();
                },
            });
        }
    });

    $(".barcode" )
        .focus(function () {
            lastVal = $(this).val();

        }).focusout(function() {
        if (lastVal != $(this).val())
        {
            $(this).css("color", "red");
            console.log($(this).val());

            $.request('onChangeBarcode', {
                data: {id: $(this).attr("data-request-data"), value: $(this).val()},
                flash: 1,
                handleFlashMessage: function(message, type) {
                    $.oc.flashMsg({ text: message, class: type })
                },
                complete: function() {
                    registerCustomHandler();
                },
            });
        }
    });

    $(".is_manual_inventory").change(function() {
        $(this).css("color", "red");
        console.log($(this).val());

        $.request('onChangeIsManualInventory', {
            data: {id: $(this).attr("data-request-data"), value: $(this).val()},
            flash: 1,
            handleFlashMessage: function(message, type) {
                $.oc.flashMsg({ text: message, class: type })
            },
            complete: function() {
                registerCustomHandler();
            },
        });
    });
    */

}
/*
$( document ).ready(function() {
    registerCustomHandler();
});

$( document ).ajaxComplete(function() {
    registerCustomHandler();
});
*/