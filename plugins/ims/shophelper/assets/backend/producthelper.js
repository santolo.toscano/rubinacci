/* --- copia Productvariation dei prodotti per gestire la modifica data --- */

function onCreatedOut(data) {
    console.log(data);

    $.request('onChangeCreated', {
        data: {id: $(data).attr("data-request-data"), value: $(data).val()},
        flash: 1,
        handleFlashMessage: function(message, type) {
            $.oc.flashMsg({ text: message, class: type })
        },
        complete: function(response) {
            if ( response.status ) {
                $(data).css("color", "green");
            }
        }
    });
}

function  esportaChange(control) {
    $(control).css("color", "red");

    $.request('onChangeIsEsporta', {
        data: {id: $(control).attr("data-request-data"), value: $(control).val()},
        flash: 1,
        handleFlashMessage: function (message, type) {
            $.oc.flashMsg({text: message, class: type})
        }
    });
}
