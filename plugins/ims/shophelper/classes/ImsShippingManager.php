<?php

namespace Ims\Shophelper\Classes;

use Carbon\Carbon;
use Illuminate\Support\Collection;
use Ims\Shophelper\Models\GeoCountry;
use Illuminate\Support\Facades\Storage;
use Ims\Shophelper\Classes\Dhlauthc\DHLAuth;
use Ims\Shophelper\Classes\Dhlratesbodyc\DHLRatesBody;
use Ims\Shophelper\Classes\Dhlshipmentsbodyc\DHLShipmentsBody;
use Lovata\Buddies\Classes\Item\UserItem;
use JakubOnderka\PhpParallelLint\Exception;
use Lovata\OrdersShopaholic\Classes\Item\OrderItem;

class ImsShippingManager
{
    public $account;

    public $accountList = ["105257899", "106173837"];
    protected $url;
    protected $siteId;
    protected $password;
    protected $basePath = "/media/dhl/";
    public const URL_SITE = 'https://marianorubinacci.com';
    public const DHL_BASE_URL = 'https://express.api.dhl.com/mydhlapi';
    /**
     * Test Dev DHL
     * public const URL_SITE = 'https://dev.marianorubinacci.com';
     * public const DHL_BASE_URL = 'https://express.api.dhl.com/mydhlapi/test';
     */
    public const DHL_USER = 'apR4oV4aX6mO5h';
    public const DHL_PASS = 'Z^4lX$7dP$1r';

    /**
     * ImsShippingManager constructor.
     * @param bool $productionMode
     */
    public function __construct($productionMode = false)
    {
        if ( $productionMode )
        {

        }
        else
        {
            $this->url = "https://xmlpi-ea.dhl.com/XMLShippingServlet";
            $this->siteId = "v62_KufsgCfMth";
            $this->password = "7yyrk5IPat";
            $this->account = "";
        }
    }

    public static function getOrderUserData($order, $field)
    {
        $returnValue = '';

        if ( array_key_exists($field, $order->property) && $order->property[$field] != '' && $order->property[$field] != '00' )
        {
            $returnValue = $order->property[$field];
        }
        else
        {
            // Legge lo stato sull'utente
            $user = UserItem::make($order->user_id);

            if ( $user && array_key_exists($field, $user->property) && $user->property[$field] != '' && $user->property[$field] != '00' )
            {
                $returnValue = $user->property[$field];
            }
        }

        return $returnValue;
    }

    public static function getOrderData($order, $field)
    {
        $returnValue = '';

        if ( array_key_exists($field, $order->property) && $order->property[$field] != '' && $order->property[$field] != '00' )
        {
            $returnValue = $order->property[$field];
        }

        return $returnValue;
    }

    public static function getShippingData($order, $shippingField, $billingField)
    {
        $returnValue = '';

        if ( array_key_exists($shippingField, $order->property) && $order->property[$shippingField] != '' && $order->property[$shippingField] != '00' )
        {
            $returnValue = $order->property[$shippingField];
        }
        else
        {
            // Legge lo stato sull'utente
            $user = UserItem::make($order->user_id);

            if ( $user && $user->property ) {
                try {
                    if ($user->property && array_key_exists($shippingField, $user->property) && isset($user->property[$shippingField]) && $user->property[$shippingField] != '' && $user->property[$shippingField] != '00') {
                        $returnValue = $user->property[$shippingField];
                    } else if (array_key_exists($billingField, $user->property) && $user->property[$billingField] != '' && $user->property[$billingField] != '00') {
                        $returnValue = $user->property[$billingField];
                    }
                } catch (\Exception $exception) {
                    dd($order, $user);
                }
            }
            else
            {
                $returnValue = '';
            }

        }

        return strtoupper($returnValue);
    }

    public static function isEU($countrycode) {
        $eu_countrycodes = array(
            'AT', 'BE', 'BG', 'CY', 'CZ', 'DE', 'DK', 'EE', 'EL',
            'ES', 'FI', 'FR', 'GR', 'HR', 'HU', 'IE', 'IT', 'LT', 'LU', 'LV',
            'MC', 'MT', 'NL', 'PL', 'PT', 'RO', 'SE', 'SI', 'SK'
        );
        return (in_array($countrycode, $eu_countrycodes));
    }

    /**
     * @param $account
     * @param OrderItem $order
     * @return string
     * @throws Exception
     */
    protected function callXmlQuotation($account, $order)
    {
        $date = Carbon::now()->format('Y-m-d\TH:i:s.000P');
        $orderModel = $order->getObject();

        $shippingDate = $orderModel->shipping_date;

        if (!$shippingDate )
        {
            $shippingDate = \Carbon\Carbon::now()->format('Y-m-d');

            // Next working day
            //$shippingDate = date('Y-m-d', strtotime($shippingDate. ' +1 Weekday'));
            $shippingDate = date('Y-m-d', strtotime($shippingDate));

            $shippingCarbon = Carbon::createFromFormat("Y-m-d H:i:s", $shippingDate.' 00:00:00');
            $shippingDate = $shippingCarbon->format('Y-m-d');
        }

        $packageType = $orderModel->shipping_package_type;
        $shippingState = strtoupper(static::getShippingData($order, 'shipping_state', 'billing_state'));
        $shippingPostcode = static::getShippingData($order, 'shipping_postcode', 'billing_postcode');
        $shippingCity = static::getShippingData($order, 'shipping_city', 'billing_city');

        if ( !$shippingState )
        {
            return ['success' => false,
                'error' => 'Impossibile leggere la nazione di spedizione'];
        }

        if ( !$packageType )
        {
            return ['success' => false,
                'error' => 'tipo pacco non specificato'];
        }

        $xmlstring = Storage::get('/media/dhl/templates/getquote.xml');
        $xml = simplexml_load_string($xmlstring, "SimpleXMLElement", LIBXML_NOCDATA);

        $xml->GetQuote->Request->ServiceHeader->MessageTime = $date;
        $xml->GetQuote->Request->ServiceHeader->SiteID = $this->siteId;
        $xml->GetQuote->Request->ServiceHeader->Password = $this->password;
        $xml->GetQuote->From->CountryCode = "IT";
        $xml->GetQuote->To->CountryCode = $shippingState;
        $xml->GetQuote->To->Postalcode = $shippingPostcode;
        $xml->GetQuote->To->City = $shippingCity;
        $xml->GetQuote->BkgDetails->PaymentAccountNumber = $account;
        $xml->GetQuote->BkgDetails->Date = $shippingDate;


        if ( $shippingState == 'IT' )
        {
            $xml->GetQuote->BkgDetails->QtdShp->GlobalProductCode = 'N';
        }
        else if ( static::isEU($shippingState) ) {
            $xml->GetQuote->BkgDetails->QtdShp->GlobalProductCode = 'U';
        }
        else if ( $shippingState == 'US' )
        {
            $xml->GetQuote->BkgDetails->QtdShp->GlobalProductCode = 'P';
            $xml->GetQuote->BkgDetails->IsDutiable = 'Y';
            $xml->GetQuote->BkgDetails->QtdShp->QtdShpExChrg->SpecialServiceType = 'WY';
            $xml->GetQuote->Dutiable->DeclaredCurrency = 'EUR';
            $xml->GetQuote->Dutiable->DeclaredValue = '100';
        }

        // Gestisce il tipo pacco
        switch ( strtoupper($packageType) )
        {
            case "A":
                $xml->GetQuote->BkgDetails->Pieces->Piece->Height = "20";
                $xml->GetQuote->BkgDetails->Pieces->Piece->Depth = "5";
                $xml->GetQuote->BkgDetails->Pieces->Piece->Width = "30";
                $xml->GetQuote->BkgDetails->Pieces->Piece->Weight = "0.5";
                break;
            case "B":
                $xml->GetQuote->BkgDetails->Pieces->Piece->Height = "26";
                $xml->GetQuote->BkgDetails->Pieces->Piece->Depth = "12";
                $xml->GetQuote->BkgDetails->Pieces->Piece->Width = "42";
                $xml->GetQuote->BkgDetails->Pieces->Piece->Weight = "1";
                break;
            case "C":
                $xml->GetQuote->BkgDetails->Pieces->Piece->Height = "36";
                $xml->GetQuote->BkgDetails->Pieces->Piece->Depth = "13";
                $xml->GetQuote->BkgDetails->Pieces->Piece->Width = "47";
                $xml->GetQuote->BkgDetails->Pieces->Piece->Weight = "2";
                break;
            case "D": // c+b
                $xml->GetQuote->BkgDetails->Pieces->Piece->Height = "36";
                $xml->GetQuote->BkgDetails->Pieces->Piece->Depth = "13";
                $xml->GetQuote->BkgDetails->Pieces->Piece->Width = "47";
                $xml->GetQuote->BkgDetails->Pieces->Piece->Weight = "2";
                // Aggiunge secondo collo
                $piece2 = $xml->GetQuote->BkgDetails->Pieces->addChild('Piece');
                $piece2->PieceID = 2;
                $piece2->PackageTypeCode = 'BOX';
                $piece2->Height = "26";
                $piece2->Depth = "12";
                $piece2->Width = "42";
                $piece2->Weight = "1";
                break;
            case "E": // c+c
                $xml->GetQuote->BkgDetails->Pieces->Piece->Height = "36";
                $xml->GetQuote->BkgDetails->Pieces->Piece->Depth = "13";
                $xml->GetQuote->BkgDetails->Pieces->Piece->Width = "47";
                $xml->GetQuote->BkgDetails->Pieces->Piece->Weight = "2";
                // Aggiunge secondo collo
                $piece2 = $xml->GetQuote->BkgDetails->Pieces->addChild('Piece');
                $piece2->PieceID = 2;
                $piece2->PackageTypeCode = 'BOX';
                $piece2->Height = "36";
                $piece2->Depth = "13";
                $piece2->Width = "47";
                $piece2->Weight = "2";
                break;
            default:
                return ['success' => false, 'error' => 'Tipo pacco non riconosciuto'];

        }

        //$xml->asXML($this->getFilePath("request-quotation-$account.xml"));


        $client = new \GuzzleHttp\Client();
        $response =$client->post(
            $this->url,
            [
                'headers' => ['Accept' => 'application/xml'],
                'body' => $xml->asXML(),
                'verify' => false
            ]
        )->getBody()->getContents();

        $xmlOutput = simplexml_load_string($response, "SimpleXMLElement", LIBXML_NOCDATA);

        if ( isset($xmlOutput->GetQuoteResponse->BkgDetails) )
        {
            $amount = (string)$xmlOutput->GetQuoteResponse->BkgDetails->QtdShp->QtdSInAdCur->TotalAmount;
        }
        else
        {
            $xml->asXML($this->getFilePath("/errors/quotation-request-web$order->id.xml"));
            $xmlOutput->asXML($this->getFilePath("/errors/quotation-response-web$order->id.xml"));
            $error = isset($xmlOutput->Response->Status->Condition->ConditionData) ? $xmlOutput->Response->Status->Condition->ConditionData : '';
            return [
                'success' => false,
                'error' => $error
            ];
        }

        //$xml->asXML($this->getFilePath("output-quotation-$account.xml"));

        if ( $amount )
        {
            return [
                'success' => true,
                'amount' => $amount,
            ];
        }
        else
        {
            return [
                'success' => false,
                'amount' => 0,
                'debug' => $xmlOutput->asXML()
            ];
        }
    }

    /**
     * @param OrderItem $order
     * @return string
     * @throws Exception
     */
    public function createLabel($order)
    {
        $date = Carbon::now()->format('Y-m-d\TH:i:s.000P');
        /** @var \Lovata\OrdersShopaholic\Models\Order $orderModel */
        $orderModel = $order->getObject();
        $account = $this->account ?: $orderModel->shipping_account;
        if ( !$account )
        {
            return ['success' => false,
                'error' => 'account non impostato'];
        }

        $packageType = $orderModel->shipping_package_type;
        if ( !$orderModel->shipping_package_type )
        {
            return ['success' => false,
                'error' => 'tipo pacco non specificato'];
        }

        $shippingDate = $orderModel->shipping_date;
        if (!$shippingDate )
        {
            $shippingDate = \Carbon\Carbon::now()->format('Y-m-d');
            $shippingDate = date('Y-m-d', strtotime($shippingDate. ' +1 Weekday'));
            $shippingCarbon = Carbon::createFromFormat("Y-m-d H:i:s", $shippingDate.' 00:00:00');
            $shippingDate = $shippingCarbon->format('Y-m-d');
        }

        $shippingState = static::getShippingData($order, 'shipping_state', 'billing_state');
        $shippingPostcode = static::getShippingData($order, 'shipping_postcode', 'billing_postcode');
        $shippingCity = static::getShippingData($order, 'shipping_city', 'billing_city');

        $shippingName = static::getShippingData($order, 'name', 'billing_name');
        $shippingLastName = static::getShippingData($order, 'last_name', 'billing_last_name');
        $shippingPersonName = $shippingName. ' '.$shippingLastName;
        $shippingAddress = static::getShippingData($order, 'shipping_address1', 'billing_address1');
        if ( $order->property['shipping_presso'] )
        {
            $shippingAddress = $shippingAddress." C/O ".$order->property['shipping_presso'];
        }
        if ( strlen($shippingAddress) >= 45 )
        {
            $shippingAddress = substr($shippingAddress, 0, 44);
        }
        $shippingPrefix = static::getOrderUserData($order, 'prefix');
        $shippingPhone = static::getOrderUserData($order, 'phone');
        if ( !$shippingPhone )
        {
            $shippingPhone = $orderModel->user->phone;
        }

        if ( !str_contains($shippingPhone, '+') )
        {
            $shippingPhone = (!str_contains($shippingPrefix, '+') ? '+' : '').$shippingPrefix.' '.$shippingPhone;
        }

        $state = GeoCountry::where('abv', '=', strtoupper($shippingState))->first();

        if ( !$shippingState || $shippingState == '00' )
        {
            throw new Exception('Impossibile leggere lo stato spedizione');
        }


        $xmlstring = Storage::get('/media/dhl/templates/ShipmentValidationECX.xml');
        $xml = simplexml_load_string($xmlstring, "SimpleXMLElement", LIBXML_NOCDATA);

        $xml->Request->ServiceHeader->MessageTime = $date;
        $xml->Request->ServiceHeader->SiteID = $this->siteId;
        $xml->Request->ServiceHeader->Password = $this->password;

        $xml->Billing->ShipperAccountNumber = $account;
        $xml->Billing->BillingAccountNumber = $account;

        $xml->Consignee->CompanyName = $shippingPersonName;
        $xml->Consignee->AddressLine = $shippingAddress;
        $xml->Consignee->City = $shippingCity;
        $xml->Consignee->PostalCode = $shippingPostcode;
        $xml->Consignee->CountryCode = $shippingState;
        if ( $state ) {
            $xml->Consignee->CountryName = $state->name;
        }
        $xml->Consignee->Contact->PersonName = $shippingPersonName;
        $xml->Consignee->Contact->PhoneNumber = $shippingPhone;


        if ( $shippingState == 'IT' )
        {
            $xml->ShipmentDetails->GlobalProductCode = 'N';
        }
        else if ( static::isEU($shippingState) ) {
            $xml->ShipmentDetails->GlobalProductCode = 'U';
        }
        else if ( $shippingState == 'US' )
        {
            $xml->ShipmentDetails->GlobalProductCode = 'P';
            $xml->SpecialService->SpecialServiceType = 'WY';
            $xml->Dutiable->IsDutiable = 'Y';
            $xml->Dutiable->DeclaredCurrency = 'EUR';
            $xml->Dutiable->DeclaredValue = '100';
        }

        // Gestisce il tipo pacco
        switch ( strtoupper($packageType) )
        {
            case "A":
                $xml->ShipmentDetails->Pieces->Piece->Height = "20";
                $xml->ShipmentDetails->Pieces->Piece->Depth = "5";
                $xml->ShipmentDetails->Pieces->Piece->Width = "30";
                $xml->ShipmentDetails->Pieces->Piece->Weight = "0.5";
                $xml->ShipmentDetails->Weight = "0.5";
                break;
            case "B":
                $xml->ShipmentDetails->Pieces->Piece->Height = "26";
                $xml->ShipmentDetails->Pieces->Piece->Depth = "12";
                $xml->ShipmentDetails->Pieces->Piece->Width = "42";
                $xml->ShipmentDetails->Pieces->Piece->Weight = "1";
                $xml->ShipmentDetails->Weight = "1";
                break;
            case "C":
                $xml->ShipmentDetails->Pieces->Piece->Height = "36";
                $xml->ShipmentDetails->Pieces->Piece->Depth = "13";
                $xml->ShipmentDetails->Pieces->Piece->Width = "47";
                $xml->ShipmentDetails->Pieces->Piece->Weight = "2";
                $xml->ShipmentDetails->Weight = "2";
                break;
            case "D":
                // Aggiunge secondo collo
                $xml->ShipmentDetails->Pieces->Piece->Height = "36";
                $xml->ShipmentDetails->Pieces->Piece->Depth = "13";
                $xml->ShipmentDetails->Pieces->Piece->Width = "47";
                $xml->ShipmentDetails->Pieces->Piece->Weight = "2";
                $piece2 = $xml->ShipmentDetails->Pieces->addChild('Piece');
                $piece2->PieceID = 2;
                $piece2->PackageType = 'YP';
                $piece2->Weight = "1";
                $piece2->Width = "47";
                $piece2->Height = "36";
                $piece2->Depth = "13";
                $xml->ShipmentDetails->Weight = "3";
                break;
            case "E":
                $xml->ShipmentDetails->Pieces->Piece->Height = "36";
                $xml->ShipmentDetails->Pieces->Piece->Depth = "13";
                $xml->ShipmentDetails->Pieces->Piece->Width = "47";
                $xml->ShipmentDetails->Pieces->Piece->Weight = "2";
                // Aggiunge secondo collo
                $piece2 = $xml->ShipmentDetails->Pieces->addChild('Piece');
                $piece2->PieceID = 2;
                $piece2->PackageType = 'YP';
                $piece2->Weight = "2";
                $piece2->Width = "47";
                $piece2->Height = "36";
                $piece2->Depth = "13";
                $xml->ShipmentDetails->Weight = "4";
                break;
            default:
                return ['success' => false, 'error' => 'Tipo pacco non riconosciuto'];

        }

        $xml->ShipmentDetails->Date = $shippingDate;

        $xml->Reference->ReferenceID = "ORDER WEB".$order->id;

        $xml->asXML($this->getFilePath("shipmentrequest-$account.xml"));
        $client = new \GuzzleHttp\Client();
        $response =$client->post(
            $this->url,
            [
                'headers' => ['Accept' => 'application/xml'],
                'body' => $xml->asXML(),
                'verify' => false
            ]
        )->getBody()->getContents();

        $response = utf8_encode($response);

        $xmlOutput = simplexml_load_string($response, "SimpleXMLElement", LIBXML_NOCDATA);

        if ( isset($xmlOutput->AirwayBillNumber) )
        {
            $tracking = (string)$xmlOutput->AirwayBillNumber;
            $charge = (string)$xmlOutput->ShippingCharge;

            $xmlOutput->asXML($this->getFilePath("/output/shipping-response-web$order->id.xml"));

            // Salva il pdf
            $file = $this->getFilePath("/labels/web$order->id.pdf");
            file_put_contents($file,base64_decode($xmlOutput->LabelImage->OutputImage));

            return [
                'success' => true,
                'tracking' => $tracking,
                'amount' => $charge,
                'date' => $shippingDate
            ];
        }
        else
        {
            $xml->asXML($this->getFilePath("/errors/shipping-request-web$order->id.xml"));
            $xmlOutput->asXML($this->getFilePath("/errors/shipping-response-web$order->id.ml"));

            return [
                'success' => false,
                'tracking' => '',
                'charge' => '',
                'date' => '',
                'error' => $xmlOutput->Response->Status->Condition->ConditionData
            ];


        }
    }

    public function checkLabel($orderId)
    {
        $url = "/labels/web$orderId.pdf";
        $file = $this->getFilePath($url);
        return file_exists($file) ? "web$orderId.pdf" : '';
    }

    public function getFilePath($fileName)
    {
        return Storage::disk('local')->path($this->basePath.$fileName);
    }

    /**
     * @param OrderItem $order
     * @return array
     */
    public function getQuote($order)
    {
        $collection = new Collection();
        $lastQuotation = 0;

        foreach ($this->accountList as $account) {
            $quotationOutput = $this->callXmlQuotation($account, $order);
            if ( $quotationOutput['success'] )
            {
                $amount = $quotationOutput['amount'];
                $collection->put($account, $amount);
                if ( $lastQuotation == 0 || $amount < $lastQuotation )
                {
                    $lastQuotation = $amount;
                    $this->account = $account;
                }
            }
            else
            {
                return $quotationOutput;
            }
        }

        return [
            'success' => true,
            'account' => $this->account,
            'rates' => $collection,
        ];
    }



    public function getQuotationDHL($data,$order_id){
        $order = OrderItem::make($order_id)->getObject();
        $dhl_rates = new DHLRatesBody($data,$order);
        $dhlauth = new DHLAuth($this::URL_SITE,$this::DHL_BASE_URL,$this::DHL_USER,$this::DHL_PASS);
        $dhlauth->authenticate();
        return [$dhlauth->send($dhl_rates,'rates', $order), $data['shipping_account']];
    }
    
    public function createShippingDHL($data){
        $order = OrderItem::make($data['order_id'])->getObject();
        $user = UserItem::make($order->user_id);
        $dhlauth = new DHLAuth($this::URL_SITE,$this::DHL_BASE_URL,$this::DHL_USER,$this::DHL_PASS);
        $dhlauth->authenticate();
        $dhl_ship = new DHLShipmentsBody($data,$order,$user,'spedizione');
        return $dhlauth->send_create($dhl_ship,'shipments', $order);
    }
    
    public function createReturnShippingDHL($data){
        $order = OrderItem::make($data['order_id'])->getObject();
        $user = UserItem::make($order->user_id);
        $dhlauth = new DHLAuth($this::URL_SITE,$this::DHL_BASE_URL,$this::DHL_USER,$this::DHL_PASS);
        $dhlauth->authenticate();
        $dhl_ship = new DHLShipmentsBody($data,$order,$user,'reso');
        return $dhlauth->send($dhl_ship,'shipments',$order);
    }



}