<?php

namespace Ims\Shophelper\Classes\DHLAuthC;

use October\Rain\Exception\AjaxException;
use October\Rain\Exception\ErrorHandler;

class DHLAuth{

    public $base_url;
    public $dhl_base_url;
    public $dhl_user;
    public $dhl_pass;
    public $dhl_headers;

    public function __construct($base_url,$dhl_base_url,$dhl_user,$dhl_pass)
    {
        $this->base_url = $base_url;
        $this->dhl_base_url = $dhl_base_url;
        $this->dhl_user = $dhl_user;
        $this->dhl_pass = $dhl_pass;
    }

    public function authenticate(){
        $curl = curl_init($this->dhl_base_url);
        $headers = array(
            'Content-Type: application/json',
            'Authorization: Basic '. base64_encode("$this->dhl_user:$this->dhl_pass")
        );
        //Set the headers that we want our cURL client to use.
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        //Execute the cURL request.
        $response = curl_exec($curl);
        
        if ($response === false) 
            $response = curl_error($curl);

        $this->dhl_headers = $headers;
    }

    public function get_tracking($url){
        $curl = curl_init($url);
        $headers = array(
            'Content-Type: application/json',
            'Authorization: Basic '. base64_encode("$this->dhl_user:$this->dhl_pass")
        );
        //Set the headers that we want our cURL client to use.
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        //Execute the cURL request.
        $response = curl_exec($curl);
        
        if ($response === false) 
            $response = curl_error($curl);

        return json_decode($response, true);
    }

    public function send($obj,$param, $order){
        $curl = curl_init();
        $userAccount = $obj->accounts[0]->number;
        $userpwd = $this->dhl_user.":".$this->dhl_pass;
        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->dhl_base_url.'/'.$param,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => json_encode($obj),
            CURLOPT_HTTPHEADER => $this->dhl_headers,
          ));
          
          $response = curl_exec($curl);

          $res = json_decode($response,true);

        if($param !== 'rates'){
            $f = fopen(storage_path("app/media/log_send_reso_dhl_".$order->id.".txt"), 'w');
            fwrite($f, json_encode($obj));
            fclose($f);  
        }

          if(isset($res['status'])){
              $error = new ErrorHandler();
              return view($error->handleDetailedError(new AjaxException(['error'=>$res['detail']])));
          }

        if (curl_errno($curl)) { 
            return curl_error($curl); 
        } 
        curl_close($curl);
        if ($param !== 'rates') {
            if (!is_dir(storage_path('app/media/shipmentsLabel/'.$order->id))) {
                mkdir(storage_path('app/media/shipmentsLabel/'.$order->id), 0777, true);
            }
            $file = json_decode($response, true);

            foreach ($file['documents'] as $valore) {
                $f = fopen(storage_path('app/media/shipmentsLabel/'.$order->id.'/etichetta_reso_'.$order->id.'.pdf'), 'wb');
                fwrite($f, base64_decode($valore['content']));
                fclose($f);            
            }
        }
        return json_decode($response);
    }

    public function send_create($obj,$param,$order){
        $curl = curl_init();
        $userpwd = $this->dhl_user.":".$this->dhl_pass;
        $userAccount = $obj->accounts[0]->number;
        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->dhl_base_url.'/'.$param,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => json_encode($obj),
            CURLOPT_HTTPHEADER => $this->dhl_headers,
          ));
          
          $response = curl_exec($curl);

          $f = fopen(storage_path('app/media/log_send_create_dhl.txt'), 'w');
          fwrite($f, json_encode($obj));
          fclose($f);  

          $res = json_decode($response,true);
          if(isset($res['status'])){
              $error = new ErrorHandler();
              return view($error->handleDetailedError(new AjaxException(['error'=>$res['detail']])));
          }

        if (curl_errno($curl)) { 
            return curl_error($curl); 
        } 

        curl_close($curl);
        $file = json_decode($response,true); //associative
        if (!is_dir(storage_path('app/media/shipmentsLabel/'.$order->id))) {
            mkdir(storage_path('app/media/shipmentsLabel/'.$order->id), 0777, true);
        }
        foreach ($file['documents'] as $valore) {
            $f = fopen(storage_path('app/media/shipmentsLabel/'.$order->id.'/etichetta_partenza_'.$order->id.'.pdf'), 'wb');
            fwrite($f, base64_decode($valore['content']));
            fclose($f);
        }
        $order->tracking_url_dhl = $file['trackingUrl'];
        $order->shipping_tracking = $file['shipmentTrackingNumber'];
        $order->shipping_account = $userAccount;
        if ($order->status_id == 3) {
            $order->status_id = 5;
        }
        $order->update();
        return json_decode($response);

    }

}

?>