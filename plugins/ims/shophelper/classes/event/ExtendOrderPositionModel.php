<?php
/**
 * Created by PhpStorm.
 * User: marco
 * Date: 09/10/2020
 * Time: 19:08
 */

namespace Ims\Shophelper\Classes\Event;


use Lovata\OrdersShopaholic\Models\OrderPosition;

/**
 * Class ExtendCartPositionModel
 * @package Lovata\BaseCode\Classes\Event\CartPosition
 */
class ExtendOrderPositionModel
{
    public function subscribe()
    {
        OrderPosition::extend(function ($obCartPosition) {

            $obCartPosition->fillable[] = 'customized_offer_id';
            $obCartPosition->fillable[] = 'customized_content';

            $obCartPosition->addCachedField(['customized_offer_id']);
            $obCartPosition->addCachedField(['customized_content']);
        });
    }
}