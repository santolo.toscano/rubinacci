<?php namespace Ims\Shophelper\Classes\Excel;

use Lovata\PropertiesShopaholic\Models\PropertyValue;
use Lovata\Shopaholic\Models\Product;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;

class ProductsExport implements FromCollection, WithStrictNullComparison, WithHeadings,ShouldAutoSize,WithColumnFormatting
{

    public function columnFormats(): array
    {
        return [
            'C' => '@',
            'D' => '@',
            'E' => '@',
        ];
    }

    public function collection(){

        $products = Product::where('is_esporta', 1)->orderBy('name')->get();
        $elements = collect();


        $properyValueList = PropertyValue::all();

        foreach ($products as $product){
            $offers = $product->offer;


            foreach ($offers as $offer) {
                $materiale = '';

                foreach ( $product->property_value as $propertyValue )
                {
                    if ( $propertyValue->property_id == 7 )
                    {
                        $propertyValue = $properyValueList->find($propertyValue->value_id);
                        $materiale = $propertyValue->value;
                        break;
                    }
                }

                $element = array( $product->name ?: '', $product->code ?: '', $offer->name ?: '', (string)$offer->quantity, $offer->price, $materiale);
                $elements->push($element);
            }
        };

        return $elements;
    }


    public function headings(): array{
        return ["Nome Prodotto", "Codice Prodotto", "Taglia", "Quantità", "Prezzo", "Materiale"];
    }
}
