<?php namespace Ims\Shophelper\Classes\Excel;

use Ims\Shophelper\Models\SizeTable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class UsersExport implements FromCollection, WithHeadings
{
    public function collection()
    {
        return SizeTable::all();
    }

    public function headings(): array
    {
        return ["your", "headings", "here"];
    }
}
