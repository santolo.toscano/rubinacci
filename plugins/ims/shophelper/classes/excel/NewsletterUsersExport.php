<?php namespace Ims\Shophelper\Classes\Excel;

use Illuminate\Support\Facades\DB;
use Ims\Shophelper\Models\ProductStock;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Ims\Shophelper\Models\NewsletterCustomer;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Lovata\PropertiesShopaholic\Models\PropertyValue;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;

class NewsletterUsersExport implements FromCollection, WithStrictNullComparison, WithHeadings,ShouldAutoSize,WithColumnFormatting
{
    public function columnFormats(): array
    {
        return [
            'A' => '@',
            'B' => '@',
            'C' => '@',
        ];
    }

    public function collection(){
        $products = DB::table('ims_shophelper_newsletter')->get();
        $elements = collect();
        foreach ($products as $product){
            $id = (string)$product->id; 
            $element = array($id, $product->email, $product->created_at);
            $elements->push($element);
        };
        return $elements;
    }
 
    public function headings(): array{
        return ['Id', 'Email Utente', 'Data Creazione'];
    }
}