<?php namespace Ims\Shophelper\Classes\Excel;

use Lovata\Shopaholic\Models\Product;
use Ims\Shophelper\Models\ProductStock;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Lovata\PropertiesShopaholic\Models\PropertyValue;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;

class StockExport implements FromCollection, WithStrictNullComparison, WithHeadings,ShouldAutoSize,WithColumnFormatting
{

    public function columnFormats(): array
    {
        return [
            'C' => '@',
            'D' => '@',
            'E' => '@',
        ];
    }

    public function collection(){

        $products = ProductStock::get();
        $elements = collect();

        foreach ($products as $product){
            $element = array( $product->p_name, $product->p_code, $product->o_code, $product->o_name, (string)$product->quantita);
            $elements->push($element);
        };

        return $elements;
    }


    public function headings(): array{
        return ["Nome Prodotto", "Codice Prodotto", "Codice Taglia", "Taglia", "Quantità"];
    }
}
