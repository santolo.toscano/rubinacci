<?php


namespace Ims\Shophelper\Classes;


use Lovata\OrdersShopaholic\Classes\Processor\CartProcessor;
use Lovata\OrdersShopaholic\Interfaces\CheckRestrictionInterface;

class ShippingWeightRestriction implements CheckRestrictionInterface
{

    /**
     * @var float
     */
    protected  $MinWeight;
    /**
     * @var float
     */
    protected  $MaxWeight;
    /**
     * @var int
     */
    protected $Weight;

    public function __construct($obShippingTypeItem, $arData, $arProperty, $sCode)
    {
        $this->MinWeight = (float) array_get($arProperty, 'weight_min');
        $this->MaxWeight = (float) array_get($arProperty, 'weight_max');
		
        // Calcola il peso
		// Fa un ciclo for sulle offer di orderpositions * quantita
        $cartProcessor = CartProcessor::instance();
        $weight = 0;
        foreach ($cartProcessor->get() as $positionItem){
            $weight = $weight + ($positionItem->offer->weight * $positionItem->quantity);
        }

        $this->Weight = $weight;
    }

    public static function getFields(): array
    {
        return [
            'property[weight_min]' => [
                'label'   => 'Peso minimo',
                'tab'     => 'lovata.toolbox::lang.tab.settings',
                'span'    => 'left',
                'type'    => 'number',
                'context' => ['update', 'preview']
            ],
            'property[weight_max]' => [
                'label'   => 'Peso massimo',
                'tab'     => 'lovata.toolbox::lang.tab.settings',
                'span'    => 'right',
                'type'    => 'number',
                'context' => ['update', 'preview']
            ],
        ];
    }

    public function check(): bool
    {
        $bResult = $this->Weight > $this->MinWeight && ($this->MaxWeight == 0 || $this->Weight <= $this->MaxWeight);
		//dd($this->Weight, $this->MinWeight, $this->MaxWeight);
        return $bResult;
    }

}