<?php

namespace Ims\Shophelper\Classes\Dhlratesbodyc;

class ReceiverDetails
{
    /**
     * @var string
     */
    public $postalCode;

    /**
     * @var string
     */
    public $cityName;

    /**
     * @var string
     */
    public $countryCode;

    /**
     * @var string
     */
    public $provinceCode;

    /**
     * @var string
     */
    public $addressLine1;
    /**
     * @var string
     */
    public $addressLine2;
    /**
     * @var string
     */
    public $addressLine3;

    /**
     * @var string
     */
    public $countyName;


    public function __construct($receiver)
    {
        $this->postalCode = $receiver->property['property']['shipping_postcode'];
        if(strlen($receiver->property['shipping_address1']) >= 45){
            if(strlen($receiver->property['shipping_address1']) >= 90){
                $indirizzi = str_split($receiver->property['shipping_address1'],45);
                $this->addressLine1 = $indirizzi[0];
                $this->addressLine2 = $indirizzi[1];
                $this->addressLine3 = $indirizzi[2];
            }else{
                $indirizzi = str_split($receiver->property['shipping_address1'],45);
                $this->addressLine1 = $indirizzi[0];
                $this->addressLine2 = $indirizzi[1];
                $this->addressLine3 = ".";
            }
        }else{
            $this->addressLine1 = $receiver->property['shipping_address1'];
            $this->addressLine2 = ".";
            $this->addressLine3 = ".";
        }
        $this->cityName = $receiver->property['property']['shipping_city'];
        $this->countryCode = $receiver->property['property']['shipping_state'];
        $this->provinceCode = "  ";
        $this->countyName = " ";
        
        if($receiver->property['property']['shipping_region']!=='EE'){
            $this->provinceCode = !empty($receiver->property['property']['shipping_region']) ? $receiver->property['property']['shipping_region'] : '  ';
            $this->countyName = "  ";
        }
    }
}

