<?php

namespace Ims\Shophelper\Classes\Dhlratesbodyc;

class Packages
{
    /**
     * @var float
     */
    public $weight;

    /**
     * @var Dimensions
     */
    public $dimensions;
    
    public function __construct($weight, $dimensions)
    {
        $this->weight = floatval($weight);
        $this->dimensions = new Dimensions($dimensions);
    }
}

