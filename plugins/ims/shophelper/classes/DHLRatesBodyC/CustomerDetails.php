<?php

namespace Ims\Shophelper\Classes\Dhlratesbodyc;

class CustomerDetails
{
    /**
     * @var ShipperDetails
     */
    public $shipperDetails;

    /**
     * @var ReceiverDetails
     */
    public $receiverDetails;


    public function __construct($order)
    {
        $this->shipperDetails = new ShipperDetails();
        $this->receiverDetails = new ReceiverDetails($order);
    }
}

