<?php

namespace Ims\Shophelper\Classes\Dhlratesbodyc;

class ShipperDetails
{
    /**
     * @var string
     */
    public $postalCode;

    /**
     * @var string
     */
    public $cityName;

    /**
     * @var string
     */
    public $countryCode;

    /**
     * @var string
     */
    public $provinceCode;

    /**
     * @var string
     */
    public $addressLine1;

    /**
     * @var string
     */
    public $countyName;

    public function __construct()
    {
        $this->postalCode = "80121";
        $this->cityName = "Napoli";
        $this->countryCode = "IT";
        $this->provinceCode = "NA";
        $this->addressLine1 = "Via Chiaia, 149";
        $this->countyName = "Campania";
    }
}

