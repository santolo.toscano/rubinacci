<?php

namespace Ims\Shophelper\Classes\Dhlratesbodyc;

class DHLRatesBody
{

    const PACKAGES = [
        'A' => [27,27,5],
        'B' => [40,13,6],
        'C' => [34,20,13],
        'D' => [41,41,7],
        'E' => [47,32,5],
        'F' => [45,35,13],
        'G' => [59,46,10],
        'H' => [25,20,3],
        'I' => [24,35,3]
    ];

    /**
     * @var CustomerDetails
     */
    public $customerDetails;

    /**
     * @var string
     */
    public $plannedShippingDateAndTime;

    /**
     * @var string
     */
    public $unitOfMeasurement;

    /**
     * @var bool
     */
    public $isCustomsDeclarable;

    /**
     * @var array
     */
    public $packages;

    /**
     * @var array
     */
    public $accounts;

    /**
     * @var array
     */
    public $monetaryAmount;
    
    /**
     * @var array
     */
    public $valueAddedServices;
    /**
     * @var bool
     */
    public $requestAllValueAddedServices;
    
    /**
     * @var bool
     */
    public $returnStandardProductsOnly;

    /**
     * @var bool
     */
    public $nextBusinessDay;

    /**
     * @var string
     */
    public $productTypeCode;


    public function __construct($data,$order)
    {
        $this->customerDetails = new CustomerDetails($order);
        $this->plannedShippingDateAndTime = $data['data_spediz'];
        $this->unitOfMeasurement = 'metric';
        $this->isCustomsDeclarable = (!(static::isEU($order->property['property']['shipping_state']))) ? true : false;
        for($i = 1; $i<=sizeof($data['packages']['pack_weight']); $i++){
            $this->packages[] = new Packages($data['packages']['pack_weight'][$i],$this::PACKAGES[$data['packages']['shipping_package_type'][$i]],$data['packages']['shipping_contents'][$i]);
        }
        $this->accounts = [new Accounts($data)];
        if((floatval($order->dazi_value)>0) && isset($order->dazi_value))
            $this->valueAddedServices[] = new ValueAddedServices();
        else
            $this->valueAddedServices = [];
        $this->monetaryAmount = [new MonetaryAmount($order)];
        $this->requestAllValueAddedServices = false;
        $this->returnStandardProductsOnly = true;
        $this->nextBusinessDay = true;
        $this->productTypeCode = 'all';
    }

    public static function isEU($countrycode) {
        $eu_countrycodes = array(
            'AT', 'BE', 'BG', 'CY', 'CZ', 'DE', 'DK', 'EE', 'EL',
            'ES', 'FI', 'FR', 'GR', 'HR', 'HU', 'IE', 'IT', 'LT', 'LU', 'LV',
            'MC', 'MT', 'NL', 'PL', 'PT', 'RO', 'SE', 'SI', 'SK'
        );
        return (in_array($countrycode, $eu_countrycodes));
    }


}

