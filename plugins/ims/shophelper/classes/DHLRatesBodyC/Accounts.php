<?php

namespace Ims\Shophelper\Classes\Dhlratesbodyc;

class Accounts
{
    /**
     * @var string
     */
    public $typeCode;

    /**
     * @var string
     */
    public $number;

    public function __construct($data)
    {
        $this->typeCode = 'shipper';
        $this->number = $data['shipping_account'];
    }
}

