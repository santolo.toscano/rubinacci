<?php

namespace Ims\Shophelper\Classes\Dhlratesbodyc;

class Dimensions
{
    /**
     * @var int
     */
    public $length;

    /**
     * @var int
     */
    public $width;

    /**
     * @var int
     */
    public $height;
    
    public function __construct($values)
    {
        $this->length = floatval($values[0]);
        $this->width = floatval($values[1]);
        $this->height = floatval($values[2]);
    }
}

