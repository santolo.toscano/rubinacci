<?php

namespace Ims\Shophelper\Classes\Dhlratesbodyc;

class ValueAddedServices
{
    /**
     * @var string
     */
    public $serviceCode;

    /**
     * @var string
     */
    public $localServiceCode;

    public function __construct()
    {
        $this->serviceCode = 'DD';
        $this->localServiceCode = 'DD';
    }
}
?>