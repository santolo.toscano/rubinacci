<?php

namespace Ims\Shophelper\Classes\Dhlratesbodyc;

class MonetaryAmount
{
    /**
     * @var string
     */
    public $typeCode;

    /**
     * @var int
     */
    public $value;

    /**
     * @var string
     */
    public $currency;

    public function __construct($order)
    {
        $this->typeCode = "declaredValue";
        $this->value = (float)$order->conv_prezzo_tot-(float)$order->shipping_price-(float)$order->dazi_value;
        $this->currency = 'EUR';
    }
}

