<?php

namespace Ims\Shophelper\Classes\Dhlshipmentsbodyc;

class CustomerDetails
{
    /**
     * @var ShipperDetails
     */
    public $shipperDetails;

    /**
     * @var ReceiverDetails
     */
    public $receiverDetails;


    public function __construct($search,$order,$user)
    {
        if($search == 'create_shipping' || $search == 'create_shipping_send'){
            $this->shipperDetails = new ShipperDetails($search,null,null);
            $this->receiverDetails = new ReceiverDetails($search,$order,$user);
        }else{
            $this->shipperDetails = new ShipperDetails($search,$order,$user);
            $this->receiverDetails = new ReceiverDetails($search,null,null);
        }
    }
}

