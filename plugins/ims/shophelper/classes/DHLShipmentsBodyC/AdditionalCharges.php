<?php

namespace Ims\Shophelper\Classes\Dhlshipmentsbodyc;

class AdditionalCharges
{
    /**
     * @var float
     */
    public $value;

    /**
     * @var string
     */
    public $caption;

    /**
     * @var string
     */
    public $typeCode;

    public function __construct($val,$param,$type)
    {
        $this->value = $val;
        $this->caption = $param;
        $this->typeCode = $type;
    }
   
}

