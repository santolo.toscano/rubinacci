<?php

namespace Ims\Shophelper\Classes\Dhlshipmentsbodyc;

class ReceiverDetails
{
    /**
     * @var PostalAddress
     */
    public $postalAddress;

    /**
     * @var ContactInformation
     */
    public $contactInformation;


    public function __construct($search,$receiver,$user)
    {
        if($search=='create_shipping' || $search=='create_shipping_send'){
            $this->postalAddress = new PostalAddress($receiver,'user');
            $nome_ship = $receiver->property['property']['shipping_name'] !== "" ? $receiver->property['property']['shipping_name'] : $user->name;
            $cognome_ship = $receiver->property['property']['shipping_last_name'] !== "" ? $receiver->property['property']['shipping_last_name'] : $user->last_name;
            $presso = !empty($receiver->property['shipping_presso']) ? "C/O : ".$receiver->property['shipping_presso'] : $nome_ship.' '.$cognome_ship;
            $this->contactInformation = new ContactInformation($presso,$receiver,$user,'user');
        }else{
            $this->postalAddress = new PostalAddress(null,'rubinacci');
            $this->contactInformation = new ContactInformation(null,null,null,'rubinacci');
        }
    }

}

