<?php

namespace Ims\Shophelper\Classes\Dhlshipmentsbodyc;

class Quantity
{
    /**
     * @var int
     */
    public $value;

    /**
     * @var string
     */
    public $unitOfMeasurement;

    public function __construct($quantity)
    {
        $this->value = $quantity;
        $this->unitOfMeasurement = 'PCS';
    }
}

