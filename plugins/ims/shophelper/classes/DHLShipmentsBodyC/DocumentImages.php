<?php

namespace Ims\Shophelper\Classes\Dhlshipmentsbodyc;

class DocumentImages
{
    /**
     * @var string
     */
    public $content;

    /**
     * @var string
     */
    public $typeCode;

    /**
     * @var string
     */
    public $imageFormat;

    public function __construct($content, $typeCode)
    {
        $this->content = $content;
        $this->typeCode = $typeCode;
        $this->imageFormat = 'PDF';
    }
}