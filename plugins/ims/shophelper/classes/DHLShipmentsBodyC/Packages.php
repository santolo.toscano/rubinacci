<?php

namespace Ims\Shophelper\Classes\Dhlshipmentsbodyc;

class Packages
{
    /**
     * @var float
     */
    public $weight;

    /**
     * @var Dimensions
     */
    public $dimensions;

    /**
     * @var string
     */
    public $description;

    /**
     * @var string
     */
    public $labelDescription;

    public function __construct($pack_weight,$shipping_package_type,$shipping_contents)
    {
        $this->weight = floatval($pack_weight);
        $this->dimensions = new Dimensions($shipping_package_type);
        $this->description = $shipping_contents;
        $this->labelDescription = $shipping_contents;
    }
}

