<?php

namespace Ims\Shophelper\Classes\Dhlshipmentsbodyc;

class Accounts
{
    /**
     * @var string
     */
    public $typeCode;

    /**
     * @var string
     */
    public $number;

    public function __construct($data, $order)
    {
        $this->typeCode = 'shipper';
        if($data['search'] == 'create_shipping' || $data['search'] == 'create_shipping_send'){
            $this->number = $data['params']['shipping_account'];
        }else{
            if (!(static::isIT($order->property['property']['shipping_state']))) {
                $this->number = "956154183";
            } else {
                $this->number = $data['params']['shipping_account'];
            }
        }
    }

    public static function isIT($state) {
        return $state === 'IT';
    }
}
