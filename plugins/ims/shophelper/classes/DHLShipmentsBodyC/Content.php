<?php

namespace Ims\Shophelper\Classes\Dhlshipmentsbodyc;

use Lovata\OrdersShopaholic\Models\OrderPosition;
use Lovata\Shopaholic\Models\Currency;
use SebastianBergmann\Exporter\Exporter;

class Content
{
    /**
     * @var array
     */
    public $packages;

    /**
     * @var bool
     */
    public $isCustomsDeclarable;

    /**
     * @var string
     */
    public $description;

    /**
     * @var string
     */
    public $incoterm;

    /**
     * @var string
     */
    public $unitOfMeasurement;

    /**
     * @var int
     */
    public $declaredValue;

    /**
     * @var string
     */
    public $declaredValueCurrency;

    /**
     * @var ExportDeclaration
     */
    public $exportDeclaration;


    public function __construct($user, $data, $order, $price_no_dazi, $dazi, $spedizDescr, $pack)
    {
        $this->packages = $pack;
        $this->isCustomsDeclarable = (!(static::isEU($order->property['property']['shipping_state']))) ? true : false;
        if($data['search'] == 'create_shipping_return'){
            $curr_rate = floatval(Currency::where('id',$order->currency_id)->first()->rate);
            $selected_positions = json_decode(stripslashes($data['params']['selected_return_products']));
            $positions = $selected_positions->selected_return_products;
            $newprice = 0;
            foreach($positions as $pos){
                $pos = explode('_',$pos);
                $order_pos = OrderPosition::where('id',(int)$pos[1])->first();
                if(isset($order_pos)){
                    $couponDiscount = count($order->order_promo_mechanism) > 0 ? $order->order_promo_mechanism->first()->discount_value : 0;
                    if($couponDiscount > 0){
                        $couponPrice = round($order_pos->price_value * ((100-$couponDiscount)/100),0);
                        $newprice += round(round(((($couponPrice)*$order_pos->quantity)/1.22),0)/$curr_rate,0);
                    }else{
                        $newprice += round(round((($order_pos->price_value*$order_pos->quantity)/1.22),2)/$curr_rate,2);
                    }
                }
            }
            $this->declaredValue = $newprice;
        }else{
            $this->declaredValue = (!(static::isEU($order->property['property']['shipping_state']))) ? $price_no_dazi : 0;
        }
        
        $this->description = empty(trim($spedizDescr)) ? "descrizione della spedizione" : trim($spedizDescr);
        $this->incoterm = $dazi == 'Si' ? 'DDP' : 'DAP';
        $this->unitOfMeasurement = 'metric';
        $this->declaredValueCurrency = 'EUR';
        $hasDazi = $dazi == 'Si' ? true : false;
        $this->exportDeclaration = new ExportDeclaration($user, $data, $hasDazi, $order);
    }

    public static function isEU($countrycode) {
        $eu_countrycodes = array(
            'AT', 'BE', 'BG', 'CY', 'CZ', 'DE', 'DK', 'EE', 'EL',
            'ES', 'FI', 'FR', 'GR', 'HR', 'HU', 'IE', 'IT', 'LT', 'LU', 'LV',
            'MC', 'MT', 'NL', 'PL', 'PT', 'RO', 'SE', 'SI', 'SK'
        );
        return (in_array($countrycode, $eu_countrycodes));
    }
}

