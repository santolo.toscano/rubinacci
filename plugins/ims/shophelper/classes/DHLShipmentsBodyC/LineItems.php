<?php

namespace Ims\Shophelper\Classes\Dhlshipmentsbodyc;

use Ims\Shophelper\Classes\Dhlshipmentsbodyc\Weight;
use Ims\Shophelper\Classes\Dhlshipmentsbodyc\Quantity;
use Lovata\Shopaholic\Classes\Item\OfferItem;

class LineItems
{
    /**
     * @var int
     */
    public $number;

    /**
     * @var string
     */
    public $description;

    /**
     * @var int
     */
    public $price;

    /**
     * @var Quantity
     */
    public $quantity;

    /**
     * @var string
     */
    public $manufacturerCountry;

    /**
     * @var string
     */
    public $exportReasonType;

    /**
     * @var Weight
     */
    public $weight;

    /**
     * @var bool
     */
    public $isTaxesPaid;

    public function __construct($hasDazi, $position, $quantity,$price_val, $j)
    {
        $this->number = $j;
        $this->description = $position->name;
        if($price_val !== 0){
            $this->price = floatval($price_val);
        }else{
            $this->price = 0;
        }
        $this->quantity = new Quantity($quantity);
        $this->manufacturerCountry = 'IT';
        $this->exportReasonType = 'commercial_purpose_or_sale';
        $this->weight = new Weight();
        $this->isTaxesPaid = $hasDazi;
    }
}

