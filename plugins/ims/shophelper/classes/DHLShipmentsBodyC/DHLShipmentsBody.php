<?php

namespace Ims\Shophelper\Classes\Dhlshipmentsbodyc;

class DHLShipmentsBody
{
    const PACKAGES = [
        'A' => [27,27,5],
        'B' => [40,13,6],
        'C' => [34,20,13],
        'D' => [41,41,7],
        'E' => [47,32,5],
        'F' => [45,35,13],
        'G' => [59,46,10],
        'H' => [25,20,3],
        'I' => [24,35,3]
    ];

    /**
     * @var string
     */
    public $plannedShippingDateAndTime;

    /**
     * @var string
     */
    public $productCode;

    /**
     * @var string
     */
    public $localProductCode;

    /**
     * @var array
     */
    public $accounts;

    /**
     * @var CustomerDetails
     */
    public $customerDetails;

    /**
     * @var Content
     */
    public $content;

    /**
     * @var bool
     */
    public $getRateEstimates;

    /**
     * @var array
     */
    public $documentImages;

    /**
     * @var bool
     */
    public $getOptionalInformation;

    /**
     * @var array
     */
    public $valueAddedServices;

    /**
     * @var Pickup
     */
    public $pickup;

    /**
     * @var array
     */
    public $customerReferences;

    /**
     * @var array
     */
    public $identifiers;

    public function __construct($data,$order,$user,$param)
    {
        $this->plannedShippingDateAndTime = $data['params']['data_spediz']."T".date('H:m:s')."GMT+01:00";
        $this->productCode = $data['params']['productCode']; //global product code
        $this->localProductCode = $data['params']['localProductCode']; //local product code
        $this->accounts = [new Accounts($data, $order)];
        $this->customerDetails = new CustomerDetails($data['search'],$order,$user);
        $pack = [];
        for($i = 1; $i<=sizeof($data['params']['packages']['pack_weight']); $i++){
            $pack[] = new Packages($data['params']['packages']['pack_weight'][$i],
            $this::PACKAGES[$data['params']['packages']['shipping_package_type'][$i]],
            $data['params']['packages']['shipping_contents'][$i]);
        }
        // Se il cliente ha i dazi price no dazi è uguale a tutto tranne spedizione
        // Devo aggiungere DD a value added service se order->dazi_value è diverso da 0
        // altrimenti price no dazi è tutto meno spezione uguale a totale merce -> non devo mettere DD
        $descrizione_spedizione = implode(', ',$data['params']['packages']['shipping_contents']);
        $price_no_dazi = floatval($order->conv_prezzo_tot) - floatval($order->shipping_price);
        $this->content = new Content($user, $data, $order, $price_no_dazi, $data['params']['shipping_duty_payment_type'], $descrizione_spedizione, $pack); 
        $this->getRateEstimates = true;
        $this->documentImages = [];
        array_push($this->documentImages, new DocumentImages($data['params']['document_invoice'], 'INV'));
        if(strcasecmp($data['params']['document_cin'],'nil')!==0)
            array_push($this->documentImages, new DocumentImages($data['params']['document_cin'], 'CIN'));
        if(strcasecmp($data['params']['document_others_1'],'nil')!==0)
            array_push($this->documentImages, new DocumentImages($data['params']['document_others_1'], 'DCL'));
        if(strcasecmp($data['params']['document_others_2'],'nil')!==0)
            array_push($this->documentImages, new DocumentImages($data['params']['document_others_2'], 'DCL'));
        if(strcasecmp($data['params']['document_others_3'],'nil')!==0)
            array_push($this->documentImages, new DocumentImages($data['params']['document_others_3'], 'DCL'));
        $this->getOptionalInformation = false;
        $hasDazi = (!(static::isEU($order->property['property']['shipping_state']))) ? "WY" : "CR";
        $hasDazi = ($order->property['property']['shipping_state']=='IT') ? 'TK' : $hasDazi;
        if($hasDazi=="CR" || $hasDazi == 'TK'){
            $this->documentImages = [];
            $this->valueAddedServices = [];
        }else{
            $this->valueAddedServices[] = new ValueAddedServices($hasDazi);
        }
        if((floatval($order->dazi_value)>0) && isset($order->dazi_value))
            array_push($this->valueAddedServices, new ValueAddedServices('DD'));
        if(strcasecmp($param,'reso')===0)
            array_push($this->valueAddedServices, new ValueAddedServices('PT')); //Spedizione di reso senza ritiro
        $this->customerReferences[] = new CustomerReferences($data['params']['shipping_number_reference'],"CDN");
        // $this->identifiers[] = new Identifiers($data['params']['shipping_reference']);
        $this->identifiers = [];
        // $this->customerReferences = [];
        $this->pickup = new Pickup();
    }

    public static function isEU($countrycode) {
        $eu_countrycodes = array(
            'AT', 'BE', 'BG', 'CY', 'CZ', 'DE', 'DK', 'EE', 'EL',
            'ES', 'FI', 'FR', 'GR', 'HR', 'HU', 'IE', 'IT', 'LT', 'LU', 'LV',
            'MC', 'MT', 'NL', 'PL', 'PT', 'RO', 'SE', 'SI', 'SK'
        );
        return (in_array($countrycode, $eu_countrycodes));
    }

}

