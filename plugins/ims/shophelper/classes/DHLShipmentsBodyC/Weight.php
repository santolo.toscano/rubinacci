<?php

namespace Ims\Shophelper\Classes\Dhlshipmentsbodyc;

class Weight
{
    /**
     * @var float
     */
    public $netValue;

    /**
     * @var float
     */
    public $grossValue;

    public function __construct()
    {
        $this->netValue = 0.5;
        $this->grossValue = 0.5;
    }
}

