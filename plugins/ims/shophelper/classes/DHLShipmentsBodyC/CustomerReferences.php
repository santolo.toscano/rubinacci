<?php

namespace Ims\Shophelper\Classes\Dhlshipmentsbodyc;

class CustomerReferences{

    /**
     * @var string
     */
    public $value;

    /**
     * @var string
     */
    public $typeCode;

    public function __construct($value, $typeCode)
    {
        $this->value = $value;
        $this->typeCode = $typeCode;
    }

}

?>