<?php

namespace Ims\Shophelper\Classes\Dhlshipmentsbodyc;

class Identifiers{

    /**
     * @var string
     */
    public $value;

    /**
     * @var string
     */
    public $typeCode;

    public function __construct($value)
    {
        $this->value = $value;
        $this->typeCode = 'shipmentId';
    }

}

?>