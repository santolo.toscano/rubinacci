<?php

namespace Ims\Shophelper\Classes\Dhlshipmentsbodyc;

class ShipperDetails
{
    /**
     * @var PostalAddress
     */
    public $postalAddress;

    /**
     * @var ContactInformation
     */
    public $contactInformation;

    /**
     * @var array
     */
    public $registrationNumbers;

    /**
     * @var string
     */
    public $typeCode;

    public function __construct($search,$order=null,$user=null)
    {
        if($search=='create_shipping' || $search == 'create_shipping_send'){
            $this->postalAddress = new PostalAddress(null,'rubinacci');
            $this->contactInformation = new ContactInformation(null,null,null,'rubinacci');
            $this->registrationNumbers[] = new RegistrationNumbers(null,'rubinacci');
            $this->typeCode = "business";
        }else{
            $this->postalAddress = new PostalAddress($order,'user');
            $presso = !empty($order->property['shipping_presso']) ? "C/O : ".$order->property['shipping_presso'] : $user->name.' '.$user->last_name;
            $this->contactInformation = new ContactInformation($presso,$order,$user,'user');
            $this->registrationNumbers[] = new RegistrationNumbers($order,'user');
            $this->typeCode = "business";
        }
    }
    
}

