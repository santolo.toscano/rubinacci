<?php

namespace Ims\Shophelper\Classes\Dhlshipmentsbodyc;

class PostalAddress
{
    /**
     * @var string
     */
    public $postalCode;

    /**
     * @var string
     */
    public $cityName;

    /**
     * @var string
     */
    public $countryCode;

    /**
     * @var string
     */
    public $provinceCode;

    /**
     * @var string
     */
    public $addressLine1;
    /**
     * @var string
     */
    public $addressLine2;
    /**
     * @var string
     */
    public $addressLine3;

    /**
     * @var string
     */
    public $countyName;


    public function __construct($receiver=null,$param)
    {
        if($param=='rubinacci'){
            $this->postalCode = "80121";
            $this->cityName = "Napoli";
            $this->countryCode = "IT";
            $this->provinceCode = "NA";
            $this->addressLine1 = "Via Chiaia, 149";
            $this->addressLine2 = "RUBINACCI SRL";
            $this->addressLine3 = ".";
            $this->countyName = "Campania";
        }else{
            if(strlen($receiver->property['shipping_address1']) >= 45){
                if(strlen($receiver->property['shipping_address1']) >= 90){
                    $indirizzi = str_split($receiver->property['shipping_address1'],45);
                    $this->addressLine1 = $indirizzi[0];
                    $this->addressLine2 = $indirizzi[1];
                    $this->addressLine3 = $indirizzi[2];
                }else{
                    $indirizzi = str_split($receiver->property['shipping_address1'],45);
                    $this->addressLine1 = $indirizzi[0];
                    $this->addressLine2 = $indirizzi[1];
                    $this->addressLine3 = ".";
                }
            }else{
                $this->addressLine1 = $receiver->property['shipping_address1'];
                $this->addressLine2 = ".";
                $this->addressLine3 = ".";
            }
            $this->postalCode = $receiver->property['property']['shipping_postcode'];
            $this->cityName = $receiver->property['property']['shipping_city'];
            $this->countryCode = $receiver->property['property']['shipping_state'];
            $this->provinceCode = "  ";
            $this->countyName = "  ";
        }
    }
}

