<?php

namespace Ims\Shophelper\Classes\Dhlshipmentsbodyc;

class ContactInformation
{
    /**
     * @var string
     */
    public $companyName;

    /**
     * @var string
     */
    public $phone;

    /**
     * @var string
     */
    public $fullName;

    /**
     * @var string
     */
    public $email;

    public function __construct($presso=null,$order=null,$receiver=null,$param)
    {
        if($param=='rubinacci'){
            $this->phone = "+39081403908";
            $this->companyName = "RUBINACCI SRL";
            $this->fullName = "RUBINACCI SRL";
            $this->email = "A.COSENZA@MARIANORUBINACCI.COM";
        }else{
            $this->phone = isset($receiver->phone) ? $receiver->phone : '+39081403908';
            $this->companyName = $presso;
            $nome = $order->property['property']['shipping_name'] !== "" ? $order->property['property']['shipping_name'] : $receiver->name;
            $cognome = $order->property['property']['shipping_last_name'] !== "" ? $order->property['property']['shipping_last_name'] : $receiver->last_name;
            $this->fullName = $nome.' '.$cognome;
            $this->email = $receiver->email;
        }
    } 
   
}
