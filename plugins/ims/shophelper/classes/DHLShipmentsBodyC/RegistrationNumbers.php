<?php

namespace Ims\Shophelper\Classes\Dhlshipmentsbodyc;

class RegistrationNumbers
{
    /**
     * @var string
     */
    public $typeCode;

    /**
     * @var string
     */
    public $number;

    /**
     * @var string
     */
    public $issuerCountryCode;

    public function __construct($order=null,$param)
    {
        if($param=='rubinacci'){
            $this->typeCode = 'VAT';
            $this->number = 'IT00436210637';
            $this->issuerCountryCode = 'IT';
        }else{
            $this->typeCode = 'VAT';
            $this->number = 'IT00436210637';
            $this->issuerCountryCode = 'IT';
        }
    }
}

