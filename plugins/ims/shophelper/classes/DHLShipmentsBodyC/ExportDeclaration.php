<?php

namespace Ims\Shophelper\Classes\Dhlshipmentsbodyc;

use Stripe\LineItem;
use Lovata\Shopaholic\Models\Currency;
use Lovata\OrdersShopaholic\Models\OrderPosition;

class ExportDeclaration
{
    /**
     * @var array
     */
    public $lineItems;

    /**
     * @var Invoice
     */
    public $invoice;

    /**
     * @var array
     */
    public $additionalCharges;

    /**
     * @var string
     */
    public $payerVATNumber;

    /**
     * @var string
     */
    public $exportReason;

    /**
     * @var string
     */
    public $exportReasonType;

    /**
     * @var string
     */
    public $shipmentType;

    public function __construct($user, $data, $hasDazi, $order)
    {
        $j = 1;
        if($data['search'] == 'create_shipping_return'){
            $total_singles_price = 0;
            $curr_rate = floatval(Currency::where('id',$order->currency_id)->first()->rate);
            $selected_positions = json_decode(stripslashes($data['params']['selected_return_products']));
            $positions = $selected_positions->selected_return_products;
            $newprice = 0;
            foreach($positions as $pos){
                $pos = explode('_',$pos);
                $order_pos = OrderPosition::where('id',(int)$pos[1])->first();
                if(isset($order_pos)){
                    $couponDiscount = count($order->order_promo_mechanism) > 0 ? $order->order_promo_mechanism->first()->discount_value : 0;
                    if($couponDiscount > 0){
                        $couponPrice = round($order_pos->price_value * ((100-$couponDiscount)/100),0);
                        $newprice = round(round(((($couponPrice)*$order_pos->quantity)/1.22),0)/$curr_rate,0);
                    }else{
                        $newprice = round(round((($order_pos->price_value*$order_pos->quantity)/1.22),2)/$curr_rate,2);
                    }
                    $total_singles_price += $newprice;
                    $this->lineItems[] = new LineItems($hasDazi, $order_pos->item->product, $order_pos->quantity,$newprice,$j);
                    $j++;
                }
            }
        }else{
            foreach ($order->order_position as $position) {
                //Gestione prezzi Dollaro (es: eur 1 ; dol 1.2)
                $curr_rate = floatval(Currency::where('id',$order->currency_id)->first()->rate);
                //Scorporo IVA da single prices
                if(!(static::isEU($order->property['property']['shipping_state']))){
                    $couponDiscount = count($order->order_promo_mechanism) > 0 ? $order->order_promo_mechanism->first()->discount_value : 0;
                    if($couponDiscount > 0){
                        $couponPrice = round($position->item->price_value * ((100-$couponDiscount)/100),0);
                        $newprice = round(round(((($couponPrice)*$position->quantity)/1.22),0)/$curr_rate,0);
                    }else{
                        $newprice = round(round((($position->item->price_value*$position->quantity)/1.22),2)/$curr_rate,2);
                    }
                }else{
                    $newprice = 0;
                }
                $this->lineItems[] = new LineItems($hasDazi, $position->item->product, $position->quantity,$newprice,$j);
                $j++;
            }
        }
        
        $this->invoice = new Invoice($data);
        if($data['search'] == 'create_shipping_return'){
            $this->additionalCharges = array();
        }else{
            if((int)$order->shipping_price > 0){
                if((int)$order->dazi_value == 0){
                    $this->additionalCharges[] = new AdditionalCharges((float)$order->shipping_price,'Shipping Charges','delivery');
                }else{
                    $this->additionalCharges = array();
                    array_push($this->additionalCharges,new AdditionalCharges((float)$order->shipping_price,'Shipping Charges','delivery'));
                    if($data['search'] == 'create_shipping_return'){
                        $dazi_prices = round(($total_singles_price * 3) / 10);
                        array_push($this->additionalCharges,new AdditionalCharges($dazi_prices,'Tax and Duties Charges','export'));
                    }else{
                        array_push($this->additionalCharges,new AdditionalCharges((float)$order->dazi_value,'Tax and Duties Charges','export'));
                    }
                }
            }else{
                $this->additionalCharges = array();
            }
        }
        $this->payerVATNumber = !empty($data['params']['shipping_number_reference']) ? $data['params']['shipping_number_reference'] : "12345ED";
        $this->exportReason = "sale";
        $this->exportReasonType = 'commercial_purpose_or_sale';
        $this->shipmentType = 'commercial';
    }
    public static function isEU($countrycode) {
        $eu_countrycodes = array(
            'AT', 'BE', 'BG', 'CY', 'CZ', 'DE', 'DK', 'EE', 'EL',
            'ES', 'FI', 'FR', 'GR', 'HR', 'HU', 'IE', 'IT', 'LT', 'LU', 'LV',
            'MC', 'MT', 'NL', 'PL', 'PT', 'RO', 'SE', 'SI', 'SK'
        );
        return (in_array($countrycode, $eu_countrycodes));
    }
}

