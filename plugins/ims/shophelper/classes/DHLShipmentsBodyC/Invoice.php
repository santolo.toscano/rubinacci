<?php

namespace Ims\Shophelper\Classes\Dhlshipmentsbodyc;

class Invoice
{
    /**
     * @var string
     */
    public $number;

    /**
     * @var string
     */
    public $date;

    public function __construct($data)
    {
        $this->number = $data['params']['shipping_reference'];
        $this->date = $data['params']['shipping_date_billing'];
    }
}

