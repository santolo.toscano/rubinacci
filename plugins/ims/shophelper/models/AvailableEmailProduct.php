<?php namespace Ims\Shophelper\Models;

use October\Rain\Database\Model;
use Lovata\Shopaholic\Models\Offer;
use Lovata\Shopaholic\Models\Product;

/**
 * Model
 *
 * @property                                        $id
 * @property string                                 $email_user
 * @property string                                 $product_id
 * @property string                                 $offer_id
 *
 * @property \Lovata\Shopaholic\Models\Product      $product
 * @property \Lovata\Shopaholic\Models\Offer        $offer
 */
class AvailableEmailProduct extends Model
{
    public $timestamps = false;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'ims_shophelper_available_email_product';

    public $belongsTo = [
        'product' => [Product::class],
        'offer'   => [Offer::class],
    ];

    public function getQuantitaAttribute()
    {
        return AvailableEmailProduct::where('offer_id',$this->offer_id)->count();
    }

    public function getProdottoAttribute(){
        return Product::where('id',$this->product_id)->pluck('name')[0];
    }
    public function getTagliaAttribute(){
        return Offer::where('id',$this->offer_id)->pluck('name')[0];
    }

}
