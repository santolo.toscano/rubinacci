<?php namespace Ims\Shophelper\Models;

use Model;

/**
 * Model
 */
class NavisionVarianti extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    protected $connection = 'sqlsrv';
    public $table = 'vista_app_varianti';
    public $timestamps = false;

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

}
