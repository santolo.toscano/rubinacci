<?php namespace Ims\Shophelper\Models;

use Model;

/**
 * Model
 *
 * @property int                                 $id
 * @property string                              $email
 * @property string                              $created_at
 * @property string                              $updated_at
 *
 */
class NewsletterCustomer extends Model
{
    use \October\Rain\Database\Traits\Validation;
    

    /**
     * @var string The database table used by the model.
     */
    public $table = 'ims_shophelper_newsletter';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
}
