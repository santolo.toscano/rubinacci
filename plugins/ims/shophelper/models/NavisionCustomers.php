<?php namespace Ims\Shophelper\Models;

use Model;

/**
 * Model
 */
class NavisionCustomers extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'ims_shophelper_navision_customers';
    public $timestamps = false;

    /**
     * @var array Validation rules
     */
    public $rules = [

    ];

}
