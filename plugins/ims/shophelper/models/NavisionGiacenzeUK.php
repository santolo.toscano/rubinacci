<?php namespace Ims\Shophelper\Models;

use Model;

/**
 * Model
 */
class NavisionGiacenzeUK extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /**
     * @var string The database table used by the model.
     */
    public $table = 'web_navision_giacenze_uk';
    public $timestamps = false;

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
}
