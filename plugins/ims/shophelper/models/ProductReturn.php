<?php namespace Ims\Shophelper\Models;

use Lovata\OrdersShopaholic\Models\Order;
use Lovata\Shopaholic\Models\Offer;
use Lovata\Shopaholic\Models\Product;
use Model;

/**
 * Model
 */
class ProductReturn extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'lovata_orders_shopaholic_order_positions';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $belongsTo = [
        'order' => 'Lovata\OrdersShopaholic\Models\Order',
        'item' => 'Lovata\Shopaholic\Models\Offer',
    ];
}
