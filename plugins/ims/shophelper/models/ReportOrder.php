<?php namespace Ims\Shophelper\Models;

use Ims\Shophelper\Classes\ImsShippingManager;
use Lovata\OrdersShopaholic\Classes\Item\OrderItem;
use Lovata\OrdersShopaholic\Models\Order;
use Model;

/**
 * Model
 */
class ReportOrder extends Order
{
    use \October\Rain\Database\Traits\Validation;

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public static function boot()
    {
        parent::boot();

        static::addGlobalScope(function ($query) {
            $query->whereIn('status_id', [3,5]);
        });
    }

    public function getModelData($filter = null)
    {
        $model = $filter ?: $this;

        // cicla gli ordini e crea una tabella per tutti i prodotti presenti
        $data = $model->get()->map(function ($order) {

            /** @var OrderItem $orderItem */
            $orderItem = OrderItem::make($order->id);

            $state = ImsShippingManager::getShippingData($order, 'shipping_state', 'billing_state');
            if ( $state == '' ) {
                $stateGroup = "4 - NC";
            }
            elseif ( $state == 'IT' ) {
                $stateGroup = "1 - ITALIA";
            }
            else
            {
                $stateGroup = ImsShippingManager::isEU($state) ? '2 - INTRA' : '3 - EXTRA';
            }

            $user = $order->user;

            return
            [
                'id' => $orderItem->id,
                'order_status' => $order->status->name,
                'first_name' => $user->name,
                'last_name' => $user->last_name,
                'total_price' => $orderItem->total_price,
                'state_code' => $state,
                'state_name' => $state,
                'state_group' => $stateGroup,
                'created_at' => $order->created_at->setTimezone('Europe/Rome')->ToDateTimeString(),
                'order_date' => $order->created_at->setTimezone('Europe/Rome')->ToDateString(),
            ];
        });

        return $data;
    }

    protected $reportCustomAttributes = [
        ['name' => 'id', 'type' => 'number'],
        ['name' => 'order_status', 'type' => 'text'],
        ['name' => 'first_name', 'type' => 'text'],
        ['name' => 'last_name', 'type' => 'text'],
        ['name' => 'state_code', 'type' => 'text'],
        ['name' => 'state_name', 'type' => 'text'],
        ['name' => 'state_group', 'type' => 'text'],
        ['name' => 'total_price', 'type' => 'number'],
        ['name' => 'created_at', 'type' => 'date', 'dateFormat' => 'YYYY-M-D h:mm:s'],
        ['name' => 'order_date', 'type' => 'date', 'dateFormat' => 'YYYY-M-D'],
    ];


    public function getModelSchema()
    {
        $helper = new \Ims\ReportManager\Helpers\Helpers();

        $schema = [];
        //$helper->getTableSchema($this->table, $this->reportExcludedAttributes);

        for ( $i = 0; $i < count($this->reportCustomAttributes); $i++ )
        {
            array_push($schema, $this->reportCustomAttributes[$i]);
        }

        return array_values($schema); // riordina array prima di ritornarlo
    }
}
