<?php namespace Ims\Shophelper\Models;

use Model;

/**
 * Model
 */
class ProductAtZeroView extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'ims_shophelper_view_product_at_zero';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
}
