<?php namespace Ims\Shophelper\Models;

use Model;

/**
 * Model
 */
class SimpleCatalogHelper extends Model
{
    use \October\Rain\Database\Traits\Validation;
    

    /**
     * @var string The database table used by the model.
     */
    public $table = 'ims_shophelper_catalog_simple';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $belongsTo =[
        "product" => "Lovata\Shopaholic\Models\Product"
    ];
}
