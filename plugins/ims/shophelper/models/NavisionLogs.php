<?php namespace Ims\Shophelper\Models;

use Model;

/**
 * Model
 */
class NavisionLogs extends Model
{
    use \October\Rain\Database\Traits\Validation;
    

    /**
     * @var string The database table used by the model.
     */
    public $table = 'ims_shophelper_navision_logs';

    protected $fillable = [
        'quantity_old',
        'quantity_new',
        'product_code',
        'product_variant_nav',
        'product_variant_name',
        'property_name',
        'product_id',
        'offer_id',
        'operation',
        'log',
        'error'
    ];
    protected $searchable = [
        'log',
        'product_code'
    ];

    protected $dates = ['in', 'out', 'created_at'];

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
}
