<?php namespace Ims\Shophelper\Models;

use Model;

/**
 * Model
 */
class CustomProductsCombinations extends Model
{
    use \October\Rain\Database\Traits\Validation;
    

    /**
     * @var string The database table used by the model.
     */
    public $table = 'ims_shophelper_custom_products_combinations';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $belongsTo = [
        'product' => 'Lovata\Shopaholic\Models\Product',
        'fabric' => 'Lovata\Shopaholic\Models\Product',
    ];

    public $attachMany = [
        'gallery' => 'System\Models\File'
    ];

}
