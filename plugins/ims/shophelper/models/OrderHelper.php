<?php namespace Ims\Shophelper\Models;

use Carbon\Carbon;
use Kharanenka\Scope\UserBelongsTo;
use Lovata\Buddies\Models\User;
use Lovata\OrdersShopaholic\Models\Order;
use Lovata\OrdersShopaholic\Models\OrderPosition;
use Lovata\OrdersShopaholic\Models\OrderPromoMechanism;
use Lovata\OrdersShopaholic\Models\PaymentMethod;
use Lovata\OrdersShopaholic\Models\ShippingType;
use Lovata\OrdersShopaholic\Models\Status;
use Lovata\Shopaholic\Models\Currency;
use Lovata\Toolbox\Classes\Helper\UserHelper;
use Model;

/**
 * Model
 */
class OrderHelper extends Order
{
    use UserBelongsTo;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'lovata_orders_shopaholic_orders';


    public $hasMany = [
        'order_position'        => [
            OrderPosition::class,
            'key' => 'order_id'
        ],

        'order_offer'           => [
            OrderPosition::class,
            'condition' => 'item_type = \Lovata\Shopaholic\Models\Offer',
            'key' => 'order_id'
        ],
        'order_promo_mechanism' => [
            OrderPromoMechanism::class,
            'key' => 'order_id'
        ],
    ];

    public $belongsTo = [
        'user' => [ User::class, 'key' => 'user_id' ],
        'status'         => [Status::class, 'order' => 'sort_order asc'],
        'payment_method' => [PaymentMethod::class, 'order' => 'sort_order asc'],
        'shipping_type'  => [ShippingType::class, 'order' => 'sort_order asc'],
        'currency'       => [Currency::class, 'order' => 'sort_order asc'],
    ];

    public function getStateAtteribute()
    {
        $user = $this->user['property'];



        //$record->user['property'] && array_key_exists('billing_state', $record->user['property']) ? $record->user['property']['billing_state']
    }

    public function getShippingPackageTypeOptions()
    {
        return
        [
            '' => '[Non selezionato]',
            'A' => 'Piccolo',
            'B' => 'Medio',
            'C' => 'Grande',
            'D' => '2 Colli (Grande + Medio)',
            'E' => '2 Colli (Grande + Grande)',
            'F' => 'Personalizzato',
        ];
    }

    public function getShippingDutyPaymentTypeOptions()
    {
        return
            [
                '' => '[Non selezionato]',
                'S' => 'Spedizioniere',
                'R' => 'Ricevente',
            ];
    }

    public function getShippingAccountOptions()
    {
        return
            [
                '' => '[Non selezionato]',
                '106173837' => '106173837',
                '105257899' => '105257899',
            ];
    }

    /**
     * @return array
     */
    public static function getOrderCountAttribute()
    {
        $shippedCount = 0;
        $todayCount = 0;
        $todayAmount = 0;
        $yesterdayCount = 0;
        $yesterdayAmount = 0;
        $sevenCount = 0;
        $sevenAmount = 0;
        $monthCount = 0;
        $monthAmount = 0;
        $totalCount = 0;
        $totalAmount = 0;

        $orders = Order::whereIn('status_id', [2,3,5])->get();

        $today = Carbon::createFromFormat('Y-m-d H:i:s', Carbon::now()->format('Y-m-d'). '00:00:00', 'Europe/Rome');
        $yesterday = Carbon::createFromFormat('Y-m-d H:i:s', Carbon::now()->format('Y-m-d'). '00:00:00', 'Europe/Rome')->addDay(-1);

        foreach ( $orders as $order )
        {
            $orderDate = Carbon::createFromFormat('Y-m-d H:i:s', $order->created_at->setTimezone('Europe/Rome')->format('Y-m-d'). '00:00:00', 'Europe/Rome');

            $diff = $today->diffInDays($orderDate);

            //query su altre tabelle, rallenta
            $totalPrice = $order->total_price;

            $totalCount++;
            $totalAmount += $totalPrice;

            if ( $orderDate == $today )
            {
                $todayCount++;
                $todayAmount += $totalPrice;
            }
            else if ( $diff == 1 )
            {
                $yesterdayCount++;
                $yesterdayAmount += $totalPrice;
            }

            if ( $diff <= 7 )
            {
                $sevenCount++;
                $sevenAmount += $totalPrice;
            }

            if ( $orderDate->format('Y-m') == $today->format('Y-m') )
            {
                $monthCount++;
                $monthAmount += $totalPrice;
            }

            if ( $order->status_id == 5 )
            {
                $shippedCount++;
            }
        }

        $returnArray = [
            "total" => $orders->count(),
            "shipped" => $shippedCount,
            "toship" => $orders->count() - $shippedCount,
            "todayCount" => $todayCount,
            "todayAmount" => $todayAmount,
            "yesterdayCount" => $yesterdayCount,
            "yesterdayAmount" => $yesterdayAmount,
            "sevenCount" => $sevenCount,
            "sevenAmount" => $sevenAmount,
            "monthCount" => $monthCount,
            "monthAmount" => $monthAmount,
            "totalCount" => $totalCount,
            "totalAmount" => $totalAmount,
        ];

        return $returnArray;
    }



}
