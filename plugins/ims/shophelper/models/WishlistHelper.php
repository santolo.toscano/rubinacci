<?php namespace Ims\Shophelper\Models;

use Model;

/**
 * Model
 */
class WishlistHelper extends Model
{
    use \October\Rain\Database\Traits\Validation;
    

    /**
     * @var string The database table used by the model.
     */
    public $table = 'ims_shophelper_wishlist';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
}
