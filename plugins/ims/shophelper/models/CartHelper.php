<?php namespace Ims\Shophelper\Models;

use Model;

/**
 * Model
 */
class CartHelper extends Model
{
    use \October\Rain\Database\Traits\Validation;
    

    /**
     * @var string The database table used by the model.
     */
    public $table = 'ims_shophelper_cart';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
}
