<?php namespace Ims\Shophelper\Models;

use October\Rain\Database\Model;
use Lovata\Shopaholic\Models\Offer;
use Lovata\Shopaholic\Models\Product;

/**
 * Model
 *
 * @property int                                 $product_id
 * @property int                                 $offer_id
 * @property int                                 $quantity
 *
 */
class AvailableEmailProductView extends Model
{
    public $timestamps = false;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'ims_shophelper_view_available_email_product';

    public function getProdottoAttribute(){
        return Product::where('id',$this->product_id)->pluck('name')[0];
    }
    public function getTagliaAttribute(){
        return Offer::where('id',$this->offer_id)->pluck('name')[0];
    }

}
