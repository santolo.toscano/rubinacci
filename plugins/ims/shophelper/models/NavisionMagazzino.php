<?php namespace Ims\Shophelper\Models;

use Model;

/**
 * Model
 */
class NavisionMagazzino extends Model
{
    use \October\Rain\Database\Traits\Validation;

    protected $connection = 'sqlsrv';

    /**
     * @var string The database table used by the model.
     */
    public $table = 'vista_trasferimenti_su_import';


    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;


    /**
     * @var array Validation rules
     */
    public $rules = [

    ];
}
