<?php namespace Ims\Shophelper\Models;

use Lovata\OrdersShopaholic\Models\OrderPosition;
use Model;

class ReturnPosition extends OrderPosition
{
    public function getReturnedStatusOptions()
    {
        return [
            'new' => 'Nuovo Reso',
            'inprogress' => 'In lavorazione',
            'refunded' => 'Rimborsato',
            'replaced' => 'Sostituito',
            'danied' => 'Negato',
            'aborted' => 'Cancellato',
        ];
    }

    public function getReturnedReasonOptions()
    {
        return [
            '' => 'Non Specificato',
            'faulty' => 'Difettoso',
            'size' => 'Taglia errata',
            'color' => 'Colore errato',
            'wrong' => 'Errato acquisto',
            'other' => 'Altro',
        ];
    }
}
