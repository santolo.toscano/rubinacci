<?php namespace Ims\Shophelper\Models;

use Model;

/**
 * Model
 */
class Carrier extends Model
{
    use \October\Rain\Database\Traits\Validation;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'ims_shophelper_carriers';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    static function getByCode($code)
    {
        return static::where('carrier_name', $code)->first();
    }
}
