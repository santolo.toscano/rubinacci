<?php namespace Ims\Shophelper\Models;

use Lovata\Shopaholic\Models\Category;
use Lovata\Shopaholic\Models\Product;
use Model;

/**
 * Model
 */
class SizeTable extends Model
{
    use \October\Rain\Database\Traits\Validation;
    

    /**
     * @var string The database table used by the model.
     */
    public $table = 'ims_shophelper_sizes_table';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $implement = ['RainLab.Translate.Behaviors.TranslatableModel'];


    public $translatable = ['size_table'];

    public $belongsTo = [
        'category' => 'Lovata\Shopaholic\Models\Category',
    ];

    public $belongsToMany = [
        'additional_category' => [
            Category::class,
            'table' => 'ims_shophelper_size_table_categories',
            'key' => 'size_table_id'
        ],
        'additional_product' => [
            Product::class,
            'table' => 'ims_shophelper_size_table_products',
            'key' => 'size_table_id'
        ],
    ];
}
