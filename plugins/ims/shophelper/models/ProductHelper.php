<?php namespace Ims\Shophelper\Models;

use Lovata\Shopaholic\Models\Product;
use Model;

/**
 * Model
 */
class ProductHelper extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'lovata_shopaholic_products';

    public $belongsTo = [
        //'product'          => [Product::class]
    ];

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
}
