<?php namespace Ims\Shophelper\Models;

use Lovata\OrdersShopaholic\Models\OrderPosition;
use Model;

/**
 * Model
 */
class OrderPositionView extends OrderPosition
{
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;
}
