<?php namespace Ims\Shophelper\Models;

use Lovata\Shopaholic\Models\Category;
use Model;

/**
 * Model
 */
class FeedCategory extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'ims_shophelper_feed_categories';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $belongsToMany = [
        'additional_category' => [
            Category::class,
            'table' => 'ims_shophelper_feed_categories_link',
            'key' => 'feed_category_id'
        ],
    ];
}
