<?php namespace Ims\Shophelper\Models;

use Model;

/**
 * Model
 */
class NavisionArticoli extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    protected $connection = 'sqlsrv';
    public $table = 'Vista_Varianti_Articoli';
    public $timestamps = false;

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

}
