<?php namespace Ims\Shophelper\Models;

use Model;
use Lovata\Shopaholic\Models\Offer;
use Lovata\Shopaholic\Models\Product;

/**
 * Model
 *
 * @property int                                 $product_id
 * @property int                                 $offer_id
 * @property int                                 $quantity
 *
 */
class ProductStock extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'ims_shophelper_view_product_stock';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
}
