<?php namespace Ims\Shophelper\Models;

use Model;

/**
 * Class ImportOrdini
 * @author  Marco somma
 *
 * @property int                                                                           $id
 * @property double                                                                        $Imponibile
 * @property int                                                                           $Quantita
 * $coupon_list
 */
class ImportOrdini extends Model
{
    use \October\Rain\Database\Traits\Validation;



    /**
     * @var string The database table used by the model.
     */
    protected $connection = 'sqlsrv';
    public $table = 'Rubinacci$Fatture ECommerce';
    public $timestamps = false;

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

}
