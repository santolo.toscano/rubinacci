<?php namespace Ims\Shophelper\Controllers;

use Backend\Classes\Controller;
use Backend\Facades\BackendMenu;

class OrderPositionsViewController extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController'    ];
    
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Ims.Shophelper', 'main-menu-orders', 'side-menu-order2');
    }

    /**
     * @param \October\Rain\Database\Builder $query
     * @return \October\Rain\Database\Builder
     */
    public function listExtendQuery($query)
    {
        return $query->whereHas('order', function ($query) {
            $orderStatuses = [2, 3, 5];
            $query->whereIn('status_id', $orderStatuses);
        });
    }
}
