<?php namespace Ims\Shophelper\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class FeedCategoryController extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController',        'Backend\Behaviors\FormController'    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public $requiredPermissions = [
        'manage' 
    ];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Ims.Shophelper', 'main-menu-item', 'menu-feeds');
    }
}
