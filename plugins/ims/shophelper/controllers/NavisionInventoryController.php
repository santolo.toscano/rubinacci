<?php namespace Ims\Shophelper\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class NavisionInventoryController extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController'    ];
    
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Ims.Shophelper', 'navision-menu', 'navision-inventory');
    }
}
