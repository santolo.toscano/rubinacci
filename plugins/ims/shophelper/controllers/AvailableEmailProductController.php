<?php namespace Ims\Shophelper\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

class AvailableEmailProductController extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController'];

    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Ims.Shophelper', 'main-menu-item', 'side-menu-item');
    }

}
