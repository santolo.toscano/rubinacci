<?php

namespace Ims\Shophelper\Controllers\Api;

use PDO;
use Carbon\Carbon;
use Guzzle\Http\Client;
use Illuminate\Support\Str;
use Illuminate\Http\Response;
use October\Rain\Argon\Argon;
use Backend\Classes\Controller;
use Lovata\Buddies\Models\User;
use Vdomah\Excel\Classes\Excel;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Ims\Shophelper\Helpers\Helpers;
use Lovata\Shopaholic\Models\Offer;
use Lovata\Shopaholic\Models\Product;
use Illuminate\Support\Facades\Schema;
use Ims\Shophelper\Models\AppGiacenze;
use Lovata\Buddies\Components\Buddies;
use Lovata\Buddies\Facades\AuthHelper;
use Lovata\Shopaholic\Models\Category;
use Illuminate\Support\Facades\Storage;
use Ims\Shophelper\Classes\Excel\NewsletterUsersExport;
use Ims\Shophelper\Models\ImportOrdini;
use Ims\Shophelper\Models\NavisionLogs;
use Ims\Shophelper\Models\ImportClienti;
use Ims\Shophelper\Helpers\VintageHelper;
use Lovata\Buddies\Classes\Item\UserItem;
use RainLab\Translate\Classes\Translator;
use Ims\Shophelper\Models\ExchangeNavision;
use Ims\Shophelper\Models\NavisionGiacenze;
use Ims\Shophelper\Models\NavisionVarianti;
use Lovata\Buddies\Components\Registration;
use Ims\Shophelper\Models\NavisionCustomers;
use Ims\Shophelper\Classes\Excel\StockExport;
use Ims\Shophelper\Classes\Excel\UsersExport;
use Lovata\Shopaholic\Classes\Item\OfferItem;
use Ims\Shophelper\Classes\ImsShippingManager;
use Ims\Shophelper\Classes\Excel\ProductExport;
use Lovata\Shopaholic\Classes\Item\ProductItem;
use Ims\Shophelper\Classes\Excel\ProductsExport;
use Ims\Shophelper\Helpers\PaymentGatewayHelper;
use Lovata\PropertiesShopaholic\Models\Property;
use Lovata\OrdersShopaholic\Models\OrderPosition;
use Lovata\OrdersShopaholic\Classes\Item\OrderItem;
use Lovata\PropertiesShopaholic\Models\PropertyValue;
use Lovata\Shopaholic\Classes\Store\CategoryListStore;
use Lovata\PropertiesShopaholic\Models\PropertyValueLink;
use Lovata\PropertiesShopaholic\Classes\Item\PropertyItem;
use Lovata\OrdersShopaholic\Classes\Item\OrderPositionItem;
use Lovata\Shopaholic\Classes\Collection\ProductCollection;
use Lovata\Shopaholic\Classes\Collection\CategoryCollection;
use Lovata\PropertiesShopaholic\Classes\Item\PropertyValueItem;
use Lovata\PropertiesShopaholic\Classes\Collection\PropertyValueCollection;

class ExcelApiController  extends Controller
{

    protected $helpers;

    /**
     * ReportApiController constructor.
     * @param $helpers
     */
    public function __construct(Helpers $helpers){
        $this->helpers = $helpers;
    }

    public function users(){
        $excel = Excel::excel();
        $excel->store(new UsersExport(), 'users.xlsx', 'local');
        return "esportazione completata";
    }

    public function products(){
        $excel = Excel::excel();
        $now = Carbon::now()->timezone('Europe/Rome');
        $now = $now->format('d-m-y_g.i.s');
        $fileName = 'media/excel/'.$now.'.xlsx';
        $result = $excel->store(new ProductsExport(), $fileName, 'local');
        if ($result) {
            return $fileName;
        } else{
            return null;
        }
    }

    public function stock(){
        $excel = Excel::excel();
        $excel->store(new StockExport(), '/media/documenti/giacenzeminime.xlsx', 'local');
        return "esportazione completata";
    }

    public function newsletterexport(){
        $excel = Excel::excel();
        $excel->store(new NewsletterUsersExport(), '/media/documenti/newsletteruserexport.xlsx', 'local');
        return "esportazione completata";
    }
}

