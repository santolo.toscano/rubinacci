<?php

namespace Ims\Shophelper\Controllers;

use stdClass;
use Carbon\Carbon;
use Illuminate\Http\Response;
use October\Rain\Argon\Argon;
use Backend\Classes\Controller;
use Lovata\Buddies\Models\User;
use Vdomah\Excel\Classes\Excel;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Ims\Shophelper\Helpers\Helpers;
use Lovata\Shopaholic\Models\Offer;
use Lovata\Shopaholic\Models\Product;
use Ims\Shophelper\Models\AppGiacenze;
use Lovata\Buddies\Facades\AuthHelper;
use Lovata\Shopaholic\Models\Category;
use Ims\Shophelper\Helpers\EmailHelper;
use Ims\Shophelper\Models\ImportOrdini;
use Ims\Shophelper\Models\NavisionLogs;
use RainLab\Translate\Models\Attribute;
use Ims\Shophelper\Components\VatHelper;
use Ims\Shophelper\Models\ImportClienti;
use Ims\Shophelper\Helpers\VintageHelper;
use Lovata\Buddies\Classes\Item\UserItem;
use Lovata\OrdersShopaholic\Models\Order;
use RainLab\Translate\Classes\Translator;
use Ims\Shophelper\Models\ExchangeNavision;
use Ims\Shophelper\Models\NavisionGiacenze;
use Ims\Shophelper\Models\NavisionVarianti;
use Lovata\Buddies\Components\Registration;
use Ims\Shophelper\Models\NavisionCustomers;
use Ims\Shophelper\Classes\ImsShippingManager;
use Lovata\Shopaholic\Classes\Item\ProductItem;
use Ims\Shophelper\Models\AvailableEmailProduct;
use Lovata\OrdersShopaholic\Models\OrderPosition;
use Lovata\OrdersShopaholic\Classes\Item\OrderItem;
use Lovata\PropertiesShopaholic\Models\PropertyValue;
use Ims\Shophelper\Controllers\Api\ExcelApiController;
use Ims\Shophelper\Classes\Excel\NewsletterUsersExport;
use Ims\Shophelper\Models\NavisionGiacenzeUK;
use Lovata\PropertiesShopaholic\Models\PropertyValueLink;
use Lovata\Shopaholic\Classes\Collection\ProductCollection;
use Lovata\PropertiesShopaholic\Classes\Item\PropertyValueItem;

class ApiController  extends Controller
{

    const ORDER_STATUSES_POSITIVE = [1,2,3,5];
    protected $helpers;

    /**
     * ReportApiController constructor.
     * @param $helpers
     */
    public function __construct(Helpers $helpers)
    {
        $this->helpers = $helpers;
    }


    public function appQueryGiacenza()
    {
        $data = post();

        if ( array_key_exists('barcode', $data) && $data['barcode'])
        {
            $barcode = $data['barcode'];

            // Legge il prodotto dal barcode per ottenere il codice prodotto
            $product = NavisionVarianti::where('barcode', '=', $barcode)->first();
            if ( $product )
            {
                $productCode = $product->product_code;
                $results = AppGiacenze::where('product_code', 'like', $productCode)->get();
            }
            else
            {
                $results = [];
            }
        }
        else if ( array_key_exists('product_code', $data) && $data['product_code'] )
        {
            $productCode = str_replace('*', '%', $data['product_code']);;
            $results = AppGiacenze::where('product_code', 'like', $productCode)->get();
        }
        else if ( array_key_exists('product_description', $data) && $data['product_description'] )
        {
            $productCode = str_replace('*', '%', $data['product_description']);;
            $results = AppGiacenze::where('product_description', 'like', $productCode)->get();
        }
        else
        {
            $results = [];
        }

        return response()->json($results ?: []);
    }

    /**
     * @param string $lang
     * @return Response
     */
    public function feeds($lang)
    {
        $count = 0;
        $properyColor = 3;
        $translator = Translator::instance();
        $translator->setLocale($lang);

        $collection = new Collection();
        $productColletion = ProductCollection::make([]); // Il make utilizza la cache
        $productColletion->active()->filterByQuantity();

        $collection->put('title', '');

        /** @var ProductItem $product */
        foreach ($productColletion as $product) {
            $count++;

            $categoryArray = array_merge([$product->category_id], $product->additional_category_id);
            // Giacche
            if (in_array(10, $categoryArray) || in_array(74, $categoryArray) || in_array(83, $categoryArray)) {
                $categoryNumber = 1594;
            } // pantaloni
            else if (in_array(13, $categoryArray) || in_array(75, $categoryArray) || in_array(86, $categoryArray)) {
                $categoryNumber = 204;
            } // camicie
            else if (in_array(11, $categoryArray) || in_array(90, $categoryArray) || in_array(91, $categoryArray) || in_array(93, $categoryArray)) {
                $categoryNumber = 212;
            } // polo
            else if (in_array(5, $categoryArray) || in_array(40, $categoryArray)) {
                $categoryNumber = 212;
            } // maglie
            else if (in_array(32, $categoryArray) || in_array(35, $categoryArray) || in_array(37, $categoryArray) || in_array(39, $categoryArray)) {
                $categoryNumber = 212;
            } // cappotto
            else if (in_array(12, $categoryArray) && in_array(85, $categoryArray)) {
                $categoryNumber = 5598;
            } // Scarpe
            else if (in_array(8, $categoryArray) || in_array(77, $categoryArray)) {
                $categoryNumber = 187;
            } // Cravatte
            else if (in_array(19, $categoryArray) || in_array(87, $categoryArray)) {
                $categoryNumber = 176;
            } // Foulard e sciarpe
            else if (in_array(6, $categoryArray) || in_array(18, $categoryArray)) {
                $categoryNumber = 177;
            } else if (in_array(2, $categoryArray))// Accessori generici
            {
                $categoryNumber = 167;
            } else // Abbigliamento e categoria generica
            {
                $categoryNumber = 166;
            }

            $name = $product->name . " Rubinacci";

            /** @var Offer $offer */
            $skip = false;
            if ( !VintageHelper::containsVintageCat($product) )
            {
                // Cerca offer differente
                $offer = $product->offer->first();
                $skip = true;
            }
            else
            {
                $offer = $product->offer->first();
                $skip = false;
            }


            $backImage = $product->getObject()->back_preview;

            if ($product->property->has($properyColor) && array_key_exists($properyColor, $product->property_value_array)) {
                $propertyColorValuesId = $product->property_value_array[$properyColor];
                $propertyValueItem = PropertyValueItem::make($propertyColorValuesId[0]);
                $color = strtolower($propertyValueItem->value);
            } else {
                $color = "";
            }

            $path = $product->preview_image ? $product->preview_image->getPath() : '';
            $category = $product->category->name;
            $price = $offer->price_value . ' EUR';

            if (!$product->getObject()->is_manual_inventory) {
                $id = $offer->code;
            } else {
                // Crea un EAN con ID del prodotto
                $id = $product->id;
                $length = 12;
                $id = substr(str_repeat(0, $length) . $id, -$length);
            }

            /** @var Offer $offer */
            $localeLink = $translator->getLocale() == 'it' ? 'https://marianorubinacci.com/it/prodotto/' . $product->slug : 'https://marianorubinacci.com/en/product/' . $product->slug;

            $array = [
                'g:id' => $id,
                'g:title' => $name,
                'g:image_link' => $path,
                'description' => $product->description,
                'link' => $localeLink,
                'g:item_group_id' => '',
                'g:condition' => 'new',
                'g:availability' => 'in stock',
                'g:price' => $price,
                'g:mpn' => $product->code,
                'g:gtin' => $offer->code,
                'g:brand' => 'Rubinacci',
                'g:google_product_category' => $categoryNumber
            ];

            if ($color) {
                $array = array_merge($array, ['g:color' => $color]);
            }

            if ($backImage) {
                $array = array_merge($array, ['g:additional_image_link' => $backImage->path]);
            }

            if ($offer->old_price_value && $offer->old_price_value != '0.00') {
                $salePrice = $offer->old_price_value . ' EUR';
                $array = array_merge($array, ['g:sale_price' => $salePrice]);
            }

            $collection->put('item' . $product->id, $array);
        }

        $data = [
            'channel' => [
                $collection->toArray()
            ],
        ];

        $response = response()->xml($data, 200, [], $xmlRoot = 'rss', $encoding = null);

        // ... process widgets in $content
        $content = $response->getContent();


        $content = str_replace('<rss>', '<rss xmlns:g="http://base.google.com/ns/1.0" version="2.0" xmlns:c="http://base.google.com/cns/1.0">', $content);

        $content = preg_replace('/<item\d+/', '<item', $content);
        $content = preg_replace('/<\/item\d+/', '</item', $content);

        $response->setContent($content);

        return $response;

    }

    public function fillTradeOffersLang() {
        //ricerca offer senza lingua inglese
        $offers = DB::select('select lovata_shopaholic_offers.id, lovata_shopaholic_offers.name, rainlab_translate_attributes.id as attr_id
        from lovata_shopaholic_offers
        left join rainlab_translate_attributes on rainlab_translate_attributes.model_id = lovata_shopaholic_offers.id and rainlab_translate_attributes.model_type = \'Lovata\\\\Shopaholic\\\\Models\\\\Offer\'
        where rainlab_translate_attributes.id is null
        or rainlab_translate_attributes.attribute_data = \'{"name":"","preview_text":"","description":""}\'
        order by 1');

        $count_new = 0;
        $count_upd = 0;
        foreach ($offers as $offer) {
            //creo attribute_data con il nome dell'offer
            $attribute_data = json_encode(array(
                "name"=>$offer->name,
                "preview_text"=>"",
                "description"=>"",
            ));
            if(is_null($offer->attr_id)) {
                //caso id null, insert di una nuova riga
                $attribute = new Attribute;
                $attribute->locale = "en";
                $attribute->model_id = $offer->id;
                $attribute->model_type = Offer::class;
                $attribute->attribute_data = $attribute_data;
                $attribute->save();
                $count_new++;
            } else {
                //caso id presente, update del solo attribute_data
                $attribute = Attribute::find($offer->attr_id);
                $attribute->attribute_data = $attribute_data;
                $attribute->save();
                $count_upd++;
            }
        }
        return array(
            "count_upd" => $count_upd,
            "count_new" => $count_new
        );
    }

    public function fillTradeOffersProperties() {
        //ricerca offer senza lingua inglese
        $offers = DB::select('select lovata_shopaholic_offers.id, lovata_shopaholic_offers.product_id, lovata_shopaholic_offers.name
            from lovata_shopaholic_offers
            left join lovata_properties_shopaholic_value_link on lovata_properties_shopaholic_value_link.element_id = lovata_shopaholic_offers.id and lovata_properties_shopaholic_value_link.element_type = \'Lovata\\\\Shopaholic\\\\Models\\\\Offer\'
            where lovata_properties_shopaholic_value_link.id is null
            order by 1');

        $count_upd = 0;
        if(!empty($offers)) {
            //reperisco tutte le coppie di chiavi con il relativo value
            $tmp_variants = DB::select("select lovata_properties_shopaholic_variant_link.*, lovata_properties_shopaholic_values.value
                from lovata_properties_shopaholic_properties
                inner join lovata_properties_shopaholic_variant_link on lovata_properties_shopaholic_variant_link.property_id = lovata_properties_shopaholic_properties.id
                inner join lovata_properties_shopaholic_values on lovata_properties_shopaholic_values.id = lovata_properties_shopaholic_variant_link.value_id
                where lovata_properties_shopaholic_properties.slug in ('taglia-int', 'taglia')");
            //creo una mappa chiave valore del tipo: {name:{value_id,property_id}}
            $variants = array();
            foreach ($tmp_variants as $variant) {
                $variants[$variant->value] = [
                    "property_id" => $variant->property_id,
                    "value_id" => $variant->value_id
                ];
            }

            foreach ($offers as $offer) {
                //se esiste un valore corrispondente, creo l'associazione
                if(array_key_exists($offer->name, $variants)) {
                    $variant = $variants[$offer->name];
                    //creo la nuova property
                    $property = new PropertyValueLink;
                    $property->value_id = $variant["value_id"];
                    $property->property_id = $variant["property_id"];
                    $property->element_id = $offer->id;
                    $property->element_type = Offer::class;
                    $property->product_id = $offer->product_id;
                    $property->save();
                    $count_upd++;
                }
            }
        }

        return array(
            "count_offers" => sizeof($offers),
            "count_upd" => $count_upd,
        );
    }

    public function checkTaglie() {
        $counts = array(
            "count_total" => 0,
            "count_available" => 0,
            "count_sent" => 0,
            "count_error" => 0
        );

        //ciclo su ogni elemento di AvailableEmailProduct
        /** @param AvailableEmailProduct[] $email_products */
        AvailableEmailProduct::chunk(200, function ($email_products) use (&$counts) {
            foreach ($email_products as $email_product) {
                //incremento il contatore di richieste totali
                $counts["count_total"]++;
                //controllo se il prodotto esiste ancora (se no, cancello)
                $product = $email_product->product;
                if(!$product) {
                    $email_product->delete();
                    continue;
                }
                //controllo se la taglia per quel prodotto esiste ancora (se no, cancello)
                $offer = $email_product->offer;
                if(!$offer) {
                    $email_product->delete();
                    continue;
                }
                //controllo se la taglia per quel prodotto è disponibile
                if($offer->quantity > 0) {
                    //incremento il contatore di taglie disponibili
                    $counts["count_available"]++;
                    //invio la mail all'indirizzo del record
                    $email_sent = EmailHelper::safeSendEmailProductAvailable($email_product);
                    //elimino il record
                    $email_product->delete();
                    //incremento il contatore di email inviate
                    if($email_sent) {
                        $counts["count_sent"]++;
                    } else {
                        $counts["count_error"]++;
                    }
                }
            }
        });

        return $counts;
    }

    // public function giacenzeUpdateCitel(){
    //     $giacenzeNav = NavisionGiacenze::where('barcode', '<>', '')->where('magazzino', '<>', 'LO')->orderBy('product_code', 'asc')->orderBy('variant_code')->get();
    //     foreach($giacenzeNav as $varianteNav){
    //         $offer = Offer::where('code','=',$varianteNav->barcode)->first();
    //         if(!empty($offer)){
    //             if ($offer->quantity !== $varianteNav->giacenza && $varianteNav->magazzino !== 'LO') {
    //                 $offer->quantity = $varianteNav->giacenza;
    //                 $offer->is_giacenze_uk = false;
    //                 $offer->save();
    //             }
    //         }
    //     }
    //     return "ok";
    // }

    public function syncGiacenzeUk($variantiNav, $productCode, $offers, $product, $debug, $updateBarcode)
    {
        //  @ilmissigno&rottenmind
        // Sync uk i campi impattati sono [quantità, is_giacenze_uk, quantity_uk]
        foreach ($variantiNav as $varianteNav) {
            $externalId = $varianteNav->product_code . '|' . $varianteNav->barcode;
            $variantName = $varianteNav->variant_name;
            $propertyName = $variantName;
            $propertyId = $varianteNav->variant_name == 'US' ? 2 : 5;  // PROPERTY_ID  TAGLIA INTERNAZIONALE:2 CORRISPONDE SU NAVISION / TAGLIA NUM:5

            // Taglia unica
            if ($varianteNav->variant_name == 'TU') {
                $updateBarcode = false;
                $variantName = 'Taglia unica';
                $propertyName = 'Taglia unica';
                $propertyId = 2;

                if ($productCode == '2770110') {
                    $offer = $offers->where('code', '=', $varianteNav->barcode)->first();

                }
            } else if ($varianteNav->variant_name == 'DR') {
                $updateBarcode = false;

                // regular
                if (ends_with($variantName, ['R'])) {
                    $propertyName = substr($variantName, 0, strlen($variantName) - 1);
                    $variantName = $propertyName;
                } elseif (ends_with($variantName, ['L'])) {
                    $propertyName = substr($variantName, 0, strlen($variantName) - 1);
                    $variantName = str_contains($productCode, '|') ? $variantName : $propertyName;
                }
            } else if (str_contains($productCode, '|')) {
                $updateBarcode = false;

                $vetCode = explode('|', $productCode);
                if ($vetCode[0] == $varianteNav->product_code) {
                    if (ends_with($variantName, ['R'])) {
                        $propertyName = substr($variantName, 0, strlen($variantName) - 1);
                        $variantName = $propertyName;
                    }

                } else if ($vetCode[1] == $varianteNav->product_code) {
                    if (!ends_with($variantName, ['L'])) {
                        $propertyName = $variantName;
                        $variantName = $propertyName . 'L';
                    }
                    // LONG
                }
            } else if ($varianteNav->variant_name == 'UO' && str_contains($productCode, '|') && starts_with($productCode, '3BL')) {
                $propertyName = $variantName;
                $variantName = $variantName . 'L';
            } else if ($varianteNav->variant_name == 'CT') {
                $variantName = $variantName . ' cm';
                $propertyName = $variantName;
            } else if ($variantName == '3XL') {
                $variantName = 'XXXL';
                $propertyName = $variantName;
            }

            // @ilmissigno&rottenmind
            //$variantiNav: sono tutte le offer del PRODOTTO SINGOLO
            //$offers: cerco la taglia corrispondente allo stesso barcode della variantNav nel Ecommerce
            //
            $offer = $offers->where('code', '=', $varianteNav->barcode)->first();

            // Prova la ricerca per nome variante se non taglia unica
            if (!$offer && $varianteNav->variant_name != 'TU') {
                $offer = $offers->where('name', '=', $variantName)->where('product_id', '=', $product->id)->first();
                if (!$offer) {
                    $offer = $offers->where('external_id', '=', $varianteNav->product_code.'|'.$varianteNav->barcode)->first();
                }
            }

            // @ilmissigno&rottenmind
            //Se esistente
            //
            if ($offer) {
                // @ilmissigno&rottenmind
                //Viene applicata solo alle offer con manual inventory a false
                //
                if ($offer->is_manual_inventory == 0) {
                    $offer->is_giacenze_uk = false;
                    $saveOffer = false;

                    // @ilmissigno&rottenmind
                    //Se la somma già presente su Ecommerce è 0 allora imposta già il flag $is_giacenze_uk a true
                    //
                    if ($offer->quantity_ita == 0) {
                        $offer->is_giacenze_uk = true;
                    }
                    $oldQuantity = $offer->quantity_uk;
                    $offer->quantity_uk = $varianteNav->giacenza;
                    $log = [
                        "quantity_old" => $oldQuantity,
                        "quantity_new" => isset($offer->quantity_uk) ? $offer->quantity_uk : 'n/d',
                        "operation" => "ok_summed_uk",
                        "error" => "",
                        "log" => json_encode([]),
                        'offer_id' => isset($offer->id) ? $offer->id : 0,
                        "product_code" => isset($product->code) ? $product->code.'_uk' : 'n/d',
                        "product_variant_nav" => isset($varianteNav->variant_name) ? $varianteNav->variant_name.'_uk' : 'n/d',
                        'product_variant_name' =>'uk',
                        'property_name' =>'uk',
                        'product_id' => isset($product->id) ? $product->id : 'n/d'
                    ];
                    NavisionLogs::create($log, $varianteNav);

                    $saveOffer = true;

                    if ($saveOffer) {
                        $offer->save();
                    }
                }
            } else {
                // @ilmissigno&rottenmind
                //Effettua un check se esiste almeno 1 offer nella collezione del prodotto nel Ecommerce altrimenti skippa tutto il processo e riporta "price equal to zero" nel Navison_LOG
                //
                $checkPrice = false;
                foreach ($offers as $offer) {
                    if ($offer->price > 0) {
                        $checkPrice = true;
                        break;
                    }
                    if (($offer->name == $variantName) || ($offer->name == $variantName.'L') || ($offer->name == $variantName.'R') || ($offer->code == $varianteNav->barcode) || ($offer->external_id == $varianteNav->product_code."|".$varianteNav->barcode)) {
                        print_r("esiste gia");
                        $checkPrice = false;
                        break;
                    }
                }

                if ($varianteNav->giacenza >= 0 && $varianteNav->variant_name != 'TU' && $checkPrice != false) {
                    DB::beginTransaction();
                    try {
                        // Crea offer per prodotto
                        $offer = new Offer();
                        $offer->active = 1;
                        $offer->product_id = $product->id;
                        $offer->name = $variantName;
                        $offer->is_manual_inventory = 0;
                        $offer->code = $varianteNav->barcode;
                        $offer->quantity = $varianteNav->giacenza;
                        $offer->quantity_in_unit = $varianteNav->giacenza;
                        $offer->external_id = $externalId;
                        $offer->is_giacenze_uk = true;
                        $offer->quantity_ita = 0;
                        $offer->quantity_uk = $varianteNav->giacenza;

                        // Importa il prezzo
                        $price = 0;
                        $oldPrice = 0;
                        foreach ($offers as $offerPrice) {
                            if ($offerPrice->price > 0) {
                                $price = $offerPrice->price;
                                $oldPrice = $offerPrice->old_price;
                                break;
                            }
                        }
                        $offer->price = $price;
                        $offer->old_price = $oldPrice;
                        
    
                        $propertyValue = PropertyValue::getByValue($propertyName)->first();
                        if (!$propertyValue) {
                            DB::rollback();
                            $log = ["operation" => "property not found", "error" => "", "log" => json_encode($varianteNav), "product_code" => $product->code, 'offer_id' =>'', "product_variant_nav" => $varianteNav->variant_name, 'product_variant_name' => $variantName, 'property_name' => $propertyName, 'product_id' => $product->id];
                            NavisionLogs::create($log);
                        } else {
                            $offer->save();
                            $propertyValueLink = new PropertyValueLink();
                            $propertyValueLink->element_type = Offer::class;
                            $propertyValueLink->element_id = $offer->id;
                            $propertyValueLink->property_id = $propertyId;
                            $propertyValueLink->product_id = $product->id;
                            $propertyValueLink->value_id = $propertyValue->id;
                            $propertyValueLink->save();
        
                            DB::commit();
        
                            $log = ["operation" => "ok_offer_created", 'product_id' => $product->id, 'offer_id' => $offer->id, "error" => "", "log" => json_encode($varianteNav), "product_code" => $product->code, "product_variant_nav" => $varianteNav->variant_name, 'product_variant_name' => $variantName, 'property_name' => $propertyName, "quantity_old" => null, "quantity_new" => $varianteNav->giacenza];
                            NavisionLogs::create($log);
                        }
                    } catch (\Exception $ex) {
                        DB::rollback();
                        $log = ["operation" => "exception", "error" => $ex->getMessage(), "product_code" => $product->code, 'offer_id' => $offer->id, "product_variant_nav" => $varianteNav->variant_name, 'product_variant_name' => $variantName, 'property_name' => $propertyName, 'product_id' => $product->id];
                        NavisionLogs::create($log);
                    }
    
                    if ($debug) {
                        dd(["OPERAZIONE" => "SALVATAGGIO OK", "NAV" => $varianteNav, "OFFER" => $offer->id, "PRODOTTO" => $product->code . ': ' . $product->name, "VARIANTE" => $variantName, 'PROPERTY' => $propertyName]);
                    }
    
                    // Verifica IDEsterno prodotto
                    if ($product->external_id == '') {
                        $product->external_id = $varianteNav->product_code;
                        $product->save();
                    }
                } else if (!$checkPrice) {
                    DB::rollback();
                    $log = [
                        "operation" => "price equal to zero",
                        "error" => "",
                        "log" => json_encode($varianteNav),
                        "product_code" => $product->code,
                        'offer_id' => '',
                        "product_variant_nav" => $varianteNav->variant_name,
                        'product_variant_name' => 'N/D',
                        'property_name' => 'N/D',
                        'product_id' => $product->id
                    ];
                    NavisionLogs::create($log);
                }

            }
        }
    }
    
    public function syncGiacenzeItaliane($variantiNav, $productCode, $offers, $product, $debug, $updateBarcode)
    {
        //  @ilmissigno&rottenmind
        // Sync italiana rismasta invariata
        foreach ($variantiNav as $varianteNav) {
            $externalId = $varianteNav->product_code . '|' . $varianteNav->barcode;
            $variantName = $varianteNav->variant_name;
            $propertyName = $variantName;
            $propertyId = $varianteNav->variant_name == 'US' ? 2 : 5;  // PROPERTY_ID  TAGLIA INTERNAZIONALE:2 CORRISPONDE SU NAVISION / TAGLIA NUM:5

            // Taglia unica
            if ($varianteNav->variant_name == 'TU') {
                $updateBarcode = false;
                $variantName = 'Taglia unica';
                $propertyName = 'Taglia unica';
                $propertyId = 2;

                if ($productCode == '2770110') {
                    $offer = $offers->where('code', '=', $varianteNav->barcode)->first();

                }
            } else if ($varianteNav->variant_name == 'DR') {
                $updateBarcode = false;

                // regular
                if (ends_with($variantName, ['R'])) {
                    $propertyName = substr($variantName, 0, strlen($variantName) - 1);
                    $variantName = $propertyName;
                } elseif (ends_with($variantName, ['L'])) {
                    $propertyName = substr($variantName, 0, strlen($variantName) - 1);
                    $variantName = str_contains($productCode, '|') ? $variantName : $propertyName;
                }
            } else if (str_contains($productCode, '|')) {
                $updateBarcode = false;

                $vetCode = explode('|', $productCode);
                if ($vetCode[0] == $varianteNav->product_code) {
                    if (ends_with($variantName, ['R'])) {
                        $propertyName = substr($variantName, 0, strlen($variantName) - 1);
                        $variantName = $propertyName;
                    }

                } else if ($vetCode[1] == $varianteNav->product_code) {
                    if (!ends_with($variantName, ['L'])) {
                        $propertyName = $variantName;
                        $variantName = $propertyName . 'L';
                    }
                    // LONG
                }
            } else if ($varianteNav->variant_name == 'UO' && str_contains($productCode, '|') && starts_with($productCode, '3BL')) {
                $propertyName = $variantName;
                $variantName = $variantName . 'L';
            } else if ($varianteNav->variant_name == 'CT') {
                $variantName = $variantName . ' cm';
                $propertyName = $variantName;
            } else if ($variantName == '3XL') {
                $variantName = 'XXXL';
                $propertyName = $variantName;
            }

            // Cerca l'offerta per barcode
            $offer = $offers->where('code', '=', $varianteNav->barcode)->first();
            
            // Prova la ricerca per nome variante se non taglia unica
            if (!$offer && $varianteNav->variant_name != 'TU') {
                $offer = $offers->where('name', '=', $variantName)->where('product_id', '=', $product->id)->first();
                if (!$offer) {
                    $offer = $offers->where('external_id', '=', $varianteNav->product_code.'|'.$varianteNav->barcode)->first();
                }
            }

            if ($offer) {

                if ($offer->is_manual_inventory == 0) {
                    $saveOffer = false;

                    if ($updateBarcode && $offer->code != $varianteNav->barcode) {
                        $log = [
                            "operation" => "barcode_confilct",
                            'offer_id' => $offer->id,
                            "quantity_old" => $offer->quantity,
                            "quantity_new" => $varianteNav->giacenza,
                            "error" => json_encode(['message' => 'Barcode conflict']),
                            "log" => json_encode(['barcode_old' => $offer->code,
                            'barcode_new' => $varianteNav->barcode]),
                            "product_code" => $product->code,
                            "product_variant_nav" => $varianteNav->variant_name,
                            'product_variant_name' => $variantName,
                            'property_name' => $propertyName,
                            'product_id' => $product->id
                        ];
                        NavisionLogs::create($log, $varianteNav);
                        // $offer->code = $varianteNav->barcode;
                        // $saveOffer = true;
                    }

                    $log = [
                        "quantity_old" => isset($offer->quantity) ? $offer->quantity : 0,
                        "quantity_new" => isset($varianteNav->giacenza) ? $varianteNav->giacenza : 0,
                        "operation" => "ok_quantity_change",
                        "error" => "",
                        "log" => json_encode([]),
                        'offer_id' => isset($offer->id) ? $offer->id : 'n/d',
                        "product_code" => isset($product->code) ? $product->code : 'n/d',
                        "product_variant_nav" => isset($varianteNav->variant_name) ? $varianteNav->variant_name : 'n/d',
                        'product_variant_name' => isset($variantName) ? $variantName : 'n/d',
                        'property_name' => isset($propertyName) ? $propertyName : 'n/d',
                        'product_id' => isset($product->id) ? $product->id : 'n/d'
                    ];
                    NavisionLogs::create($log, $varianteNav);
                    $offer->quantity = $varianteNav->giacenza;
                    $offer->quantity_ita = $varianteNav->giacenza;
                    $saveOffer = true;

                    // verifica idesterno variante
                    if ($offer->external_id != $externalId) {
                        $offer->external_id = $externalId;
                        $saveOffer = true;
                    }

                    if ($saveOffer) {
                        $offer->save();
                    }

                    // Verifica IDEsterno prodotto
                    if ($product->external_id == '') {
                        $product->external_id = $varianteNav->product_code;
                        $product->save();
                    }
                }
            } else {

                $checkPrice = false;
                foreach ($offers as $offer) {
                    if ($offer->price > 0) {
                        $checkPrice = true;
                        break;
                    }
                    if (($offer->name == $variantName) || ($offer->name == $variantName.'L') || ($offer->name == $variantName.'R') || ($offer->code == $varianteNav->barcode) || ($offer->external_id == $varianteNav->product_code."|".$varianteNav->barcode)) {
                        $checkPrice = false;
                        break;
                    }
                }

                if ($varianteNav->giacenza >= 0 && $varianteNav->variant_name != 'TU' && $checkPrice != false) {
                    DB::beginTransaction();
                    try {
                        // Crea offer per prodotto
                        $offer = new Offer();
                        $offer->active = 1;
                        $offer->product_id = $product->id;
                        $offer->name = $variantName;
                        $offer->is_manual_inventory = 0;
                        $offer->code = $varianteNav->barcode;
                        $offer->quantity = $varianteNav->giacenza;
                        $offer->quantity_in_unit = $varianteNav->giacenza;
                        $offer->external_id = $externalId;
                        $offer->is_giacenze_uk = false;
                        $offer->quantity_uk = 0;
                        $offer->quantity_ita = $varianteNav->giacenza;

                        // Importa il prezzo
                        $price = 0;
                        $oldPrice = 0;
                        foreach ($offers as $offerPrice) {
                            if ($offerPrice->price > 0) {
                                $price = $offerPrice->price;
                                $oldPrice = $offerPrice->old_price;
                                break;
                            }
                        }
                        $offer->price = $price;
                        $offer->old_price = $oldPrice;

                        $propertyValue = PropertyValue::getByValue($propertyName)->first();
                        if (!$propertyValue) {
                            DB::rollback();
                            $log = ["operation" => "property not found", "error" => "", "log" => json_encode($varianteNav), "product_code" => $product->code, 'offer_id' => '', "product_variant_nav" => $varianteNav->variant_name, 'product_variant_name' => $variantName, 'property_name' => $propertyName, 'product_id' => $product->id];
                            NavisionLogs::create($log);
                        } else {
                            $offer->save();
                            $propertyValueLink = new PropertyValueLink();
                            $propertyValueLink->element_type = Offer::class;
                            $propertyValueLink->element_id = $offer->id;
                            $propertyValueLink->property_id = $propertyId;
                            $propertyValueLink->product_id = $product->id;
                            $propertyValueLink->value_id = $propertyValue->id;
                            $propertyValueLink->save();
    
                            DB::commit();
    
                            $log = ["operation" => "ok_offer_created", 'product_id' => $product->id, 'offer_id' => $offer->id, "error" => "", "log" => json_encode($varianteNav), "product_code" => $product->code, "product_variant_nav" => $varianteNav->variant_name, 'product_variant_name' => $variantName, 'property_name' => $propertyName, "quantity_old" => null, "quantity_new" => $varianteNav->giacenza];
                            NavisionLogs::create($log);
                        }

                    } catch (\Exception $ex) {
                        DB::rollback();
                        $log = ["operation" => "exception", "error" => $ex->getMessage(), "product_code" => $product->code, 'offer_id' => $offer->id, "product_variant_nav" => $varianteNav->variant_name, 'product_variant_name' => $variantName, 'property_name' => $propertyName, 'product_id' => $product->id];
                        NavisionLogs::create($log);
                    }

                    // Verifica IDEsterno prodotto
                    if ($product->external_id == '') {
                        $product->external_id = $varianteNav->product_code;
                        $product->save();
                    }
                }
            }
        }
    }

    public function refactorQuantityInPending($offers, $giacenzeNav, $giacenzeNavUk)
    {
        //  @ilmissigno&rottenmind
        // Per ogni offer del prodotto SINGOLO tramite barcode va ad effettuare un controllo nella tabella degli ordini verificando se ci sono ordini in stato [pagato, spedito] non ancora importati in NAV
        // 
        foreach ($offers as $offer) {
            if ($offer->is_manual_inventory == 0 && !isset($offer->deleted_at) && $offer->active == 1) {
                $offer->quantity = $offer->quantity_ita + $offer->quantity_uk;
                $offer->save();
                //  @ilmissigno&rottenmind
                // Setto per ragionamento lineare:
                //  $boughtQuantity: Quantità finale che verrà calcolata
                //  $isChanged: Un semplice flag per capire se effettivamente esiste almeno 1 prodotto in "pending"
                //  $positions: Get all di prodotti con quel barcode 
                // 
                $boughtQuantity = 0;
                $isChanged = false;
                $positions = OrderPosition::where(['item_id' => $offer->id])->get();
                foreach ($positions as $position) {
                    //  @ilmissigno&rottenmind
                    // Get dell'ordine da order_id di position
                    // 
                    $order = Order::where(['id' => $position->order_id])->first();
                    if (isset($order)) {
                        //  @ilmissigno&rottenmind
                        // Controllo se è stato importato in NAV e se il suo stato è pagato o spedito
                        // empty($order->external_id)
                        if($order->external_id == null || empty($order->external_id)){
                            if(in_array($order->status_id, self::ORDER_STATUSES_POSITIVE)){
                                //  @ilmissigno&rottenmind
                                // Inizio il conteggio per la nuova quantità e setto il flag a true
                                // 
                                $boughtQuantity += $position->quantity;
                                $isChanged = true;
                            }
                        }
                    }
                }
                //  @ilmissigno&rottenmind
                // Se e solo se c'è almeno una position entrarà in questo check
                // 
                if ($isChanged) {
                    //  @ilmissigno&rottenmind
                    // Get delle taglie dalle tabelle di appoggio tramite barcode
                    // 
                    $giacenzeNav = NavisionGiacenze::where(['barcode' => $offer->code])->first();
                    $giacenzeNavUk = NavisionGiacenzeUk::where(['barcode' => $offer->code])->first();

                    //  @ilmissigno&rottenmind
                    // Setto per ragionamento lineare:
                    //  $itQuantity: quantità totale delle/a taglie/a dalla tabella di appoggio web_navision_giacenze
                    //  $ukQuantity: quantità totale delle/a taglie/a dalla tabella di appoggio web_navision_giacenze_uk
                    // 
                    $itQuantity = 0;
                    $ukQuantity = 0;

                    //  @ilmissigno&rottenmind
                    // Ciclo necessariamente dato che uso il metodo get, potrei usare il metodo first() ma dato che avevano più taglie con barcode uguali in nav...
                    // 
                    if (isset($giacenzeNav)) {
                        $itQuantity = $giacenzeNav->giacenza;
                    }
                    if (isset($giacenzeNavUk)) {
                        $ukQuantity = $giacenzeNavUk->giacenza;
                    }

                    //  @ilmissigno&rottenmind
                    // Se la quantità italiana è maggiore di 0 imposta is_giacenze_uk a true (per debug)
                    // 
                    if ($itQuantity > 0) {
                        $offer->is_giacenze_uk = false;
                    }

                    if ($boughtQuantity <= $offer->quantity) {
                        //  @ilmissigno&rottenmind
                        // Se invece la quantità italiana è minore uguale a 0 e quella uk è maggiore di 0 imposta il flag a true (per debug)
                        // 
                        if ($itQuantity <= 0 && $ukQuantity > 0) {
                            $offer->is_giacenze_uk = true;
                        } else if ($itQuantity <= 0 && $ukQuantity <= 0) {
                            $offer->is_giacenze_uk = false;
                        }
                        
                        if ($offer->quantity_ita > 0) {
                            $offer->quantity_ita = $offer->quantity_ita - $boughtQuantity;
                            if ($offer->quantity_ita < 0) {
                                $offer->quantity_uk = $offer->quantity_uk - abs($offer->quantity_ita);
                                $offer->quantity_ita = 0;
                            }
                        } else if ($offer->quantity_uk > 0 && $offer->quantity_ita < 0) {
                            $offer->quantity_uk = $offer->quantity_uk - $boughtQuantity;
                        }
                        $offer->quantity = ($offer->quantity_uk + $offer->quantity_ita) - $boughtQuantity;
                        $offer->save();
                    } else {
                        $log = [
                            "operation" => "Quantity in pending",
                            "error" => "Not enough quantity in EC",
                            "log" => 'The sum of the quantities of the pending orders exceeds the sum of the quantity of the sizes from navision_ita and navision_uk if the problem persists contact a technician',
                            "product_code" => $offer->code,
                            "product_variant_nav" => '',
                            'product_variant_name' => '',
                            'property_name' => '',
                            'product_id' => $offer->product_id,
                            'offer_id' => $offer->id
                        ];
                        NavisionLogs::create($log);
                        $offer->quantity = 0;
                        $offer->save();
                    }
                }
            }
        }
    }

    public function findVariantiFromNavision($productCode, $offers, $giacenzeNav, $product, $giacenzeNavUk)
    {
        foreach ($offers as $off) {
            if (empty($off->code) || $off->code == null || isset($off->deleted_at) || in_array($product->category->id, [110, 120,121])) {
                unset($offers[$off->name]);
            }
        }
        // Divide lo / per le info aggiuntive sul codice prodotto
        if (strpos($productCode, '/')!==false) {
            $productCodeNotExploded = $productCode;
            $productCode = explode('/', $productCode)[0];
            $variantiNav = $giacenzeNav->where('product_code', '=', $productCode);
            $variantiNavUk = $giacenzeNavUk->where('product_code', '=', $productCode);

            if ($variantiNav->count() == 0) {
                if (strpos($productCode, '|')!==false) {
                    $variantiNav = $giacenzeNav->whereIn('product_code', [explode('|', $productCode)[0], explode('|', $productCode)[1]]);
                } else {
                    $variantiNav = $giacenzeNav->where('product_code', '=', $productCodeNotExploded);
                }
            }
            if ($variantiNavUk->count() == 0) {
                if (strpos($productCode, '|')!==false) {
                    $variantiNavUk = $giacenzeNavUk->whereIn('product_code', [explode('|', $productCode)[0], explode('|', $productCode)[1]]);
                } else {
                    $variantiNavUk = $giacenzeNavUk->where('product_code', '=', $productCodeNotExploded);
                }
            }
        } else {
            $variantiNav = $giacenzeNav->where('product_code', '=', $productCode);
            $variantiNavUk = $giacenzeNavUk->where('product_code', '=', $productCode);
            if (strpos($productCode, '|')===false) {
                // Prodotto standard ricerca per codice
                $variantiNav = $giacenzeNav->where('product_code', '=', $productCode);
                $variantiNavUk = $giacenzeNavUk->where('product_code', '=', $productCode);
                //Check su taglia unica e/o productCode differente su navisionUk e navisionIt
                if ($variantiNav->count() == 0) {
                    $productCode = preg_replace('/[^A-Za-z0-9\-]/', '', $productCode);
                    $variantiNav = $giacenzeNav->where('product_code', '=', $productCode);
                }            
                if ($variantiNavUk->count() == 0) {
                    $productCode = preg_replace('/[^A-Za-z0-9\-]/', '', $productCode);
                    $variantiNavUk = $giacenzeNavUk->where('product_code', '=', $productCode);
                }
            } 
            if (strpos($productCode, '|')===true) {
                $variantiNav = $giacenzeNav->whereIn('product_code', explode('|', $productCode));
                $variantiNavUk = $giacenzeNavUk->whereIn('product_code', explode('|', $productCode));
                //Check su taglia unica e/o productCode differente su navisionUk e navisionIt
                if ($variantiNav->count() == 0) {
                    $productCode = preg_replace('/[^A-Za-z0-9\-]/', '', explode('|', $productCode)[0]);
                    $variantiNav = $giacenzeNav->whereIn('product_code', explode('|', $productCode));
                }
                if ($variantiNavUk->count() == 0) {
                    $productCode = preg_replace('/[^A-Za-z0-9\-]/', '', explode('|', $productCode)[0]);
                    $variantiNavUk = $giacenzeNavUk->whereIn('product_code', explode('|', $productCode));
                }
            }
            
            if ($variantiNav->count() == 0) {
                foreach ($product->offer as $offer) {
                    $variantiNav = $giacenzeNav->where('barcode', '=', $offer->code);
                    if ($variantiNav->count() == 0) {
                        break;
                    }
                }
            }
            if ($variantiNavUk->count() == 0) {
                foreach ($product->offer as $offer) {
                    $variantiNavUk = $giacenzeNavUk->where('barcode', '=', $offer->code);
                    if ($variantiNavUk->count() == 0) {
                        break;
                    }
                }
            }
        }

        return [$variantiNav, $variantiNavUk, $offers];
    }

    public function giancenzeNavSync()
    {
        $giacenzeNav = NavisionGiacenze::where('barcode', '<>', '')->orderBy('product_code', 'asc')->orderBy('variant_code')->get();
        $giacenzeNavUk = NavisionGiacenzeUK::where('barcode', '<>', '')->orderBy('product_code', 'asc')->orderBy('variant_code')->get();
        $products = Product::active()->get()->keyBy("code"); 
        foreach ($products as $product) {
            if (!in_array($product->category->id, [110, 120, 121])) {
            $offers = $product->offer->keyBy('name');
            $debug = false;
            $updateBarcode = true;
            
            $productCode = $product->code;

            $returnedArr = $this->findVariantiFromNavision($productCode, $offers, $giacenzeNav, $product, $giacenzeNavUk);
            $variantiNav = $returnedArr[0];
            $variantiNavUk = $returnedArr[1];
            $offers = $returnedArr[2];
            
            //In caso di lista di offer di un prodotto non trovata inserisce un log
            if ($variantiNav->count() == 0) {
                $log = [
                    "operation" => "product not found",
                    "error" => "",
                    "log" => '',
                    "product_code" => $productCode,
                    "product_variant_nav" => '',
                    'product_variant_name' => '',
                    'property_name' => '',
                    'product_id' => $product->id,
                    'offer_id' => 0
                ];
                NavisionLogs::create($log);
            }
            if ($variantiNavUk->count() == 0) {
                $log = [
                    "operation" => "productUk not found",
                    "error" => "",
                    "log" => '',
                    "product_code" => $productCode,
                    "product_variant_nav" => '',
                    'product_variant_name' => '',
                    'property_name' => '',
                    'product_id' => $product->id,
                    'offer_id' => 0
                ];
                NavisionLogs::create($log);
            }
            
            // @ilmissigno&rottenmind SYNC ITALIANA (INVARIATA)
            // viene lanciata prima la sync italiana che comprende:
            // $isUk => la prima volta viene settata a false per gestire la logica nella funzione 
            // $offers => tutte le offer del singolo prodotto prese da Ecommerce 
            // $product => è il singolo prodotto 
            // [$debug, $updateBarcode] => variabili di supporto alla sync italiana
            //
            $this->syncGiacenzeItaliane($variantiNav, $productCode, $offers, $product, $debug, $updateBarcode);

            // @ilmissigno&rottenmind SYNC UK 
            // viene lanciata dopo la sync italiana, si limita solo a cercare la offer (se esistente in EC confrontando il barcode):
            // ESISTENTE: Si limita ad addizionare le quantità dalla tabella di appoggio_uk
            //     - se quantità italiana <= 0 allora imposta anche il flag is_giacenze_uk a true (giusto per debug)
            //     - se quantità italiana > 0 allora imposta anche il flag is_giacenze_uk a false (giusto per debug)
            // NON ESISTENTE: Aggiunge la nuova taglia se e solo se questo prodotto ha almeno gia 1 taglia nella sua collezione questo accade perche altrimenti applicherebbe il prezzo a 0€ per la nuova taglia da uk
            //
            $offers = Offer::where('product_id', '=', $product->id)->get();
            $this->syncGiacenzeUk($variantiNavUk, $productCode, $offers, $product, $debug, $updateBarcode);
            

            // @ilmissigno&rottenmind REFACTOR DELLE TAGLIE NON ANCORA IMPORTATE IN NAVISION
            // La seguente funzione prende tutti gli ordini in stato [pagato, spedito] ma con il bottone importa ancora visibile "quindi non importati in NAV" e va a sottrarre le quantità dalle quantità dopo il SYNC
            //
            $offers = Offer::where('product_id', '=', $product->id)->get();
            $this->refactorQuantityInPending($offers, $giacenzeNav, $giacenzeNavUk);
            }
        }
        return "ok";
    }

    public function importOrder($id)
    {
        $order = OrderItem::make($id);
        $orderModel = $order->getObject();
        $user = UserItem::make($order->user_id);
        $cliente = null;
        $emailWarning = '';
        $addressWarning = '';

        //reperimento stato di spedizione per calcolo iva
        $shipping_state = $order->property["shipping_state"];
        //vat italiano con relativo moltiplicatore
        $itVat = VatHelper::getVatFromState();
        $itVatMultiplier = VatHelper::getVatMultiplier($itVat);
        //vat in base allo stato con relativo moltiplicatore
        $vat = VatHelper::getVatFromState($shipping_state);
        $vatMultiplier = VatHelper::getVatMultiplier($vat);

        $couponCode = count($order->coupon_list) > 0 ? $order->coupon_list[0] : '';
        $couponDiscount = $couponCode ? $order->order_promo_mechanism->first()->discount_value : 0;

        $numero_ordine = "WEB" . $order->id;
        $fatturaImport = ImportOrdini::where('Nr_ Documento', '=', $numero_ordine)->first();
        if ($fatturaImport) {
            $orderModel->external_id = $numero_ordine;
            $orderModel->save();
            return ['status' => false, 'message' => "Fattura gia importata"];
        }

        $user = User::getByEmail($user->email)->first();

        if ($user->property) {
            $userProperty = $user->property;
        } else {
            $userProperty = $order->property['property'];
        }

        if ($user->external_id) {
            $codCliente = $user->external_id;
            $cliente = $codCliente;
        } else {
            try {
                $codCliente = "WEB" . $user->id;

                // Crea nuovo cliente
                $cliente = new ImportClienti();

                // Campi predefiniti
                $field = "Importato in NAV";
                $cliente->$field = 0;
                $field = "Home Page";
                $cliente->$field = "";
                $cliente->Fax = "";

                $field = "Codice Cliente";
                $cliente->$field = "WEB" . $user->id;
                $cliente->nome = $user->name . ' ' . $user->last_name;


                // Legge i dati dall'ordine
                $importAddress = $orderModel->property['billing_address1'];
                if (strlen($importAddress) > 50) {
                    $importAddress = substr($importAddress, 0, 49) . '^';
                    $addressWarning = $importAddress;
                }

                $importCity = $orderModel->property['billing_city'];
                $importCap = $orderModel->property['billing_postcode'];

                $importState = $orderModel->property['billing_state'];

                if ($importState == '' || $importState == '00') {
                    $importState = isset($userProperty['shipping_state']) ? $userProperty['shipping_state'] : '';
                }

                // italia
                if (strtoupper($importState) == 'IT') {
                    $importProv = $orderModel->property['billing_region'];

                    if (!$importProv || $importProv == '00') {
                        $importProv = isset($userProperty['shipping_region']) ? $userProperty['shipping_region'] : '';
                    }

                    if (!$importProv || $importProv == '00') {
                        $importProv = isset($userProperty['billing_region']) ? $userProperty['billing_region'] : '';
                    }

                    $importPhone = isset($orderModel->property['phone']) ? $orderModel->property['phone'] : '';
                    if (!$importPhone) {
                        $importPhone = $user->phone;
                    }
                } else {
                    $importProv = '';
                    $importPrefix = isset($orderModel->property['prefix']) ? $orderModel->property['prefix'] : '';

                    if (!$importPrefix) {
                        $importPrefix = isset($userProperty['prefix']) ? $userProperty['prefix'] : '';
                    }

                    $importPhone = isset($orderModel->property['phone']) ? $orderModel->property['phone'] : '';
                    if (!$importPhone) {
                        $importPhone = $user->phone;
                    }

                    // Aggiunge prefisso
                    if ($importPrefix && !starts_with($importPhone, '+')) {
                        $importPhone = '+' . $importPrefix . ' ' . $importPhone;
                    }
                }

                $importAzienda = isset($orderModel->property['company_name']) ? $orderModel->property['company_name'] : '';
                $importPartitaIva = isset($orderModel->property['p_iva']) ? $orderModel->property['p_iva'] : '';
                $importCF = isset($orderModel->property['cf']) ? $orderModel->property['cf'] : '';

                // Su stato italiano cerca il codice fiscale sull'untente
                if (!$importCF && !$importPartitaIva && strtoupper($importState) == 'IT') {
                    $importCF = isset($orderModel->property['cf']) ? $orderModel->property['cf'] : '';
                }


                $cliente->indirizzo = $importAddress;
                $cliente->cap = $importCap;
                $cliente->citta = $importCity;
                $cliente->provincia = $importProv;; // PROVINCIA
                $cliente->nazione = $importState;
                $cliente->azienda = $importAzienda;

                $field = "Partita IVA";
                $cliente->$field = $importPartitaIva;
                $field = "Codice Fiscale";
                $cliente->$field = $importCF;
                $cliente->Telefono = $importPhone;

                $email = $user->email;
                if (strlen($email) > 30) {
                    $email = substr($email, 0, 29) . '^';
                    $emailWarning = $user->email;
                }

                $cliente->Email = $email;

                $cliente->save();

                $user->external_id = $codCliente;
                $user->save();
            } catch (\Exception $ex) {
                return ['status' => false, 'message' => 'Errore creazione utente: ' . $ex->getMessage(), 'user' => $user, 'cliente' => $cliente];
            }
        }

        $fattura = ImportOrdini::where('Nr_ Documento', "$order->id")->first();
        if ($fattura) {
            return ["Fattura già caricata per l'importazione" => $fattura];
        }

        $dataFattura = Carbon::create($order->created_at->year, $order->created_at->month, $order->created_at->day, 0, 0, 0);

        $righeImport = new Collection();
        $riga = 0;

        foreach ($order->order_position as $position) {
            $barcode = $position->item->product->code == "CUSTOM" ? "" : $position->item->code;
            $riga++;

            $productCode = $position->item->product->code;

            // Gestione giacche
            if (str_contains($productCode, '|')) {
                $productCode = explode('|', $productCode)[0];
            }

            // Gestione articoli Marcella con giacenza manuale
            if (str_contains($productCode, '/')) {
                $productCode = explode('/', $productCode)[0];
            }

            if(strlen($productCode)>20){
                return ['status' => false, 'message' => 'Errore Codice Prodotto '.$productCode.' Deve essere minore o uguale a 20 caratteri', 'user' => $user, 'cliente' => $cliente];
            }

            // Calcola Prezzo unitario
            if ($couponCode && $position->old_price == 0) {
                $sconto = $couponDiscount;
                $unitPriceExVat = $position->old_total_price_per_unit_with_tax_value / $itVatMultiplier;
                $prezzoTotaleRiga = $position->old_total_price;
            } else {
                $sconto = 0;
                $unitPriceExVat = $position->price_with_tax / $itVatMultiplier;
                $prezzoTotaleRiga = $position->total_price_value;
            }

            //calcolo l'imponibile totale riga
            $imponibileTotaleRiga = $prezzoTotaleRiga / $itVatMultiplier;

            //se l'iva dell'ordine è diversa da quella italiana, ricalcolo il prezzo totale della riga
            if($itVatMultiplier !== $vatMultiplier) {
                $prezzoTotaleRiga = $imponibileTotaleRiga * $vatMultiplier;
            }

            $arrayRiga = [
                "Nr_ Documento" => "WEB" . $order->id,
                "Nr_ Riga" => $riga,
                "Cod_ Cliente" => $codCliente,
                "Data Fattura" => $dataFattura,
                "Imponibile" => $order->conv_prezzo_tot / $vatMultiplier, //diviso per il vat di destinazione, visto che viene salvato con il prezzo corretto
                "Importo IVA Inclusa" => floatval($order->conv_prezzo_tot),
                "Codice Articolo" => $productCode,
                "Barcode Articolo" => $barcode,
                "Quantita" => $position->quantity,
                "Percentuale Iva" => $vat,
                "Prezzo Unitario IVA Esclusa" => $unitPriceExVat,
                "Importo Riga IVA Inclusa" => $prezzoTotaleRiga,
                "Importo Riga Imponibile" => $imponibileTotaleRiga,
                "Metodo Pagamento" => strtolower($order->payment_method->code) == "paypal" ? "PAYPAL" : "CC",
                "Nr_ Fattura Provvisoria NAV" => 0,
                "Codice IVA" => $vat,
                "Percentuale Sconto" => $sconto,
                "Importato in NAV" => 0
            ];

            $rigaFattura = new ImportOrdini();
            foreach ($arrayRiga as $key => $value) {
                $rigaFattura->$key = $value;
            }

            $righeImport->put($riga, $rigaFattura);
        }

        // Aggiunge spedizione
        if ($order->shipping_price_value > 0) {
            $riga++;

            $arrayRiga = [
                "Nr_ Documento" => "WEB" . $order->id,
                "Nr_ Riga" => $riga,
                "Cod_ Cliente" => $codCliente,
                "Data Fattura" => $dataFattura,
                "Imponibile" => $order->conv_prezzo_tot / $vatMultiplier, //diviso per il vat di destinazione, visto che viene salvato con il prezzo corretto
                "Importo IVA Inclusa" => floatval($order->conv_prezzo_tot),
                "Codice Articolo" => "SHIPPING",
                "Barcode Articolo" => "",
                "Quantita" => 1,
                "Percentuale Iva" => $itVat,
                "Prezzo Unitario IVA Esclusa" => $order->shipping_price_value / $itVatMultiplier,
                "Importo Riga Imponibile" => $order->shipping_price_value / $itVatMultiplier,
                "Importo Riga IVA Inclusa" => $order->shipping_price_value,
                "Metodo Pagamento" => strtolower($order->payment_method->code) == "paypal" ? "PAYPAL" : "CC",
                "Nr_ Fattura Provvisoria NAV" => 0,
                "Codice IVA" => $itVat,
                "Percentuale Sconto" => 0,
                "Importato in NAV" => 0
            ];

            $rigaFattura = new ImportOrdini();
            foreach ($arrayRiga as $key => $value) {
                $rigaFattura->$key = $value;
            }

            $righeImport->put($riga, $rigaFattura);
        }

        foreach ($righeImport as $rigaimport) {
            $rigaimport->save();
        }

        $orderModel = $order->getObject();
        $orderModel->external_id = $numero_ordine;
        $orderModel->save();

        $returnArray = ['status' => true, 'message' => 'Ordine importato correttamnete', 'ordine' => $rigaFattura, 'cliente' => $cliente, 'user' => $user];
        if ($emailWarning) {
            $returnArray = array_merge(['email warning: ' => $emailWarning], $returnArray);
        }
        if ($addressWarning) {
            $returnArray = array_merge(['address warning: ' => $addressWarning], $returnArray);
        }


        return $returnArray;
    }

    /**
     * Carica le anagrafiche dei clienti
     */
    public function navisionCustomers()
    {
        $navUsers = NavisionCustomers::where('E-mail', '<>', '')->get();
        $messages = new Collection();
        foreach ($navUsers as $navUser) {

            $id = $navUser['No_'];
            $country = $navUser['Country_Region Code'];
            $email = $navUser['E-Mail'];

            $user = User::getByEmail($email)->first();

            if (!$user) {
                $arName = explode(' ', $navUser['Name']);
                $name = $arName[0];
                $lastName = '';
                for ($i = 1; $i < count($arName); $i++) {
                    if ($lastName) {
                        $lastName = $lastName . ' ';
                    }
                    $lastName = $lastName . $arName[$i];
                }
                $password = 'changeme';

                $arAuthUserData = [
                    'external_id' => $id,
                    'email' => $navUser['E-Mail'],
                    'name' => $name,
                    'last_name' => $lastName,
                    'middle_name' => '',
                    'phone' => $navUser['Phone No_'],
                    'password' => $password,
                    'password_confirmation' => $password
                ];

                $arUserData = ['property' => [
                    'cf' => $navUser['Fiscal Code'],
                    'company_name' => '',
                    'p_iva' => $navUser['VAT Registration No_'],
                    'sdi_code' => '',
                    'pec_code' => '',
                    'legal_person' => '',
                    'prefix' => $country == 'IT' ? '+39' : '',
                    'birthdate' => '',
                    'billing_name' => $name,
                    'billing_last_name' => $lastName,
                    'billing_region' => $navUser['County'],
                    'billing_state' => $navUser['Country_Region Code'],
                    'billing_city' => $navUser['City'],
                    'billing_address' => $navUser['Address'],
                    'billing_postcode' => $navUser['Post Code'],
                    'shipping_name' => $name,
                    'shipping_last_name' => $lastName,
                    'shipping_region' => $navUser['County'],
                    'shipping_state' => $navUser['Country_Region Code'],
                    'shipping_city' => $navUser['City'],
                    'shipping_address1' => $navUser['Address'],
                    'shipping_postcode' => $navUser['Post Code'],
                    'billing_address2' => '',
                    'billing_street' => '',
                    'billing_house' => '',
                    'billing_flat' => '',
                    'shipping_address2' => '',
                    'shipping_street' => '',
                    'shipping_house' => '',
                    'shipping_flat' => ''
                ]];

                $arUserData = array_merge($arAuthUserData, $arUserData);

                $registration = new Registration();
                try {
                    $user = AuthHelper::register($arUserData, false);
                    $user->external_id = $id;
                    $user->activate();
                    $user->save();
                } catch (\Exception $ex) {
                    $messages->put($id, $ex->getMessage());
                }
            }
        }

        return $messages;
    }


    /**
     * Aggionra il codice prodotto cravatte e braccialetti con il doppio codice diviso dal pipe
     */
    public function productCodeFix()
    {
        $data = ExchangeNavision::whereIn('codice1', ['01EY', '01JK', 'CORNETTIARG'])->get();

        foreach ($data as $record) {
            $product = ProductItem::make($record->product_id);
            // Evita che vengono fatti 2 volte
            if ($product && !str_contains($product->code, '|')) {
                /** @var Product $model */
                $model = $product->getObject();
                $model->code = $model->code . '|' . $record->codice;
                $model->save();
            }
        }
    }


    public function productsBatch()
    {
        $count = 0;
        $category = Category::getBySlug('outlet')->first();

        $productCollection = ProductCollection::make([])->active()->category($category->id, true);

        /** @var ProductItem $productItem */
        foreach ($productCollection as $productItem) {
            /** @var Product $product */
            if (!str_contains($productItem->code, '/')) {
                $product = $productItem->getObject();
                $product->code = $product->code . '/A';
                $product->save();
                $count++;
            }
        }

        return "Operazione completata su $count articoli";
    }

    /*
    public function navExchangeEan()
    {
        $processedCode = 0;
        $processedBarcode = 0;

        $data = ExchangeNavision::where('barcode', '<>', '')->get();

        foreach ($data as $record) {
            $product = Product::getByCode($record->codice)->first();

            if ( $product && $product->code != $record->codice1 ) {
                $processedCode++;
                $record->product_id = $product->id;
                $product->code = $record->codice1;
                $product->save();


                if ($product->offer->count() == 1) {
                    $offer = $product->offer->first();
                    $record->offer_id = $offer->id;
                    $offer->code = $record->barcode;
                    $offer->save();
                    $processedBarcode++;
                }

                $record->save();
            }
        }

        return [
            "Records letti" => count($data),
            "Codici gestiti" => $processedCode,
            "Barcode gestiti" => $processedBarcode,
        ];
    }
    */


    // Allinea con tabella interscambio dove sono presenti anche i barcode
    /*
    public function navExchangeCode()
    {
        $data = ExchangeNavision::where('barcode', '=', '')->get();

        $collection = new Collection();

        foreach ($data as $record) {
            $product = Product::getByCode($record->codice)->first();
            if ( $product && $product->code != $record->codice1 ) {
                $record->product_id = $product->id;
                $oldCode = $product->code;
                $product->code = $record->codice1;
                $product->save();
                $record->save();
                $collection->put($product->id, ['origine' => $oldCode, 'destinazione' => $record->codice1]);
            }
        }

        return ["risultato" => $collection];
    }
    */

    /*
    public function barcodeSync()
    {
        $products = Product::all();
        $collection = new Collection();

        $query = "SELECT [Item No_] as productcode, Code as code, Description, [Descrizione Taglia] as taglia, Barcode as barcode
                  FROM [Rubinacci2012].[dbo].[Vista_Varianti_Articoli]
                  ORDER BY [Item No_], barcode";

        $data = DB::connection('sqlsrv')->select($query);

        foreach ( $products as $product )
        {
            $code = $product->code;
            $offers = $product->offer;

            foreach ( $offers as $offer ) {

                $tagliaweb = $offer->name;

                $variantedb = array_filter($data, function ($record) use ($code, $offer) {
                    return ($record->productcode == $code && $record->taglia == $offer->name);
                });


                if ( count($variantedb) > 0 )
                {
                    $key = array_keys($variantedb)[0]; // ottiene la chiave per array associativo

                    if ( $offer->code != $variantedb[$key]->barcode )
                    {
                        $offer->code = $variantedb[$key]->barcode;
                        $offer->external_id = $variantedb[$key]->productcode.'_'.$variantedb[$key]->code;
                        $offer->save();

                        $collection->put($offer->id, $offer->code);
                    }
                }
                else
                {
                    // Non trovato
                }
            }

        }

        return ["barcode updated" => $collection];
    }

    public function quantity()
    {
        $products = Product::all();
        $collection = new Collection();

        $query = "SELECT [Item No_] as productcode, Code as code, Description, [Descrizione Taglia] as taglia, Barcode as barcode, Giacenza as giacenza, [Location Code] as magazzino
                  FROM [Rubinacci2012].[dbo].[Vista_Varianti_Articoli_Giacenze]
                  WHERE [location code] IN ('07','n1','nc','04')
                  --AND [Item No_] like 'SAHARIANA-A2460'
                  ORDER BY [Item No_], barcode";
        $data = DB::connection('sqlsrv')->select($query);


        foreach ( $products as $product )
        {
            $code = $product->code;
            $offers = $product->offer;

            foreach ( $offers as $offer ) {

                $tagliaweb = $offer->name;

                $variantedb = array_filter($data, function ($record) use ($code, $offer) {
                    return ($record->productcode == $code && $record->taglia == $offer->name);
                });


                if ( count($variantedb) > 0 )
                {
                    $key = array_keys($variantedb)[0]; // ottiene la chiave per array associativo
                    //$offer->code = $variantedb[$key]->barcode;
                    //$offer->external_id = $variantedb[$key]->productcode.'_'.$variantedb[$key]->taglia;
                    if ( $offer->quantity <> $variantedb[$key]->giacenza )
                    {
                        // Logga
                        $offer->quantity = $variantedb[$key]->giacenza;
                        $offer->save();
                    }

                    if ( !$product->external_id ) {
                        $product->external_id = $variantedb[$key]->productcode;
                        $product->save();
                    }

                }
                else
                {
                    // non trovata
                    $collection->put($product->code, "not found");
                }
            }

        }

        return $collection;
    }

    public function errors()
    {
        $errors = new Collection();
        $products = Product::all();

        $query = "SELECT [Item No_] as productcode, Code as code, Description, [Descrizione Taglia] as taglia, Barcode as barcode, Giacenza as giacenza, [Location Code] as magazzino
                  FROM [Rubinacci2012].[dbo].[Vista_Varianti_Articoli_Giacenze]
                  WHERE [location code] IN ('07','n1','nc','04')
                  --AND Giacenza > 0
                  --AND [Item No_] like 'SAHARIANA-A2460'
                  ORDER BY [Item No_], barcode";
        $data = DB::connection('sqlsrv')->select($query);


        foreach ( $products as $product )
        {
            $code = $product->code;

            $variantedb = array_filter($data, function ($record) use ($code) {
                return ($record->productcode == $code );
            });


            if ( count($variantedb) > 0 )
            {
                $key = array_keys($variantedb)[0]; // ottiene la chiave per array associativo

            }
            else
            {
               $errors->push(["code" => $code, "descrizione" => $product->name]);
            }


        }

        return response()->json($errors);
    }
    */


    public function giancenzeNavBarcode()
    {
        $giacenzeNav = NavisionGiacenze::orderBy('product_code', 'asc')->orderBy('variant_code')->get();
        $products = Product::active()->get()->keyBy("code");

        /** @var Product $product */
        foreach ($products as $product) {
            $offers = $product->offer->keyBy('name');

            $variantiNav = $giacenzeNav->where('product_code', '=', $product->code);
            foreach ($variantiNav as $varianteNav) {
                $offer = $offers->find($varianteNav->variant_code);
            }
        }

        return "ok";
    }

    public function ordineFix()
    {
        //UPDATE lovata_shopaholic_products set created_at = DATE_ADD(created_at,INTERVAL -3 MONTH) where LCASE(name) like 'cravatta%'

        $products = ProductCollection::make()->active();
        foreach ($products as $product) {
            if (starts_with(strtolower($product->name), "cravat")) {
                /** @var Argon $date */
                $date = $product->getObject()->created_at;
                $date = $date->addMonth(-1);
                $product->created_at = $date;
                $product->save();
            }
        }

        dd('operazione completata');
    }


    public function categoryFix()
    {
        $products = ProductCollection::make()->active();
        $sostituzioni = [
            'cravatta' => 57,
            'pochette' => 16,
            'foulard' => 6,
            'sahariana' => 90,
            'braccialetto' => 71,
            'sciarpa' => 60,

        ];

        $i = 0;

        try {
            /** @var ProductItem $item */
            foreach ($products as $item) {

                if ($item->category_id != 3) // Salta OUTLET
                {
                    foreach ($sostituzioni as $key => $value) {
                        if (starts_with(strtolower($item->name), $key)) {
                            if ($item->category_id != $value) {
                                /** @var Product $model */
                                $model = $item->getObject();
                                $model->category_id = $value;
                                $model->save();
                                $i++;
                            }
                        }
                    }
                }
            }
        } catch (\Exception $ex) {
            dd($ex, $item);
        }

        return "Operazione completata su $i articoli";


    }

    public function clean()
    {
        $query = "SELECT * FROM lovata_shopaholic_offers WHERE id IN
                  (
                  SELECT lovata_shopaholic_offers.id
                  FROM lovata_shopaholic_offers
                  LEFT JOIN lovata_shopaholic_products ON lovata_shopaholic_offers.product_id = lovata_shopaholic_products.id
                  where lovata_shopaholic_products.id is null
                  )";


        $data = DB::select($query);

        /*
        $data = DB::connection('sqlsrv')->select($query);
        dd($data);
        */

        return response()->json($data);
    }

    public function dhl($id)
    {
        $shippingManager = new ImsShippingManager();
        $order = OrderItem::make($id);

        $quoteResult = $shippingManager->getQuote($order);
        $labelResult = $shippingManager->createLabel($order);

        return [
            'quote' => $quoteResult,
            'label' => $labelResult
        ];

    }

    public function oldcart()
    {
        $abandonedCart =[];

        $query_get_cart_with_user = "SELECT user_id,id,updated_at FROM lovata_orders_shopaholic_carts  WHERE user_id IS NOT NULL";
        $cartWhitUserObj = DB::select($query_get_cart_with_user);

        for ($i=0; $i < count($cartWhitUserObj); $i++) {
            $query_get_product_in_cart = "SELECT item_id FROM lovata_orders_shopaholic_cart_positions WHERE cart_id = ? AND deleted_at IS NULL";
            $query_get_user = "SELECT id, email, name, last_name FROM lovata_buddies_users WHERE id = ?";
            $userObj = DB::select($query_get_user, [$cartWhitUserObj[$i]->user_id]);
            $productId = DB::select($query_get_product_in_cart, [$cartWhitUserObj[$i]->id]);
            foreach ($userObj as $usr) {
                $productInfo = [];
                foreach ($productId as $off) {
                    $offer = Offer::where('id', $off->item_id)->get();
                    foreach ($offer as $prod) {
                        $product = Product::where('id', $prod->product_id)->get();
                        foreach ($product as $key) {
                            if($key->preview_image) {
                                $img_path = $key->preview_image->path;
                            } else {
                                $img_path = "NO_IMAGE";
                            }
                            if(!empty($key->lang('en')->slug) && empty($key->lang('it')->slug)){
                                //inglese
                                $url = "https://marianorubinacci.com/en/product/".$key->lang('en')->slug;
                            }else if(!empty($key->lang('it')->slug) && empty($key->lang('en')->slug)){
                                //italiano
                                $url = "https://marianorubinacci.com/it/prodotto/".$key->lang('it')->slug;
                            }else if(!empty($key->lang('en')->slug) && !empty($key->lang('it')->slug)){
                                //entrambi prendo sempre italia
                                $url = "https://marianorubinacci.com/it/prodotto/".$key->lang('it')->slug;
                            }else{
                                //non succederà mai ma prendi sempre italiano
                                $url = "https://marianorubinacci.com/it/prodotto/".$key->lang('it')->slug;
                            }
                            array_push($productInfo,
                            [
                                'id' => $key->id,
                                'url' => $url,
                                'img_path' => $img_path,
                                'taglia' => [
                                    'offer_code' => $prod->code,
                                    'offer_external_id' => $prod->external_id,
                                    'offer_name' => $prod->name,
                                ],
                                'info' => [
                                    'name' => $key->name,
                                    'vendor_code' => $key->code,
                                ]
                            ]);
                        }
                    }
                }
                if (!empty($productInfo)) {
                    array_push($abandonedCart,
                        [
                            'last_update' => (string)($cartWhitUserObj[$i]->updated_at),
                            'id' => $usr->id,
                            'email' => $usr->email,
                            'name' => $usr->name,
                            'last_name' => $usr->last_name,
                            'product' => $productInfo,
                        ]
                    );
                }
            }
        }
        return response()->json($abandonedCart);
    }

    public function updateProductsMC(){
        $data = date('Y_m_d');
        $filename = "product_catalog_$data.csv";
        $columns = ['UniqueID','SkuID','ProductCode','ProductName','ProductName_en','ProductType','ProductLink','ImageLink','RegularPrice','Quantity','BrandName','Color'];
        $headers = array(
            "Content-type"        => "text/csv",
            "Content-Disposition" => "attachment; filename=$filename",
            "Pragma"              => "no-cache",
            "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
            "Expires"             => "0"
        );
        $callback = function() use($columns){
            $file = fopen('php://output','w');
            fputcsv($file,$columns,'|');
            $products = Product::all();
            foreach($products as $p){
                foreach($p->offer as $o){
                    $single = new stdClass;
                    if($o->active == 1){
                        if($p->preview_image) {
                            $img_path = $p->preview_image->path;
                        } else {
                            $img_path = "NO_IMAGE";
                        }
                        $single->UniqueID = $o->id;
                        $single->SkuID = $o->code;
                        $single->ProductCode = $p->code;
                        $single->ProductName = $p->lang('it')->name;
                        $single->ProductName_en = $p->lang('en')->name;
                        $single->ProductType = $p->category->name;
                        $single->ProductLink = "https://marianorubinacci.com/it/prodotto/".$p->lang('it')->slug;
                        $single->ImageLink = $img_path;
                        $single->RegularPrice = floatval($o->price);
                        $single->Quantity = intval($o->quantity);
                        $single->BrandName = $o->name;
                        $single->Color = isset($p->property[3]) ? implode(", ",$p->property[3]) : "";
                        fputcsv($file, (array)$single,'|');
                    }
                }
            }
            fclose($file);
        };
        return response()->stream($callback, 200, $headers);
    }

    public function importProductsSalesforce($id = null){
        $prod_export = array();
        if(!isset($id))
            $products = Product::all();
        else
            $products = Product::where('id',$id)->get();
        foreach($products as $value){
            if($value->preview_image) {
                $img_path = $value->preview_image->path;
            } else {
                $img_path = "NO_IMAGE";
            }

            //get active offers
            $offers = [];
            foreach ($value->offer as $off) {
                if ($off->active == 1) {
                    $offer = [
                        'id' => $off->id,
                        'name' => $off->name,
                        'name_en' => $off->lang('en')->name,
                        'code' => $off->code,
                        'external_id' => $off->external_id,
                        'price' => (float) $off->price,
                        'old_price' => (float) $off->old_price,
                        'quantity' => $off->quantity,
                    ];
                    array_push($offers,$offer);
                }
            }
            if(!empty($value->lang('en')->slug) && empty($value->lang('it')->slug)){
                //inglese
                $url = "https://marianorubinacci.com/en/product/".$value->lang('en')->slug;
            }else if(!empty($value->lang('it')->slug) && empty($value->lang('en')->slug)){
                //italiano
                $url = "https://marianorubinacci.com/it/prodotto/".$value->lang('it')->slug;
            }else if(!empty($value->lang('en')->slug) && !empty($value->lang('it')->slug)){
                //entrambi prendo sempre italia
                $url = "https://marianorubinacci.com/it/prodotto/".$value->lang('it')->slug;
            }else{
                //non succederà mai ma prendi sempre italiano
                $url = "https://marianorubinacci.com/it/prodotto/".$value->lang('it')->slug;
            }
            $prod = [
                'product_id' => $value->id,
                'vendor_code' => $value->code,
                'external_id' => $value->external_id,
                'description' => $value->name,
                'description_en' => $value->lang('en')->name,
                'img_path' => $img_path,
                'url' => $url,
                'offer' => $offers,
                'properties' => [
                    'colors' => isset($value->property[3]) ? $value->property[3] : [],
                    'material' => isset($value->property[7]) ? $value->property[7] : [],
                    'type' => isset($value->property[10]) ? $value->property[10] : [],
                    'season' => isset($value->property[11]) ? $value->property[11] : [],
                    ]
                ];
            array_push($prod_export,$prod);
        }
        return response()->json($prod_export);
    }

    public function importOrdersSalesforce($year, $month){
        if( $this->getBearerToken() == 'yaTfcN9HrEdCHGrCLbbkUwF7ek3HHP'){
            $prod_export = [];
            $orders = Order::all();
            foreach($orders as $value){
                $date = explode('-', $value->created_at);
                $order_year = $date[0];
                $order_month = $date[1];
                if ($order_year == $year && $order_month == $month) {
                    $products = [];
                    foreach($value->order_position as $c){
                        array_push($products,$this->getProduct($c->item_id));
                    };

                    empty($value->coupon[0]) ? $coupon = [] : $coupon = [
                            'id' => empty($value->coupon[0]) ? [] : $value->coupon[0]->id,
                            'group_id' => empty($value->coupon[0]) ? [] : $value->coupon[0]->group_id,
                            'code' => empty($value->coupon[0]) ? [] : $value->coupon[0]->code,
                        ];

                    $order = [
                        'created_at' => explode(' ', $value->created_at)[0],
                        'user' => [
                            'email' => $value->user->email,
                            'name' => $value->user->name,
                            'last_name' => $value->user->last_name,
                            'property' => $value->user->property,
                        ],
                        'coupon' => $coupon,
                        'products' => $products,
                    ];
                    array_push($prod_export, $order);
                }
            }
            return response()->json($prod_export);
        } else {
            $contents = '';
            if($year==-1 && $month==-1) {
                $contents = $_SERVER;
                if(function_exists('apache_request_headers')) {
                    array_push($contents, apache_request_headers());
                } else {
                    array_push($contents, "NO apache_request_headers function");
                }
            }
            $statusCode = 401;
            return response()->make($contents, $statusCode);
        }
    }

    /**
     * Get header Authorization
     * */
    private function getAuthorizationHeader(){
        $headers = null;
        if (isset($_SERVER['Auth-Rubinacci'])) {
            $headers = trim($_SERVER["Auth-Rubinacci"]);
        } else if (isset($_SERVER['HTTP_AUTH_RUBINACCI'])) { //Nginx or fast CGI
            $headers = trim($_SERVER["HTTP_AUTH_RUBINACCI"]);
        } elseif (function_exists('apache_request_headers')) {
            $requestHeaders = apache_request_headers();
            if (isset($requestHeaders['Auth-Rubinacci'])) {
                $headers = trim($requestHeaders['Auth-Rubinacci']);
            }
        }
        return $headers;
    }

    /**
    * get access token from header
    * */
    private function getBearerToken() {
        $headers = $this->getAuthorizationHeader();
        // HEADER: Get the access token from the header
        if (!empty($headers)) {
            if (preg_match('/Bearer\s(\S+)/', $headers, $matches)) {
                return $matches[1];
            }
        }
        return null;
    }


    private function getProduct($id){
        $off_export = array();
        $offer = Offer::where('id',$id)->get();
        foreach($offer as $value){
            if (isset($value->product)) {
                $prod = [
                    'product_id' => $value->product->id,
                    'vendor_code' => $value->product->code,
                    'external_id' => $value->product->external_id,
                    'description' => $value->product->name,
                    'url' => "https://marianorubinacci.com/it/prodotto/".$value->product->slug,
                    'offer' => [
                        'id' => $value->id,
                        'name' => $value->name,
                        'code' => $value->code,
                        'external_id' => $value->external_id,
                        'price' => (float) $value->price,
                        'old_price' => (float) $value->old_price,
                        'quantity' => $value->quantity,
                    ],
                    'properties' => [
                        'colors' => isset($value->product->property[3]) ? $value->product->property[3] : [],
                        'material' => isset($value->product->property[7]) ? $value->product->property[7] : [],
                        'type' => isset($value->product->property[10]) ? $value->product->property[10] : [],
                        'season' => isset($value->product->property[11]) ? $value->product->property[11] : [],
                    ]
                ];
                return $prod;
            } else {
                return [];
            }
        }
    }
}

