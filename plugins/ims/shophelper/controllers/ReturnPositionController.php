<?php namespace Ims\Shophelper\Controllers;

use Backend\Classes\Controller;
use Backend\Facades\BackendMenu;

class ReturnPositionController extends Controller
{
    public $implement = [
        'Backend\Behaviors\ListController',
        'Backend\Behaviors\FormController'
    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Ims.Shophelper', 'main-menu-item', 'side-menu-item4');
    }


    /**
     * @param \October\Rain\Database\Builder $query
     * @return \October\Rain\Database\Builder
     */
    public function listExtendQuery($query)
    {
        return $query->whereHas('order', function ($query) {
            $query->where('is_returned', 1);
        });
    }

}
