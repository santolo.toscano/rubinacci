<?php namespace Ims\Shophelper\Controllers;

use Backend\Classes\Controller;
use Backend\Facades\BackendMenu;
use Ims\Shophelper\Controllers\Api\ExcelApiController;
use Ims\Shophelper\Helpers\Helpers;
use Lovata\Shopaholic\Classes\Item\OfferItem;
use Lovata\Shopaholic\Classes\Item\ProductItem;

class ProductHelperController extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController',        'Backend\Behaviors\FormController',        'Backend\Behaviors\ReorderController'    ];

    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $reorderConfig = 'config_reorder.yaml';

    public $requiredPermissions = [
        'manage'
    ];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Ims.Shophelper', 'main-menu-item', 'side-menu-item');
    }

    protected function changeRecord($recordName, $label)
    {
        $data = Post();

        $id = $data['id'];
        $value = $data['value'];

        try {
            $record = OfferItem::make($id)->getObject();
            $record->$recordName = $value;
            $record->save();
            //OfferItem::clearCache($id);

            \Flash::success(" $label aggiornato!");

            return ['status' => true];
        }
        catch ( \Exception $ex )
        {
            \Flash::error($id.' not updated '.$ex->getMessage());
        }
    }

    protected function changeProductRecord($recordName, $label)
    {
        $data = Post();

        $id = $data['id'];
        $value = $data['value'];

        try {
            $record = ProductItem::make($id)->getObject();
            $record->$recordName = $value;
            $record->save();
            //OfferItem::clearCache($id);

            \Flash::success(" $label aggiornato!");

            return ['status' => true];
        }
        catch ( \Exception $ex )
        {
            \Flash::error($id.' not updated '.$ex->getMessage());
        }
    }

    public function onChangeQuantity(){

        return $this->changeRecord("quantity", "quantita");
    }

    public function onChangeBarcode(){

        return $this->changeRecord("code", "barcode");
    }

    public function onChangeIsManualInventory(){

        $this->changeRecord("is_manual_inventory", "giacenza manuale");
        return $this->listRefresh();
    }


    /**
     * Cambia il codice della relazione prodotto
     * @return mixed
     */
    public function onChangeCode(){
        $data = Post();

        $id = $data['id'];
        $value = $data['value'];

        $item = OfferItem::make($id);

        $record = ProductItem::make($item->product_id)->getObject();
        $record->code = $value;
        $record->save();
        //ProductItem::clearCache($item->product_id);

        \Flash::success('codice prodotto aggiornato!');

        //return $this->listRefresh();
    }

    /**
     * Cambia la data di creazione del prodotto
     * @return mixed
     */
    public function onChangeCreated(){
        $data = Post();

        $id = $data['id'];
        $value = $data['value'];
        $new_data = str_replace ('T', ' ', $value);

        $record = ProductItem::make($id)->getObject();
        $record->created_at = $new_data;
        $record->save();
        //ProductItem::clearCache($item->product_id);

        \Flash::success('Data creazione prodotto aggiornato!');

    }

    public function onChangeIsEsporta(){

        $this->changeProductRecord("is_esporta", "Esporta");
        return $this->listRefresh();
    }

    public function onExcelExport(){
        $apiObject = new ExcelApiController(new Helpers());
        $result = $apiObject->products();

        if ($result){
            \Flash::success('Elenco excel dei prodotti esportato con successo!');
            return $baseUrl = url('storage/app/'.$result);
        } else {
            \Flash::success('Errore: esportazione excel non andata a buon fine!');
            return null;
        }
    }
}
