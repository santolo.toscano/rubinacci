<?php namespace Ims\Shophelper\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Class ImportOrders
 * @package Ims\Shophelper\Controllers
 */
class ImportOrdiniController extends Controller
{
    public $implement = [
        'Backend.Behaviors.ListController',
        'Backend.Behaviors.FormController',
    ];

    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    /**
     * ImportOrders constructor.
     */
    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Ims.Shophelper', 'navision-menu', 'navision-import');

    }

}