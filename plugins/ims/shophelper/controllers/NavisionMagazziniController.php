<?php namespace Ims\Shophelper\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class NavisionMagazziniController extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController'    ];
    
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Ims.Shophelper', 'navision-menu', 'nav-trasferimenti');
    }
}
