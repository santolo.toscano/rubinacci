<?php namespace Ims\Shophelper\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Illuminate\Support\Facades\DB;
use Ims\Shophelper\Helpers\Helpers;
use Ims\Shophelper\Models\NewsletterCustomer;
use Ims\Shophelper\Controllers\Api\ExcelApiController;

class NewsletterCustomerController extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController'    ];
    
    public $listConfig = 'config_list.yaml';

    public $requiredPermissions = [
        'manage' 
    ];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Ims.Shophelper', 'main-menu-item', 'side-menu-item9');
    }

    public function onExcelExport(){
        $apiObject = new ExcelApiController(new Helpers());
        $result = $apiObject->newsletterexport();

        if ($result){
            \Flash::success('Elenco excel degli Utenti Iscritti alla Newsletter esportato con successo!');
            return redirect('/storage/app/media/documenti/newsletteruserexport.xlsx');
        } else {
            \Flash::success('Errore: esportazione excel non andata a buon fine!');
            return null;
        }
    }

    public function onRemoveNewsletter($email)
    {
        $obEmail = NewsletterCustomer::where(['email' => $email])->pluck('email');
        if ($obEmail !== null) {
            NewsletterCustomer::where(['email' => $email])->delete();
            return redirect('unsubscribe');
        } else {
            return redirect('unsubscribe');
        }
    }
}
