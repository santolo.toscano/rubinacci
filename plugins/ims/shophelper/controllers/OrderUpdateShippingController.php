<?php namespace Ims\Shophelper\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Ims\Shophelper\Components\OrderHelper;
use Lovata\OrdersShopaholic\Classes\Item\OrderItem;

class OrderUpdateShippingController extends Controller
{
    public $implement = ['Backend\Behaviors\FormController'];
    
    public $formConfig = 'config_form.yaml';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Ims.Shophelper', 'main-menu-orders', 'side-menu-item2');
    }

    public function onSave2()
    {
        $data = post();

        $order = OrderItem::make($data['id'])->getObject();
        $property = $order->property['property'];

        $property = [
            'property' =>[
                'shipping_name' => $data['OrderHelper']['shipping_name'],
                'shipping_last_name' => $data['OrderHelper']['shipping_last_name'],
                'shipping_address1' => $data['OrderHelper']['shipping_address1'],
                'shipping_postcode' => $data['OrderHelper']['shipping_postcode'],
                'shipping_city' => $data['OrderHelper']['shipping_city'],
                'shipping_state' => $data['OrderHelper']['shipping_state'],
                'shipping_region' => $data['OrderHelper']['shipping_state'] == 'IT' ? $data['OrderHelper']['shipping_region'] : 'EE',
            ],
            'shipping_address1' => $data['OrderHelper']['shipping_address1'],
            'shipping_presso' => $data['OrderHelper']['shipping_presso'],
        ];

        $order->update([
            'property' => $property
        ]);

        $order->save();

        \Flash::success('Modifica spedizione effettuata correttamente');

        return redirect()->to("/backend/ims/shophelper/ordercontroller/update/$order->id");
    }





}
