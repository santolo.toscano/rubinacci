<?php

namespace Ims\Shophelper\Controllers;

use Illuminate\Support\Facades\Request;
use Stripe\Customer;
use Stripe\PaymentIntent;
use Stripe\Stripe;

class StripeController
{

    public function checkoutConfirmPayment(Request $request)
    {
        Stripe::setApiKey('sk_test_lTUzN4qn2greSLXAcJvYAAR500Zup4Uexl');

        header('Content-Type: application/json');

        //$customer = Customer::create();

        # retrieve json from POST body
        $json_str = file_get_contents('php://input');
        $json_obj = json_decode($json_str);

        $intent = null;
        try {
            if (isset($json_obj->payment_method_id)) {
                # Create the PaymentIntent
                $intent = PaymentIntent::create([
                    'payment_method' => $json_obj->payment_method_id,
                    //'customer' => $customer->id,
                    'confirmation_method' => 'manual',
                    'confirm' => true,
                    'amount'   => 5000,
                    'currency' => 'eur',
                    'description' => "Pagamento ECommerce Rubinacci",
                ]);
            }
            if (isset($json_obj->payment_intent_id)) {
                $intent = PaymentIntent::retrieve(
                    $json_obj->payment_intent_id
                );
                $intent->confirm();
            }
            if ($intent->status == 'requires_action' &&
                $intent->next_action->type == 'use_stripe_sdk') {
                # Tell the client to handle the action
                echo json_encode([
                    'requires_action' => true,
                    'payment_intent_client_secret' => $intent->client_secret
                ]);
            } else if ($intent->status == 'succeeded') {
                // Pagamento OK

                echo json_encode([
                    "success" => true,
                    "id" => $intent->id,
                    "amount" => $intent->amount
                ]);
            } else {
                http_response_code(500);
                echo json_encode(['error' => 'Invalid PaymentIntent status']);
            }
        } catch (\Exception $e) {
            # Display error on client
            echo json_encode([
                'error' => $e->getMessage()
            ]);
        }
    }


    /**
     * Funzione utilizzata nella demo Stripe V3
     * @param Request $request
     */
    function process(Request $request) {

        Stripe::setApiKey('sk_test_lTUzN4qn2greSLXAcJvYAAR500Zup4Uexl');

        header('Content-Type: application/json');

        # retrieve json from POST body
        $json_str = file_get_contents('php://input');
        $json_obj = json_decode($json_str);

        $intent = null;
        try {
            if (isset($json_obj->payment_method_id)) {
                # Create the PaymentIntent
                $intent = PaymentIntent::create([
                    'payment_method' => $json_obj->payment_method_id,
                    'confirmation_method' => 'manual',
                    'confirm' => true,
                    'amount'   => 5000,
                    'currency' => 'eur',
                    'description' => "Mon paiement"
                ]);
            }
            if (isset($json_obj->payment_intent_id)) {
                $intent = PaymentIntent::retrieve(
                    $json_obj->payment_intent_id
                );
                $intent->confirm();
            }
            if ($intent->status == 'requires_action' &&
                $intent->next_action->type == 'use_stripe_sdk') {
                # Tell the client to handle the action
                echo json_encode([
                    'requires_action' => true,
                    'payment_intent_client_secret' => $intent->client_secret
                ]);
            } else if ($intent->status == 'succeeded') {
                // Paiement Stripe accepté

                echo json_encode([
                    "success" => true
                ]);
            } else {
                http_response_code(500);
                echo json_encode(['error' => 'Invalid PaymentIntent status']);
            }
        } catch (\Exception $e) {
            # Display error on client
            echo json_encode([
                'error' => $e->getMessage()
            ]);
        }
    }
}