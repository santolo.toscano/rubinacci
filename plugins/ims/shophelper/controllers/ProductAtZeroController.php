<?php namespace Ims\Shophelper\Controllers;

use BackendMenu;
use Illuminate\Http\Request;
use Backend\Classes\Controller;
use Illuminate\Support\Facades\DB;
use Lovata\Shopaholic\Models\Product;
use October\Rain\Support\Facades\Flash;

class ProductAtZeroController extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController',        'Backend\Behaviors\ReorderController'    ];
    
    public $listConfig = 'config_list.yaml';
    public $reorderConfig = 'config_reorder.yaml';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Ims.Shophelper', 'main-menu-item', 'side-menu-item8');
    }

    public function onDeactive()
    {
        /*
         * Validate checked identifiers
         */
        $checkedIds = post('checked');
        
        foreach ($checkedIds as $id) {
            DB::update('UPDATE lovata_shopaholic_products SET active = 0 WHERE id = ?', [$id]);
        }
        Flash::success("Prodotti disabilitati con successo!");
        return back();
    }

    public function onActive()
    {
        /*
         * Validate checked identifiers
         */
        $checkedIds = post('checked');
        
        foreach ($checkedIds as $id) {
            DB::update('UPDATE lovata_shopaholic_products SET active = 1 WHERE id = ?', [$id]);
        }
        Flash::success("Prodotti abilitati con successo!");
        return back();
    }
}
