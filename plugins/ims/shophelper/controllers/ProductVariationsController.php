<?php namespace Ims\Shophelper\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use Lovata\Shopaholic\Classes\Item\OfferItem;
use Lovata\Shopaholic\Classes\Item\ProductItem;

class ProductVariationsController extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController',        'Backend\Behaviors\FormController',        'Backend\Behaviors\ReorderController'    ];

    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $reorderConfig = 'config_reorder.yaml';

    public $requiredPermissions = [
        'manage'
    ];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Ims.Shophelper', 'main-menu-item', 'side-menu-item');
    }

    protected function changeRecord($recordName, $label)
    {
        $data = Post();

        $id = $data['id'];
        $value = $data['value'];

        try {
            $record = OfferItem::make($id)->getObject();
            $record->$recordName = $value;
            $record->save();
            //OfferItem::clearCache($id);

            \Flash::success(" $label aggiornato!");

            return ['status' => true];
        }
        catch ( \Exception $ex )
        {
            \Flash::error($id.' not updated '.$ex->getMessage());
        }
    }

    public function onChangeQuantity(){

        return $this->changeRecord("quantity", "quantita");
    }

    public function onChangeBarcode(){

        return $this->changeRecord("code", "barcode");
    }

    public function onChangeIsManualInventory(){

        $this->changeRecord("is_manual_inventory", "giacenza manuale");
        return $this->listRefresh();
    }

    public function onChangeIsEsporta(){

        $this->changeRecord("is_esporta", "Esporta");
        return $this->listRefresh();
    }


    /**
     * Cambia il codice della relazione prodotto
     * @return mixed
     */
    public function onChangeCode(){
        $data = Post();

        $id = $data['id'];
        $value = $data['value'];

        $item = OfferItem::make($id);

        $record = ProductItem::make($item->product_id)->getObject();
        $record->code = $value;
        $record->save();
        //ProductItem::clearCache($item->product_id);

        \Flash::success('codice prodotto aggiornato!');

        //return $this->listRefresh();
    }
}
