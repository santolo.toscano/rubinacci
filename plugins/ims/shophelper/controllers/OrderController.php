<?php namespace Ims\Shophelper\Controllers;

use Log;
use Event;
use Input;
use BackendMenu;
use Illuminate\Http\Request;
use Backend\Classes\Controller;
use Guzzle\Http\Message\Response;
use Ims\Shophelper\Models\Carrier;
use Ims\Shophelper\Models\Carriers;
use Lovata\Shopaholic\Models\Currency;
use Illuminate\Support\Facades\Storage;
use Ims\Shophelper\Helpers\EmailHelper;
use October\Rain\Support\Facades\Flash;
use Ims\Shophelper\Components\VatHelper;
use Ims\Shophelper\Classes\ImsShippingManager;
use Ims\Shophelper\Helpers\PaymentGatewayHelper;
use Lovata\OrdersShopaholic\Models\ShippingType;
use Lovata\OrdersShopaholic\Classes\Item\OrderItem;
use Lovata\OrdersShopaholic\Classes\Processor\OrderProcessor;

/**
 * Class Orders
 * @package Lovata\OrdersShopaholic\Controllers
 * @author Andrey Kharanenka, a.khoronenko@lovata.com, LOVATA Group
 */
class OrderController extends Controller
{
    public $implement = [
        'Backend.Behaviors.ListController',
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.RelationController',
    ];

    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $relationConfig = 'config_relation.yaml';
    public $file_path = '';
    public $file_dir;

    /**
     * Orders constructor.
     */
    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Ims.Shophelper', 'main-menu-orders', 'side-menu-order1');

        $this->addCss('/plugins/lovata/ordersshopaholic/assets/css/jjsonviewer.css');
        $this->addCss('/plugins/lovata/ordersshopaholic/assets/css/backend.css');
        $this->addJs('/plugins/lovata/ordersshopaholic/assets/js/jjsonviewer.js');
    }

    public function onCancelOrder(){

        $data = Post();

        $id = $data['id'];

        try {

            $order = OrderItem::make($id);
            PaymentGatewayHelper::cancelOrder($order->getObject());
            PaymentGatewayHelper::restoreInventory($order->getObject());

            \Flash::success('ordine annullato!');
            return $this->listRefresh();
        }
        catch ( \Exception $ex )
        {
            \Flash::error($id.' not updated '.$ex->getMessage());
        }
    }

    public function isUeState($state) {
        return in_array($state, array('IT', 'AT', 'BE', 'BG', 'CY', 'CZ', 'DE', 'DK', 'EE', 'ES', 'FI', 'FR', 'GR', 'EL', 'HR', 'HU', 'IE', 'LT', 'LU', 'LV', 'MC', 'MT', 'NL', 'PL', 'PT', 'RO', 'SE', 'SI', 'SK'));
    }

    public function isItState($state) {
        return $state === 'IT';
    }

    private function calculateNewPrices($obOrder){
        if((int)$obOrder->dazi_value !== 0){
            $vat = VatHelper::getVatFromState($obOrder->property['property']['shipping_state']);
            $itvatMultiplier = VatHelper::getVatMultiplierFromState();
            $vatMul = VatHelper::getVatMultiplier($vat);
            $currencyRate = floatval(Currency::where(['id' => $obOrder->currency_id])->first()->rate);
            $originaPrice = round($obOrder->position_total_price_data->price);
            if($vatMul === $itvatMultiplier) {
                $price1 = $originaPrice;
            } else {
                $price1 = round(($originaPrice/$itvatMultiplier)*$vatMul);
            }
            $price1_eur = round(round($price1/$currencyRate)/$vatMul);
            if($price1_eur >= 350){
                $shipping_type_code = "it-free";
            }else{
                if($this->isItState($obOrder->property['property']['shipping_state'])){
                    $shipping_type_code = "nuovaitalia-express";
                }else if($this->isUeState($obOrder->property['property']['shipping_state'])){
                    $shipping_type_code = "nuovaeuropa-express";
                }else{
                    $shipping_type_code = "nuovaextraue-express";
                }
            }
            $shipping_type = ShippingType::getByCode($shipping_type_code)->first();
            $eur_shipping_price = floatval($shipping_type->price);
            $eur_dazi_price = round(($price1_eur * 3) / 10);
            $newPrice = $price1_eur + $eur_shipping_price + $eur_dazi_price;
            $obOrder->shipping_type_id = $shipping_type->id;
            $obOrder->shipping_price = $eur_shipping_price;
            $obOrder->dazi_value = $eur_dazi_price;
            $price = $originaPrice;
        }else{
            $obPositionPriceData = $obOrder->position_total_price_data;
            $price = floatval($obPositionPriceData->price);
            
            $vat = VatHelper::getVatFromState($obOrder->property['property']['shipping_state']);
            $itvatMultiplier = VatHelper::getVatMultiplierFromState();
            $vatMul = VatHelper::getVatMultiplier($vat);
            $currencyRate = floatval(Currency::where(['id' => $obOrder->currency_id])->first()->rate);
            $price1_eur = ($this->isItState($obOrder->property['property']['shipping_state'])) ? round($price/$currencyRate) : round(round($price/$currencyRate)/$vatMul);
            if($price1_eur >= 350){
                $shipping_type_code = "it-free";
            }else{
                if($this->isItState($obOrder->property['property']['shipping_state'])){
                    $shipping_type_code = "nuovaitalia-express";
                }else if($this->isUeState($obOrder->property['property']['shipping_state'])){
                    $shipping_type_code = "nuovaeuropa-express";
                }else{
                    $shipping_type_code = "nuovaextraue-express";
                }
            }
            $shipping_type = ShippingType::getByCode($shipping_type_code)->first();
            $eur_shipping_price = floatval($shipping_type->price);
            $obOrder->shipping_type_id = $shipping_type->id;
            $obOrder->shipping_price = $eur_shipping_price;
            $newPrice = $price1_eur + $eur_shipping_price;
        }
        return [$newPrice,$price];
    }

    /**
     * Create relation object handler
     * @return mixed
     */
    public function onRelationManageCreate()
    {
        $arResult = parent::onRelationManageCreate();
        if (empty($arResult) || !is_array($arResult)) {
            $arResult = [];
        }

        //Get order object
        /** @var \Lovata\OrdersShopaholic\Models\Order $obOrder */
        $obOrder = $this->relationObject->getParent();

        //Update price block partial
        $this->initForm($obOrder, 'update');
        $arrRes = $this->calculateNewPrices($obOrder);
        $obOrder->conv_prezzo_tot = $arrRes[0];
        $obOrder->prezzo_tot = $arrRes[1];
        $obOrder->update();
        
        $arResult['#Form-field-Order-user_block-group'] = $this->formGetWidget()->renderField('user_block', ['useContainer' => false]);
        $obMechanismList = $obOrder->order_promo_mechanism;
        if ($obMechanismList->isNotEmpty()) {
            $arResult['#Form-field-Order-discount_log_position_total_price-group'] = $this->formGetWidget()->renderField('discount_log_position_total_price', ['useContainer' => false]);
            $arResult['#Form-field-Order-discount_log_shipping_price-group'] = $this->formGetWidget()->renderField('discount_log_shipping_price', ['useContainer' => false]);
            $arResult['#Form-field-Order-discount_log_total_price-group'] = $this->formGetWidget()->renderField('discount_log_total_price', ['useContainer' => false]);
        }
        
        return $arResult;
    }

    /**
     * Update relation object handler
     * @return mixed
     */
    public function onRelationManageUpdate()
    {
        $arResult = parent::onRelationManageUpdate();
        if (empty($arResult) || !is_array($arResult)) {
            $arResult = [];
        }

        //Get order object
        /** @var \Lovata\OrdersShopaholic\Models\Order $obOrder */
        $obOrder = $this->relationObject->getParent();
        $sRelationName = $this->relationName;

        //Update price block partial
        $this->initForm($obOrder, 'update');
        if (in_array($sRelationName, ['active_task', 'completed_task'])) {
            $arResult = array_merge($arResult, $this->relationRefresh('active_task'), $this->relationRefresh('completed_task'));
        }

        return $arResult;
    }

    /**
     * Remove relation object handler
     * @return mixed
     */
    public function onRelationButtonDelete()
    {
        $arResult = parent::onRelationButtonDelete();
        if (empty($arResult) || !is_array($arResult)) {
            $arResult = [];
        }

        //Get order object
        /** @var \Lovata\OrdersShopaholic\Models\Order $obOrder */
        $obOrder = $this->relationObject->getParent();
        /**
         * Ricalcolo dazi se valore è diverso da zero
         */
        $this->initForm($obOrder, 'update');
        $arrRes = $this->calculateNewPrices($obOrder);
        $obOrder->conv_prezzo_tot = $arrRes[0];
        $obOrder->prezzo_tot = $arrRes[1];
        //Update price block partial
        $obOrder->update();
        $arResult['#Form-field-Order-user_block-group'] = $this->formGetWidget()->renderField('user_block', ['useContainer' => true]);
        $obMechanismList = $obOrder->order_promo_mechanism;
        if ($obMechanismList->isNotEmpty()) {
            $arResult['#Form-field-Order-discount_log_position_total_price-group'] = $this->formGetWidget()->renderField('discount_log_position_total_price', ['useContainer' => false]);
            $arResult['#Form-field-Order-discount_log_shipping_price-group'] = $this->formGetWidget()->renderField('discount_log_shipping_price', ['useContainer' => false]);
            $arResult['#Form-field-Order-discount_log_total_price-group'] = $this->formGetWidget()->renderField('discount_log_total_price', ['useContainer' => false]);
        }

        return $arResult;
    }

    public function onSendShippingMail()
    {
        $post = post();

        $orderItem = OrderItem::make($post['order']);
        $order = $orderItem->getObject();
        /*
        $this->file_path = Input::file('percorso_file');
        if(isset($this->file_path)){
            $nomefile = $this->file_path->getClientOriginalName();
            $directory = $order->id;
            $path_to_dir = Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix()."/".$directory;
            if(!is_dir($path_to_dir)){
                mkdir($path_to_dir,0777);
            }
            $this->file_path->move($path_to_dir,$this->file_path->getClientOriginalName());
            $this->file_dir = $path_to_dir."/".$nomefile;
            $order->file_dhl = (string)$this->file_dir;
            $order->save();
        }
        */

        $orderLanguage = $orderItem->property['locale'];
        $tracking_field = 'tracking_url_'.$orderLanguage;


        if ( !$order->shipping_company )
        {
            Flash::error('Seleziona il vettore e effettua un salvataggio');
            return false;
        }


        if ( !$order->shipping_tracking )
        {
            Flash::error('Insierisci il numero di tracking e effettua un salvataggio');
            return false;
        }

        $carrier = Carrier::where('carrier_name', '=', $order->shipping_company)->first();

        if ( $carrier )
        {
            EmailHelper::sendShippingEmail($orderItem, $carrier->$tracking_field);
            Flash::success("Email di spedizione inviata correttamente");
            return true;
        }
        else
        {
            Flash::error("Vettore non trovato in archivio, contattare l'amministratore di sistema");
            return false;
        }
    }

    public function onSubmit()
    {
        $this->file_path = Input::file('percorso_file');
        if(isset($this->file_path)){
            Flash::success("File Caricato");
            return true;
        }else{
            Flash::error("Errore Caricamento File");
            return false;
        }
        
    }

    public function onGetShippingQuote()
    {
        $post = post();

        $orderItem = OrderItem::make($post['order']);
        $order = $orderItem;
        $orderModel = $orderItem->getObject();

        if ( !$orderModel->shipping_company )
        {
            Flash::error('Seleziona il vettore e effettua un salvataggio');
            return false;
        }

        if ( strtolower($orderModel->shipping_company) != 'dhl' )
        {
            Flash::error('Dunzione attiva solo per DHL');
            return false;
        }


        if ( !$orderModel->shipping_type )
        {
            Flash::error('Impostare le dimensioni del pacco');
            return false;
        }

        $shippingManger = new ImsShippingManager();
        $quoteResult = $shippingManger->getQuote($order);

        if ( $quoteResult['success'] )
        {
            Flash::success("Quotazione calcolata con successo");
            $orderModel->shipping_account = $quoteResult['account'];
            $orderModel->shipping_quote = $quoteResult['rates'][$orderModel->shipping_account];
            $orderModel->save();
            return redirect()->to("/backend/ims/shophelper/ordercontroller/update/$order->id");
        }
        else
        {
            Flash::error("Errore sul calcolo quotazione");
            return false;
        }
    }

    public function onCreateLabel()
    {
        $post = post();
        $orderItem = OrderItem::make($post['order']);
        $order = $orderItem;
        $orderModel = $orderItem->getObject();

        if ( !$orderModel->shipping_company )
        {
            Flash::error('Seleziona il vettore e effettua un salvataggio');
            return false;
        }

        if ( strtolower($orderModel->shipping_company) != 'dhl' )
        {
            Flash::error('Dunzione attiva solo per DHL');
            return false;
        }

        if ( !$orderModel->shipping_package_type )
        {
            Flash::error('Impostare le dimensioni del pacco');
            return false;
        }

        $shippingManager = new ImsShippingManager();

        // Calcola quotazione se non già calcolata
        if ( !$orderModel->shipping_quote )
        {
            $quoteResult = $shippingManager->getQuote($order);
            $orderModel->shipping_account = $quoteResult['account'];
            $orderModel->shipping_quote = $quoteResult['rates'][$orderModel->shipping_account];
            $orderModel->save();

            if ( !$quoteResult['success'] )
            {
                Flash::error("Errore sul calcolo quotazione");
                return false;
            }
        }

        $labelResult = $shippingManager->createLabel($order);
        if ( $labelResult['success'] ) {
            try {
                $orderModel->shipping_tracking = $labelResult['tracking'];
                $orderModel->shipping_quote = $labelResult['amount'];
                /*
                if (!$orderModel->shipping_date) {
                    $orderModel->shipping_date = $labelResult['date'];
                }*/
                $orderModel->save();
            }
            catch ( \Exception $ex )
            {
                Flash::error("Etichetta creata ma errore su salvataggio dati");
                return false;
            }

            Flash::success("Etichetta creata correttamente");
            return redirect()->to("/backend/ims/shophelper/ordercontroller/update/$order->id");
        }
        else
        {
            Flash::error("Errore su creazione etichetta:\r\n". $labelResult['error']);
            return false;
        }

    }

    public function onLoadPopup()
    {
        $data = post();

        $body = "";

        $config = $this->makeConfig('$/ims/shophelper/models/orderhelper/popup.yaml');
        $config->model = new \Ims\Shophelper\Models\OrderHelper;

        $widget = $this->makeWidget('Backend\Widgets\Form', $config);

        // imposta il valore di default di prova
        $widget->fields['id'] = array_merge($widget->fields['id'], [
            'default' => $data['id'],
            'label' => 'Numero Ordine']
        );

        $body = $widget->render();

        /*
         * ATTENZIONE
         * il file della partial necessario per renderizzare il popup("popup.htm")
         * con i dati di un model diverso da quello da cui si sta lanciando il popup,
         * deve risiedere nella cartella del controller del model che si vuole visualizzare
         * poichè altrimenti non riesce a reperire tutti i file necessari a risolvere
         * le eventuali relazioni presenti con altri model
         */
        $partial = $this->renderPartial('/ims/shophelper/controllers/ordercontroller/popup.htm', "");

        $output = str_replace("#body#", $body, $partial);
        $output = str_replace("#title#", "Aggiorna stato", $output);

        return $output;
    }

    public function onSavePopup()
    {
        $data = post();

        $order = OrderItem::make($data['id'])->getObject();
        $user = $order->user;
        $state = $data['stato'];

        $property = $order->property['property'];
        $property['shipping_state'] = $state;

        $order->update([
            'property' => [
                'billing_state' => $state,
                'property' => $property
            ],
        ]);

        $user->update([
            'property' => [
                'shipping_state' => $state,
                'billing_state' => $state,
            ]
        ]);

        $order->save();

        \Flash::success('Creazione completata');

        return redirect()->to("/backend/ims/shophelper/ordercontroller/update/$order->id");
    }

    public function onInterfaceShipping(){
        $data = post();
        $val = $data['search'];
        $order_id = $data['order_id'];
        $ims_quotation = new ImsShippingManager();
        switch($val){
            case 'quotation':
                return json_encode($ims_quotation->getQuotationDHL($data['params'],$order_id));
            case 'create_shipping':
                return json_encode($ims_quotation->createShippingDHL($data));
            case 'return_shipping':
                return json_encode($ims_quotation->createReturnShippingDHL($data));
            default:
                break;
        }
    }

    public function onInterfaceShipping2(){
        $data = post();
        $val = $data['search'];
        $order_id = $data['order_id'];
        $ims_quotation = new ImsShippingManager();
        switch($val){
            case 'create_shipping_send':
                return json_encode($ims_quotation->createShippingDHL($data));
            case 'create_shipping_return':
                return json_encode($ims_quotation->createReturnShippingDHL($data));
            default:
                break;
        }
    }

    protected function renderPartial($partial, $body)
    {
        $path = plugins_path();
        $partialFile = $path.$partial;
        $params =  ['formBody' => $body];

        return $this->makeFileContents($partialFile, $params);
    }

    public function onDownload() {
        $order_id = post('order_id');
        $orderId_Order = explode(',', $order_id)[1];
        $typeRequest = explode(',', $order_id)[0];
        if ($typeRequest == 'partenza') {
            $pathibolo = "storage/app/media/shipmentsLabel/$orderId_Order/etichetta_partenza_$orderId_Order.pdf";
        } else {
            $pathibolo = "storage/app/media/shipmentsLabel/$orderId_Order/etichetta_reso_$orderId_Order.pdf";
        }
        return $pathibolo;
    }
}