<?php namespace Ims\Shophelper\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Ims\Shophelper\Helpers\Helpers;
use Ims\Shophelper\Controllers\ProductStockExport;
use Ims\Shophelper\Controllers\Api\ExcelApiController;
use Ims\Shophelper\Models\ProductStock as ProdottiStock;

class ProductStock extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController'   ];

    public $importExportConfig = 'config_import_export.yaml';
    
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Ims.Shophelper', 'main-menu-item', 'side-menu-item7');
    }

    public function onExcelExport(){
        $apiObject = new ExcelApiController(new Helpers());
        $result = $apiObject->stock();

        if ($result){
            \Flash::success('Elenco excel delle giacenze esportato con successo!');
            return redirect('/storage/app/media/documenti/giacenzeminime.xlsx');
        } else {
            \Flash::success('Errore: esportazione excel non andata a buon fine!');
            return null;
        }
    }
}
