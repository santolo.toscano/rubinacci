<?php namespace Ims\Shophelper;

use Carbon\Carbon;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;
use Ims\Shophelper\Classes\Event\ExtendCartPositionModel;
use Ims\Shophelper\Classes\Event\ExtendOrderPositionModel;
use Ims\Shophelper\Controllers\ProductHelperController;
use Ims\Shophelper\Controllers\ProductVariationsController;
use Ims\Shophelper\Helpers\ImsHelper;
use Ims\Shophelper\Helpers\PaymentGatewayHelper;
use Ims\Shophelper\Models\Carrier;
use Ims\Shophelper\Models\CustomProductsCombinations;
use Lovata\OmnipayShopaholic\Classes\Helper\PaymentGateway;
use Lovata\OrdersShopaholic\Classes\Item\OrderPositionItem;
use Ims\Shophelper\Classes\ShippingWeightRestriction;
use Lovata\OrdersShopaholic\Classes\Restriction\RestrictionByTotalPrice;
use Lovata\OrdersShopaholic\Classes\Restriction\ShippingRestrictionByPaymentMethod;
use Lovata\OrdersShopaholic\Controllers\OrderPositions;
use Lovata\OrdersShopaholic\Controllers\Orders;
use Lovata\OrdersShopaholic\Models\Order;
use Lovata\OrdersShopaholic\Models\OrderPosition;
use Lovata\Shopaholic\Controllers\Offers;
use Lovata\Shopaholic\Controllers\Products;
use Lovata\Shopaholic\Models\Offer;
use Lovata\Shopaholic\Models\Price;
use Lovata\Shopaholic\Models\Product;
use Lovata\Shopaholic\Controllers\Categories;
use Lovata\Shopaholic\Models\Category;
use RainLab\Translate\Classes\Translator;
use RainLab\Translate\Models\Attribute;
use System\Classes\PluginBase;
use Event;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
		return [
            'Ims\Shophelper\Components\HomepageHelper'   => 'HomepageHelper',
		    'Ims\Shophelper\Components\ProductList'   => 'ProductList',
            'Ims\Shophelper\Components\ProductView'   => 'ProductView',
            'Ims\Shophelper\Components\CategoryHelper'   => 'CategoryHelper',
            'Ims\Shophelper\Components\OrderHelper'   => 'OrderHelper',
            'Ims\Shophelper\Components\CartHelper'   => 'CartHelper',
            'Ims\Shophelper\Components\WishlistHelper'   => 'WishlistHelper',
            'Ims\Shophelper\Components\SimpleCatalogHelper'   => 'SimpleCatalogHelper',
            'Ims\Shophelper\Components\ConfirmationHelper'   => 'ConfirmationHelper',
            'Ims\Shophelper\Components\UserWelcomeHelper'   => 'UserWelcomeHelper',
            'Ims\Shophelper\Components\OrderReturnHelper'   => 'OrderReturnHelper',
		];
    }

    public function registerSettings()
    {

    }

    public function registerMarkupTags()
    {
        $filters =
        [
            'msglang' => function ($langconst, $param = '') {
                $var = "ims.shophelper::lang.messages.".$langconst;
                $message = Lang::get($var);
                $message = str_replace("{param}", $param, $message);
                return $message;
            },
            'priceformat' => function ($price, $param = '') {
                $returnValue = str_replace(".00", '', $price);

                return $returnValue;
            },
            'cleanhtml' => function ($html, $allowtag = '', $charlist = " \t\n\r\0\x0B"){
                $html = strip_tags($html, $allowtag);
                $string = rtrim($html, $charlist);
                $string = ltrim($string, $charlist);
                return $string;
            },
            'uniquebarcode' => /**
             * @param Offer $offer
             * @return string
             */
             function ($offer) {
                $product = $offer->product;
                if (!$product->getObject()->is_manual_inventory) {
                    $id = $offer->code;
                } else {
                    // Crea un EAN con ID del prodotto
                    $id = $product->id;
                    $length = 12;
                    $id = substr(str_repeat(0, $length) . $id, -$length);
                }

                return $id;
            },
            'trackingurl' => function ($order) {
                if ( $order->status_id == 5 ) {
                    $orderModel = $order->getObject();
                    if ($orderModel->shipping_company) {
                        $carrier = Carrier::getByCode($orderModel->shipping_company);
                        if ( $carrier) {
                            $locale = $orderModel->property['locale'];
                            $varName = "tracking_url_$locale";
                            $trackingUrl =  $carrier->$varName;
                            $trackingMessage = $locale == 'it' ?  'Clicca ed inserisci il codice tracking' : 'Click and    insert tracking';
                            return '<a href="'.$trackingUrl.'" target="_blank"> <u>'.$trackingMessage.' '.$orderModel->shipping_tracking.'</u></a>';
                        }
                    }
                }
                return "";
            },
            'trackingurl2' => function ($order) {
                $orderModel = $order->getObject();
                if ($orderModel->shipping_company) {
                    $carrier = Carrier::getByCode($orderModel->shipping_company);
                    if ( $carrier->carrier_name == 'dhl') {
                        $curlSES = curl_init();
                        $idtrack = (string)$orderModel->shipping_tracking;
                        $payload = 'trackingNumber='.$idtrack;
                        curl_setopt($curlSES, CURLOPT_URL, "https://api-eu.dhl.com/track/shipments?".$payload);
                        curl_setopt($curlSES, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($curlSES, CURLOPT_HEADER, false);
                        curl_setopt($curlSES, CURLOPT_HTTPHEADER, [
                            'Content-Type: application/json',
                            'Accept:application/json',
                            'DHL-API-Key:DeznTQtbsAgGHHXc4wetR7Y01PmH2ABh'
                        ]);
                        //step3
                        $result = curl_exec($curlSES);
                        //step4
                        curl_close($curlSES);
                        $res = json_decode($result);
                        if(isset($res)
                        && isset($res->shipments)
                        && isset($res->shipments[0])
                        && isset($res->shipments[0]->status)
                        && isset($res->shipments[0]->status->description)) {
                            return $res->shipments[0]->status->description;
                        } else {
                            return "";
                        }
                    }else{
                        $locale = $orderModel->property['locale'];
                        $varName = "tracking_url_$locale";
                        $trackingUrl =  $carrier->$varName;
                        $trackingMessage = $locale == 'it' ?  'Clicca ed inserisci il codice tracking' : 'Click and    insert tracking';
                        return '<a href="'.$trackingUrl.'" target="_blank"> <u>'.$trackingMessage.' '.$orderModel->shipping_tracking.'</u></a>';
                    }
                }
            },
            'customized_content' => function ($cust_cont, $param = '') {
                $element = ImsHelper::manageCustomizedContent($cust_cont, $param);
                return $element;
            },
        ];

        return [
            'filters'   => $filters
        ];
    }


   /**
     * Add event listeners
     */
    protected function addEventListener()
    {
        //Common events
        Event::subscribe(ExtendCartPositionModel::class);
        Event::subscribe(ExtendOrderPositionModel::class);

        // Gestisce il redirect url per il gateway di pagamento
        Event::listen(
            PaymentGateway::EVENT_GET_PAYMENT_GATEWAY_RETURN_URL, function($order, $paymentMethod) {
            return PaymentGatewayHelper::getRedirectUrlForSuccess($order, $paymentMethod);
        });
        // Gestisce il redirect url per il gateway di pagamento
        Event::listen(
            PaymentGateway::EVENT_GET_PAYMENT_GATEWAY_CANCEL_URL, function($order, $paymentMethod) {
            return PaymentGatewayHelper::getRedirectUrlForCancel($order, $paymentMethod);
        });

        Event::listen(
            'shopaholic.sorting.get.list', function($order) {
            $sorting = explode('|', $order);
            switch ( $sorting[1] )
            {
                case 'created';
                    $arElementIDList = (array)Product::orderBy('created_at', 'desc')->lists('id');
                    return $arElementIDList;
                case 'season_asc':
                    $arElementIDList = (array)Product::orderBy('brand_id')->orderBy('created_at', 'desc')->lists('id');
                    return $arElementIDList;
                case 'season_desc':
                    $arElementIDList = (array)Product::orderBy('brand_id', 'desc')->orderBy('created_at', 'desc')->lists('id');
                    return $arElementIDList;
                case 'abc':
                    $arElementIDList = (array)Product::orderBy('created_at', 'asc')->lists('id');
                    return $arElementIDList;
                default:
                    $arElementIDList = (array)Product::orderBy('id', 'desc')->lists('id');
                    return $arElementIDList;
            }
        });

        Event::listen('shopaholic.shippingtype.get.restriction.list', function() {
            $arResult = [
                RestrictionByTotalPrice::class            => 'lovata.ordersshopaholic::lang.restriction.handler.by_total_price',
                ShippingRestrictionByPaymentMethod::class => 'lovata.ordersshopaholic::lang.restriction.handler.by_payment_method',
                ShippingWeightRestriction::class => 'Restrizione sul peso'
            ];

            return $arResult;
        });
    }

    public function boot(){
        Validator::extend('min_price', function($attribute, $value, $parameters) {
            if((float)$value > (float)$parameters[0]){
                return true;
            }else{
                $errorMessage = Lang::get('ims.shophelper::lang.component_order.error_gb_total_price');
                \Flash::error($errorMessage);
                return false;
            }
        });


        $this->addEventListener();

        //estensione controller Products per aggiunta clone button
        Products::extend(function ($controller) {
            $controller->listConfig = __DIR__.'/controllers/products/config_list.yaml';
            $controller->addDynamicMethod('onDuplicate', function () use ($controller) {
                $checked_items_ids = input('checked');

                foreach ($checked_items_ids as $id) {

                    $original =  Product::where("id", $id)->first();
                    $original->duplicate();// funzione aggiunta estendendo il model del product(vedi sotto)
                }

                \Flash::success('Record clonato');
                return $controller->listRefresh();
            });
        });

        Product::extend(function ($model){
            $model->addDynamicMethod('duplicate', function () use ($model){
                return ImsHelper::cloneProduct($model);
            });
        });

        OrderPosition::extend(function ($model){
            $model->addDynamicMethod('getCustomizedTextAttribute', function () use ($model){
                $element = ImsHelper::manageCustomizedContent($model->customized_content, 'it');
                return $element;
            });
        });


        Offers::extendFormFields(function($form, $model, $context) {

            // check if the model returned is the right one
            if (! $model instanceof Offer) {
                return;
            }

            $form->addFields([
                'is_manual_inventory' => [
                    'label' => 'Giacenza manuale',
                    'type' => 'switch',
                    'span' => 'right'
                ],
            ]);
        });

        Product::extend(function($model){
            $model->attachOne['back_preview'] = ['System\Models\File'];
        });

        OrderPosition::extend(function($model){
            $model->belongsTo['customized_offer'] = [Offer::class, 'key' => 'customized_offer_id', 'otherKey' => 'id'];
        });

        OrderPositionItem::extend(function($item){
            $item->addDynamicMethod('getPippo', function() use ($item) {
                return ;
            });
        });

        Products::extendFormFields(function($form, $model, $context) {

            // check if the model returned is the right one
            if (! $model instanceof Product) {
                return;
            }

            $form->addTabFields([
                'back_preview' => [
                    'label' => 'Preview image - Back',
                    'mode' => 'image',
                    'type' => 'fileupload',
                    'tab' => 'lovata.toolbox::lang.tab.images',
                    'span' => 'left'
                ]
            ]);

            $form->addFields([
                'is_manual_inventory' => [
                    'label' => 'Giacenza manuale',
                    'type' => 'switch',
                    'span' => 'left'
                ],
                'ims_iscustomizable' => [
                    'label' => 'Prodotto personalizzabile?',
                    'type' => 'switch',
                    'span' => 'right'
                ],
                'is_esporta' => [
                    'label' => 'Esporta',
                    'type' => 'switch',
                    'span' => 'left'
                ],
                'created_at' => [
                    'label' => 'Data creazione (per ordinamento)',
                    'type' => 'datepicker',
                    'mode' => 'date',
                    'span' => 'left'
                ],
                'customization_type' => [
                    'label' => 'Modalità personalizzazione',
                    'type' => 'dropdown',
                    'emptyOption' => '--seleziona--',
                    'options' => [
                        'tono su tono' => 'Tono su tono',
                        'rosso' => 'Rosso',
                    ],
                    'span' => 'right',
                    'trigger' => [
                        'action' => 'show',
                        'field' => 'ims_iscustomizable',
                        'condition' => 'checked'
                    ]
                ],
            ]);

        });

        OrderPositions::extendFormFields(function($form, $model, $context) {

            // check if the model returned is the right one
            if (! $model instanceof OrderPosition) {
                return;
            }

            $form->addTabFields([
                'is_returned' => [
                    'label' => 'Returned Product',
                    'type' => 'switch',
                    'span' => 'left',
                    'tab' => 'Gestione Reso'
                ],
                'returned_date' => [
                    'label' => 'Returned date',
                    'type' => 'datepicker',
                    'mode' => 'date',
                    'span' => 'right',
                    'tab' => 'Gestione Reso'
                ],
                'returned_status' => [
                    'label' => 'Return Status',
                    'type' => 'dropdown',
                    'placeholder' => '-- select a status --',
                    'options' =>[
                        'Nuovo reso' => 'Nuovo reso',
                        'Reso in lavorazione' => 'Reso in lavorazione',
                        'Reso completato' => 'Reso completato',
                    ],
                    'span' => 'left',
                    'tab' => 'Gestione Reso'
                ],
                'returned_reason' => [
                    'label' => 'Motivo reso',
                    'type' => 'text',
                    'span' => 'right',
                    'tab' => 'Gestione Reso'
                ],
                'returned_note' => [
                    'label' => 'Return Note',
                    'type' => 'textarea',
                    'size' => 'large',
                    'span' => 'left',
                    'tab' => 'Gestione Reso'
                ],

            ]);

        });

        // Check if we are currently in backend module.
        if ( App::runningInBackend()) {
            // Listen for `backend.page.beforeDisplay` event and inject js to current controller instance.
            Event::listen('backend.page.beforeDisplay', function($controller, $action, $params) {

                if ($controller instanceof ProductVariationsController && $action == 'index' ) {
                    $controller->addJs('/plugins/ims/shophelper/assets/backend/productvariations.js');
                    $controller->addCss('/plugins/ims/shophelper/assets/backend/productvariations.css');
                }
                if ($controller instanceof ProductHelperController && $action == 'index' ) {
                    $controller->addJs('/plugins/ims/shophelper/assets/backend/producthelper.js');
                }
            });
        }
    }

    public function getReturnedStatusAttribute()
    {
        $result = OrderPosition::attributes['returned_status'];
        $options = OrderPosition::getSampleOptions();

        foreach($options as $key=>$value)
        {
            if($key == $result)
            {
                return $value;
            }
        }
    }

}