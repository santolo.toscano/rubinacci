<?php namespace Ims\Shophelper\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateImsShophelperWishlist extends Migration
{
    public function up()
    {
        if (Schema::hasTable('ims_shophelper_wishlist')) {
            return;
        }
        Schema::create('ims_shophelper_wishlist', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('ims_shophelper_wishlist');
    }
}
