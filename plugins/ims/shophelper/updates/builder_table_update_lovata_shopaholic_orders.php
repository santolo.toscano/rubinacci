<?php namespace Ims\Shophelper\Updates;

use October\Rain\Database\Updates\Migration;
use October\Rain\Support\Facades\Schema;

class BuilderTableUpdateLoavataShopaholicOrders extends Migration
{
    const TABLE_NAME = 'lovata_orders_shopaholic_orders';

    const COLUMN_NAME_1 = 'shipping_tracking';
    const COLUMN_NAME_2 = 'shipping_company';

    public function up()
    {
        if (Schema::hasColumn(self::TABLE_NAME, self::COLUMN_NAME_1)
        && Schema::hasColumn(self::TABLE_NAME, self::COLUMN_NAME_2)) {
            return;
        }

        Schema::table(self::TABLE_NAME, function($table)
        {
            if(!$table->hasColumn(self::COLUMN_NAME_1)) {
                $table->string(self::COLUMN_NAME_1);
            }
            if(!$table->hasColumn(self::COLUMN_NAME_2)) {
                $table->string(self::COLUMN_NAME_2);
            }
        });
    }

    public function down()
    {
        Schema::table(self::TABLE_NAME, function($table)
        {
            $table->dropColumn(self::COLUMN_NAME_1);
            $table->dropColumn(self::COLUMN_NAME_2);
        });
    }
}
