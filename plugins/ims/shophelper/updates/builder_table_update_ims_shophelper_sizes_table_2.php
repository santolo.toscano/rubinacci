<?php namespace Ims\Shophelper\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateImsShophelperSizesTable2 extends Migration
{
    public function up()
    {
        if (Schema::hasColumn('ims_shophelper_sizes_table', 'size_table_en')
        && Schema::hasColumn('ims_shophelper_sizes_table', 'size_table_it')) {
            return;
        }
        Schema::table('ims_shophelper_sizes_table', function($table)
        {
            $table->string('size_table_en', 5000);
            $table->renameColumn('size_table', 'size_table_it');
        });
    }

    public function down()
    {
        Schema::table('ims_shophelper_sizes_table', function($table)
        {
            $table->dropColumn('size_table_en');
            $table->renameColumn('size_table_it', 'size_table');
        });
    }
}
