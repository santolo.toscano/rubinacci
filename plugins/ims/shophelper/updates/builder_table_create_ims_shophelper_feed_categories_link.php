<?php namespace Ims\Shophelper\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateImsShophelperFeedCategoriesLink extends Migration
{
    public function up()
    {
        Schema::create('ims_shophelper_feed_categories_link', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('feed_category_id');
            $table->integer('category_id');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('ims_shophelper_feed_categories_link');
    }
}
