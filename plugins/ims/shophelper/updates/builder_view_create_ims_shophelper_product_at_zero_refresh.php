<?php

namespace Ims\Shophelper\Updates;

use Illuminate\Support\Facades\DB;
use October\Rain\Database\Updates\Migration;

class BuilderViewCreateImsShophelperProductAtZeroRefresh extends Migration
{ 
  const VIEW_NAME = 'ims_shophelper_view_product_at_zero';

  public function up()
  {
    $i = DB::statement("CREATE OR REPLACE VIEW ". self::VIEW_NAME ." AS
      SELECT p.id AS p_id, p.active AS p_active , p.code AS p_code, b.name AS brand, c.name AS category, p.name AS p_name, SUM(o.quantity) AS somma 
      FROM lovata_shopaholic_products p 
      INNER JOIN lovata_shopaholic_offers o on p.id = o.product_id 
      INNER JOIN lovata_shopaholic_categories c on c.id = p.category_id
      INNER JOIN lovata_shopaholic_brands b ON b.id = p.brand_id
      WHERE o.deleted_at IS NULL
      GROUP BY p_id
      HAVING somma=0"
    );
  }

  public function down()
  {
    DB::statement("DROP VIEW IF EXISTS ". self::VIEW_NAME);
  }
}