<?php namespace Ims\Shophelper\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateImsShophelperCarriers extends Migration
{
    public function up()
    {
        Schema::create('ims_shophelper_carriers', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('carrier_name', 50);
            $table->string('tracking_url', 255);
            $table->boolean('is_enabled');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('ims_shophelper_carriers');
    }
}
