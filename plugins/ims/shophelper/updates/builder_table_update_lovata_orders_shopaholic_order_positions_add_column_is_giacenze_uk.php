<?php namespace Ims\Shophelper\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateLoavataShopaholicOrderPositions2 extends Migration
{
    public function up()
    {
        if (Schema::hasColumn('lovata_orders_shopaholic_order_positions', 'is_giacenze_uk')) {
            return;
        }
        Schema::table('lovata_orders_shopaholic_order_positions', function($table)
        {
            $table->boolean('is_giacenze_uk')->default(0);
        });
    }
    
    public function down()
    {
        Schema::table('lovata_orders_shopaholic_order_positions', function($table)
        {
            $table->dropColumn('is_giacenze_uk');
        });
    }
}
