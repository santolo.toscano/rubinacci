<?php namespace Lovata\BaseCode\Updates;

use Schema;
use Illuminate\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

/**
 * Class UpdateTableOrders1
 * @package Lovata\BaseCode\Updates
 */
class UpdateLovataOrdersShopaholicOrders3 extends Migration
{
    /**
     * Apply migration
     */
    public function up()
    {
        if (Schema::hasTable('lovata_orders_shopaholic_orders') && !Schema::hasColumn('lovata_orders_shopaholic_orders', 'is_giacenze_uk')) {
            Schema::table('lovata_orders_shopaholic_orders', function (Blueprint $obTable) {
                $obTable->boolean('is_giacenze_uk')->default(0);
            });
        }
    }

    /**
     * Rollback migration
     */
    public function down()
    {
        if (Schema::hasTable('lovata_orders_shopaholic_orders') && Schema::hasColumn('lovata_orders_shopaholic_orders', 'is_giacenze_uk')) {
            Schema::table('lovata_orders_shopaholic_orders', function (Blueprint $obTable) {
                $obTable->dropColumn(['is_giacenze_uk']);
            });
        }
    }
}