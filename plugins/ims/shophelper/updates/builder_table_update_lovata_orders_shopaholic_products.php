<?php namespace Ims\Shophelper\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateLoavataShopaholicProducts extends Migration
{
    public function up()
    {
        if (Schema::hasColumn('lovata_shopaholic_products', 'ims_highlight')
        && Schema::hasColumn('lovata_shopaholic_products', 'ims_iscustomizable')) {
            return;
        }
        Schema::table('lovata_shopaholic_products', function($table)
        {
            $table->boolean('ims_highlight')->default(0);
            $table->boolean('ims_iscustomizable')->default(0);
        });
    }

    public function down()
    {
        Schema::table('lovata_shopaholic_products', function($table)
        {
            $table->dropColumn('ims_highlight');
            $table->dropColumn('ims_iscustomizable');
        });
    }
}
