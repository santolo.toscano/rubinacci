<?php namespace Lovata\BaseCode\Updates;

use Schema;
use Illuminate\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

/**
 * Class UpdateTableUsersAddCurrencyField
 * @package Lovata\Shopaholic\Updates
 */
class UpdateTableOfferAddColumnQuantityuk extends Migration
{
    /**
     * Apply migration
     */
    public function up()
    {
        if (Schema::hasTable('lovata_shopaholic_offers') && !Schema::hasColumn('lovata_shopaholic_offers', 'quantity_uk')) {
            Schema::table('lovata_shopaholic_offers', function (Blueprint $obTable) {
                $obTable->integer('quantity_uk')->default(0);
            });
        }
    }

    /**
     * Rollback migration
     */
    public function down()
    {
        if (Schema::hasTable('lovata_shopaholic_offers') && Schema::hasColumn('lovata_shopaholic_offers', 'quantity_uk')) {
            Schema::table('lovata_shopaholic_offers', function (Blueprint $obTable) {
                $obTable->dropColumn(['quantity_uk']);
            });
        }
    }
}
