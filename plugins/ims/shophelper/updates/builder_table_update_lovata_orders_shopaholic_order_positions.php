<?php namespace Ims\Shophelper\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateLoavataShopaholicOrderPositions extends Migration
{
    public function up()
    {
        if (Schema::hasColumn('lovata_orders_shopaholic_order_positions', 'is_returned')
        && Schema::hasColumn('lovata_orders_shopaholic_order_positions', 'returned_date')
        && Schema::hasColumn('lovata_orders_shopaholic_order_positions', 'returned_status')
        && Schema::hasColumn('lovata_orders_shopaholic_order_positions', 'returned_reason')
        && Schema::hasColumn('lovata_orders_shopaholic_order_positions', 'returned_note')
        ) {
            return;
        }
        Schema::table('lovata_orders_shopaholic_order_positions', function($table)
        {
            $table->tinyInteger('is_returned')->default(0);
            $table->timestamp('returned_date')->nullable();
            $table->string('returned_status');
            $table->string('returned_reason');
            $table->string('returned_note')->default('returned note');
        });
    }

    public function down()
    {
        Schema::table('lovata_orders_shopaholic_order_positions', function($table)
        {
            $table->dropColumn('is_returned');
            $table->dropColumn('returned_date');
            $table->dropColumn('returned_status');
            $table->dropColumn('returned_reason');
            $table->dropColumn('returned_note');
        });
    }
}
