<?php namespace Ims\Shophelper\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateImsShophelperGeoCountries extends Migration
{
    public function up()
    {   
        if (Schema::hasTable('ims_shophelper_geo_countries')) {
            return;
        }
        Schema::create('ims_shophelper_geo_countries', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('name');
            $table->string('abv');
            $table->string('abv3');
            $table->string('abv3_alt');
            $table->string('code');
            $table->string('slug');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('ims_shophelper_geo_countries');
    }
}
