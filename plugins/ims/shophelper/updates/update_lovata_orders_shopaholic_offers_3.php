<?php namespace Lovata\BaseCode\Updates;

use Schema;
use Illuminate\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

/**
 * Class UpdateTableOrders1
 * @package Lovata\BaseCode\Updates
 */
class UpdateTableLovataShopaholicOffer3 extends Migration
{
    public function up()
    {
        if (Schema::hasTable('lovata_shopaholic_offers') && !Schema::hasColumn('lovata_shopaholic_offers', 'quantity_ita')) {
            Schema::table('lovata_shopaholic_offers', function (Blueprint $obTable) {
                $obTable->integer('quantity_ita')->default(0);
            });
        }
    }

    /**
     * Rollback migration
     */
    public function down()
    {
        if (Schema::hasTable('lovata_shopaholic_offers') && Schema::hasColumn('lovata_shopaholic_offers', 'quantity_ita')) {
            Schema::table('lovata_shopaholic_offers', function (Blueprint $obTable) {
                $obTable->dropColumn(['quantity_ita']);
            });
        }
    }
}