<?php

namespace Ims\Shophelper\Updates;

use Illuminate\Support\Facades\DB;
use October\Rain\Database\Updates\Migration;

class BuilderViewCreateImsShophelperAvailableEmailProductM extends Migration
{
  const VIEW_NAME = 'ims_shophelper_view_available_email_product';

  public function up()
  {
    DB::statement("CREATE OR REPLACE VIEW ". self::VIEW_NAME ." AS
      SELECT product_id, offer_id, count(offer_id) AS quantita
      FROM ims_shophelper_available_email_product
      GROUP BY product_id, offer_id");
  }

  public function down()
  {
    DB::statement("DROP VIEW IF EXISTS ". self::VIEW_NAME);
  }
}