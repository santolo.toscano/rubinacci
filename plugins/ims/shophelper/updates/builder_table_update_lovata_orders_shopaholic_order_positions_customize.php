<?php namespace Ims\Shophelper\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateLoavataShopaholicOrderPositions extends Migration
{
    public function up()
    {
        Schema::table('lovata_orders_shopaholic_order_positions', function($table)
        {
            $table->integer('customized_offer_id')->default('0');
            $table->string('customized_content')->default('');
        });
    }
    
    public function down()
    {
        Schema::table('lovata_orders_shopaholic_order_positions', function($table)
        {
            $table->dropColumn('customized_offer_id');
            $table->dropColumn('customized_content');
        });
    }
}
