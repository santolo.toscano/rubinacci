<?php namespace Ims\Shophelper\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateImsShophelperSizesTable extends Migration
{
    public function up()
    {
        if (Schema::hasColumn('ims_shophelper_sizes_table', 'category_id')) {
            return;
        }
        Schema::table('ims_shophelper_sizes_table', function($table)
        {
            $table->integer('category_id');
        });
    }

    public function down()
    {
        Schema::table('ims_shophelper_sizes_table', function($table)
        {
            $table->dropColumn('category_id');
        });
    }
}
