<?php namespace Ims\Shophelper\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateImsShophelperCatalogSimple extends Migration
{
    public function up()
    {
        if (Schema::hasColumn('ims_shophelper_catalog_simple', 'product_id')) {
            return;
        }
        Schema::table('ims_shophelper_catalog_simple', function($table)
        {
            $table->integer('product_id')->unsigned();
        });
    }

    public function down()
    {
        Schema::table('ims_shophelper_catalog_simple', function($table)
        {
            $table->dropColumn('product_id');
        });
    }
}
