<?php namespace Ims\Shophelper\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateImsShophelperOrder extends Migration
{
    public function up()
    {
        Schema::table('lovata_orders_shopaholic_orders', function($table)
        {
            $table->string('shipping_package_type')->nullable();
            $table->timestamp('shipping_date_requested')->nullable();
            $table->string('shipping_contents')->nullable();
            $table->string('shipping_duty_payment_type')->nullable();
            $table->string('shipping_reference')->nullable();
            $table->decimal('shipping_duty_value', 15, 2)->nullable();
            $table->string('shipping_account')->nullable();
            $table->decimal('shipping_quote', 15, 2)->nullable();
            $table->string('shipping_result', 15, 2)->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('lovata_orders_shopaholic_orders', function($table)
        {
            $table->dropColumn('test');
        });
    }
}
