<?php namespace Ims\Shophelper\Updates;

use Illuminate\Support\Facades\DB;
use Ims\Shophelper\Models\HeaderPhrase;
use October\Rain\Support\Facades\DbDongle;
use October\Rain\Database\Updates\Migration;
/**
 * Class UpdateTableOrders1
 * @package Lovata\BaseCode\Updates
 */
class BuilderTableUpdateImsShophelperHeadersPhrase extends Migration
{
    public $phrases = [
        1 => ['ISCRIVI ALLA NEWSLETTER PER BENEFICIARE DI UN COUPON -10%', 'SUBSCRIBE TO THE NEWSLETTER TO ENJOY A -10% COUPON'],
        2 => ['CONSEGNA GRATUITA PER ORDINI SUPERIORI A 350 EURO', 'FREE DELIVERY ON ALL ORDERS OVER 350 EURO'],
        3 => ["IL COUPON NON E' VALIDO PER LA CATEGORIA OUTLET.", 'THE COUPON DOES NOT APPLY TO ITEMS INTO OUTLET CATEGORY.'],
        4 => ['', 'Price are IT VAT incl. and they might change at checkout accordingly to your Country destination'],
        5 => ['Tutti gli ordini effettuati dopo il 1 Agosto saranno evasi a partire dal 31 Agosto', 'From Aug 1st all orders submitted will be processed from September 1st onwards'],
    ];

    public function up()
    {
        DbDongle::disableStrictMode();
        foreach ($this->phrases as $key) {
            HeaderPhrase::insert([
                'italiano' => $key[0],
                'english' => $key[1],
            ]);
        }
    }

    public function down()
    {
        foreach ($this->phrases as $key) {
            HeaderPhrase::where('italiano', $key[0])
                ->where('english', $key[1])
                ->delete();
        }
    }
}