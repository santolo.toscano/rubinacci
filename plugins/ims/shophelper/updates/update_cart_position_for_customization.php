<?php namespace Lovata\BaseCode\Updates;

use Schema;
use Illuminate\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

/**
 * Class UpdateTableCartPositions1
 * @package Lovata\BaseCode\Updates
 */
class UpdateTableCartPositions1 extends Migration
{
    const TABLE_NAME = 'lovata_orders_shopaholic_cart_positions';

    public function up()
    {
        if (!Schema::hasTable(self::TABLE_NAME) || Schema::hasColumn(self::TABLE_NAME, 'customized_offer_id')) {
            return;
        }

        Schema::table(self::TABLE_NAME, function (Blueprint $obTable)
        {
            $obTable->integer('customized_offer_id')->default(0);
            $obTable->string('customized_content', 255)->default('');
        });
    }

    public function down()
    {
        if (!Schema::hasTable(self::TABLE_NAME) || !Schema::hasColumn(self::TABLE_NAME, 'customized_offer_id')) {
            return;
        }

        Schema::table(self::TABLE_NAME, function (Blueprint $obTable)
        {
            $obTable->dropColumn(['customized_offer_id']);
            $obTable->dropColumn(['customized_content']);
        });
    }
}