<?php namespace Ims\Shophelper\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateImsShophelperSizeTableCategories extends Migration
{
    public function up()
    {
        Schema::create('ims_shophelper_size_table_categories', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('size_table_id');
            $table->integer('category_id');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('ims_shophelper_size_table_categories');
    }
}
