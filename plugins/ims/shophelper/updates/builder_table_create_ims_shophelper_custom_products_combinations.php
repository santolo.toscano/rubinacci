<?php namespace Ims\Shophelper\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateImsShophelperCustomProductsCombinations extends Migration
{
    public function up()
    {
        if (Schema::hasTable('ims_shophelper_custom_products_combinations')) {
            return;
        }
        Schema::create('ims_shophelper_custom_products_combinations', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('product_id');
            $table->integer('fabric_id');
            $table->string('main_img');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('ims_shophelper_custom_products_combinations');
    }
}
