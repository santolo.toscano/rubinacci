<?php namespace Ims\Shophelper\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateImsShophelperProductReturns extends Migration
{
    public function up()
    {
        if (Schema::hasColumn('ims_shophelper_product_returns', 'order_id')) {
            return;
        }
        Schema::table('ims_shophelper_product_returns', function($table)
        {
            $table->renameColumn('orders_id', 'order_id');
        });
    }

    public function down()
    {
        Schema::table('ims_shophelper_product_returns', function($table)
        {
            $table->renameColumn('order_id', 'orders_id');
        });
    }
}
