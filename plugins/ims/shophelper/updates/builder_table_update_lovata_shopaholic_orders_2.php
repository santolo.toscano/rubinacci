<?php namespace Ims\Shophelper\Updates;

use October\Rain\Database\Updates\Migration;
use October\Rain\Support\Facades\Schema;

class BuilderTableUpdateLoavataShopaholicOrders2 extends Migration
{
    const TABLE_NAME = 'lovata_orders_shopaholic_orders';

    const COLUMN_NAME_1 = 'tracking_url_dhl';

    public function up()
    {

        Schema::table(self::TABLE_NAME, function($table)
        {
            
            $table->text(self::COLUMN_NAME_1);
            
        });
    }

    public function down()
    {
        Schema::table(self::TABLE_NAME, function($table)
        {
            $table->dropColumn(self::COLUMN_NAME_1);
        });
    }
}
