<?php namespace Ims\Shophelper\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateImsShophelperFestivita extends Migration
{
    public function up()
    {
        Schema::table('ims_shophelper_festivita', function($table)
        {
            $table->increments('id')->unsigned();
            $table->boolean('attivo')->default(0)->change();
        });
    }
    
    public function down()
    {
        Schema::table('ims_shophelper_festivita', function($table)
        {
            $table->dropColumn('id');
            $table->boolean('attivo')->default(null)->change();
        });
    }
}
