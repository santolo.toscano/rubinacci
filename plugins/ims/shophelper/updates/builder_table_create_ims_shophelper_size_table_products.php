<?php namespace Ims\Shophelper\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateImsShophelperSizeTableProducts extends Migration
{
    public function up()
    {
        Schema::create('ims_shophelper_size_table_products', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('size_table_id');
            $table->integer('product_id');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('ims_shophelper_size_table_products');
    }
}
