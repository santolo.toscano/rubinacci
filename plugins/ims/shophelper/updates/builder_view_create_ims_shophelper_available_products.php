<?php

namespace Ims\Shophelper\Updates;

use Illuminate\Support\Facades\DB;
use October\Rain\Database\Updates\Migration;

class BuilderViewCreateImsShophelperAvailableProducts extends Migration
{
  const VIEW_NAME = 'ims_shophelper_view_available_products';

  public function up()
  {
    DB::statement("CREATE OR REPLACE VIEW ". self::VIEW_NAME ." AS
      select products.name, sum(offers.quantity) as quantity, products.active, categories.name as category_name, brands.name as brand_name, products.code, products.slug, products.id, products.external_id
      from lovata_shopaholic_products products
      inner join lovata_shopaholic_offers offers on offers.product_id = products.id
      inner join lovata_shopaholic_categories categories on categories.id = products.category_id
      inner join lovata_shopaholic_brands brands on brands.id = products.brand_id
      where quantity > 0
      group by 1, 3, 4, 5, 6, 7, 8, 9
      order by products.id desc");
  }

  public function down()
  {
    DB::statement("DROP VIEW IF EXISTS ". self::VIEW_NAME);
  }
}