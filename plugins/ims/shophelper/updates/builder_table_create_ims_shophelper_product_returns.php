<?php namespace Ims\Shophelper\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateImsShophelperProductReturns extends Migration
{
    public function up()
    {
        if (Schema::hasTable('ims_shophelper_product_returns')) {
            return;
        }
        Schema::create('ims_shophelper_product_returns', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('orders_id');
            $table->integer('products_id');
            $table->integer('offers_id');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('ims_shophelper_product_returns');
    }
}
