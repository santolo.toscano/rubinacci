<?php namespace Ims\Shophelper\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateImsShophelperFeedsCategory extends Migration
{
    public function up()
    {
        Schema::create('ims_shophelper_feed_categories', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('number');
            $table->string('full_name', 500);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('ims_shophelper_feed_categories');
    }
}
