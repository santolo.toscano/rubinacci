<?php namespace Ims\Shophelper\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateImsShophelperProductReturns2 extends Migration
{
    public function up()
    {
        if (Schema::hasColumn('ims_shophelper_product_returns', 'product_id')
        && Schema::hasColumn('ims_shophelper_product_returns', 'offer_id')
        && !Schema::hasColumn('ims_shophelper_product_returns', 'products_id')
        && !Schema::hasColumn('ims_shophelper_product_returns', 'offers_id')
        ) {
            return;
        }
        Schema::table('ims_shophelper_product_returns', function($table)
        {
            $table->integer('product_id');
            $table->integer('offer_id');
            $table->dropColumn('products_id');
            $table->dropColumn('offers_id');
        });
    }

    public function down()
    {
        Schema::table('ims_shophelper_product_returns', function($table)
        {
            $table->dropColumn('product_id');
            $table->dropColumn('offer_id');
            $table->integer('products_id');
            $table->integer('offers_id');
        });
    }
}
