<?php namespace Ims\Shophelper\Updates;

use October\Rain\Database\Updates\Migration;
use October\Rain\Support\Facades\Schema;

class BuilderTableCreateImsShophelperHeadersPhrase extends Migration
{
    const TABLE_NAME = "ims_shophelper_headers_phrase";

    public function up()
    {
        if (Schema::hasTable(self::TABLE_NAME)) {
            return;
        }
        Schema::create(self::TABLE_NAME, function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->text('italiano')->nullable();
            $table->text('english')->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists(self::TABLE_NAME);
    }
}
