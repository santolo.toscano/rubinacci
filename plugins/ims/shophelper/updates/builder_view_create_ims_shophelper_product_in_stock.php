<?php

namespace Ims\Shophelper\Updates;

use Illuminate\Support\Facades\DB;
use October\Rain\Database\Updates\Migration;

class BuilderViewCreateImsShophelperProductInStock extends Migration
{
  const VIEW_NAME = 'ims_shophelper_view_product_stock';

  public function up()
  {
    $i = DB::statement("CREATE OR REPLACE VIEW ". self::VIEW_NAME ." AS
      SELECT p.name AS p_name, o.name AS o_name, o.quantity AS quantita
      FROM lovata_shopaholic_offers o
      INNER JOIN lovata_shopaholic_products p
      ON p.id = o.product_id
      WHERE o.quantity < 3
      GROUP BY p.id"
    );

  }

  public function down()
  {
    DB::statement("DROP VIEW IF EXISTS ". self::VIEW_NAME);
  }
}