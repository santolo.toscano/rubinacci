<?php namespace Ims\Shophelper\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateImsShophelperSizesTable3 extends Migration
{
    public function up()
    {
        if (Schema::hasColumn('ims_shophelper_sizes_table', 'size_table_name')) {
            return;
        }
        Schema::table('ims_shophelper_sizes_table', function($table)
        {
            $table->string('size_table_name', 255);
        });
    }

    public function down()
    {
        Schema::table('ims_shophelper_sizes_table', function($table)
        {
            $table->dropColumn('size_table_name');
        });
    }
}
