<?php

namespace Ims\Shophelper\Updates;

use October\Rain\Support\Facades\Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateImsShophelperAvailableEmailProduct extends Migration
{
    const TABLE_NAME = 'ims_shophelper_available_email_product';

    public function up()
    {
        if (Schema::hasTable(self::TABLE_NAME)) {
            return;
        }
        Schema::create(self::TABLE_NAME, function ($table) {
            $table->engine = 'InnoDB';
            $table->increments('id')->nullable(false)->unsigned();
            $table->text('email_user');
            $table->integer('product_id');
            $table->integer('offer_id');
        });
    }

    public function down()
    {
        Schema::dropIfExists(self::TABLE_NAME);
    }
}