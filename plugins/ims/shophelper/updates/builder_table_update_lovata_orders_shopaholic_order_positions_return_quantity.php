<?php namespace Ims\Shophelper\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateLoavataShopaholicOrderPositionsReturnQuantity extends Migration
{
    public function up()
    {
        Schema::table('lovata_orders_shopaholic_order_positions', function($table)
        {
            $table->integer('quantity_return')->default(0);
        });
    }
    
    public function down()
    {
        Schema::table('lovata_orders_shopaholic_order_positions', function($table)
        {
            $table->dropColumn('quantity_return');
        });
    }
}
