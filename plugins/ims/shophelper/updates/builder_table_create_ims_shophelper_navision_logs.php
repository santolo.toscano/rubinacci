<?php namespace Ims\Shophelper\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateImsShophelperNavisionLogs extends Migration
{
    public function up()
    {
        Schema::create('ims_shophelper_navision_logs', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('operation', 20);
            $table->integer('product_id');
            $table->string('offer_id', 50);
            $table->string('product_code', 50);
            $table->string('product_variant_nav', 20);
            $table->string('product_variant_name', 20);
            $table->string('property_name', 20);
            $table->integer('quantity_old')->nullable();
            $table->integer('quantity_new')->nullable();
            $table->text('error');
            $table->text('log');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('ims_shophelper_navision_logs');
    }
}
