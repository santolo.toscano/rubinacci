<?php namespace Ims\Shophelper\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateImsShophelperCustomElementImageLink extends Migration
{
    public function up()
    {
        Schema::create('ims_shophelper_custom_element_image_link', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->text('title_it')->nullable();
            $table->string('link_it', 255)->nullable();
            $table->text('additional_text_it')->nullable();
            $table->text('title_en')->nullable();
            $table->string('link_en', 255)->nullable();
            $table->text('additional_text_en')->nullable();
            $table->text('url_img')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('ims_shophelper_custom_element_image_link');
    }
}
