<?php namespace Ims\Shophelper\Components;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Ims\Shophelper\Helpers\VintageHelper;
use Ims\Shophelper\Models\CustomProductsCombinations;
use Ims\Shophelper\Models\SizeTable;
use Lang;
use Lovata\PropertiesShopaholic\Models\PropertyValue;
use Lovata\Shopaholic\Classes\Collection\CategoryCollection;
use Lovata\Shopaholic\Classes\Collection\OfferCollection;
use Lovata\Shopaholic\Classes\Collection\ProductCollection;
use Lovata\Shopaholic\Models\Category;
use Lovata\Shopaholic\Models\Offer;
use RainLab\Translate\Classes\Translator;
use System\Classes\PluginManager;
use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\Log;

use Illuminate\Support\Str;
use RainLab\Translate\Models\Locale as LocaleModel;
use October\Rain\Router\Router as RainRouter;
use Lovata\Toolbox\Classes\Component\ElementPage;
use Lovata\Shopaholic\Models\Product;
use Lovata\Shopaholic\Classes\Item\ProductItem;
use Event;

class ProductView extends ElementPage
{
    /**
     * @return array
     */
    public function componentDetails()
    {
        return [
            'name'        => 'IMS Shophelper - Product Details',
            'description' => 'IMS helper for Shopaholic e-commerce product details.',
        ];
    }

    /**
     * @return array
     */
    public function defineProperties()
    {
        $this->arPropertyList = array_merge($this->arPropertyList, $this->getElementPageProperties());
        if ($this->bHasWildCard) {
            $this->arPropertyList['has_wildcard'] = [
                'title'   => 'lovata.toolbox::lang.component.has_wildcard',
                'type'    => 'checkbox',
                'default' => 0,
            ];
        }

        $this->arPropertyList['skip_error'] = [
            'title'   => 'lovata.toolbox::lang.component.skip_error',
            'type'    => 'checkbox',
            'default' => 0,
        ];

        $this->arPropertyList['debug'] = [
            'title'   => 'Debug',
            'type'    => 'checkbox',
            'default' => 0,
        ];

        return $this->arPropertyList;
    }

    /**
     * Cicla le lingue attive ed estrae url in lingua
     * @param $obProduct Richiede istanza oggetto
     */
    public function getLocalizedPage($obProduct)
    {
        // Ottiene lingue attive
        $locales = collect(LocaleModel::listEnabled());

        // Transform it to contain the new urls
        $locales->transform(function ($item, $key) use ( $obProduct ) {
            return $this->translatePage($key, $obProduct);
        });

        $this->page['localesurl'] = $locales;
    }

    public function translatePage($locale, $item)
    {
        $translator = Translator::instance();
        $page = $this->getPage();
        $router = new RainRouter;

        $params = $this->getRouter()->getParameters();
        $oldLocale = $translator->getLocale();

        if ( $locale != $oldLocale )
        {
            $model = $item->getObject();
            $model->translateContext($locale);
            $params['slug'] = $model->slug;
            $model->translateContext($oldLocale);
        }

        $page->rewriteTranslatablePageUrl($locale);
        $localeUrl = $router->urlFromPattern($page->url, $params);
        return $translator->getPathInLocale($localeUrl, $locale);
    }

    public function getSizeTable($obProduct)
    {
        $prod = $obProduct->id;
        $sizetableS = DB::table('ims_shophelper_size_table_products')->where('product_id', $prod)->get();
        if($sizetableS->count()>0){
            $sizetable = SizeTable::find($sizetableS->first()->size_table_id);
        }else{
            $category = $obProduct->category;

            $sizetable = SizeTable::where('category_id', $category->id)->first();

            if ( !$sizetable ) {
                $category = $category->parent;
                if ($category->nest_depth != 0) {
                    $sizetable = SizeTable::where('category_id', $category->id)->first();
                }

                if ( !$sizetable ) {
                    $additional_categories = $obProduct->additional_category_id; // array con gli id delle categorie aggiuntive
                    $categorySize = DB::table('ims_shophelper_size_table_categories')->whereIn('category_id', $additional_categories)->get();
                    if ( $categorySize->count() )
                    {
                        $sizetable = SizeTable::find($categorySize->first()->size_table_id);
                    }
                }
            }
        }

        return $sizetable;
    }

    public function getProductsForOtherCategory($obProduct)
    {

        /** @var array $categories */
        $category = $obProduct->category;
        if (  $category->nest_depth != 0 )
        {
            $otherCatProducts = ProductCollection::make([])->active()->category($obProduct->category_id)->filterByQuantity()->page(1, 7);
        }
        else
        {
            $categories = $obProduct->additional_category_id;

            // Calcola la categoria per gli altri prodotti
            $otherCategory = null;
            foreach ($categories as $value) {
                // Esclude le macrocategorie
                if ($value > 4 && $value != 22) {
                    $otherCategory = $value;
                    break;
                }
            }

            if ($otherCategory) {
                // Implementare logiche RANDOM
                $otherCatProducts = ProductCollection::make([])->active()->category($otherCategory)->filterByQuantity()->page(1, 7);
            }
            else
            {
                $otherCatProducts = [];
            }
        }

        return $otherCatProducts;
    }


    public function onRun(){
        /** @var ProductItem $obProduct */
        $obProduct = $this->get();

        /* controllo se presente la slug :option, quindi significa che siamo nei prodotti personalizzati col tessuto e quindi devo recuperare il tessuto dall'id per varie cose(es. le img) */
        if($this->param('option')){
            $fabricProduct = Product::find($this->param('option'));
            $this->page['obFabric'] = $fabricProduct;

            $combination = CustomProductsCombinations::where('product_id', $obProduct->id)->where('fabric_id', $fabricProduct->id)->first();
            if(isset($combination)){
                $this->page['combination'] = $combination;
            }
        }

        /* Gestione controllo categoria del prodotto per evitare quando si è in vintage/tessuti di renderizzare più volte la stessa img poichè in fase di sviluppo abbiamo messo la stessa foto per frontale posteriore e galleria  */
        $catId = array(108, 109, 110);
        $this->page['isMultiImg'] = in_array($obProduct->category_id, $catId);

        if(!isset($obProduct)){
            $this->page['error404'] = true;
            $this->controller->setStatusCode(404);
            return;
        }

        if ( $this->property('debug') )
        {
            $product = Product::getByCode('MANNYL-08')->first();
            $obProduct = ProductCollection::make([$product->id])->first();
        }

        $this->getLocalizedPage($obProduct);
        $this->page['obProduct'] = $obProduct;

        $this->page['sizetable'] = $this->getSizeTable($obProduct);
        $this->page['obCategoryOtherProducts'] = $this->getProductsForOtherCategory($obProduct);


        /** @var OfferCollection $offerList */
        $offerList = $obProduct->offer; // Taglie disponibili
        $related = $obProduct->related->active()->filterByQuantity(); // Prodotti correlati

        /** @var Offer $offer */
        $offer = $obProduct->offer->first();


        $oneSize = ( $offer && ( str_contains(strtolower($offer->name), 'unica') || str_contains(strtolower($offer->name), 'size')));
        $this->page['isOneSize'] = $oneSize;

        /*$offerCollection = new Collection();
        foreach ( $offerList as $offerItem )
        {
            //$offerItem->property_order =
            //$offerCollection->push()
        }*/

        // Espone le offerte che hanno disponibilità
        $this->page['obOffer'] = $offer; // Verificare l'utilità...
        $this->page['obOfferList'] = $offerList;
        $this->page['obRelated'] = $related;
        $isSoldout = strtolower($offer->name) == 'soldout' ? true : false;// variabile esposta per gestire la grafica frontend
        $this->page['isSoldout'] = $isSoldout;

        $sizeInterSort = false;
        // Gestisce il sort
        try {
            if ($offer->property_value_array) {
                $array = $offer->property_value_array;
                $popertyValueId = array_first($array)[0];
                $popertyValue = PropertyValue::where('id', '=', $popertyValueId)->first();
                $sizeInterSort = $popertyValue->property_order ? true : false;
            }
        }
        catch ( \Exception $ex )
        {
            // logga errore
        }

        $this->page['isInternationalSort'] = $sizeInterSort;

        $isTessuto = VintageHelper::isTessuto($obProduct);
        $this->page['isTessuto'] = $isTessuto;

        if ( $isTessuto )
        {
            $isNotCustomOffer = null;
            $offerteAttive = 0;
            $offertaTessuto = null;
            // $obProduct->offer->count() non conta eveltuali offer con quantity=0, posso sfruttare $offerteAttive per controllare quando le offer non custom hanno quantità 0 per lanciare la 404
            if ($obProduct->offer->count() > 0) {// cambiato il controllo da >1 a >0 poichè anche se solo un'offer bisogna controllare se non è la custom - logica in parte errata perchè prima gestivamo male il decremento del tessuto, eventualmente da migliorare
                foreach($obProduct->offer as $offer){
                    if ($offer->price != 0){
                        $offerteAttive++;
                        if ( !$offertaTessuto ) {
                            $offertaTessuto = $offer;
                        }
                    }elseif (strtolower($offer->name) == 'soldout'){//altrimenti non risultano offerte attive nel controllo della pagina, che in caso negativo reindirizza alla 404
                        $offerteAttive++;
                    }
                }
            }
            $this->page['offertaTessuto'] = $offertaTessuto;
            $this->page['offerteAttive'] = $offerteAttive;
        }
        else
        {
            $this->page['offertaTessuto'] = null;
            $this->page['offerteAttive'] = $offerList->count();
        }

    }

    /**
     * Funzione personalizzata in quanto piu veloce del getbytransslug
     * @param $sElementSlug
     * @param $locale
     * @return ProductItem|null
     */
    public function getProductFromSlug($sElementSlug, $locale)
    {

        $obElement = null;
        $products = ProductCollection::make()->active()->getIDList();
        foreach ($products as $id) {
            $product = ProductItem::make($id);
            $slug = "slug|" . $locale;
            try {
                // funzione in errore in quanto non esiste lo slug, prova metodo classico
                if ( rtrim($product->$slug) == rtrim($sElementSlug) || rtrim($product->slug) == rtrim($sElementSlug) ) {
                    $obElement = $product;
                    break;
                }
            }
            catch ( \Exception $ex )
            {

            }
        }

        return $obElement;
    }

    /**
     * Controllo se nell'url è presente la lingua
     * @param String $locale minuscolo
     * @return bool
     */
    public function isUrlLocalized($locale)
    {
        $array = explode('/', $this->currentPageUrl());
        return $array[3] == $locale;
    }

    /**
     * Get element object
     * @param string $sElementSlug
     * @return Product
     */
    protected function getElementObject($sElementSlug)
    {
        if (empty($sElementSlug)) {
            return null;
        }

        $translator = Translator::instance();
        $locale = $translator->getLocale();

        if ($this->isSlugTranslatable() && $locale != $translator->getDefaultLocale() ) {
            // Va sugli oggetti in cache piuttosto che utilizzare la chiamata al model
            $obElement = $this->getProductFromSlug($sElementSlug, $locale);
            if ( !$obElement && !$this->isUrlLocalized('en') )
            {
                $obElement = Product::active()->getBySlug($sElementSlug)->first();
            }
            /* capire utilita
            if (!$this->checkTransSlug($obElement, $sElementSlug)) {
                $obElement = null;
            }
            */
        } else {
            //  Ottiene elemento lingua italiana
            $obElement = Product::active()->getBySlug($sElementSlug)->first();

            if ( !$obElement && !$this->isUrlLocalized('it') ) // Controlla che non ci sia it nell'url evitando la ricerca in inglese
            {
                // Effettua comunque la ricerca in inglese se url non specificato
                $obElement = $this->getProductFromSlug($sElementSlug, 'en');
            }

        }

        if (!empty($obElement)) {
            Event::fire('shopaholic.product.open', [$obElement]);
        }

        return $obElement;
    }

    /**
     * Make new element item
     * @param int     $iElementID
     * @param Product $obElement
     * @return ProductItem
     */
    protected function makeItem($iElementID, $obElement)
    {
        return ProductItem::make($iElementID, $obElement);
    }

}