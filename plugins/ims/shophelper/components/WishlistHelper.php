<?php namespace Ims\Shophelper\Components;

use Cms\Classes\ComponentBase;
use Lang;
use Log;
use Lovata\Shopaholic\Classes\Collection\ProductCollection;
use Redirect;
use Event;
use Validator;
use ValidationException;
use Lovata\WishListShopaholic\Classes\Helper\WishListHelper as lovatawishlisthelper;

class WishlistHelper extends ComponentBase
{
    /**
     * @return array
     */
    public function componentDetails()
    {
        return [
            'name'        => 'IMS Shophelper - Wishlist helper',
            'description' => 'IMS helper for Shopaholic e-commerce Wishlist page.',
        ];
    }

    public function onRun()
    {
        $obList = ProductCollection::make()->wishList()->active();
        $this->page['wishlist'] = $obList;
    }

    public function onRemoveFromWishlist()
    {
        $data = post();

        /** @var lovatawishlisthelper $obWishListHelper */
        $obWishListHelper = app(lovatawishlisthelper::class);

        $obWishListHelper->remove($data['product_id']);
        $arProductIDList = $obWishListHelper->getList();
        $this->page['wishlist'] = $arProductIDList;

        return [
            '#wishdata' => $this->renderPartial('wishlist')
        ];
    }

}