<?php namespace Ims\Shophelper\Components;

use Event;
use Validator;
use Cookie;
use Carbon\Carbon;
use ValidationException;
use Illuminate\Support\Str;
use Cms\Classes\ComponentBase;
use Lovata\Buddies\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Mail;
use Ims\Shophelper\Helpers\ImsHelper;
use Lovata\Buddies\Facades\AuthHelper;
use Lovata\Shopaholic\Models\Category;
use Ims\Shophelper\Helpers\EmailHelper;
use October\Rain\Support\Facades\Flash;
use Illuminate\Support\Facades\Redirect;
use RainLab\Translate\Classes\Translator;
use Ims\Shophelper\Models\NewsletterCustomer;
use Lovata\Buddies\Components\RestorePassword;
use Lovata\Shopaholic\Classes\Item\CategoryItem;
use Lovata\Toolbox\Classes\Component\ElementPage;
use Lovata\Shopaholic\Classes\Collection\ProductCollection;
use Ims\Shophelper\Models\SimpleCatalogHelper as SimpleCatalog;
use Lovata\Shopaholic\Components\CurrencyList as Currency;
use Input;

class SimpleCatalogHelper extends ElementPage
{
    /**
     * @return array
     */
    public function componentDetails()
    {
        return [
            'name'        => 'IMS Shophelper - Simple Catalog helper',
            'description' => 'IMS helper for Shopaholic e-commerce Catalog - simple version.',
        ];
    }

    public function onRun()
    {
        $startingDate = Carbon::create('2020', '11', '03', '00');
        $currentUser = AuthHelper::getUser();

        if( isset($currentUser) ){
            $currentUserCreatedAt = $currentUser->created_at;
            $datesComparision = $currentUserCreatedAt < $startingDate;
            $this->page['isOldUser'] = $datesComparision;
        }

        /*$productColletion = ProductCollection::make([]); // Il make utilizza la cache
        $productColletion->active();
        foreach($productColletion as $product){
            if($product->getObject()->ims_highlight == 1)
                $highlighted[$product->getObject()->id] = $product;
        };
        $this->page['highlighted'] = $highlighted;*/
        $this->page['highlighted'] = $this->gethighlighed();

        /*
        $component = new RestorePassword();
        $result = $component->sendRestoreMail(['email' => 'imsdimarcosomma@gmail.com']);
        */
        $email = get('customer-email-newsletter');
        if(isset($email)){
            $this->removeFromNewsletter($email);
        }
    }

    public function gethighlighed(){
        $products = SimpleCatalog::get()->sortByDesc('id');
        return $products;
    }

    public function onSearchForm(){
        $data = post();

        $translator = Translator::instance();
        $locale = $translator->getLocale();

        $searchUrl = ($locale == 'it' ? '/it/ricerca/' : '/en/search/').$data['search'];

        return Redirect::to($searchUrl);
    }

    /**
     * Get element object
     * @param string $sElementSlug
     * @return Category
     */
    protected function getElementObject($sElementSlug)
    {
        if (empty($sElementSlug)) {
            return null;
        }

        if (!$this->property('has_wildcard')) {
            $obElement = $this->getElementBySlug($sElementSlug);
        } else {
            $obElement = $this->getElementByWildcard($sElementSlug);
        }

        if (!empty($obElement)) {
            Event::fire('shopaholic.category.open', [$obElement]);
        }

        return $obElement;
    }

    /**
     * Get category by default
     * @param string $sElementSlug
     * @return Category|null
     */
    protected function getElementBySlug($sElementSlug)
    {
        if ($this->isSlugTranslatable()) {
            $obElement = Category::active()->transWhere('slug', $sElementSlug)->first();
            if (!$this->checkTransSlug($obElement, $sElementSlug)) {
                $obElement = null;
            }
        } else {
            $obElement = Category::active()->getBySlug($sElementSlug)->first();
        }

        return $obElement;
    }

    /**
     * Get category by wildcard
     * @param string $sElementSlug
     * @return Category|null
     */
    protected function getElementByWildcard($sElementSlug)
    {
        $arSlugList = explode('/', $sElementSlug);
        if (empty($arSlugList)) {
            return null;
        }

        $arSlugList = array_reverse($arSlugList);
        $sElementSlug = array_shift($arSlugList);

        $obElement = $this->getElementBySlug($sElementSlug);
        if (empty($obElement)) {
            return null;
        }

        if (empty($arSlugList) && empty($obElement->parent)) {
            return $obElement;
        }

        $obNestingElement = $obElement;

        foreach ($arSlugList as $sSlug) {
            $obNestingElement = $this->getNestingElement($sSlug, $obNestingElement);
            if (empty($obNestingElement)) {
                return null;
            }
        }

        if (!empty($obNestingElement->parent)) {
            return null;
        }

        return $obElement;
    }

    /**
     * Get nesting element
     * @param string   $sElementSlug
     * @param Category $obNestingElement
     * @return Category
     */
    protected function getNestingElement($sElementSlug, $obNestingElement)
    {
        if (empty($obNestingElement) || empty($sElementSlug)) {
            return null;
        }

        $obElement = $obNestingElement->parent;

        if (empty($obElement) || $obElement->slug != $sElementSlug) {
            return null;
        }

        return $obElement;
    }

    /**
     * Make new element item
     * @param int      $iElementID
     * @param Category $obElement
     * @return CategoryItem
     */
    protected function makeItem($iElementID, $obElement)
    {
        return CategoryItem::make($iElementID, $obElement);
    }

    public function onSendTailoredEmail(){
        $data = post();

        if(!isset($data['privacy'])){
            $data['privacy'] = '';
        }

        $validationData = [
            'name' => $data['name'],
            'surname' => $data['surname'],
            'email' => $data['email'],
            'phone' => $data['phone'],
            'location' => $data['location'],
            'date' => $data['date'],
            'privacy' => $data['privacy'],
        ];

        $rules = [
            'name' => 'required',
            'surname' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'location' => 'required',
            'date' => 'required',
            'privacy' => 'required',
        ];

        $messages = [
            'name.required' => Lang::get('ims.shophelper::lang.component_simple.error_name'),
            'surname.required' => Lang::get('ims.shophelper::lang.component_simple.error_surname'),
            'email.required' => Lang::get('ims.shophelper::lang.component_simple.error_email'),
            'phone.required' => Lang::get('ims.shophelper::lang.component_simple.error_phone'),
            'location.required' => Lang::get('ims.shophelper::lang.component_simple.error_location'),
            'date.required' => Lang::get('ims.shophelper::lang.component_simple.error_date'),
            'date.privacy' => Lang::get('ims.shophelper::lang.component_simple.error_privacy'),
        ];

        // Validazione campi standard
        $validator = Validator::make($validationData, $rules, $messages);

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        //$email = Settings::getValue('creating_order_manager_email_list');

        $emailTemplate = 'tailored_email';
        $locale = ImsHelper::getLocale();
        // Aggoimge la lingua al template email solo se diversa dalla lingua predefinita
        $translator = Translator::instance();
        if ( $locale != $translator->getDefaultLocale() )
        {
            $emailTemplate = $emailTemplate.'_'.$locale;
        }

        $emailData = [
            'name' => $data['name'],
            'surname' => $data['surname'],
            'email' => $data['email'],
            'phone' => $data['phone'],
            'location' => $data['location'],
            'date' => $data['date'],
            'privacy' => $data['privacy'],
            'note' => $data['note'],
            'site_url' => config('app.url'),
        ];

        Mail::send($emailTemplate, $emailData, function($message) use($data) {

            $message->to($data['email'], $data['name']);
            $message->cc('info@marianorubinacci.net');

        });

        return [
            '#gform_body' => $this->renderPartial('tailoredSuccess')
        ];

    }

    public function onNewsletterRegister(){
        $data = post();

        // rimuovo il cookie della chiusura con la X del popup
        if(isset($_COOKIE['newsletternotifyclosed'])) {
            setcookie("newsletternotifyclosed", "", 1);
        }
        // rimuovo il cookie della chiusura con la X del popup

        // aggiungo quello della newsletter
        if(!isset($_COOKIE['newsletternotify'])) {
            Cookie::queue(Cookie::forever('newsletternotify', $data['email']));
        }
        // aggiungo quello della newsletter

        $validationData = [
            'privacy' => $data['privacy'],
            'email' => $data['email'],
        ];
        $rules = [
            'privacy' => 'required|accepted',
            'email' => 'required|email',
        ];
        $messages = [
            'privacy.required' => Lang::get('ims.shophelper::lang.component_simple.error_privacy'),
            'privacy.accepted' => Lang::get('ims.shophelper::lang.component_simple.error_privacy'),
            'email.required' => Lang::get('ims.shophelper::lang.component_simple.error_email'),
        ];
        // Validazione campi standard
        $validator = Validator::make($validationData, $rules, $messages);
        if ($validator->fails()) {
            throw new ValidationException($validator);
        }
        $isSignIn = NewsletterCustomer::where('email', '=', $data['email'])->exists();
        //if (DB::table('ims_shophelper_newsletter')->where('email', '=', $data['email'])->exists()) {
        if ($isSignIn){
            \Flash::success('Attenzione, utente già iscritto alla newsletter');
        }else{
            //$newRecord = DB::table('ims_shophelper_newsletter')->insert(['email'=> $data['email']]);
            $newsletterCustomer = new NewsletterCustomer();
            $newsletterCustomer->email = $data['email'];
            $newsletterCustomer->save();

            if($newsletterCustomer){
                //Inviare email di conferma iscrizione
                EmailHelper::sendNewsletterConfirmation($data['email']);

                \Flash::success(Lang::get('ims.shophelper::lang.messages.newsletter_success'));
            }else{
                \Flash::warning(Lang::get('ims.shophelper::lang.messages.newsletter_fails'));
            }
        }
    }

    public function removeFromNewsletter($email){
        $this->page['email'] = $email;
        $isRemoved = false;
        if (isset($email)){
            $customer = NewsletterCustomer::where('email', '=', $email)->first();
            if ($customer){
                $isRemoved = $customer->delete();
            }else{
                $isRemoved = true;
            }

        }
        $this->page['isREmoved'] = $isRemoved;
    }

    public function onSwitch()
    {
        $sActiveCurrency = Input::get('currency');

        (new Currency)->switch($sActiveCurrency);
    }

}