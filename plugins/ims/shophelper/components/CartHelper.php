<?php namespace Ims\Shophelper\Components;

use Lang;
use Carbon\Carbon;
use Kharanenka\Helper\Result;
use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\DB;
use Lovata\Shopaholic\Models\Offer;
use Illuminate\Support\Facades\Input;
use Lovata\Shopaholic\Models\Product;
use Lovata\Buddies\Facades\AuthHelper;
use Lovata\OrdersShopaholic\Models\Cart;
use Lovata\CouponsShopaholic\Models\Coupon;
use Lovata\Shopaholic\Classes\Helper\TaxHelper;
use Lovata\Shopaholic\Classes\Item\ProductItem;
use Ims\Shophelper\Models\AvailableEmailProduct;
use Lovata\CouponsShopaholic\Models\CouponGroup;
use Lovata\CouponsShopaholic\Classes\Helper\CouponHelper;
use Lovata\OrdersShopaholic\Classes\Item\ShippingTypeItem;
use Lovata\CouponsShopaholic\Classes\Store\ProductListStore;
use Lovata\OrdersShopaholic\Classes\Processor\CartProcessor;
use Lovata\OrdersShopaholic\Classes\Processor\OfferCartPositionProcessor;

class CartHelper extends ComponentBase
{
    /**
     * @return array
     */
    public function componentDetails()
    {
        return [
            'name'        => 'IMS Shophelper - Cart helper',
            'description' => 'IMS helper for Shopaholic e-commerce Cart function.',
        ];
    }

    /**
     * @return array
     */
    public function defineProperties()
    {
        $properties = [
            'debug' => [
                'title'   => 'Debug',
                'type'    => 'checkbox',
                'default' => 0,
            ],
            'category_filters' => [
                'title'   => 'Category Filters',
                'type'    => 'checkbox',
                'default' => 0,
            ]
        ];

        return $properties;

    }

    public function onAddEmail(){
        $offer_id = Input::get('offer_id');
        $email_user = Input::get('email_user');

        $offer = Offer::where('id', $offer_id)->first();
        $available = AvailableEmailProduct::where('email_user',$email_user)->where('offer_id',$offer_id)->where('product_id',$offer->product_id)->get();
        if(count($available)==0){
            //inserisco il record solo nel caso in cui non sia già presente
            AvailableEmailProduct::insert([
                'email_user'=>$email_user,
                'product_id'=>$offer->product_id,
                'offer_id'=>$offer_id
            ]);
        }
        \Flash::success("Ti invieremo un messaggio quando il prodotto sarà disponibile.");
    }

    /**
     * Add product to cart
     * @return array
     */
    public function onAddToBasket()
    {
        $cartProcessor = CartProcessor::instance();

        $post = Input::get('cart')[0];
        $offer_id = $post['offer_id'];
        $customized_content = isset($post['customized_content']) ? $post['customized_content'] : '';
        $quantity = 1;

        if( $offer_id == 'notselect'){
            \Flash::warning(Lang::get('ims.shophelper::lang.component_cart.not_selected'));
            return;
        }

        $cartPositionCollection = $cartProcessor->get();
        $cart_id = $cartProcessor->getCartObject()->id;
        DB::update('update lovata_orders_shopaholic_carts set updated_at = ? where id = ?', [date('Y-m-d H:m:s'),$cart_id]);

        // Verifica se esiste gia nel carrello, nel caso aumenta quantita
        if ( $cartPositionCollection->hasOffer($offer_id) )
        {
            foreach ( $cartPositionCollection as $positionItem )
            {
                if ( $positionItem->offer->id == $offer_id )
                {
                    $quantity = $positionItem->quantity + 1;
                    // Verifica se nel carrello sto aggiungendo piu di quando ne ho in archivio
                    if ( $quantity > $positionItem->offer->quantity )
                    {
                        \Flash::error(Lang::get('ims.shophelper::lang.component_cart.product_not_added'));
                        return;
                    }
                }
            }
        }

        // Luca: spostato nel controllo se è un prodotto personalizzato/prenotazione tessuto in modo da controllare prima che tipo di prodotto sis ta aggiungendo al carrello
        /*$arRequestData = [
            0 => [
                'offer_id' => $post['offer_id'],
                'quantity' => $quantity
            ],
        ];*/
        // Luca: spostato dopo il controllo se è un prodotto personalizzato/prenotazione tessuto [la struttura del prodotto aggiunto è diversa]
        //$cartProcessor->add($arRequestData, OfferCartPositionProcessor::class);

        //Verifico se è un prodotto personalizzabile con le iniziali, in quel caso è presente un secondo elemento nell'array
        if ( $customized_content )
        {
            if($customized_content == '_isBespoke'){
                //aggiungo il prodotto come prenotazione tessuto, quindi nel campo "customized_content" aggiungo '_isBespoke' che mi serve per riconoscere il tipo di acquisto
                $arRequestData = [
                    0 => [
                        'offer_id' => $post['offer_id'],
                        'quantity' => $quantity,
                        'customized_offer_id' => 0,
                        'customized_content' => '#bespoke#'
                    ],
                ];
            }else {
                //aggiunta prodotto con iniziali personalizzate
                $arRequestData = [
                    0 => [
                        'offer_id' => $post['offer_id'],
                        'quantity' => $quantity
                    ],
                ];
                //aggiunge un record in più per le iniziali
                $this->addCustomizationToOffer($post['offer_id'], $post['customized_content']);
            }
        }else{
            // aggiunta prodotto normale
            $arRequestData = [
                0 => [
                    'offer_id' => $post['offer_id'],
                    'quantity' => $quantity
                ],
            ];
        }

        $cartProcessor->add($arRequestData, OfferCartPositionProcessor::class);

        Result::setData($cartProcessor->getCartData());

        \Flash::success(Lang::get('ims.shophelper::lang.component_cart.product_added'));

        return Result::get();
    }

    /**
     * @param ProductItem $obProduct
     * @return Offer|null
     */
    public function getOfferZeroPriced($obProduct)
    {
        $offerZeroPriced = null;
        /** @var Offer $offerItem */
        foreach($obProduct->offer as $offerItem ){
            if ( $offerItem->price == 0 )
            {
                $offerZeroPriced = $offerItem;
                break;
            }
        }
        return $offerZeroPriced;
    }

    /**
     * Add product to cart - Versione per i capi combinati al tessuto
     * @return array
     */
    public function onAddToBasketCustom()
    {
        $cartProcessor = CartProcessor::instance();

        $post = Input::get('cart')[0];

        $offer_id = $post['offer_id'];
        $customized_content = isset($post['customized_content']) ? $post['customized_content'] : '';
        $customized_options = isset($post['customized_options']) ? $post['customized_options'] : '';


        if( $offer_id == 'notselect'){
            \Flash::warning(Lang::get('ims.shophelper::lang.component_cart.not_selected'));
            return;
        }

        /** @var ProductItem $obProductFabric */
        $obProductFabric = ProductItem::make($post['fabric_id']);

        $obOfferZeroPriced = $this->getOfferZeroPriced($obProductFabric);
        if ( !$obOfferZeroPriced )
        {
            // errore le impostazioni del tessuto non consentono l'aggiunta al capo personalizzato, contattare l'assistenza
            \Flash::warning(Lang::get('ims.shophelper::lang.component_cart.fabric_not_allowed'));
            return;
        }
        $fabric_offer_id = $obOfferZeroPriced->id;
        $quantity = 1;



        $cartPositionCollection = $cartProcessor->get();

        // Verifica se esiste gia nel carrello, nel caso aumenta quantita
        if ( $cartPositionCollection->hasOffer($offer_id) )
        {
            foreach ( $cartPositionCollection as $positionItem )
            {
                if ( $positionItem->offer->id == $offer_id )
                {
                    $quantity = $positionItem->quantity + 1;
                    // Verifica se nel carrello sto aggiungendo piu di quando ne ho in archivio
                    if ( $quantity > $positionItem->offer->quantity )
                    {
                        \Flash::error(Lang::get('ims.shophelper::lang.component_cart.product_not_added'));
                        return;
                    }
                }
            }
        }

        //Verifico se è un prodotto personalizzabile con le iniziali, in quel caso è presente un secondo elemento nell'array
        if ( $customized_content )
        {
            // Verifico se presenti customized_options, quindi si tratta di una camicia
            if($customized_options){
                //preparo collection delle due info della camicia che poi convertirò in json
                $options = collect(['polsi' => $customized_options['option-1'], 'dett-bianchi' => $customized_options['option-2']]);
                $arRequestData = [
                    0 => [
                        'offer_id' => $post['offer_id'],
                        'quantity' => $quantity,
                        'customized_offer_id' => 0,
                        'customized_content' => $options->toJson(),//converto in json
                    ],
                ];
                //aggiunge un record in più per le iniziali
                $this->addCustomizationToOffer($post['offer_id'], $post['customized_content']);

                //aggiungere il tessuto da cui si è partito per confezionare il capo personalizzato
                $this->addFabricToOffer($post['offer_id'], $fabric_offer_id);
            }else {
                //aggiungo prodotto con iniziali personalizzate
                $arRequestData = [
                    0 => [
                        'offer_id' => $post['offer_id'],
                        'quantity' => $quantity
                    ],
                ];
                //aggiunge un record in più per le iniziali
                $this->addCustomizationToOffer($post['offer_id'], $post['customized_content']);

                //aggiungere il tessuto da cui si è partito per confezionare il capo personalizzato
                $this->addFabricToOffer($post['offer_id'], $fabric_offer_id);
            }
        }else{//aggiungo un prodotto senza personalizzazione delle iniziali
            // Verifico se presenti customized_options, quindi si tratta di una camicia
            if($customized_options) {
                //preparo collection delle due info della camicia che poi convertirò in json
                $options = collect(['polsi' => $customized_options['option-1'], 'dett-bianchi' => $customized_options['option-2']]);
                $arRequestData = [
                    0 => [
                        'offer_id' => $post['offer_id'],
                        'quantity' => $quantity,
                        'customized_offer_id' => 0,
                        'customized_content' => $options->toJson(),//converto in json
                    ],
                ];

                //aggiungere il tessuto da cui si è partito per confezionare il capo personalizzato
                $this->addFabricToOffer($post['offer_id'], $fabric_offer_id);
            }else {
                // aggiunta prodotto normale
                $arRequestData = [
                    0 => [
                        'offer_id' => $post['offer_id'],
                        'quantity' => $quantity
                    ],
                ];

                //aggiungere il tessuto da cui si è partito per confezionare il capo personalizzato
                $this->addFabricToOffer($post['offer_id'], $fabric_offer_id);
            }
        }


        $cartProcessor->add($arRequestData, OfferCartPositionProcessor::class);

        Result::setData($cartProcessor->getCartData());

        \Flash::success(Lang::get('ims.shophelper::lang.component_cart.product_added'));

        return Result::get();
    }

    /**
     * Update cart
     * @return array
     */
    public function onUpdateCart() // DA COMPLETARE PER CONTROLLARE LA QUANTITA' QUANDO SI AUMENTANO I PRODOTTI NEL CARRELLO
    {
        $data = Input::get('cart')[0];

        $cartProcessor = CartProcessor::instance();
        $cartPositionCollection = $cartProcessor->get();

        $offer_id = $data['offer_id'];

        // Verifica se esiste gia nel carrello, nel caso aumenta quantita
        if ( $cartPositionCollection->hasOffer($offer_id) )
        {
            foreach ( $cartPositionCollection as $positionItem )
            {
                if ( $positionItem->offer->id == $offer_id )
                {
                    // Verifica se nel carrello sto aggiungendo piu di quando ne ho in archivio
                    if ($positionItem->offer->quantity < $data['quantity'])
                    {
                        \Flash::error(Lang::get('ims.shophelper::lang.component_cart.product_update_ko'));
                        return;
                    }

                    $arRequestData = [
                        0 => $data,
                    ];
                }
            }
        }

        CartProcessor::instance()->update($arRequestData, OfferCartPositionProcessor::class);
        Result::setData(CartProcessor::instance()->getCartData());

        \Flash::success(Lang::get('ims.shophelper::lang.component_cart.product_update_ok'));

        return Result::get();
    }

    public function onApplyCoupon()
    {
        $code = Input::get('coupon');

        //Get coupon by code
        if (empty($code)) {
            \Flash::warning(Lang::get('ims.shophelper::lang.messages.coupon_insert'));
            return false;
        }

        if($this->applyCoupon($code)){
            return true;
        }else{
            return false;
        }
    }


    public function applyCoupon($code)
    {
        $obCoupon = Coupon::getByCode($code)->first();

        if (empty($obCoupon)) {
            $message = str_replace("{param}", $code, Lang::get('ims.shophelper::lang.messages.coupon_not_found'));
            \Flash::warning($message);
            return false;
        }

        $obCart = CartProcessor::instance()->getCartObject();

        $arCouponIDList = (array) $obCart->coupon()->lists('id');
        if ( count($arCouponIDList) > 0 )
        {
            $presente = in_array($obCoupon->id, $arCouponIDList);

            if ( $presente )
            {
                $message = str_replace("{param}", $code, Lang::get('ims.shophelper::lang.messages.coupon_already_exists'));
                \Flash::warning($message);
                return false;
            }
            else
            {
                $couponHelper = CouponHelper::instance();
                $couponHelper->clearCouponList();
            }
        }

        // gestione coupon - controllo utilizzo massimo su coupon o su user
        $maxUsage = $obCoupon->max_usage;
        $maxUsagePerUser = $obCoupon->coupon_group->max_usage_per_user;

        if ( $maxUsagePerUser ) {
            $user = AuthHelper::getUser();

            if ( !$user )
            {
                \Flash::warning(Lang::get('ims.shophelper::lang.messages.coupon_login'));
                return false;
            }

            // GESTIRE ANCHE SUGLI ORDINI NON ANDATI A BUON FINE
            $totalUsagePerUser = $obCoupon->order()->getByUser($user->id)->count();
            if ( $totalUsagePerUser >= $maxUsagePerUser )
            {
                 \Flash::warning(Lang::get('ims.shophelper::lang.messages.coupon_max_usage_user'));
                 return false;
            }
        }

        if ( $maxUsage )
        {
            $totalUsage = $obCoupon->total_usage;
            if ( $totalUsage >= $maxUsage   )
            {
                \Flash::warning(Lang::get('ims.shophelper::lang.messages.coupon_max_usage'));
                return false;
            }
        }

        //Luca - controllo aggiuntivo sulla scadenza del coupon
        $couponGroup = CouponGroup::find($obCoupon->group_id);
        if($couponGroup->date_end < Carbon::now()){
            \Flash::warning(Lang::get('ims.shophelper::lang.messages.coupon_expired'));
            return false;
        }


        $obCart->coupon()->add($obCoupon);


        CartProcessor::instance()->updateCartData();
        \Flash::success(Lang::get('ims.shophelper::lang.messages.coupon_applied'));

        return true;

        // NEL CASO LA FUNZIONE TORNA TRUE VA FATTO UN REFRESH DELLA PAGINA CHE PUO ESSERE FATTO VIA JAVASCRIPT
    }

    public function onRemoveCoupon()
    {
        // RIMUOVE IL COUPON
        $code = Input::get('coupon');

        //Get coupon by code
        $obCoupon = Coupon::getByCode($code)->first();
        if (empty($obCoupon)) {
            $message = str_replace("{param}", $code, Lang::get('ims.shophelper::lang.messages.coupon_not_found'));
            \Flash::warning($message);
            return false;
        }

        $obCart = CartProcessor::instance()->getCartObject();
        $obCart->coupon()->remove($obCoupon);

        return true;
    }

    public function addCustomizationToOffer($offerid, $customizedContent)
    {
        $product = Product::active()->getBySlug('custom')->first();

        $cartPositionCollection = CartProcessor::instance()->get();

        foreach ( $product->offer as  $offer )
        {
            // verifico se esiste gia una personalizzazione con questo offer, aggiungo la prima libera
            if ( !$cartPositionCollection->hasOffer($offer->id) )
            {
                $arRequestData = [
                    0 => [
                        'offer_id' => $offer->id,
                        'quantity' => 1,
                        'customized_offer_id' => $offerid,
                        'customized_content' => $customizedContent
                    ],
                ];
                $cartProcessor = CartProcessor::instance();
                $cartProcessor->add($arRequestData, OfferCartPositionProcessor::class);

                // Aggiunge la combinazione in sessione

                break;
            }
        }
    }

    public function addFabricToOffer($offerid, $fabric_id)
    {
        $arRequestData = [
            0 => [
                'offer_id' => $fabric_id,
                'quantity' => 1,
                'customized_offer_id' => $offerid,
            ],
        ];
        $cartProcessor = CartProcessor::instance();
        $cartProcessor->add($arRequestData, OfferCartPositionProcessor::class);
    }

    public function onRun()
    {
        $obCart = CartProcessor::instance()->getCartObject();

        // pulisco lista coupon se evntualmente presenti ma scaduti
        $coupon = $obCart->coupon()->first();
        if ($coupon) {
            $couponGroup = CouponGroup::find($coupon->group_id);
            if($couponGroup->date_end < Carbon::now()){
                $obCart->coupon()->detach();
            }
        }

        $this->page['arCouponList'] = $obCart->coupon;
        if($obCart->coupon->count() > 0) {
            $this->page['arCouponProducts'] = ProductListStore::instance()->coupon_group->get($obCart->coupon[0]->coupon_group->id, true);
            $this->page['arCouponDiscount'] = $obCart->coupon[0]->coupon_group->mechanism->discount_value;
        } else {
            $this->page['arCouponDiscount'] = 0;
        }
    }

    public function onApplyTaxes(){
        $countr = Input::get('country_code');
        $countr = (string)$countr;
        TaxHelper::instance()->switchCountry($countr);
        CartProcessor::instance()->updateCartData();
        Result::setData(CartProcessor::instance()->getCartData());
        return Result::get();
    }
    public function onNotApplyTaxes(){
        CartProcessor::instance()->updateCartData();
        Result::setData(CartProcessor::instance()->getCartData());
        return Result::get();
    }

    /**
     * Set active shipping type with using AJAX request
     * @deprecated
     */
    public function onSetShippingType()
    {
        $iShippingTypeID = Input::get('shipping_type_id');

        if (empty($iShippingTypeID)) {
            $sMessage = \Illuminate\Support\Facades\Lang::get('lovata.toolbox::lang.message.e_not_correct_request');
            Result::setFalse()->setMessage($sMessage);
            return Result::get();
        }

        //Get shipping type object
        $obShippingTypeItem = ShippingTypeItem::make($iShippingTypeID);
        if ($obShippingTypeItem->isEmpty()) {
            $sMessage = \Illuminate\Support\Facades\Lang::get('lovata.toolbox::lang.message.e_not_correct_request');
            Result::setFalse()->setMessage($sMessage);
            return Result::get();
        }
        CartProcessor::instance()->setActiveShippingType($obShippingTypeItem);
        Result::setData(CartProcessor::instance()->getCartData());

        return Result::get();
    }

    /**
     * Remove offers from cart
     * @return array
     */
    public function onRemoveFromCart()
    {
        $arRequestData = Input::get('cart');


        $sType = Input::get('type', 'offer');

        /* DA COMPLETARE
        //se in cart processor esiste in "prodotto personalizzato" con "customized_offer_id" uguale a quello del prodotto che si sta rimuovendo , allora si deve rimuovere anche quest'ultimo
        $cart = CartProcessor::instance();

        foreach($cart as $item){

            if ($item->id == '1791' and $item->customized_offer_id == $arRequestData){
                $cart->remove($item->id, OfferCartPositionProcessor::class, $sType);
            }
        }*/

        CartProcessor::instance()->remove($arRequestData, OfferCartPositionProcessor::class, $sType);
        Result::setData(CartProcessor::instance()->getCartData());

        return Result::get();
    }

}