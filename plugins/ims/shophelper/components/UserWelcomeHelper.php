<?php namespace Ims\Shophelper\Components;

use Carbon\Carbon;
use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Mail;
use Ims\Shophelper\Helpers\EmailHelper;
use Ims\Shophelper\Helpers\ImsHelper;
use Lovata\Buddies\Classes\Item\UserItem;
use Lovata\Buddies\Components\Buddies;
use Lovata\Buddies\Components\RestorePassword;
use Lovata\Buddies\Facades\AuthHelper;
use Lovata\Buddies\Models\User;
use Lovata\Shopaholic\Classes\Collection\ProductCollection;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Redirect;
use Lovata\Shopaholic\Classes\Item\CategoryItem;
use Lovata\Shopaholic\Models\Category;
use Lovata\Toolbox\Classes\Component\ElementPage;
use Event;
use October\Rain\Database\ModelException;
use October\Rain\Support\Facades\Flash;
use PharIo\Manifest\Email;
use Validator;
use ValidationException;
use Ims\Shophelper\Models\SimpleCatalogHelper as SimpleCatalog;

class UserWelcomeHelper extends ComponentBase
{


    /**
     * @return array
     */
    public function componentDetails()
    {
        return [
            'name'        => 'Shophelper - User Welcome Back',
            'description' => '',
        ];
    }

    public function onRun()
    {
        //$slug = $this->param('slug');
        //$user = User::where('external_id', '=', $slug)->first();

        /*$component = new RestorePassword();
        $result = $component->sendRestoreMail(['email' => 'imsdimarcosomma@gmail.com']);*/
    }


    /**
     *  Mostra i dettaglia dell'utente nel panel
     */
    public function onCheckEmail()
    {
        $post = post();
        $email = $post['email'];
        $slug = $this->param('slug');

        $user = User::getByEmail($email)->first();

        /** @var User $user */
        if ( $user && $user->external_id == $slug )
        {
            $user->activate();
            $user->save();
            Flash::success('email riconosciuta');
            /*
            return [
                '.panel-password' => $this->renderPartial('password', $user),
            ];
            */
            AuthHelper::login($user, true);

            $this->onChangePassword($post);
            return ['status' => true];
        }
        else
        {
            Flash::error('email non riconosciuta '.$email);
            return ['status' => false];
        }
    }

    public function onChangePassword()
    {
        //$post = post();
        $post = post();
        $email = $post['email'];
        $password = $post['password'];
        $passwordConfirmation = $post['password-confirmation'];
        $slug = $this->param('slug');

        $user = User::getByEmail($email)->first();
        if ( $user && $user->external_id == $slug )
        {
            if ( strlen($password) == 0 )
            {
                return ['status' => false, 'message' => Lang::get('ims.shophelper::lang.messages.new_password_request')];
            }

            //Set new password
            $user->password = $password;
            $user->password_confirmation = $passwordConfirmation;
            $user->reset_password_code = null;
            $user->activate();

            try {
                $user->save();
            }
            catch (ModelException $obException) {
                return ['status' => false, 'message' => Lang::get('ims.shophelper::lang.messages.error_password_request'). ' '. $obException->getMessage()];
            }

            Flash::success(Lang::get('ims.shophelper::lang.messages.correct_password_request'));

            EmailHelper::sendUserEmailWelcomeConfirmation($email);

            return ['status' => true, 'message' => Lang::get('ims.shophelper::lang.messages.thanks_password_request')];

            /*return [
                '#welcome-data-container' => $this->renderPartial('welcome-user-data')
            ];*/
        }
        else
        {
            return ['status' => false, 'message' => Lang::get('ims.shophelper::lang.messages.wrong_email_provided')];
        }
    }

    public function onUserRegistration(){
        $data = post();

        EmailHelper::sendUserRegistrationEmail($data['email']);
    }


}