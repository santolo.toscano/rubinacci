<?php namespace Ims\Shophelper\Components;

use Carbon\Carbon;
use Cms\Classes\ComponentBase;
use Event;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Mail;
use Ims\Shophelper\Helpers\ImsHelper;
use Lovata\OrdersShopaholic\Models\Order;
use Lovata\OrdersShopaholic\Models\OrderPosition;
use Lovata\Shopaholic\Models\Settings;
use RainLab\Translate\Classes\Translator;
use Validator;
use ValidationException;

class OrderReturnHelper extends ComponentBase
{


    /**
     * @return array
     */
    public function componentDetails()
    {
        return [
            'name'        => 'Shophelper - Order Return Helper',
            'description' => '',
        ];
    }


    public function onRun()
    {
        //recupero ordine e controllo se la position da rendere appartiene a quell'ordine
        $secret = Input::get("order");
        $positionId = Input::get("position");

        if(isset($secret)) {
            $order = Order::getBySecretKey($secret)->first();
            if (isset($order) && $order->order_position->find($positionId) ) {
                $currentOrderPosition = $order->order_position->find($positionId);
                $customPositions = OrderPosition::where('order_id', $order->id)->where('customized_offer_id', $currentOrderPosition->item_id)->select('id')->get();

                if($currentOrderPosition->customized_offer_id != 0 or $customPositions->isNotEmpty()){
                    $this->page['notReturnable'] = 1;
                }else{
                    $this->page['notReturnable'] = 0;
                }
                $product = $order->order_position->find($positionId);
                $this->page['product'] = $product;
            }else{
                $this->page['product'] = 'empty';
            }

        }else{
            $this->page['product'] = 'empty';
        }

    }

    public function onReturnItem(){
        $data = Input::all();
        $order = Order::getBySecretKey($data['order'])->first();
        $locale = ImsHelper::getLocale();

        // recupero il prodotto dell'ordine(in realtà è la riga dell'ordine) da rendere, tramite l'id della position(in realtà è l'id della riga) che ho passato in get
        $orderPosition = OrderPosition::find($data['position']);

        //controllo se l'id dell'ordine legato al prodotto recuperato corrisponde a quello dell'ordine passato sempre in get(che sarebbe l'ordine aperto) : se l'esito è positivo setto tutti i parametri del reso e salvo
        if($orderPosition->order_id == $order->id){
            $orderPosition->is_returned = 1; // oggetto ritornato
            $orderPosition->returned_date = Carbon::now(); // data del reso
            $orderPosition->returned_status = 'start'; // stato del reso
            $orderPosition->returned_reason = $data['reason']; // motivo del reso
            $orderPosition->returned_note = $data['returned_note']; // note del reso
            $orderPosition->quantity_return = $data['returned_quantity']; // note del reso

            $orderPosition->save();
        }

        // recupero l'offer vera è propria ed il prodotto originale - li recupero tutti poichè i valori che ci servono ad esempio nella mail, non sono contenuri tutti nella riga dell'ordine
        $orderPositionOffer = $orderPosition->offer;
        $orderPositionOfferProduct = $orderPosition->offer->product;

        /* vecchia implementazione molto grezza che agiva sul db direttamente - da cancellare!!
        Db::table('lovata_orders_shopaholic_order_positions')->where('id', $data['orderPositionId'])->update(['is_returned' => 1, 'returned_date' => Carbon::now(), 'returned_status' => 'start', 'returned_reason' => $data['reason'], 'returned_note' => $data['returned_note']]);

        //RECUPERO IL PRODOTTO RITORNATO PER PASSARLO ALL'EMAIL
        $returnedProductPosition = Db::table('lovata_orders_shopaholic_order_positions')->where('id', $data['orderPositionId'])->first();
        $returnedProductOffer = Db::table('lovata_shopaholic_offers')->where('id', $returnedProductPosition->item_id)->first();
        $returnedProductOriginal = Db::table('lovata_shopaholic_products')->where('id', $returnedProductOffer->product_id)->first();
        */

        // messaggio di presa in carico del reso ed invio email a cliente el gestore
        \Flash::success(Lang::get('ims.shophelper::lang.component_order.return'));
        $userEmail = $order['email'];
        $userMailTemplate = 'user_return';
        // Aggoimge la lingua al template email solo se diversa dalla lingua predefinita
        $translator = Translator::instance();
        if ( $locale != $translator->getDefaultLocale() )
        {
            $userMailTemplate = $userMailTemplate.'_'.$locale;
        }

        $userMailData = [
            'returnedProductPosition' => $orderPosition,
            'returnedProductOffer' => $orderPositionOffer,
            'returnedProductOriginal' => $orderPositionOfferProduct,
            'order' => $order,
            /*'userName' => $order['property']['name'],
            'userSurname' => $order['property']['last_name'],*/
            'oggi' => Carbon::now(),
            'site_url' => config('app.url'),
        ];
        Mail::sendTo($userEmail, $userMailTemplate, $userMailData);

        $managerEmail = Settings::getValue('creating_order_manager_email_list');
        //Process email list
        if (is_string($managerEmail)) {
            $arEmailList = explode(',', $managerEmail);
        } else {
            $arEmailList = $managerEmail;
        }

        $managerMailTemplate = 'user_return';
        $managerMailData = [
            'returnedProductPosition' => $orderPosition,
            'returnedProductOffer' => $orderPositionOffer,
            'returnedProductOriginal' => $orderPositionOfferProduct,
            'order' => $order,
            'user_email' => $order['email'],
            'oggi' => Carbon::now(),
            'site_url' => config('app.url'),
        ];
        Mail::sendTo($arEmailList, $managerMailTemplate, $managerMailData);

        /*$orderProducts = Db::table('lovata_orders_shopaholic_order_positions')->where('order_id', $data['order_id'])->get();

        $orderProductsDetails = $orderProducts->map(function ($item){
            $offerProduct = Db::table('lovata_shopaholic_offers')->where('id', $item->item_id)->first();
            $originalProduct = Db::table('lovata_shopaholic_products')->where('id', $offerProduct->product_id)->first();

            $orderProductDetails = [
                'orderProduct' => $item,
                'offerProduct' => $offerProduct,
                'originalProduct' => $originalProduct
            ];

            return $orderProductDetails;
        });

        $productList = [
            'orderProductsDetails' => collect($orderProductsDetails),
            'orderNumber' => $data['order_num'],
            'orderId' => $data['order_id'],
        ];

        return [
            '#orderdata' => $this->renderPartial('order-details', $productList)
        ];*/

    }

}