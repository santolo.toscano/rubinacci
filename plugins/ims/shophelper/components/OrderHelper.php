<?php namespace Ims\Shophelper\Components;

use Guzzle\Common\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Log;
use Ims\Shophelper\Classes\ImsShippingManager;
use Ims\Shophelper\Helpers\EmailHelper;
use Ims\Shophelper\Helpers\ImsHelper;
use Ims\Shophelper\Helpers\ImsUserHelper;
use Ims\Shophelper\Helpers\PaymentGatewayHelper;
use Ims\Shophelper\Models\Festivita;
use Kharanenka\Helper\Result;
use Illuminate\Support\Facades\Redirect;
use Lovata\Buddies\Classes\Item\UserItem;
use Lovata\Buddies\Facades\AuthHelper;
use Lovata\OrdersShopaholic\Classes\Collection\ShippingTypeCollection;
use Lovata\OrdersShopaholic\Classes\Item\OrderItem;
use Lovata\OrdersShopaholic\Classes\Item\ShippingTypeItem;
use Lovata\OrdersShopaholic\Classes\Processor\CartProcessor;
use Lovata\OrdersShopaholic\Classes\Processor\OrderProcessor;
use Lovata\OrdersShopaholic\Models\CartPosition;
use Lovata\OrdersShopaholic\Models\Order;
use Lovata\Ordersshopaholic\Models\UserAddress;
use Lovata\Shopaholic\Classes\Helper\CurrencyHelper;
use Lovata\Shopaholic\Models\Currency;
use Lovata\Shopaholic\Models\Offer;
use Lovata\Toolbox\Classes\Component\ComponentSubmitForm;
use Lovata\Toolbox\Classes\Helper\PageHelper;
use Lovata\Toolbox\Classes\Helper\UserHelper;
use Lovata\Shopaholic\Models\Settings;
use Lovata\Toolbox\Traits\Helpers\TraitValidationHelper;
use Event;
use Ims\Shophelper\Classes\Dhlshipmentsbodyc\Quantity;
use Lovata\OrdersShopaholic\Classes\Item\PaymentMethodItem;
use Lovata\OrdersShopaholic\Models\PaymentMethod;
use Lovata\OrdersShopaholic\Models\ShippingType;
use October\Rain\Support\Facades\Flash;
use stdClass;
use Str;
use Stripe\Customer;
use Stripe\PaymentIntent;
use Stripe\Stripe;
use Validator;
use ValidationException;

class OrderHelper extends ComponentSubmitForm
{
    /**
     * @return array
     */
    public function componentDetails()
    {
        return [
            'name'        => 'IMS Shophelper - Order helper',
            'description' => 'IMS helper for Shopaholic e-commerce order maker.',
        ];
    }


    use TraitValidationHelper;

    protected $bCreateNewUser = true;
    protected $arOrderData = [];
    protected $arUserData = [];
    protected $arBillingAddressOrder = [];
    protected $arShippingAddressOrder = [];
    protected $obUser;
    protected $obOrder;
    protected $obPaymentGateway;
    public $ordineSF;
    
    /**
     * @return array
     */
    public function defineProperties()
    {
        $arResult = $this->getModeProperty();

        return $arResult;
    }

    /**
     * Init plugin method
     */
    public function init()
    {
        $this->bCreateNewUser = Settings::getValue('create_new_user');
        $this->obUser = UserHelper::instance()->getUser();

        parent::init();
    }

    public function testOpenOrder($number, $sendmail)
    {
        $order = OrderItem::make($number);

        //dd($order->payment_method->getObject(), $order->payment_method->getObject()->gateway_property);

        $arrayTotale = [
            "subtotale" =>  $order->position_total_price,
            "totale" => $order->total_price,
            "coupon" => $order->coupon_list,
            "pagamento" => $order->payment_method->name,
            "stato" => $order->status->name
        ];

        /*
        foreach ( $order->order_promo_mechanism as $promo ) {
            $isPromo = array_key_exists('code', $promo->element_data);
        }
        */

        $colletion = new Collection();

        foreach ( $order->order_position as $item )
        {

            if ( $item->discount_total_price ) {
                $array = [
                    'oldprice' => $item->old_price,
                    'qt' => $item->quantity,
                    'prezzo singolo intero' => $item->old_price ?: $item->price,
                    'prezzo singolo scontato' => $item->total_price / $item->quantity,
                    'prezzo totale riga' => $item->total_price_value
                ];
            } else
            {
                $array = [
                    'oldprice' => $item->old_price,
                    'qt' => $item->quantity,
                    'prezzo singolo' => $item->price_value,
                    'prezzo totale riga' => $item->total_price_value
                ];
            }

            $colletion->add($item->id, $array);

        }

        $colletion->add("generali", $arrayTotale);

        if ( $sendmail ) {
            EmailHelper::sendEmailUser($order, 'it');
        }


        //dd($order, $colletion, 'email sent');

        //dd($order->coupon_list);
        $this->page['collection'] = $colletion;
        $this->page['order'] = $order;
    }

    /**
     * Legge gli oggetti dal carrello prima che vengono svuotati
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getCartPositions()
    {
        $cartId = CartProcessor::instance()->getCartObject()->id;
        return CartPosition::getByCart($cartId)->get();
    }

    /**
     * Set active shipping type with using AJAX request
     * @deprecated
     */
    public function onSetShippingType()
    {
        $iShippingTypeID = Input::get('shipping_type_id');

        return $this->changeShippingType($iShippingTypeID);
    }

    public function changeShippingType($shippingType)
    {
        if ( !$shippingType) {
            $sMessage = \Illuminate\Support\Facades\Lang::get('lovata.toolbox::lang.message.e_not_correct_request');
            Result::setFalse()->setMessage($sMessage);
            return Result::get();
        }

        //Get shipping type object
        $obShippingTypeItem = ShippingTypeItem::make($shippingType);
        if ($obShippingTypeItem->isEmpty()) {
            $sMessage = \Illuminate\Support\Facades\Lang::get('lovata.toolbox::lang.message.e_not_correct_request');
            Result::setFalse()->setMessage($sMessage);
            return Result::get();
        }

        CartProcessor::instance()->setActiveShippingType($obShippingTypeItem);
        Result::setData(CartProcessor::instance()->getCartData());

        return Result::get();
    }

    public function shipmentList($state)
    {
        //$sessionCode = Session::get('shipment', 'IT');
        $shipmentList = ShippingTypeCollection::make(null)->active()->sort()->available();

        $filteredShipmentList = collect([]);
        $alternativeShipmentList = collect([]);

        $isStateEu = ImsShippingManager::isEU($state);

        foreach ($shipmentList as $shipment){
            if( $isStateEu and Str::startsWith($shipment->code, 'it') ){
                $filteredShipmentList->push($shipment);
            } elseif ( !$isStateEu and !Str::startsWith($shipment->code, 'it')){
                $filteredShipmentList->push($shipment);
            }else{
                $alternativeShipmentList->push($shipment);
            }
        }

        $data = [
            'filteredShipmentList' => $filteredShipmentList,
            'alternativeShipmentList' => $alternativeShipmentList,
        ];

        return $data;
    }

    public function prepareUserData()
    {
        $obUser = AuthHelper::getUser();
        $this->page['logged'] = isset($obUser) ? 1 : 0;
        $defaultShipping = 'IT';

        if( isset($obUser) ) {
            $obUser = UserItem::make($obUser->id);
            $userData = ImsUserHelper::parseUserData($obUser);
            $shippingState = $userData['shipping_state'];
            $this->page['imsUserData'] = $userData;
        }
        else
        {
            $shippingState = $defaultShipping;
        }

        $shipmentLists = $this->shipmentList($shippingState);

        $activeShipping = $shipmentLists['filteredShipmentList'][0];
        $obShippingTypeItem = ShippingTypeItem::make($activeShipping->id);

        $cartProcessor = CartProcessor::instance();
        $cartProcessor->setActiveShippingType($obShippingTypeItem);

        $this->page['obUser'] = $obUser;
        $this->page['obCart'] = $cartProcessor->get();

        $this->page['shipmentList'] = $shipmentLists['filteredShipmentList'];
        $this->page['alternativeShipmentList'] = $shipmentLists['alternativeShipmentList'];
    }

    public function onRun() {
        //reperimento chiavi pubbliche per metodi di pagamento stripe3d e stripe3d_apple
        $payments = PaymentMethod::all();
        foreach ($payments as $payment) {
            if($payment->code === "stripe3d") {
                $this->page['pk_stripe3d'] = $payment->gateway_property['api_key_public'];
            } else if($payment->code === "stripe3d_apple") {
                $this->page['pk_stripe3d_apple'] = $payment->gateway_property['api_key_public'];
            }
        }

        $this->prepareUserData();

        //recupero ordine per dettagli ordine nell'area utente
        $secret = Input::get("order");

        if(isset($secret)){
            $order = Order::getBySecretKey($secret)->first();
            if(isset($order)) {
                $this->page['orderObj'] = $order;
                $order = OrderItem::make($order->id);
                //ho bisogno di recuperare dall'order position lo slug dell'offer
            }else {
                $order = 'empty';
            }
        }else{
            $order = 'empty';
        }

        $this->page['order'] = $order;


    }

    /**
     * @param $arOrderData
     * @param $arUserData
     * @return OrderItem|null
     */
    public function createOrderCustomized($arOrderData, $arUserData)
    {
        $cartPositions = $this->getCartPositions();
        $order = $this->create($arOrderData, $arUserData);
        try {
            $this->orderAddCustomization($order, $cartPositions);
        }
        catch ( \Exception $ex )
        {

        }

        $order = $order ? OrderItem::makeNoCache($order->id) : null;


        return $order;
    }

    public function resultAppendData($moredata)
    {
        $data = Result::data();
        $data = array_merge($data ?: [], $moredata);
        Result::setData($data);
    }

    public function isState($state) {
        return $this->isUeState($state) || $this->isExtraUeState($state);
    }

    public function isExtraUeState($state) {
        return in_array($state, array('AF','AX','AL','DZ','AS','AD','AO','AI','AQ','AG','AR','AM','AW','AU','AZ','BS','BH','BD','BB','BY','BZ','BJ','BM','BT','BO','BQ','BA','BW','BV','BR','IO','BN','BF','BI','KH','CM','CA','CV','KY','CF','TD','CL','CN','CX','CC','CO','KM','CG','CD','CK','CR','CI','CU','CW','DJ','DM','DO','EC','EG','SV','GQ','ER','ET','FK','FO','FJ','GF','PF','TF','GA','GM','GE','GH','GI','GL','GD','GP','GU','GT','GG','GN','GW','GY','HT','HM','VA','HN','HK','IS','IN','ID','IR','IQ','IM','IL','JM','JP','JE','JO','KZ','KE','KI','KP','KR','KW','KG','LA','LB','LS','LR','LY','LI','MO','MK','MG','MW','MY','MV','ML','MH','MQ','MR','MU','YT','MX','FM','MD','MN','ME','MS','MA','MZ','MM','NA','NR','NP','NC','NZ','NI','NE','NG','NU','NF','MP','NO','OM','PK','PW','PS','PA','PG','PY','PE','PH','PN','PR','QA','RE','RW','BL','SH','KN','LC','MF','PM','VC','WS','SM','ST','SA','SN','RS','SC','SL','SG','SX','SB','SO','ZA','GS','SS','LK','SD','SR','SJ','SZ','CH','SY','TW','TJ','TZ','TH','TL','TG','TK','TO','TT','TN','TR','TM','TC','TV','UG','UA','AE','GB','US','UM','UY','UZ','VU','VE','VN','VG','VI','WF','EH','YE','ZM','ZW'));
    }

    public function isUeState($state) {
        return in_array($state, array('IT', 'AT', 'BE', 'BG', 'CY', 'CZ', 'DE', 'DK', 'EE', 'ES', 'FI', 'FR', 'GR', 'EL', 'HR', 'HU', 'IE', 'LT', 'LU', 'LV', 'MC', 'MT', 'NL', 'PL', 'PT', 'RO', 'SE', 'SI', 'SK'));
    }

    public function isItState($state) {
        return $state === 'IT';
    }

    public function onGetPrices() {
        $state = post("state");
        //controllo presenza stato
        if(!$state || !$this->isState($state)) {
            return [
                "status" => false,
                "error" => "missing state",
            ];
        }

        //ritorno prezzi con stato a true
        return [
            "status" => true,
            "data" => $this->getPrices($state, post("dazi")),
        ];
    }

    private function getPrices($state, $dazi = "No") {
        //TODO: capire se reperibili da qualche parte
        $free_ship_price = 350;

        //definizione oggetto di ritorno
        $ret = new stdClass();

        //reperimento moltiplicatore vat in base allo stato
        $ret->vat = VatHelper::getVatFromState($state);
        //reperisco il vat italiano per estrapolare l'imponibile dal totale
        $itVatMultiplier = VatHelper::getVatMultiplierFromState();
        //reperisco il vat dello stato per calcolare l'iva e il totale
        $vatMultiplier = VatHelper::getVatMultiplier($ret->vat);
        //reperisco il rate della currency (es: €=1, $=1.2)
        $currency_rate = floatval(CurrencyHelper::instance()->getActive()->rate);

        //prendo il total price dal cart (calcolato in automatico e dipendente dalla currency)
        $original_cart_price = round(CartProcessor::instance()->get()->getTotalPriceValue());

        //controllo se l'iva di partenza (italia) e l'iva di destinazione ($state) coincidono
        if($vatMultiplier === $itVatMultiplier) {
            $ret->cart_price = $original_cart_price;
        } else {
            //calcolo il total price scorporando l'iva italiana e incorporando l'iva del paese selezionato
            $ret->cart_price = round(($original_cart_price / $itVatMultiplier) * $vatMultiplier);
        }

        //calcolo tax
        $ret->no_tax_price = round($ret->cart_price/$vatMultiplier);
        $ret->tax_price = $ret->cart_price - $ret->no_tax_price;

        //conversione prezzi a €
        $ret->eur_cart_price = round($ret->cart_price/$currency_rate);
        $ret->eur_no_tax_price = round($ret->eur_cart_price/$vatMultiplier);
        $ret->eur_tax_price = $ret->eur_cart_price - $ret->eur_no_tax_price;

        //INIZIO calcolo spedizione
        //controllo stato ue/extra ue
        $ret->is_ue_state = $this->isUeState($state);
        //in caso di ue prendo il prezzo comprensivo di iva, altrimenti quello senza iva
        $prezzo_da_valutare = $ret->is_ue_state ? $ret->eur_cart_price : $ret->eur_no_tax_price;
        if($prezzo_da_valutare >= $free_ship_price) {
            //caso spedizione gratuita
            $ret->shipping_type_code = "it-free";
        } else {
            if($this->isItState($state)) {
                //caso spedizione it
                $ret->shipping_type_code = "nuovaitalia-express";
            } else if($ret->is_ue_state) {
                //caso spedizione ue
                $ret->shipping_type_code = "nuovaeuropa-express";
            } else {
                //caso spedizione extra ue
                $ret->shipping_type_code = "nuovaextraue-express";
            }
        }
        //reperisco il metodo di spedizione tramite code
        $shipping_type = ShippingType::getByCode($ret->shipping_type_code)->first();
        //salvo l'id e il prezzo
        $ret->shipping_type_id = $shipping_type->id;
        $ret->eur_shipping_price = floatval($shipping_type->price);
        //FINE calcolo spedizione

        //calcolo eventuali dazi (30% del prezzo in euro senza iva)
        if(!$ret->is_ue_state && $dazi === "Si") {
            $ret->eur_dazi_price = round(($ret->eur_no_tax_price * 3) / 10);
        } else {
            $ret->eur_dazi_price = 0;
        }

        //calcolo prezzo totale (tot+sped+dazi)
        $ret->eur_total_price = $prezzo_da_valutare + $ret->eur_shipping_price + $ret->eur_dazi_price;

        //calcolo valori in valuta selezionata
        $ret->shipping_price = round($ret->eur_shipping_price * $currency_rate);
        $ret->dazi_price = round($ret->eur_dazi_price * $currency_rate);
        $ret->total_price = round($ret->eur_total_price * $currency_rate);

        return $ret;
    }

    private function createOrderValidationData($data) {
        $validationData = [
            'name' => $data['user']['name'],
            'last_name' => $data['user']['last_name'],
            'email' => trim($data['user']['email']),
            'prefix' => $data['user']['prefix'],
            'phone' => $data['user']['phone'],
            'state' => $data['order']['property']['state'],
            'city' => $data['order']['property']['city'],
            'address' => $data['order']['property']['address1'],
            'postcode' => $data['order']['property']['postcode'],
            'payment_method_id' => $data['order']['payment_method_id'],
        ];

        $rules = [
            'name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email',
            'prefix' => 'required',
            'phone' => 'required',
            'state' => 'required',
            'city' => 'required',
            'address' => 'required',
            'postcode' => 'required',
            'payment_method_id' => 'required',
        ];

        $messages = [
            'name.required' => Lang::get('ims.shophelper::lang.component_order.error_name'),
            'last_name.required' => Lang::get('ims.shophelper::lang.component_order.error_last_name'),
            'email.required' => Lang::get('ims.shophelper::lang.component_order.error_email'),
            'prefix.required' => Lang::get('ims.shophelper::lang.component_order.error_prefix'),
            'phone.required' => Lang::get('ims.shophelper::lang.component_order.error_phone'),
            'state.required' => Lang::get('ims.shophelper::lang.component_order.error_state'),
            'city.required' => Lang::get('ims.shophelper::lang.component_order.error_city'),
            'address.required' => Lang::get('ims.shophelper::lang.component_order.error_address'),
            'postcode.required' => Lang::get('ims.shophelper::lang.component_order.error_postcode'),
            'payment_method_id.required' => 'payment_method_id required', //TODO: aggiungere messaggio errore payment_method_id "Errore: Inserire il metodo di pagamento, è richiesto per proseguire"
        ];

        if ($data['order']['property']['state'] == 'IT') {
            $validationData['cf'] = $data['user']['cf'];
            $rules['cf'] = 'required|size:16';
            $messages['cf.required'] = Lang::get('ims.shophelper::lang.component_order.error_cf');
            $messages['cf.size'] = Lang::get('ims.shophelper::lang.component_order.error_cf_size');
        }

        // Validazione campi standard
        $validator = Validator::make($validationData, $rules, $messages);

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }
    }

    /**
     * Create new order (AJAX)
     * @return \Illuminate\Http\RedirectResponse|array
     * @throws \Exception
     */
    public function onCreateOrder()
    {
        $data = post();

        //validazione input
        $this->createOrderValidationData($data);

        //Calcolo di tutti i prezzi legati al carrello
        $prices = $this->getPrices($data['order']['property']['state'], $data['order']['dazi_e_dogana']);

        //definizione oggetti per creazione ordine
        $arOrderData = [
            'prezzo_tot' => $prices->total_price,
            'conv_prezzo_tot' => $prices->eur_total_price,
            'dazi_value' => $prices->eur_dazi_price,
            'shipping_type_id' => $prices->shipping_type_id,
            'payment_method_id' => $data['order']['payment_method_id'],
            'giftbox' => $data['order']['giftbox'],
            'packaging' => $data['order']['packaging'],
            'dazi_e_dogana' => $data['order']['dazi_e_dogana'],
            'info_pagamento' => $data['order']['info_pagamento'],
            'property' => [
                'locale' => ImsHelper::getLocale(),
                'note' => $data['order']['property']['note'],
                'cf' => $data['user']['cf'],
                'company_name' => $data['user']['company_name'],
                'p_iva' => $data['user']['p_iva'],
                'sdi_code' => $data['user']['sdi_code'],
                'pec_code' => $data['user']['pec_code'],
                'legal_person' => $data['legal_person'],
                'prefix' => $data['user']['prefix'],
                'birthdate' => $data['user']['birthdate'],
                'billing_name' => $data['billing_address']['name'],
                'billing_last_name' => $data['billing_address']['last_name'],
                'billing_region' => $data['billing_address']['region'],
                'billing_state' => $data['billing_address']['state'],
                'billing_city' => $data['billing_address']['city'],
                'billing_street' => '',
                'billing_house' => '',
                'billing_flat' => '',
                'billing_address1' => $data['billing_address']['address1'],
                'billing_address2' => '',
                'billing_postcode' => $data['billing_address']['postcode'],
                'shipping_region' => $data['shipping_address']['region'],
                'shipping_state' => $data['shipping_address']['state'],
                'shipping_city' => $data['shipping_address']['city'],
                'shipping_street' => '',
                'shipping_house' => '',
                'shipping_flat' => '',
                'shipping_address1' => $data['shipping_address']['address1'],
                'shipping_address2' => '',
                'shipping_postcode' => $data['shipping_address']['postcode'],
                'shipping_presso' => $data['shipping_address']['presso'],
            ]
        ];
        $arUserData = [
            'name' => $data['user']['name'],
            'last_name' => $data['user']['last_name'],
            'email' => trim($data['user']['email']),
            'phone' => $data['user']['phone'],
            'payment_method_id' => $data['order']['payment_method_id'],
            'shipping_type_id' => $prices->shipping_type_id,
            'property' => [
                'cf' => $data['user']['cf'],
                'company_name' => $data['user']['company_name'],
                'p_iva' => $data['user']['p_iva'],
                'sdi_code' => $data['user']['sdi_code'],
                'pec_code' => $data['user']['pec_code'],
                'legal_person' => $data['legal_person'],
                'prefix' => $data['user']['prefix'],
                'birthdate' => $data['user']['birthdate'],
                'billing_name' => $data['billing_address']['name'],
                'billing_last_name' => $data['billing_address']['last_name'],
                'billing_region' => $data['billing_address']['region'],
                'billing_state' => $data['billing_address']['state'],
                'billing_city' => $data['billing_address']['city'],
                'billing_address' => $data['billing_address']['address1'],
                'billing_postcode' => $data['billing_address']['postcode'],
                'shipping_name' => $data['shipping_address']['name'],
                'shipping_last_name' => $data['shipping_address']['last_name'],
                'shipping_region' => $data['shipping_address']['region'],
                'shipping_state' => $data['shipping_address']['state'],
                'shipping_city' => $data['shipping_address']['city'],
                'shipping_address1' => $data['shipping_address']['address1'],
                'shipping_postcode' => $data['shipping_address']['postcode'],
                'billing_address2' => '',
                'billing_street' => '',
                'billing_house' => '',
                'billing_flat' => '',
                'shipping_address2' => '',
                'shipping_street' => '',
                'shipping_house' => '',
                'shipping_flat' => ''
            ]
        ];

        $order = $this->createOrderCustomized($arOrderData, $arUserData);
        if ( $order ) {
            // Invio manuale email solo se quello automatico è disabilitato
            $isAutoEmail = Settings::getValue('send_email_after_creating_order');
            if ( $isAutoEmail ) {
                Flash::error('disabilitare invio automatico email');
                Log::error('disabilitare invio automatico email');
            }

            // gestione prodotti vintage: controlla se nel'ordine è presente un tessuto per impostare le sue offer a 0 e crearne una "soldout" per sfruttarla frontend
            foreach($this->obOrder->order_position as $position){
                //recupero l'offer dall'order position attuale, per poter poi recuperare il product
                $cartOffer = $position->offer;

                //recupero il prodotto dall'offer
                $product = $cartOffer->product;


                //controllo se il prodotto fa parte della categoria vintage
                if( PaymentGatewayHelper::containsVintageCat($product) ) {
                    //in caso faccia parte del "vintage" recupero le offer in modo poi da manipolarle
                    $soldOutOffer = null;

                    //per le offer esistenti imposto la qnt a 0 e le disattivo
                    foreach ($product->offer as $offer) {
                        // Se non è soldout
                        if ( $offer->name != 'SOLDOUT' ) {
                            $offer->quantity = 0;
                            $offer->active = 0;
                            $offer->save();
                        }
                        else
                        {
                            $soldOutOffer = $offer;
                            $soldOutOffer->quantity = 1;
                            $soldOutOffer->active = 1;
                            $soldOutOffer->save();
                        }
                    }

                    //creo un offer soldout per gestire il frontend di quel prodotto se non esiste
                    if ( !$soldOutOffer ) {
                        $soldOutOffer = new Offer();
                        $soldOutOffer->product_id = $product->id;
                        $soldOutOffer->name = 'SOLDOUT';
                        $soldOutOffer->quantity = 1;
                        $soldOutOffer->active = 1;
                        $soldOutOffer->save();
                    }
                }

            }
        }
        else
        {
            // In caso di prodotto non disponibile sovrascrive il messaggio di errore con il nome e la tagia del prodotto
            if ( Result::message() == Lang::get('lovata.ordersshopaholic::lang.message.insufficient_amount') )
            {
                $cartProcessor = CartProcessor::instance();
                $cartPositionCollection = $cartProcessor->get();
                $cartPositionId = Result::data()['cart_position_id'];

                foreach ( $cartPositionCollection as $positionItem )
                {
                    if ( $positionItem->id == $cartPositionId )
                    {
                        $nomeProdotto = $positionItem->offer->product->name.' '.$positionItem->offer->name;
                        break;
                    }
                }

                $message = $nomeProdotto." ".Lang::get('ims.shophelper::lang.component_cart.offer_not_available');
                Result::setMessage($message);
            }
        }

        //$evento = Event::fire('shopaholic.order.after_create', $data['stripeToken']);

        if ( $order && Str::startsWith($order->payment_method->code, 'stripe') ) {
            $result = [
                'status' => true,
                'data' => [
                    "id" => $order->id,
                    "number" => $order->order_number,
                    "payment_method_code" => $order->payment_method->code,
                    "key" => $order->secret_key,
                    "gateway" => "stripe3d"
                ],
                'message' => 'In attesa di pagamento stripe',
                'code' => 'stripe3d'
            ];

            return $result;
        }
        else
        {
            //Fire event and get redirect URL
            $sRedirectURL = Event::fire(OrderProcessor::EVENT_ORDER_GET_REDIRECT_URL, $this->obOrder, true);
            if (!Result::status() && !empty($this->obPaymentGateway) && $this->obPaymentGateway->isRedirect()) {
                $sRedirectURL = $this->obPaymentGateway->getRedirectURL();

                return Redirect::to($sRedirectURL);
            } else if (empty($this->obPaymentGateway) || !Result::status()) {
                $this->prepareResponseData();

                return $this->getResponseModeAjax($sRedirectURL);
            }

            $this->resultAppendData(['gateway' => 'other']);

            // Gestione ordinaria gateway di pagamento
            if ($this->obPaymentGateway->isRedirect()) {
                $sRedirectURL = $this->obPaymentGateway->getRedirectURL();

                return Redirect::to($sRedirectURL);
            } else if ($this->obPaymentGateway->isSuccessful()) {
                Result::setTrue($this->obPaymentGateway->getResponse());
            } else {
                Result::setFalse($this->obPaymentGateway->getResponse());
            }

            Result::setMessage($this->obPaymentGateway->getMessage());

            $this->prepareResponseData();
            return $this->getResponseModeAjax($sRedirectURL);
        }

    }
    /**
     *
     * @param $arOrderData
     * @param $arUserData
     * @return Order|null
     */
    public function create($arOrderData, $arUserData)
    {
        $this->arOrderData = (array) $arOrderData;
        $this->arUserData = (array) $arUserData;

        //Find or create new user
        if (empty($this->obUser) && $this->bCreateNewUser) {
            $this->findOrCreateUser();
        } else if (!empty($this->obUser)) {
            $arAuthUserData = [
                'email'       => $this->obUser->email,
                'name'        => $this->obUser->name,
                'last_name'   => $this->obUser->last_name,
                'middle_name' => $this->obUser->middle_name,
                'phone'       => $this->obUser->phone,
            ];

            $this->arUserData = array_merge($arAuthUserData, $this->arUserData);
        }

        $obCart = CartProcessor::instance()->getCartObject();
        $this->arUserData = array_merge((array) $obCart->user_data, $this->arUserData);

        $this->processOrderAddress();

        if (!Result::status()) {
            return false;
        }

        $arOrderData = $this->arOrderData;

        $obActiveCurrency = CurrencyHelper::instance()->getActive();
        if (empty(array_get($arOrderData, 'currency')) && !empty($obActiveCurrency)) {
            $arOrderData['currency_id'] = $obActiveCurrency->id;
        }
        if (!isset($arOrderData['property']) || !is_array($arOrderData['property'])) {
            $arOrderData['property'] = [];
        }

        $arOrderData['property'] = array_merge($arOrderData['property'], $this->arUserData, $this->arBillingAddressOrder, $this->arShippingAddressOrder);

        $arPaymentData = Input::get('payment');
        if (!empty($arPaymentData) && is_array($arPaymentData)) {
            $arOrderData['payment_data'] = $arPaymentData;
        }

        $this->obOrder = OrderProcessor::instance()->create($arOrderData, $this->obUser);

        $this->obPaymentGateway = OrderProcessor::instance()->getPaymentGateway();

        return $this->obOrder;
    }



    /**
     * @param Order $order
     */
    public function orderAddCustomization($order, $cartPositions)
    {
        foreach ( $cartPositions as $cartItem )
        {
            if ( $cartItem->customized_content )
            {
                foreach ( $order->order_position as $orderItem )
                {
                    if ( $orderItem->item_id == $cartItem->item_id )
                    {
                        $orderItem->customized_offer_id = $cartItem->customized_offer_id;
                        $orderItem->customized_content = $cartItem->customized_content;
                        $orderItem->save();
                        break;
                    }
                }
            }
        }
    }

    /**
     * Create new order
     * @param                             $arOrderData
     * @param \Lovata\Buddies\Models\User $obUser
     * @return Order|null
     * @throws \Exception
     */
    public function orderProcessorCreate($arOrderData, $obUser = null)
    {
        $this->initOrderData($arOrderData);
        $this->initUser($obUser);
        $this->setOrderStatus();
        $this->updateOrderData();
        $this->fillShippingTypePrice();

        $this->initCartPositionList();
        if (!Result::status()) {
            return null;
        }

        //Begin transaction
        DB::beginTransaction();

        //Fire event before create order
        if (Event::fire(self::EVENT_UPDATE_ORDER_BEFORE_CREATE, [$this->arOrderData, $this->obUser], true) === false) {
            return null;
        }

        $this->createOrder();
        $this->processOrderPositionList();

        if (!empty($this->obOrder)) {
            //Fire event after create order
            Event::fire(self::EVENT_UPDATE_ORDER_AFTER_CREATE, $this->obOrder);
            $this->attachPromoMechanism();

            OrderPromoMechanismProcessor::update($this->obOrder);
            if ($this->obOrder->total_price_value > 0) {
                $this->sendPaymentPurchase();
            }
        }

        if (!Result::status()) {
            DB::rollBack();
            return null;
        }

        DB::commit();

        $this->obOrder->save();

        Event::fire(self::EVENT_ORDER_CREATED, $this->obOrder);

        CartProcessor::instance()->clear();

        $arResult = [
            'id'     => $this->obOrder->id,
            'number' => $this->obOrder->order_number,
            'key'    => $this->obOrder->secret_key,
        ];

        Result::setTrue($arResult);

        return $this->obOrder;
    }


    protected function findOrCreateUser()
    {
        $sUserPluginName = UserHelper::instance()->getPluginName();
        if (!empty($this->obUser) || empty($this->arUserData) || empty($sUserPluginName)) {
            return;
        }

        $this->findUserByEmail();
        if (!empty($this->obUser) || !$this->bCreateNewUser) {
            return;
        }

        $this->createUser();
    }

    /**
     * Find user by email
     * @return void
     */
    protected function findUserByEmail()
    {
        if (empty($this->arUserData) || !isset($this->arUserData['email']) || empty($this->arUserData['email'])) {
            return;
        }

        //Find user by email
        $sEmail = $this->arUserData['email'];
        $this->obUser = UserHelper::instance()->findUserByEmail($sEmail);
        if (empty($this->obUser)) {
            $this->obUser = Event::fire(OrderProcessor::EVENT_ORDER_FIND_USER_BEFORE_CREATE, $this->arUserData, true);
        }

        //if Buddies plugin is installed, then we need to process "phone" field
        if (UserHelper::instance()->getPluginName() == 'Lovata.Buddies') {
            $this->processUserPhone();
            $this->processUserPhoneList();
        }
    }

    /**
     * Process user phone
     */
    protected function processUserPhone()
    {
        if (empty($this->obUser) || empty($this->arUserData)) {
            return;
        }

        if (!isset($this->arUserData['phone']) || empty($this->arUserData['phone'])) {
            return;
        }

        $sPhone = $this->arUserData['phone'];

        $arPhoneList = $this->obUser->phone_list;
        $arPhoneList[] = $sPhone;
        $arPhoneList = array_unique($arPhoneList);

        $this->obUser->phone_list = $arPhoneList;
        $this->obUser->save();
    }

    /**
     * Process user phone list
     */
    protected function processUserPhoneList()
    {
        if (empty($this->obUser) || empty($this->arUserData)) {
            return;
        }

        if (!isset($this->arUserData['phone_list']) || empty($this->arUserData['phone_list'])) {
            return;
        }

        $arRequestPhoneList = $this->arUserData['phone_list'];

        $arPhoneList = $this->obUser->phone_list;
        $arPhoneList = array_merge($arPhoneList, $arRequestPhoneList);
        $arPhoneList = array_unique($arPhoneList);

        $this->obUser->phone_list = $arPhoneList;
        $this->obUser->save();
    }

    /**
     * Create new user
     * @return void
     */
    protected function createUser()
    {

        if (empty($this->arUserData)) {
            return;
        }

        //$sPassword = md5(microtime(true));
        $sPassword = 'changeme';

        //Get user email
        if (Settings::getValue('generate_fake_email') && (!isset($this->arUserData['email']) || empty($this->arUserData['email']))) {
            $this->arUserData['email'] = 'fake'.$sPassword.'@fake.com';
        }

        $arUserData = (array) $this->arUserData;

        if (!isset($arUserData['password']) || empty($arUserData['password'])) {
            $arUserData['password'] = $sPassword;
        }

        $arUserData['password_confirmation'] = $arUserData['password'];

        try {
            //Create new user
            $this->obUser = UserHelper::instance()->register($arUserData, true);
        } catch (\October\Rain\Database\ModelException $obException) {
            $this->processValidationError($obException);
            return;
        }

        Event::fire(OrderProcessor::EVENT_ORDER_USER_CREATED, $this->obUser);
    }

    /**
     * Process shipping/billing addresses. Create new user address or get data from exist address
     */
    protected function processOrderAddress()
    {
        $arShippingAddressData = (array) Input::get('shipping_address');
        $arBillingAddressData = (array) Input::get('billing_address');

        $obCart = CartProcessor::instance()->getCartObject();
        $arShippingAddressData = array_merge((array) $obCart->shipping_address, $arShippingAddressData);
        $arBillingAddressData = array_merge((array) $obCart->billing_address, $arBillingAddressData);

        $this->arShippingAddressOrder = $this->addOrderAddress(UserAddress::ADDRESS_TYPE_SIPPING, $arShippingAddressData);
        $this->arBillingAddressOrder = $this->addOrderAddress(UserAddress::ADDRESS_TYPE_BILLING, $arBillingAddressData);
    }

    /**
     * Add user address data
     * @param string $sType
     * @param array  $arAddressData
     * @return array
     */
    protected function addOrderAddress($sType, $arAddressData) : array
    {
        if (empty($arAddressData) || empty($sType) || empty($this->obUser)) {
            return $this->prepareAddressData($sType, $arAddressData);
        }

        $arResult = $this->findAddressByID($sType, $arAddressData);

        if (empty($arResult)) {
            $obAddress = UserAddress::findAddressByData($arAddressData, $this->obUser->id);
            if (!empty($obAddress)) {
                $arResult = $this->getAddressData($obAddress);
            }
        }

        if (empty($arResult)) {
            $arResult = $this->createUserAddress($sType, $arAddressData);
        }

        return $this->prepareAddressData($sType, $arResult);
    }

    /**
     * Prepare address array to save in Order properties
     * @param string $sType
     * @param array  $arAddressData
     * @return array
     */
    protected function prepareAddressData($sType, $arAddressData) : array
    {
        if (empty($arAddressData)) {
            return [];
        }

        $arResult = [];
        foreach ($arAddressData as $sKey => $sValue) {
            $arResult[$sType.'_'.$sKey] = $sValue;
        }

        return $arResult;
    }

    /**
     * Find Address object by ID, type and user_id
     * @param string $sType
     * @param array  $arAddressData
     * @return array
     */
    protected function findAddressByID($sType, $arAddressData) : array
    {
        $iAddressID = array_get($arAddressData, 'id');
        if (empty($iAddressID)) {
            return [];
        }

        $obAddress = UserAddress::getByUser($this->obUser->id)->getByType($sType)->find($iAddressID);
        if (empty($obAddress)) {
            return [];
        }

        return $this->getAddressData($obAddress);
    }

    /**
     * @param string $sType
     * @param array  $arAddressData
     * @return array
     */
    protected function createUserAddress($sType, $arAddressData) : array
    {
        if (empty($arAddressData)) {
            return [];
        }

        $arAddressData['type'] = $sType;
        $arAddressData['user_id'] = $this->obUser->id;

        try {
            //Create new address for user
            $obAddress = UserAddress::create($arAddressData);
        } catch (\October\Rain\Database\ModelException $obException) {
            $this->processValidationError($obException);
            return [];
        }

        return $this->getAddressData($obAddress);
    }

    /**
     * Get address data from object
     * @param UserAddress $obAddress
     * @return array
     */
    protected function getAddressData($obAddress) : array
    {
        if (empty($obAddress)) {
            return [];
        }

        $arResult = $obAddress->toArray();
        array_forget($arResult, ['id', 'type']);

        return $arResult;
    }

    /**
     * Fire event and prepare response data
     */
    protected function prepareResponseData()
    {
        if (!Result::status()) {
            return;
        }

        $arResponseData = Result::data();
        $arEventData = Event::fire(OrderProcessor::EVENT_UPDATE_ORDER_RESPONSE_AFTER_CREATE, [$arResponseData, $this->obOrder, $this->obUser, $this->obPaymentGateway]);
        if (empty($arEventData)) {
            return;
        }

        foreach ($arEventData as $arData) {
            if (empty($arData)) {
                continue;
            }

            $arResponseData = array_merge($arResponseData, $arData);
        }

        Result::setData($arResponseData);
    }

    protected function getRedirectPageProperties()
    {
        if (!Result::status() || empty($this->obOrder)) {
            return [];
        }

        $arResult = [
            'id'     => $this->obOrder->id,
            'number' => $this->obOrder->order_number,
            'key'    => $this->obOrder->secret_key,
        ];

        $sRedirectPage = $this->property(self::PROPERTY_REDIRECT_PAGE);
        if (empty($sRedirectPage)) {
            return $arResult;
        }

        $arPropertyList = PageHelper::instance()->getUrlParamList($sRedirectPage, 'OrderPage');
        if (!empty($arPropertyList)) {
            $arResult[array_shift($arPropertyList)] = $this->obOrder->secret_key;
        }

        return $arResult;
    }

    /**
     * User auth
     * @return array
     * @internal param array $arUserData
     * @internal param bool $bRemember
     */
    public function onCheckoutLogin()
    {
        $arUserData = Input::only(['email', 'password']);
        $bRemember = (bool) Input::get('remember_me', false);

        if (empty($arUserData) || !is_array($arUserData)) {
            $sMessage = Lang::get('lovata.toolbox::lang.message.e_not_correct_request');
            Result::setFalse()->setMessage($sMessage);

            return ['logged' => false];
        }

        //Check user auth already logged
        if (!empty($this->obUser)) {
            return ['logged' => true];
        }

        $this->obUser = AuthHelper::authenticate($arUserData, true);

        if (empty($this->obUser)) {
            $sMessage = Lang::get('lovata.buddies::lang.message.e_login_not_correct');
            Flash::error($sMessage);
            return ['logged' => false];
        }

        /*

        Result::setMessage($sMessage)->setTrue($this->obUser->id);
        */

        $sMessage = Lang::get('lovata.buddies::lang.message.login_success');
        Flash::success($sMessage);

        return ['logged' => true];
    }

    public function onGoBack(){

        return;
    }

    private function createCustomer($user) {
        return Customer::create([
            'name' => $user['name'] . ' ' . $user['last_name'],
            'email' => $user['email'],
            'address' => ['line1' => $user['address'], 'city' => $user['city'], 'postal_code' => $user['postcode'], 'state' => $user['state']]
        ]);
    }

    public function onCreateIntentApple(){
        $data = post();

        //preparazione dati come in input per onCreateOrder
        $data['order'] = array(
            'dazi_e_dogana' => $data['dazi_e_dogana'],
            'payment_method_id' => $data['payment_method_id'],
            'property' => array(
                'region' => $data['user']['region'],
                'state' => $data['user']['state'],
                'city' => $data['user']['city'],
                'address1' => $data['user']['address'],
                'postcode' => $data['user']['postcode'],
            ),
        );

        //validazione dati come per la creazione dell'ordine
        $this->createOrderValidationData($data);

        Stripe::setApiKey(PaymentMethod::getByCode("stripe3d_apple")->first()->gateway_property['apiKey']);

        $customer = $this->createCustomer($data['user']);

        //calcolo prezzi
        $prices = $this->getPrices($data['user']['state'], $data['dazi_e_dogana']);

        $paymentIntent = PaymentIntent::create([
            "amount" => $prices->eur_total_price * 100, //stripe vuole il prezzo definito in centesimi
            "currency" => "eur",
            'customer' => $customer->id
        ]);

        return $paymentIntent;
    }

    public function onConfirmIntentApple(){
        $data = post();
        //reperimento ordine
        $order = OrderItem::make($data['order_id']);

        try {
            //set privateKey di stripe
            Stripe::setApiKey($order->payment_method->getObject()->gateway_property['apiKey']);

            $paymentIntent = PaymentIntent::retrieve(
                $data['payment_intent_id']
            );

            return $this->checkIntentStatus($paymentIntent, $order, $data);
        }
        catch ( \Exception $ex ) {
            PaymentGatewayHelper::cancelOrder($order->getObject());

            return [
                'success' => false,
                'order' => $order->id,
                'error' => Lang::get('ims.shophelper::lang.messages.payment_ko_title').': '.$ex->getMessage()
            ];
        }
    }

    /**
     * Auesta funzione ajax dovrebbe creare il customer di stripe, aggiornare lo stato dell'ordine
     * @return array
     */
    public function onCheckoutConfirmPayment()
    {
        $post = post();

        $order = OrderItem::make($post['paymentData']['id']);

        try {
            $privateKey = $order->payment_method->getObject()->gateway_property['apiKey'];
            Stripe::setApiKey($privateKey);

            //crea il customer e passa i dati del cliente a stripe
            $customer = $this->createCustomer($post['user']);

            $totale = floatval($order->conv_prezzo_tot);

            $orderNumber = $order->order_number;
            $intent = null;

            if (array_key_exists('payment_method_id', $post)) {
                # Create the PaymentIntent
                $intent = PaymentIntent::create([
                    'payment_method' => $post['payment_method_id'],
                    'confirmation_method' => 'manual',
                    'confirm' => true,
                    'amount' => floatval($totale*100),
                    'currency' => 'eur',
                    'description' => $orderNumber . '-' . $order->id,
                    'customer' => $customer->id,
                ]);
            }
            if (array_key_exists('payment_intent_id', $post)) {
                $intent = PaymentIntent::retrieve(
                    $post['payment_intent_id']
                );
                $intent->confirm();
            }
            return $this->checkIntentStatus($intent, $order, $post);
        }
        catch ( \Exception $ex )
        {
            PaymentGatewayHelper::cancelOrder($order->getObject());

            return [
                'success' => false,
                'order' => $order->id,
                'error' => Lang::get('ims.shophelper::lang.messages.payment_ko_title').': '.$ex->getMessage()
            ];
        }
    }

    private function checkIntentStatus($intent, $order, $payment_data) {
        if ($intent && $intent->status == 'requires_action' &&
            $intent->next_action->type == 'use_stripe_sdk'
        ) {

            # Tell the client to handle the action Effettua verifica
            return [
                'requires_action' => true,
                'payment_intent_client_secret' => $intent->client_secret,
            ];
        } else if ($intent && $intent->status == 'succeeded') {
            // Pagamento OK

            // Salva il model
            $orderModel = $order->getObject();
            $orderModel->payment_data = $payment_data;
            $orderModel->payment_response = $intent->toJSON();
            $orderModel->payment_token = $intent->id;
            $orderModel->status_id = $order->payment_method->getObject()->after_status_id;
            $orderModel->save();

            // Invia email per STRIPE
            $orderItem = OrderItem::makeNoCache($order->id);
            EmailHelper::sendEmailUser($orderItem, ImsHelper::getLocale());
            EmailHelper::sendEmailManager($orderItem, 'it');

            /** @ilmissigno&rottenmind
             * Chiamo la funzione di check se nell'ordine c'è qualche giacenza uk.
             * Ritorna un intero come flag
             */
            $counter = $this->checkPositionUk($orderModel);
            if ($counter > 0) {
                EmailHelper::sendEmailUkPosition($order);
            }

            return [
                "success" => true,
                'order' => $order->id,
                "redirect" => '/order-success?order=' . $order->secret_key

            ];
        } else {
            // Pagamento non riuscito
            PaymentGatewayHelper::cancelOrder($order->getObject());

            return [
                'error' => 'Invalid PaymentIntent status'
            ];
        }
    }

    public function festivita(){
        $festivita = Festivita::first();
        return  [$festivita->attivo, $festivita->saldi]; //[0 = Festivita],[1 = Saldi]
    }

    public function checkPositionUk($orderModel)
    {
        /** @ilmissigno&rottenmind
         * Get di tutte le taglie dell'ordine e setto un counter a 0
         */
        $posititons = $orderModel->order_position;
        $counter = 0;
        foreach ($posititons as $posItem) {
            /** @ilmissigno&rottenmind
             * Un secondo flag per rafforzare l'algoritmo
             */
            $checkGiacenzeFromUk = false;
            /** @ilmissigno&rottenmind
             * Se la position NON è nulla (caso impossibile)
             */
            if ($posItem != null) {
                $offer = Offer::where(['id' => $posItem->item_id])->first();
                /** @ilmissigno&rottenmind 
                 * Quantità totale acquistata
                 */
                $totalQuantityBought = $posItem->quantity;
                if (isset($offer) && $offer->is_manual_inventory != 1) {
                    /** @ilmissigno&rottenmind
                     * Se la quantità acquistata è maggiore della quantità italiana allora passa il controllo
                     */
                    if ($offer->quantity_ita <= 0 && $offer->quantity_uk <= 0) {
                        continue;
                    }
                    if ($totalQuantityBought > $offer->quantity_ita) {
                        /** @ilmissigno&rottenmind
                         * Calcolo la quantita rimanente se maggiore di 0 passa il controllo
                         */

                        $remaningQuantity = $totalQuantityBought - $offer->quantity_ita;
                        if ($remaningQuantity > 0) {
                            /** @ilmissigno&rottenmind 
                             * Imposto il flag a true
                             */
                            $checkGiacenzeFromUk = true;
                            /** @ilmissigno&rottenmind 
                             * Imposto il flag a true
                             */
                            $posItem->is_giacenze_uk = true;
                            /** @ilmissigno&rottenmind 
                             * Ricalcolo la quantità_uk della offer detraendo il restante delle taglie della position
                             */
                            $offer->quantity_uk = $offer->quantity_uk - $remaningQuantity;
                            $offer->quantity_ita = 0;
                            /** @ilmissigno&rottenmind 
                             * Setto la nuova quantita della position con la quantità rimanente altrimenti invia l'email con la quantità sia uk che italiana al magazzino uk
                             */
                            $posItem->weight = $remaningQuantity;
                            /** @ilmissigno&rottenmind 
                             * incremento il counter dato che c'è una taglia che deve essere presa da uk
                             */
                            $offer->save();
                            $counter++;
                        }
                        /** @ilmissigno&rottenmind 
                         * Imposto il flag (per debug)
                         */
                        $offer->is_giacenze_uk = $checkGiacenzeFromUk;
                    } else {
                        $offer->quantity_ita = $offer->quantity_ita - $posItem->quantity;
                        $posItem->is_giacenze_uk = false;
                        $offer->is_giacenze_uk = false;
                    }
                    $offer->quantity = $offer->quantity_ita + $offer->quantity_uk;
                    $offer->save();
                    $posItem->save();
                }
            }
        }
         return $counter;
    }
}