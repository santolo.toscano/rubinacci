<?php namespace Ims\Shophelper\Components;

use Event;
use Validator;
use ValidationException;
use Illuminate\Support\Str;
use Lovata\Shopaholic\Models\Product;
use Lovata\Shopaholic\Models\Category;
use Illuminate\Support\Facades\Redirect;
use RainLab\Translate\Classes\Translator;
use Illuminate\Database\Eloquent\Collection;
use October\Rain\Router\Router as RainRouter;
use Lovata\Shopaholic\Classes\Item\CategoryItem;
use Lovata\Toolbox\Classes\Component\ElementPage;

use RainLab\Translate\Models\Locale as LocaleModel;
use Lovata\Shopaholic\Classes\Collection\OfferCollection;

use Lovata\Shopaholic\Classes\Collection\ProductCollection;
use Lovata\Shopaholic\Classes\Collection\CategoryCollection;
use Lovata\PropertiesShopaholic\Classes\Collection\PropertySetCollection;

/**
 * Class CategoryPage
 * @package Lovata\Shopaholic\Components
 * @author  Andrey Kharanenka, a.khoronenko@lovata.com, LOVATA Group
 */
class CategoryHelper extends ElementPage
{
    protected $bNeedSmartURLCheck = true;
    protected $bHasWildCard = true;

    /** @var \Lovata\Shopaholic\Models\Category */
    protected $obElement;

    /** @var \Lovata\Shopaholic\Classes\Item\CategoryItem */
    protected $obElementItem;


    protected $obCategory;

    /**
     * @return array
     */
    public function componentDetails()
    {
        return [
            'name'        => 'Category Helper',
            'description' => 'lovata.shopaholic::lang.component.category_page_description',
        ];
    }

    /**
     * @return array
     */
    public function defineProperties()
    {
        $this->arPropertyList = array_merge($this->arPropertyList, $this->getElementPageProperties());

        $this->arPropertyList['debug'] = [
            'title'   => 'Debug',
            'type'    => 'checkbox',
            'default' => 0,
        ];

        $this->arPropertyList['has_wildcard'] = [
            'title'   => 'Has wildcard',
            'type'    => 'checkbox',
            'default' => 1,
        ];

        $this->arPropertyList['skip_error'] = [
            'title'   => 'Skip error',
            'type'    => 'checkbox',
            'default' => 1,
        ];

        return $this->arPropertyList;
    }

   /* public function onInit()
    {
        $this->controller->addComponent('\Lovata\FilterShopaholic\Components\FilterPanel', 'filterPanel');
    }*/

   public function showProductsForCategory($obCategory)
   {
       $productColletion = ProductCollection::make([]); // Il make utilizza la cache
    //    $productColletion = ProductCollection::make([])->filterByQuantity(); // Il make utilizza la cache
       $productColletion = ProductCollection::make([]); // Il make utilizza la cache
       $productColletion->active()->category($obCategory->id, true)->sort('custom|season_desc');

       // NB IN CASO SI VUOLE VISUALIZZARE IL SOLDOUT RIMUOVERE FILTERBYQUANTITY
       $this->page['obProductList'] = $productColletion;

       $obPropertySetList = PropertySetCollection::make()->sort()->code(['main']);

       $obProductPropertyList = $obPropertySetList->getProductPropertyCollection($productColletion);
       $this->page['obProductPropertyList'] = $obProductPropertyList;

       $obOfferPropertyList = $obPropertySetList->getOfferPropertyCollection($productColletion);
       $this->page['obOfferPropertyList'] = $obOfferPropertyList;

       $this->page['obCategory'] = $obCategory;
   }

   public function searchCategoryFromSlug()
   {
       $slug = $this->property('slug');
       $lang = Translator::instance()->getLocale();

       // Release 1 legge dal nome
       $property_name = "name|$lang";
       $categoryslug = explode('/', $slug);

       $categoryCollection = CategoryCollection::make([]);

       foreach ( $categoryCollection->active() as $item )
       {
           $categoryname = strtolower($item->$property_name);
           $categoryname = str_replace(' ', '-', $categoryname);

           // verifica solo ultima categoria
           if ( strtolower(end($categoryslug)) == $categoryname )
           {
               return $item;
           }
       }

       // Nessuna categoria trovata
       return null;
   }

    /**
     * Cicla le lingue attive ed estrae url in lingua
     * @param $obProduct Richiede istanza oggetto
     */
    public function getLocalizedPage($obCategory)
    {
        // Ottiene lingue attive
        $locales = collect(LocaleModel::listEnabled());

        // Transform it to contain the new urls
        $locales->transform(function ($item, $key) use ( $obCategory ) {
            return $this->translatePage($key, $obCategory);
        });

        $this->page['localesurl'] = $locales;
    }

    public function translatePage($locale, $obCategory)
    {
        $translator = Translator::instance();
        $page = $this->getPage();
        $router = new RainRouter;

        $params = $this->getRouter()->getParameters();

        if ( $locale != $translator->getLocale() )
        {
            $slug = array_key_exists('catalog', $params) ? 'catalog' : (array_key_exists('slug', $params) ? 'slug' : '');
            if($slug){
                $hasParent = str_contains($params[$slug], '/');
            }else{
                $hasParent = false;
            }

            $model = $obCategory->getObject();
            // effettua la traduzione dello slug
            $model->translateContext($locale);
            $params['catalog'] = $model->slug;

            // Aggiunge il prefisso per il parent
            if ( $hasParent )
            {
                $model = $obCategory->parent->getObject();
                $model->translateContext($locale);
                $params['catalog'] = $model->slug."/".$params['catalog'];
            }

        }

        $page->rewriteTranslatablePageUrl($locale);
        $localeUrl = $router->urlFromPattern($page->url, $params);
        return $translator->getPathInLocale($localeUrl, $locale);
    }

    /**
     * @param CategoryItem $obCategory
     */
    public function categoryTree($obCategory)
    {
        // Sostituito per i children
        // $obCategoryTree = CategoryCollection::make([])->tree();

        $obCategoryChildren = $obCategory->children;
        $this->page['obCategoryTree'] = $obCategoryChildren;
    }

    public function onSearchPagination()
    {
        $data = post();

        $productColletion = ProductCollection::make([])->active(); // Il make utilizza la cache
        // $productColletion = ProductCollection::make([])->active()->filterByQuantity(); // Il make utilizza la cache
        $productColletion = ProductCollection::make([])->active(); // Il make utilizza la cache
        $productColletion->search($this->param('slug'));

        $productList = [
            'obProductList' => $productColletion,
            'pagination' => $data['pagination'],
            'page' => $data['pageCost']
        ];

        return [
            '.prod-list' => $this->renderPartial('productListSearch', $productList),
        ];
    }


    public function onRun()
    {
        // Effettua il routing delle chiamate
        if ( $this->properties['debug'])
        {
            $obCategoryTree = CategoryCollection::make([])->tree();
            $filtro_categorie = [];

            $this->page['obCategoryTree'] = $obCategoryTree;

            $filtri = array (
                'properties' =>
                    array (
                        /*
                        0 =>
                            array (
                                'name' => 'category_34',
                                'value' => 'category_34-parent_10',
                            ),
                        0 =>
                            array (
                                'name' => 'category_42',
                                'value' => 'category_5-main',
                            ),
                        */
                        1 =>
                            array (
                                'name' => 'category_5',
                                'value' => 'category_5-main',
                            ),
                        /*
                        2 =>
                            array (
                                'name' => 'property_8',
                                'value' => 'blazerfoderata',
                            ),
                        3 =>
                            array (
                                'name' => 'property_5',
                                'value' => '41',
                            ),
                        */
                    ),
                'pagination' => '1',
            );



            foreach ( $filtri['properties'] as $valueArray )
            {
                if ( starts_with( $valueArray['name'], 'category') )
                {
                    $category = str_after($valueArray['name'], 'category_');
                    array_push($filtro_categorie, str_after($valueArray['name'], 'category_'));
                }
            }

            if (  !$filtro_categorie )
            {
                $productColletion = ProductCollection::make([])->active();
                // $productColletion = ProductCollection::make([])->active()->filterByQuantity();
                $productColletion = ProductCollection::make([])->active();
            }
            else
            {
                $productColletion = ProductCollection::make([])->active()->category($filtro_categorie, true);
                // $productColletion = ProductCollection::make([])->active()->category($filtro_categorie, true)->filterByQuantity();
                $productColletion = ProductCollection::make([])->active()->category($filtro_categorie, true);
            }


            //dd($filtro_categorie, $productColletion);
        }

        //$obCategoryTree = CategoryCollection::make([])->tree();
        //$this->page['obCategoryTree'] = $obCategoryTree;



        // Controlla se url inglese con scritta prodotto
        $urlProdottoEn  = $this->isProdottoEn();
        if ( $urlProdottoEn )
        {
            return redirect($urlProdottoEn, 301);
        }

        $obCategory = $this->get();

        // Lingua in inglese ma link senza prefisso in inglese, cerco in italiano
        if ( !$obCategory && Translator::instance()->getLocale() == 'en' && !$this->isUrlLocalized('en') )
        {
            $slug = last(explode('/', $this->controller->getRouter()->getUrl()));
            // Potrebbe essere in italiano
            $obCategory = Category::active()->getBySlug($slug)->first();
            if ( $obCategory ) {
                $obCategory = CategoryItem::make($obCategory->id);
            }
        }

        $this->page['obCategory'] = $obCategory;
        if($obCategory){
            $this->categoryTree($obCategory);
        }

        $this_url = explode('/',$this->page->url);

        if ($obCategory) {
            $this->getLocalizedPage($obCategory);
            $this->showProductsForCategory($obCategory);
            $this->page['pagination'] = 1;
            $this->page['page'] = 1;
        }
        // Prova a leggere la categoria in lingua dal nome
        elseif( $this_url[1] == 'search' || $this_url[1] == 'ricerca' ) {
            $productColletion = ProductCollection::make([])->active(); // Il make utilizza la cache
            // $productColletion = ProductCollection::make([])->active()->filterByQuantity(); // Il make utilizza la cache
            $productColletion = ProductCollection::make([])->active(); // Il make utilizza la cache
            $productColletion->search($this->param('slug'));
            $this->page['obProductList'] = $productColletion;
        }
        // Sono sulla pagina dedicata della categoria in lingua
        elseif ( $this->param('collection') )
        {
            // Release 1 legge dal nome
            $obCategory = $this->searchCategoryFromSlug();
            if ( $obCategory ) {
                $this->showProductsForCategory($obCategory);
                $this->getLocalizedPage($obCategory);
                $this->page['pagination'] = 1;
                $this->page['page'] = 1;
            }
            else
            {
                $this->page['error404'] = true;
                $this->controller->setStatusCode(404);
                return;
            }
        }
        elseif ( $this_url[1] == ':catalog*' )
        {
            if ( Translator::instance()->getLocale() == 'en' )
            {
                $obCategory = $this->searchCategoryFromSlug();
            }

            if ( $obCategory ) {
                $this->showProductsForCategory($obCategory);
                $this->getLocalizedPage($obCategory);
            }
            else
            {
                $this->page['error404'] = true;
                $this->controller->setStatusCode(404);
                return;
            }

            /* Caricava tutti i prodotti
            $productColletion = ProductCollection::make([]); // Il make utilizza la cache
            $productColletion->active();
            $this->page['obProductList'] = $productColletion;
            foreach ($productColletion as $product) {
                if ($product->getObject()->ims_highlight == 1)
                    $highlighted[$product->getObject()->id] = $product;
            };
            if (isset($highlighted)) {
                $this->page['highlighted'] = $highlighted;
            }
            */
        }


       /*$this->page['obOfferPropertyList'] = $obCategory->offer_filter_property;
        $this->page['obProductPropertyList'] = $obCategory->product_filter_property;*/

        // dopo aver lanciato una ricerca o un filtro avrebbe senso che i filtri si adeguassero


        // Passaggio per accedere al vero e proprio oggetto prodotto
        //$product->getObject()->back_preview->path);

        /*
        $this->page['obProductPropertyList'] = FilterPanel::getProductPropertyList(['main'], $productColletion);
        $this->page['obOfferPropertyList'] = FilterPanel::getOfferPropertyList(['main'], $productColletion);
        */

        /* gestione search??
         * $session = Session::Get('obProductlist');
        \Log::alert($session);
        */
    }

    public function onSearchForm(){
        $data = post();
        // \Flash::success('Searching for: '.$data['search_label']);

        $obCategory = $this->get();
        $this->page['obCategory'] = $obCategory ;

        if($obCategory){
            $productColletion = ProductCollection::make([]); // Il make utilizza la cache
            $productColletion->active()->category($obCategory->id, true);
            // $productColletion->active()->category($obCategory->id, true)->filterByQuantity();
            $productColletion->active()->category($obCategory->id, true);
        }else{
            $productColletion = ProductCollection::make([]); // Il make utilizza la cache
            $productColletion->active();
            // $productColletion->active()->filterByQuantity();
            $productColletion->active();
        }

        //$productColletion = ProductCollection::make([]);

        // Controllo la lunghezza della parola cercata perchè la ricerca parte da 3 caratteri, inquesto modo l'elenco no nrimane vuoto se appena inizi a digitare
        // o quando cancelli tutto dalla ricerca
        if(Str::length($data['search']) > 2){
            $productColletion->active()->search($data['search']);
        }else{
            $productColletion->active();
        }

        /*
         * Search input - controlla se la ricerca è stata lanciata dal catalogo, e quindi aggiorna solo la productlist, o da una qualsiasi altra pagina,
         * e quindi reinderizza alla pagina di catalogo
         */
        $this_url = explode('/',$this->page->url);

        $params = [
            'obProductList' => $productColletion,
            'pagination' => $data['pagination']
        ];

        if($this_url[1] != 'search' ){
            return [
                '.panel-product-list' => $this->renderPartial('productListOnly', $params),
            ];
        }else{
            return Redirect::to('/search/'.$data['search']);
        }
    }

    /**
     * Ritorna i prodotti filtrati e ordinati
     * @param array $data
     * @return ProductCollection
     */
    public function getProductsCollectionWithOptions($data)
    {
        $productColletion = ProductCollection::make([])->active()->filterByQuantity(false);
        $appliedFilters = New Collection();
        $filtro_categorie = [];

        if ( isset($data['properties']) ) {

            foreach ($data['properties'] as $item) {
                if (starts_with($item['name'], 'category')) {
                    $category = str_after($item['name'], 'category_');
                    array_push($filtro_categorie, $category);

                    $productColletion = ProductCollection::make([])->active()->category($filtro_categorie, true)->filterByQuantity(false);
                }
                else if (starts_with($item['name'], 'property')) {

                    if(!isset($productColletion)){
                        $productColletion = ProductCollection::make([])->active()->filterByQuantity(false);
                    }

                    $obCategory = $this->get();
                    if($obCategory){
                        $productColletion->category($obCategory->id, true);
                    }

                    $controlName = $item['name'];
                    $controlValue = $item['value'];

                    if ($controlName != 'search' && !empty($controlValue)) {
                        $split = explode('_', $item['name']);

                        $propKey = $split[1];
                        if (!$appliedFilters->has($propKey)) {
                            //$appliedFilters->put($propKey, [$split[2]]);
                            $appliedFilters->put($propKey, [$controlValue]);
                        } else {
                            $arrayValues = $appliedFilters[$propKey];
                            //array_push($arrayValues, $split[2]);
                            array_push($arrayValues, $controlValue);
                            $appliedFilters[$propKey] = $arrayValues;
                        }
                    } else if ($item['value']) // Verifica se è stata inserita la ricerca
                    {
                        // Applica ricerca
                        $productColletion = $productColletion->search($item['value']);
                    }
                }
            }

            $obPropertySetList = PropertySetCollection::make()->sort()->code(['main']);
            $obProductPropertyList = $obPropertySetList->getProductPropertyCollection($productColletion);
            $obOfferPropertyList = $obPropertySetList->getOfferPropertyCollection($productColletion);

            // controllare se è possibile recuperare una collection delle offerte relative alla collection dei prodotti che si sta lavorando
            $offerCollection = OfferCollection::make([]);

            if ( $appliedFilters->count() > 0  ) {
                $productColletion = $productColletion->filterByProperty($appliedFilters->toArray(), $obProductPropertyList);
                $productColletion = $productColletion->filterByProperty($appliedFilters->toArray(), $obOfferPropertyList, $offerCollection->filterByQuantity(false));
            }
        } else if($data['sort_type'] == 'custom|season_desc') {
            $obCategory = $this->get();

            if($obCategory) {
                $productColletion->category($obCategory->id, true);
            }
        } else {
            // NESSUN FILTRO APPLICATO
            $obCategory = $this->get();

            if($obCategory) {
                $productColletion->category($obCategory->id, true);
            }

            $productColletion;
        }

        return $productColletion->sort($data['sort_type']);
    }

    public function onFilterProducts()
    {
        $data = post();

        $productColletion = $this->getProductsCollectionWithOptions($data);


        $productList = [
            'obProductList' => $productColletion,
            'pagination' => $data['pagination']
        ];

        return [
            //'.prod-list' => $this->renderPartial('productListUpdated', $productList)
            '.panel-product-list' => $this->renderPartial('productListOnly', $productList),
        ];

    }

    public function onChangeSorting(){
        $data = post();

        $productColletion = $this->getProductsCollectionWithOptions($data);

        $params = [
            'obProductList' => $productColletion,
            'pagination' => $data['pagination']
        ];

        return [
            //'.prod-list' => $this->renderPartial('productListUpdated', $params)
            '.panel-product-list' => $this->renderPartial('productListOnly', $params),
        ];
    }

    public function onPagination()
    {
        $data = post();


        $productColletion = $this->getProductsCollectionWithOptions($data);

        $productList = [
            'obProductList' => $productColletion,
            'pagination' => $data['pagination'],
            'page' => $data['pageCost']
        ];
        $pagOption = [
            'pagination' => $data['pagination'],
            'page' => $data['pageCost']
        ];


        return [
            //'.prod-list' => $this->renderPartial('productListUpdated', $productList),
            '.panel-product-list' => $this->renderPartial('productListOnly', $productList),
        ];
    }


    /* Vecchia funzione di paginazione
    public function onAjaxHelperRequest()
    {
        $data = post();

        $obCategory = $this->get();
        $this->page['obCategory'] = $obCategory;

        if ($obCategory) {
            $productColletion = ProductCollection::make([]); // Il make utilizza la cache
            // $productColletion = ProductCollection::make([])->filterByQuantity(); // Il make utilizza la cache
            $productColletion = ProductCollection::make([]); // Il make utilizza la cache
            $productColletion->active()->category($obCategory->id, true);
        } else {
            $productColletion = ProductCollection::make([]); // Il make utilizza la cache
            // $productColletion = ProductCollection::make([])->filterByQuantity(); // Il make utilizza la cache
            $productColletion = ProductCollection::make([]); // Il make utilizza la cache
            $productColletion->active();
        }

        $productList = [
            'obProductList' => $productColletion,
            'pagination' => $data['pagination'],
            'page' => $data['pageCost']
        ];
        $pagOption = [
            'pagination' => $data['pagination'],
            'page' => $data['pageCost']
        ];


        return [
            '.prod-list' => $this->renderPartial('productListUpdated', $productList),
            //'.pagination' => $this->renderPartial('pagination', $pagOption)
        ];
    }*/

    public function onValidation(){

        $data = post();

        //\Log::alert($data);

        $rules = [
            'name'  => 'required',
        ];

        $messages = [
            'name.required' => 'Errore: Inserire il nome, è richiesto per proseguire',
        ];

        // Validazione campi standard
        $validator = Validator::make($data, $rules, $messages);


        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        $arOrderData = [
            'order' => [
                'payment_method_id' => $data['shipping_type_id'],
                'shipping_type_id' => $data['shipping_type_id']
            ]
        ];

        $arUserData = [
            'user' => [
                'name' => $data['name'],
                'last_name' => $data['last_name'],
                'email' => $data['email'],
            ]
        ];


        //$order = MakeOrder::make([]);
        $order = app('Lovata\OrdersShopaholic\Components\MakeOrder');
        $order->create($arOrderData, $arUserData);
    }

    /**
     * Create new order (AJAX)
     * @return \Illuminate\Http\RedirectResponse|array
     * @throws \Exception
     */
    public function onCreate()
    {
        $arOrderData = (array) Input::get('order');
        $arUserData = (array) Input::get('user');

        $this->create($arOrderData, $arUserData);

        //Fire event and get redirect URL
        $sRedirectURL = Event::fire(OrderProcessor::EVENT_ORDER_GET_REDIRECT_URL, $this->obOrder, true);
        if (!Result::status() && !empty($this->obPaymentGateway) && $this->obPaymentGateway->isRedirect()) {
            $sRedirectURL = $this->obPaymentGateway->getRedirectURL();

            return Redirect::to($sRedirectURL);
        } else if (empty($this->obPaymentGateway) || !Result::status()) {
            $this->prepareResponseData();

            return $this->getResponseModeAjax($sRedirectURL);
        }

        if ($this->obPaymentGateway->isRedirect()) {
            $sRedirectURL = $this->obPaymentGateway->getRedirectURL();

            return Redirect::to($sRedirectURL);
        } else if ($this->obPaymentGateway->isSuccessful()) {
            Result::setTrue($this->obPaymentGateway->getResponse());
        } else {
            Result::setFalse($this->obPaymentGateway->getResponse());
        }

        Result::setMessage($this->obPaymentGateway->getMessage());
        $this->prepareResponseData();

        return $this->getResponseModeAjax($sRedirectURL);
    }

    /**
     * Get element object
     * @param string $sElementSlug
     * @return Category
     */
    protected function getElementObject($sElementSlug)
    {
        if (empty($sElementSlug)) {
            return null;
        }

        if (!$this->property('has_wildcard')) {
            $obElement = $this->getElementBySlug($sElementSlug);
        } else {
            $obElement = $this->getElementByWildcard($sElementSlug);
        }

        if (!empty($obElement)) {
            Event::fire('shopaholic.category.open', [$obElement]);
        }

        return $obElement;
    }

    /**
     * Controllo se nell'url è presente la lingua
     * @param String $locale minuscolo
     * @return bool
     */
    public function isUrlLocalized($locale)
    {
        $array = explode('/', $this->currentPageUrl());
        return $array[3] == $locale;
    }

    public function isProdottoEn()
    {
        $translator = Translator::instance();

        // Verifica se un prodotto in inglese e fa un redirect
        $pageFullUrl =  $this->currentPageUrl();
        $array = explode('/', $pageFullUrl);
        if ( $array[3] == 'prodotto' && $translator->getLocale() == 'en' )
        {
            $pageFullUrl = str_replace('prodotto', 'product', $pageFullUrl);
            return $pageFullUrl;
        }
        else
        {
            return "";
        }
    }

    /**
     * Get category by default
     * @param string $sElementSlug
     * @return Category|null
     */
    protected function getElementBySlug($sElementSlug)
    {

        $translator = Translator::instance();

        if ( $this->isSlugTranslatable() && $translator->getLocale() != $translator->getDefaultLocale() ) {
            $obElement = Category::active()->transWhere('slug', $sElementSlug)->first();
            if (!$this->checkTransSlug($obElement, $sElementSlug)) {
                $obElement = null;
            }

            if ( !$obElement && !$this->isUrlLocalized('it') ) {
                // Sono in inglese, url non in lingua effetto ricerca anche in italiano
                $obElement = Category::active()->getBySlug($sElementSlug)->first();
            }
        } else {
            $obElement = Category::active()->getBySlug($sElementSlug)->first();

            if ( !$obElement ) {
                $obElement = Category::active()->transWhere('slug', $sElementSlug, 'en')->first();
            }
        }

        return $obElement;
    }

    /**
     * Get category by wildcard
     * @param string $sElementSlug
     * @return Category|null
     */
    protected function getElementByWildcard($sElementSlug)
    {
        $arSlugList = explode('/', $sElementSlug);
        if (empty($arSlugList)) {
            return null;
        }

        $arSlugList = array_reverse($arSlugList);
        $sElementSlug = array_shift($arSlugList);

        $obElement = $this->getElementBySlug($sElementSlug);
        if (empty($obElement)) {
            return null;
        }

        if (empty($arSlugList) && empty($obElement->parent)) {
            return $obElement;
        }

        $obNestingElement = $obElement;

        foreach ($arSlugList as $sSlug) {
            $obNestingElement = $this->getNestingElement($sSlug, $obNestingElement);
            if (empty($obNestingElement)) {
                return null;
            }
        }

        if (!empty($obNestingElement->parent)) {
            return null;
        }

        return $obElement;
    }

    /**
     * Get nesting element
     * @param string   $sElementSlug
     * @param Category $obNestingElement
     * @return Category
     */
    protected function getNestingElement($sElementSlug, $obNestingElement)
    {
        if (empty($obNestingElement) || empty($sElementSlug)) {
            return null;
        }

        $obElement = $obNestingElement->parent;

        if (empty($obElement) || $obElement->slug != $sElementSlug) {
            return null;
        }

        return $obElement;
    }

    /**
     * Make new element item
     * @param int      $iElementID
     * @param Category $obElement
     * @return CategoryItem
     */
    protected function makeItem($iElementID, $obElement)
    {
        return CategoryItem::make($iElementID, $obElement);
    }
    
    public function getProductCount($category){
        $numerovuoto = 0; 
        $activeCat = Category::where(['id' => $category])->where(['active' => 1])->first();   //Padre
        if ($activeCat != null) {
            $productActiveChildCount = Product::where(['category_id' => $activeCat->id])->where(['active' => 1])->get();
            foreach ($productActiveChildCount as $productChild) {
                foreach($productChild->offer as $quantity){
                    $numerovuoto += $quantity->quantity;
                };
            }
            if ($numerovuoto == 0) {
                $activeCatChildren = Category::where(['parent_id' => $activeCat->id])->where(['active' => 1])->get();   //Figli
                if ($activeCatChildren != null) {
                    foreach ($activeCatChildren as $catChild) {
                        $productActiveChildChildCount = Product::where(['category_id' => $catChild->id])->where(['active' => 1])->get();
                        foreach ($productActiveChildChildCount as $productChildChild) {
                            foreach($productChildChild->offer as $quantityCount){
                                $numerovuoto += $quantityCount->quantity;
                            };
                        }
                    }
                }
            }
        }
        return  $numerovuoto;
    }
}
 