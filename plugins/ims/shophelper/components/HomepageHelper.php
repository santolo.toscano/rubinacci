<?php namespace Ims\Shophelper\Components;

use Carbon\Carbon;
use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Mail;
use Ims\Shophelper\Classes\Excel\UserExport;
use Ims\Shophelper\Helpers\EmailHelper;
use Ims\Shophelper\Helpers\ImsHelper;
use Ims\Shophelper\Models\NewsletterCustomer;
use Lovata\Buddies\Components\RestorePassword;
use Lovata\Buddies\Facades\AuthHelper;
use Lovata\Buddies\Models\User;
use Lovata\Shopaholic\Classes\Collection\ProductCollection;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Redirect;
use Lovata\Shopaholic\Classes\Item\CategoryItem;
use Lovata\Shopaholic\Models\Category;
use Lovata\Toolbox\Classes\Component\ElementPage;
use Event;
use October\Rain\Support\Facades\Flash;
use RainLab\Translate\Classes\Translator;
use Validator;
use ValidationException;
use Ims\Shophelper\Models\SimpleCatalogHelper as SimpleCatalog;
use Vdomah\Excel\Classes\Excel;

class HomepageHelper extends ComponentBase
{
    /**
     * @return array
     */
    public function componentDetails()
    {
        return [
            'name'        => 'IMS Shophelper - Homepage',
            'description' => 'IMS helper for homepage',
        ];
    }

    public function onRun()
    {
        $this->page['highlighted'] =  SimpleCatalog::get()->sortByDesc('id');
    }

}