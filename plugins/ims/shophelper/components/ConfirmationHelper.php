<?php

namespace Ims\Shophelper\Components;

use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\Input;
use Ims\Shophelper\Helpers\EmailHelper;
use Ims\Shophelper\Helpers\PaymentGatewayHelper;
use Lovata\OrdersShopaholic\Classes\Collection\OrderCollection;
use Lovata\OrdersShopaholic\Classes\Item\OrderItem;
use Lovata\OrdersShopaholic\Classes\Item\OrderPositionItem;
use Lovata\OrdersShopaholic\Models\Order;
use RainLab\Translate\Classes\Translator;

class ConfirmationHelper extends ComponentBase
{

    public function componentDetails()
    {
        return [
            'name'        => 'Confirmation Helper',
            'description' => ''
        ];
    }

    /*
    public function defineProperties()
    {
        return [
            'max' => [
                'description'       => 'The most amount of items allowed',
                'title'             => 'Max items',
                'default'           => 10,
                'type'              => 'string',
                'validationPattern' => '^[0-9]+$',
                'validationMessage' => 'The Max Items value is required and should be integer.'
            ]
        ];
    }
    */

    public function onRun()
    {
        $secret = Input::get("order");
        $success = false;

        /*$orderModel = Order::getBySecretKey($secret)->first();
        $orderModel = OrderItem::make($orderModel->id);*/
        if(isset($secret)){

            $orderModel = Order::getBySecretKey($secret)->first();
            //$orderItem = OrderItem::make($orderModel->id);

            //$couponCode = count($orderModel->coupon_list) > 0 ? $orderModel->coupon_list[0] : '';
            $success = true;

            if(isset($orderModel)) {
                $orderModel = OrderItem::make($orderModel->id);

                $mail = $orderModel && Input::get("mail", 0) == 1;
                $cancel = $orderModel && Input::get("cancel", 0) == 1;
                $fail = $orderModel && Input::get("fail", 0) == 1;

                // Annulla manualmente un ordine
                if ( $cancel || $fail )
                {
                    PaymentGatewayHelper::cancelOrder($orderModel->getObject());
                    $success = false;
                }

                // Invio manuale email
                if ( $mail ) {
                    EmailHelper::sendEmailUser($orderModel, Translator::instance()->getLocale());
                    EmailHelper::sendEmailManager($orderModel, 'it');
                }

                // Pagamento annullato ripristina la giacenza
                if ( $fail )
                {
                    PaymentGatewayHelper::restoreInventory($orderModel->getObject());
                    $success = false;
                }

                $is_payed = $orderModel->status->code == "complete";

                $this->page['is_payed'] = $is_payed;
            }else {
                $orderModel = 'empty';
            }
        }else{
            $orderModel = 'empty';
        }

        $this->page['order'] = $orderModel;
        $this->page['success'] = $success;
    }

}