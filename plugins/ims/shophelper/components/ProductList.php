<?php namespace Ims\Shophelper\Components;

use Lang;
use System\Classes\PluginManager;
use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\Log;

use Lovata\Toolbox\Classes\Component\SortingElementList;
use Lovata\Shopaholic\Classes\Collection\ProductCollection;
use Lovata\Shopaholic\Classes\Store\ProductListStore;
use Lovata\Shopaholic\Components\ProductList as PList;
use Lovata\Shopaholic\Components\CategoryPage;
use Lovata\Shopaholic\Classes\Item\CategoryItem;
use Lovata\FilterShopaholic\Components\FilterPanel;

use Illuminate\Support\Str;


class ProductList extends ComponentBase
{
    /**
     * @return array
     */
    public function componentDetails()
    {
        return [
            'name'        => 'IMS Shophelper - Product List',
            'description' => 'IMS helper for Shopaholic e-commerce.',
        ];
    }

    /**
     * @return array
     */
    public function defineProperties()
    {
        return [
            'sorting' => [
                'title'   => 'Tipo di ordinamento',
                'type'    => 'dropdown',
                'default' => ProductListStore::SORT_NO,
                'options' => [
                    ProductListStore::SORT_NO         => Lang::get('lovata.shopaholic::lang.component.sorting_no'),
                    ProductListStore::SORT_PRICE_ASC  => Lang::get('lovata.shopaholic::lang.component.sorting_price_asc'),
                    ProductListStore::SORT_PRICE_DESC => Lang::get('lovata.shopaholic::lang.component.sorting_price_desc'),
                    ProductListStore::SORT_NEW        => Lang::get('lovata.shopaholic::lang.component.sorting_new'),
                ],
            ],
        ];

    }
	
    public function onRun(){

        $productColletion = ProductCollection::make([]); // Il make utilizza la cache
		$productColletion->active();
		$this->page['arProductList'] = $productColletion;

//		$categoryitem = CategoryItem::make(5, $obElement);

    }
	
	public function onChangeSorting(){
		$data = post();
		\Flash::success('Sorting option: '.$data['sort_type']);
		
		$productColletion = ProductCollection::make([]);
		$productColletion->active()->sort($data['sort_type']);
		$productList['arProductList'] = $productColletion;
			
		return [
			'.prod-list' => $this->renderPartial('productListUpdated', $productList)
		]; 
		
	}
	
	public function onSearchForm(){
		$data = post();
		// \Flash::success('Searching for: '.$data['search_label']);
		
		$productColletion = ProductCollection::make([]);
		
		// Controllo la lunghezza della parola cercata perchè la ricerca parte da 3 caratteri, inquesto modo l'elenco no nrimane vuoto se appena inizi a digitare
        // o quando cancelli tutto dalla ricerca
		if(Str::length($data['search']) > 2){
			$productColletion->active()->search($data['search']);
		}else{
			$productColletion->active();
		}

		$productList['obProductList'] = $productColletion;

		return [
			'.prod-list' => $this->renderPartial('productListUpdated', $productList)
		]; 
		
	}

    public function onFilterOption(){
        $data = post();

        \Flash::success('called');


        $productColletion = ProductCollection::make([]);
        //$productColletion->active()->filterByProperty($data['applied_property'], $data['offer_property']);
        $productList['obProductList'] = $productColletion;

        return [
            '.prod-list' => $this->renderPartial('productListUpdated', $productList)
        ];


    }

}