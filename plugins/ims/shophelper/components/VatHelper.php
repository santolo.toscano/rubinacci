<?php namespace Ims\Shophelper\Components;

class VatHelper {

    private const defaultState = "IT";

    private const vatList = [
        "AT"=>20,
        "BE"=>21,
        "BG"=>20,
        "CY"=>19,
        "CZ"=>21,
        "DE"=>19,
        "DK"=>25,
        "EE"=>20,
        "EL"=>24,
        "GR"=>24,
        "ES"=>21,
        "FI"=>24,
        "FR"=>20,
        "HR"=>25,
        "HU"=>27,
        "IE"=>23,
        "IT"=>22,
        "LT"=>21,
        "LU"=>17,
        "LV"=>21,
        "MC"=>20,
        "MT"=>18,
        "NL"=>21,
        "PL"=>23,
        "PT"=>23,
        "RO"=>19,
        "SE"=>25,
        "SI"=>22,
        "SK"=>20,
    ];

    /**
     * Return vat from input state
     * If state isn't in the vatList, return defaultState vat
     */
    static function getVatFromState($state = VatHelper::defaultState) {
        if(!$state || !array_key_exists($state, VatHelper::vatList)) {
            return VatHelper::vatList[VatHelper::defaultState];
        }
        return VatHelper::vatList[$state];
    }

    /**
     * Return vat multiplier, equals to 1+($vat/100)
     */
    static function getVatMultiplier($vat) {
        return 1+($vat/100);
    }

    /**
     * Return vat multiplier from input state, equals to 1+($vat/100)
     * If state isn't in the vatList, return defaultState vat multiplier
     */
    static function getVatMultiplierFromState($state = VatHelper::defaultState) {
        $vat = VatHelper::getVatFromState($state);
        return VatHelper::getVatMultiplier($vat);
    }

}