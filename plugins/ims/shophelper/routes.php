<?php
//Newsletter
Route::get('unsubscribenewsletter/{email}', array('as' => 'errors', 'uses' => 'Ims\Shophelper\Controllers\NewsletterCustomerController@onRemoveNewsletter'));

//Navision
Route::get('api/shophelper/giacenzenav', array('as' => 'errors', 'uses' => 'Ims\Shophelper\Controllers\ApiController@giancenzeNavSync'));
Route::get('api/shophelper/import_order/{id}', array('as' => 'errors', 'uses' => 'Ims\Shophelper\Controllers\ApiController@importOrder'));

// Import products to Salesforce
Route::get('api/get/product/{id?}', array('as' => 'errors', 'uses' => 'Ims\Shophelper\Controllers\ApiController@importProductsSalesforce'));

Route::get('api/get/marketingcloud', array('as' => 'errors', 'uses' => 'Ims\Shophelper\Controllers\ApiController@updateProductsMC'));

// Import orders to Salesforce
Route::get('api/get/order/{year}/{month}', array('as' => 'errors', 'uses' => 'Ims\Shophelper\Controllers\ApiController@importOrdersSalesforce'));

//check email controllo presenza taglie
Route::get('api/shophelper/checkTaglie', 'Ims\Shophelper\Controllers\ApiController@checkTaglie');

//fix trade offer
Route::get('api/shophelper/fillTradeOffers/lang', array('as' => 'errors', 'uses' => 'Ims\Shophelper\Controllers\ApiController@fillTradeOffersLang'));
Route::get('api/shophelper/fillTradeOffers/properties', array('as' => 'errors', 'uses' => 'Ims\Shophelper\Controllers\ApiController@fillTradeOffersProperties'));

Route::get('api/shophelper/test', array('as' => 'errors', 'uses' => 'Ims\Shophelper\Controllers\ApiController@test'));

Route::get('api/shophelper/navusers', array('as' => 'errors', 'uses' => 'Ims\Shophelper\Controllers\ApiController@navisionCustomers'));

Route::get('api/feeds/products/{id}', array('as' => 'errors', 'uses' => 'Ims\Shophelper\Controllers\ApiController@feeds'));

Route::get('api/shophelper/categoryfix', array('as' => 'errors', 'uses' => 'Ims\Shophelper\Controllers\ApiController@categoryFix'));
//Route::get('api/shophelper/barcodesync', array('as' => 'barcode', 'uses' => 'Ims\Shophelper\Controllers\ApiController@barcodeSync'));
//Route::get('api/shophelper/navexean', array('as' => 'barcode', 'uses' => 'Ims\Shophelper\Controllers\ApiController@navExchangeEan'));
//Route::get('api/shophelper/navexcode', array('as' => 'barcode', 'uses' => 'Ims\Shophelper\Controllers\ApiController@navExchangeCode'));
//Route::get('api/shophelper/quantity', array('as' => 'quantity', 'uses' => 'Ims\Shophelper\Controllers\ApiController@quantity')););
//Route::get('api/shophelper/errors', array('as' => 'errors', 'uses' => 'Ims\Shophelper\Controllers\ApiController@errors'));
Route::get('api/shophelper/productcodefix', array('as' => 'errors', 'uses' => 'Ims\Shophelper\Controllers\ApiController@productCodeFix'));
Route::get('api/shophelper/productsbatch', array('as' => 'errors', 'uses' => 'Ims\Shophelper\Controllers\ApiController@productsBatch'));

Route::get('/api/stripe/payment', 'Ims\Shophelper\Controllers\StripeController@test');
Route::post('/api/stripe/payment', 'Ims\Shophelper\Controllers\StripeController@process');
Route::post('/api/stripe/checkout_confirm_payment', 'Ims\Shophelper\Controllers\StripeController@checkoutConfirmPayment');
Route::get('api/shophelper/clean', array('as' => 'clean', 'uses' => 'Ims\Shophelper\Controllers\ApiController@clean'));

Route::get('api/shophelper/dhl/{id}', array('as' => 'errors', 'uses' => 'Ims\Shophelper\Controllers\ApiController@dhl'));

Route::get('api/shophelper/app/giacenze', array('as' => 'errors', 'uses' => 'Ims\Shophelper\Controllers\ApiController@appQueryGiacenza'));

Route::get('api/excel/products', array('as' => 'errors', 'uses' => 'Ims\Shophelper\Controllers\Api\ExcelApiController@products'));
Route::get('api/excel/users', array('as' => 'errors', 'uses' => 'Ims\Shophelper\Controllers\Api\ExcelApiController@users'));

Route::get('api/get/oldcart', array('as' => 'errors', 'uses' => 'Ims\Shophelper\Controllers\ApiController@oldcart'));