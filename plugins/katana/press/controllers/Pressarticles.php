<?php namespace Katana\Press\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * pressarticles Back-end Controller
 */
class Pressarticles extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Katana.Press', 'press', 'pressarticles');
    }
}