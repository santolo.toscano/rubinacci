<?php namespace Katana\Press\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreatePressarticlesTable extends Migration
{

    public function up()
    {
        Schema::create('katana_press_pressarticles', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('katana_press_pressarticles');
    }

}
