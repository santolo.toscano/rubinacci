<?php namespace Katana\Press;

use Backend;
use System\Classes\PluginBase;

/**
 * press Plugin Information File
 */
class Plugin extends PluginBase
{

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Press',
            'description' => 'Managing press articles on Library page',
            'author'      => 'katana',
            'icon'        => 'icon-book'
        ];
    }

    public function registerNavigation()
    {
        return [
            'press' => [
                'label'       => 'Press',
                'url'         => Backend::url('katana/press/pressarticles'),
                'icon'        => 'icon-book',
                'permissions' => ['katana.pressarticles.*'],
                'order'       => 127,

                'sideMenu' => [
                    'collectionsAdd' => [
                        'label'       => 'Add',
                        'icon'        => 'icon-copy',
                        'url'         => Backend::url('katana/press/pressarticles/create'),
                        'permissions' => ['pressarticles.access_posts'],
                    ],
                    'collectionsList' => [
                        'label'       => 'List',
                        'icon'        => 'icon-copy',
                        'url'         => Backend::url('katana/press/pressarticles'),
                        'permissions' => ['pressarticles.access_posts'],
                    ]
                ]

            ]
        ];
    }

    public function registerComponents()
    {
        return [
            'Katana\Press\Components\PressArticleList' => 'PressArticleList'
        ];
    }    

}
