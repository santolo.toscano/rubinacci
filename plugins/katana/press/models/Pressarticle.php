<?php namespace Katana\Press\Models;

use Model;

/**
 * pressarticle Model
 */
class Pressarticle extends Model
{

    /**
     * @var string The database table used by the model.
     */
    public $table = 'katana_press_articles';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [
        'sample_image' => ['System\Models\File'],
        'article_pdf' => ['System\Models\File']
    ];
    public $attachMany = [];
    public $implement = ['RainLab.Translate.Behaviors.TranslatableModel'];


    public $translatable = [
        'name',


    ];
}