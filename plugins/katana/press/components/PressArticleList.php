<?php namespace Katana\Press\Components;

use Cms\Classes\ComponentBase;
use Katana\Press\Models\Pressarticle;

class PressArticleList extends ComponentBase
{

    public function componentDetails()
    {
        return [
            'name'        => 'Press Article List Component',
            'description' => 'Shows a list of Press Articles'
        ];
    }

    public function onRun()
    {
        $pressList = Pressarticle::orderBy('date', 'asc')->get();
        $this->page['presslist'] = $pressList;
    }


    public function defineProperties()
    {
        return [];
    }

}
