<?php namespace Lovata\PayPalShopaholic\Classes\Event;

use Lovata\PayPalShopaholic\Classes\Helper\PayPalExpressPaymentGateway;
use Lovata\PayPalShopaholic\Classes\Helper\PayPalProPaymentGateway;

/**
 * Class PaymentGatewayHandler
 * @package Lovata\PayPalShopaholic\Classes\Event
 * @author  Andrey Kharanenka, a.khoronenko@lovata.com, LOVATA Group
 */
class PaymentGatewayHandler
{
    /**
     * Add event listeners
     * @param \Illuminate\Events\Dispatcher $obEvent
     */
    public function subscribe($obEvent)
    {
        $obEvent->listen(PayPalExpressPaymentGateway::EVENT_GET_PAYMENT_GATEWAY_CLASS, function($sGatewayCode) {
            if ($sGatewayCode == PayPalExpressPaymentGateway::CODE) {
                return PayPalExpressPaymentGateway::class;
            } elseif ($sGatewayCode == PayPalProPaymentGateway::CODE) {
                return PayPalProPaymentGateway::class;
            }

            return null;
        });
    }
}