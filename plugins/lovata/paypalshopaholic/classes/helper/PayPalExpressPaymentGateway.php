<?php namespace Lovata\PayPalShopaholic\Classes\Helper;

use Input;
use Event;
use Redirect;
use Lovata\OmnipayShopaholic\Classes\Helper\PaymentGateway;

/**
 * Class PayPalExpressPaymentGateway
 * @package Lovata\PayPalShopaholic\Classes\Helper
 * @author  Andrey Kharanenka, a.khoronenko@lovata.com, LOVATA Group
 */
class PayPalExpressPaymentGateway extends PaymentGateway
{
    const CODE = 'PayPal_Express';

    const SUCCESS_RETURN_URL = 'shopaholic/payment/paypal_express/success/';
    const CANCEL_RETURN_URL = 'shopaholic/payment/paypal_express/cancel/';
    const NOTIFY_RETURN_URL = 'shopaholic/payment/paypal_express/notice/';

    const STATUS_SUCCESS = 'PaymentActionCompleted';
    const STATUS_IN_PROGRESS = 'PaymentActionInProgress';
    const STATUS_FAILED = 'PaymentActionFailed';
    const STATUS_CANCEL = 'PaymentActionNotInitiated';

    /**
     * Process success request
     * @param string $sSecretKey
     * @return \Illuminate\Http\RedirectResponse
     */
    public function processSuccessRequest($sSecretKey)
    {
        //Init order object
        $this->initOrderObject($sSecretKey);
        if (!$this->checkGatewayID() || !$this->checkToken()) {
            return Redirect::to('/');
        }

        //Prepare purchase data and send complete request
        $this->preparePurchaseData();
        $this->sendCompletePurchaseData();

        //Send "fetch checkout" request and update transaction data
        $this->updateCheckoutData();
        if (!empty($this->obResponse)) {
            //Update order status
            $this->updateOrderStatus(array_get($this->obResponse->getData(), 'CHECKOUTSTATUS'));
        }

        Event::fire(self::EVENT_PROCESS_RETURN_URL, [
            $this->obOrder,
            $this->obPaymentMethod,
        ]);

        //Get redirect URL
        $sRedirectURL = $this->getReturnURL();

        return Redirect::to($sRedirectURL);
    }

    /**
     * Process cancel request
     * @param string $sSecretKey
     * @return \Illuminate\Http\RedirectResponse
     */
    public function processCancelRequest($sSecretKey)
    {
        //Init order object
        $this->initOrderObject($sSecretKey);
        if (!$this->checkGatewayID() || !$this->checkToken()) {
            return Redirect::to('/');
        }

        //Prepare purchase data and send "fetch checkout" request
        $this->preparePurchaseData();
        $this->updateCheckoutData();

        //Fire event
        Event::fire(self::EVENT_PROCESS_CANCEL_URL, [
            $this->obOrder,
            $this->obPaymentMethod,
        ]);

        $this->setCancelStatus();

        //Get redirect URL
        $sRedirectURL = $this->getCancelURL();

        return Redirect::to($sRedirectURL);
    }

    /**
     * Process notify URL
     * @param string $sSecretKey
     */
    public function processNotifyRequest($sSecretKey)
    {
        //Init order object
        $this->initOrderObject($sSecretKey);
        if (!$this->checkGatewayID() || !$this->checkToken()) {
            return;
        }

        //Fire event
        Event::fire(self::EVENT_PROCESS_NOTIFY_URL, [
            $this->obOrder,
            $this->obPaymentMethod,
        ]);
    }

    /**
     * Prepare purchase data
     */
    protected function preparePurchaseData()
    {
        parent::preparePurchaseData();

        $this->arPurchaseData['returnUrl'] = url(self::SUCCESS_RETURN_URL.$this->obOrder->secret_key);
        $this->arPurchaseData['cancelUrl'] = url(self::CANCEL_RETURN_URL.$this->obOrder->secret_key);
        $this->arPurchaseData['notifyUrl'] = url(self::NOTIFY_RETURN_URL.$this->obOrder->secret_key);
    }

    /**
     * Check payment token
     * @return bool
     */
    protected function checkToken()
    {
        //Get token from request
        $sToken = Input::get('token');
        if (empty($sToken) || empty($this->obOrder) || $this->obOrder->payment_token != $sToken) {
            return false;
        }

        return true;
    }

    /**
     * Check payment method and gateway ID
     * @return bool
     */
    protected function checkGatewayID()
    {
        if (empty($this->obOrder) || empty($this->obPaymentMethod) || empty($this->obPaymentMethod->gateway_id)) {
            return false;
        }

        return $this->obPaymentMethod->gateway_id == self::CODE;
    }

    /**
     * Send "fetch checkout" request and update transaction data
     */
    protected function updateCheckoutData()
    {
        $this->sendFetchCheckoutData();
        $this->processFetchCheckoutResponse();
    }

    /**
     * Send fetchCheckout request to payment gateway
     */
    protected function sendFetchCheckoutData()
    {
        try {
            $this->obResponse = $this->obGateway->fetchCheckout($this->arPurchaseData)->send();
        } catch (\Exception $obException) {
            $this->sResponseMessage = $obException->getMessage();
            return;
        }
    }

    /**
     * Process fetchCheckout request to payment gateway
     */
    protected function processFetchCheckoutResponse()
    {
        if (empty($this->obResponse)) {
            return;
        }

        $arPaymentResponse = (array) $this->obOrder->payment_response;
        $arPaymentResponse['checkout'] = (array) $this->obResponse->getData();

        $this->obOrder->payment_response = $arPaymentResponse;
        $this->obOrder->transaction_id = array_get($arPaymentResponse, 'checkout.TRANSACTIONID');
        $this->obOrder->save();
    }

    /**
     * Update order status
     * @param string $sStatusCode
     */
    protected function updateOrderStatus($sStatusCode)
    {
        switch ($sStatusCode) {
            case self::STATUS_SUCCESS:
                $this->setSuccessStatus();
                break;
            case self::STATUS_CANCEL:
                $this->setCancelStatus();
                break;
            case self::STATUS_FAILED:
                $this->setFailStatus();
                break;
            case self::STATUS_IN_PROGRESS:
                $this->setWaitPaymentStatus();
                break;
        }
    }
}
