<?php namespace Lovata\PayPalShopaholic\Classes\Helper;

use Lovata\OmnipayShopaholic\Classes\Helper\PaymentGateway;

/**
 * Class PayPalProPaymentGateway
 * @package Lovata\PayPalShopaholic\Classes\Helper
 * @author  Andrey Kharanenka, a.khoronenko@lovata.com, LOVATA Group
 */
class PayPalProPaymentGateway extends PaymentGateway
{
    const CODE = 'PayPal_Pro';

    /**
     * Process purchase request to payment gateway
     */
    protected function processPurchaseResponse()
    {
        if (empty($this->obResponse)) {
            return;
        }

        $this->bIsSuccessful = $this->obResponse->isSuccessful();

        $arPaymentResponse = (array) $this->obOrder->payment_response;
        $arPaymentResponse['response'] = (array) $this->obResponse->getData();

        $this->obOrder->payment_response = $arPaymentResponse;
        $this->obOrder->payment_token = $this->obResponse->getTransactionReference();
        $this->obOrder->transaction_id = array_get($this->obResponse->getData(), 'TRANSACTIONID');
        $this->obOrder->save();

        if ($this->bIsSuccessful) {
            $this->updateTransactionData();
            $this->setSuccessStatus();
        }
    }

    /**
     * Send "fetch checkout" request and update transaction data
     */
    protected function updateTransactionData()
    {
        $this->sendFetchTransactionData();
        $this->processFetchTransactionResponse();
    }

    /**
     * Send fetchTransaction request to payment gateway
     */
    protected function sendFetchTransactionData()
    {
        try {
            $this->obResponse = $this->obGateway->fetchTransaction($this->arPurchaseData)->send();
        } catch (\Exception $obException) {
            $this->sResponseMessage = $obException->getMessage();
            return;
        }
    }

    /**
     * Process fetchTransaction request to payment gateway
     */
    protected function processFetchTransactionResponse()
    {
        if (empty($this->obResponse)) {
            return;
        }

        $arPaymentResponse = (array) $this->obOrder->payment_response;
        $arPaymentResponse['transaction'] = (array) $this->obResponse->getData();

        $this->obOrder->payment_response = $arPaymentResponse;
    }
}
