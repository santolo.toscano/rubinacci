<?php

use Lovata\PayPalShopaholic\Classes\Helper\PayPalExpressPaymentGateway;

Route::get(PayPalExpressPaymentGateway::SUCCESS_RETURN_URL.'{slug}', function ($sSecretKey) {
    $obPayPalExpressPaymentGateway = new PayPalExpressPaymentGateway();
    return $obPayPalExpressPaymentGateway->processSuccessRequest($sSecretKey);
});

Route::get(PayPalExpressPaymentGateway::CANCEL_RETURN_URL.'{slug}', function ($sSecretKey) {
    $obPayPalExpressPaymentGateway = new PayPalExpressPaymentGateway();
    return $obPayPalExpressPaymentGateway->processCancelRequest($sSecretKey);
});

Route::any(PayPalExpressPaymentGateway::NOTIFY_RETURN_URL.'{slug}', function ($sSecretKey = null) {
    $obPayPalExpressPaymentGateway = new PayPalExpressPaymentGateway();
    return $obPayPalExpressPaymentGateway->processNotifyRequest($sSecretKey);
});
