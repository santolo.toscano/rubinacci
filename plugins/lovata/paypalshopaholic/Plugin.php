<?php namespace Lovata\PayPalShopaholic;

use Event;
use System\Classes\PluginBase;

use Lovata\PayPalShopaholic\Classes\Event\PaymentGatewayHandler;

/**
 * Class Plugin
 * @package Lovata\PayPalShopaholic
 * @author Andrey Kharanenka, a.khoronenko@lovata.com, LOVATA Group
 */
class Plugin extends PluginBase
{
    /** @var array Plugin dependencies */
    public $require = ['Lovata.OmnipayShopaholic', 'Lovata.OrdersShopaholic', 'Lovata.Shopaholic', 'Lovata.Toolbox'];

    /**
     * Boot plugin method
     */
    public function boot()
    {
        $this->addListeners();
    }

    private function addListeners()
    {
        Event::subscribe(PaymentGatewayHandler::class);
    }
}
