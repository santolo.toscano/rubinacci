# PayPal for Shopaholic plugin for October CMS

## Description

E-Commerce plugin by [LOVATA](https://lovata.com) for October CMS.

[PayPal for Shopaholic](http://octobercms.com/plugin/lovata-paypalshopaholic) plugin adds integration with
[PayPal](https://www.paypal.com) payment system.
 
 ## Installation guide
 
Add **ignited/laravel-omnipay** and **omnipay/paypal** packages to the composer.json of your project.
 ```
 {
     "require": [
         ...
        "ignited/laravel-omnipay": "2.*",
         "omnipay/paypal": "^2.6",
     ],
 ```
 
 Execute below at the root of your project.
 ```
 composer update
 ```
 You can also install only packages and its dependencies without updating other packages by specifying the package.
 ```
 composer require ignited/laravel-omnipay
 composer require omnipay/paypal
 ```
 
## License

© 2018, [LOVATA Group, LLC](https://lovata.com) under Commercial License.

Developed by [Andrey Kharanenka](https://github.com/kharanenka).