<?php namespace Lovata\Shopaholic\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

/**
 * Class UpdateTableUsersAddCurrencyField
 * @package Lovata\Shopaholic\Updates
 */
class UpdateTableOfferAddColumnIsGiacenzeUk extends Migration
{
    /**
     * Apply migration
     */
    public function up()
    {
        if (Schema::hasTable('lovata_shopaholic_offers') && !Schema::hasColumn('lovata_shopaholic_offers', 'is_giacenze_uk')) {

            Schema::table('lovata_shopaholic_offers', function (Blueprint $obTable) {
                $obTable->boolean('is_giacenze_uk')->default(0);
            });
        }
    }

    /**
     * Rollback migration
     */
    public function down()
    {
        if (Schema::hasTable('lovata_shopaholic_offers') && Schema::hasColumn('lovata_shopaholic_offers', 'is_giacenze_uk')) {
            Schema::table('lovata_shopaholic_offers', function (Blueprint $obTable) {
                $obTable->dropColumn(['is_giacenze_uk']);
            });
        }
    }
}
