<?php namespace Lovata\Shopaholic\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableInsertLovataShopaholicSalesPeriod extends Migration
{
    public function up()
    {
        DB::table('lovata_shopaholic_sales_period')->insert([
            'id' => 1,
            'enabled' => 0,
            'coupon_id' => 0,
            'start_date' => null,
            'end_date' => null,
        ]);
    }
    
    public function down()
    {
    }
}
