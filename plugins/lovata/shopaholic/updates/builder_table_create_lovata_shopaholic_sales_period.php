<?php namespace Lovata\Shopaholic\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateLovataShopaholicSalesPeriod extends Migration
{
    public function up()
    {
        Schema::create('lovata_shopaholic_sales_period', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->boolean('enabled')->default(0);
            $table->integer('coupon_id');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('lovata_shopaholic_sales_period');
    }
}
