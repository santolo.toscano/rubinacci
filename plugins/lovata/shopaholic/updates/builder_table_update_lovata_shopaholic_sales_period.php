<?php namespace Lovata\Shopaholic\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateLovataShopaholicSalesPeriod extends Migration
{
    public function up()
    {
        Schema::table('lovata_shopaholic_sales_period', function($table)
        {
            $table->date('start_date')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('lovata_shopaholic_sales_period', function($table)
        {
            $table->dropColumn('start_date');
        });
    }
}
