<?php namespace Lovata\Shopaholic\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateLovataShopaholicSalesPeriod2 extends Migration
{
    public function up()
    {
        Schema::table('lovata_shopaholic_sales_period', function($table)
        {
            $table->date('end_date')->nullable();
            $table->date('start_date')->default(null)->change();
        });
    }
    
    public function down()
    {
        Schema::table('lovata_shopaholic_sales_period', function($table)
        {
            $table->dropColumn('end_date');
            $table->date('start_date')->default(NULL)->change();
        });
    }
}
