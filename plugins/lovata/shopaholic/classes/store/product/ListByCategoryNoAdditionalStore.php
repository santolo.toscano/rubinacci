<?php namespace Lovata\Shopaholic\Classes\Store\Product;

use DB;
use Event;
use Lovata\Toolbox\Classes\Store\AbstractStoreWithParam;

use Lovata\Shopaholic\Models\Product;

/**
 * Class ListByCategoryNoAdditionalStore
 * @package Lovata\Shopaholic\Classes\Store\Product
 * @author  Andrey Kharanenka, a.khoronenko@lovata.com, LOVATA Group
 */
class ListByCategoryNoAdditionalStore extends AbstractStoreWithParam
{
    protected static $instance;

    /**
     * Clear element ID list
     * @param string $sFilterValue
     */
    public function clear($sFilterValue)
    {
        parent::clear($sFilterValue);

        Event::fire('shopaholic.product.category_no_additional.clear', [$sFilterValue]);
    }

    /**
     * Get ID list from database
     * @return array
     */
    protected function getIDListFromDB() : array
    {
        $arElementIDList = (array) Product::getByCategory($this->sValue)->lists('id');

        return $arElementIDList;
    }
}
