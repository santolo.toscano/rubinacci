<?php namespace Lovata\Shopaholic\Models;

use October\Rain\Database\Model;

/**
 * Class AvailableProduct
 * @package Lovata\Shopaholic\Models
 * @author  Vincenzo Salerno, Citel Group
 *
 * @property                                                                                           $id
 * @property string                                                                                    $name
 * @property int                                                                                       $quantity
 * @property bool                                                                                      $active
 * @property string                                                                                    $category_name
 * @property string                                                                                    $brand_name
 * @property string                                                                                    $code
 * @property string                                                                                    $slug
 * @property string                                                                                    $external_id
 */
class AvailableProduct extends Model
{
    public $table = 'ims_shophelper_view_available_products';

    public $timestamps = false;
}
