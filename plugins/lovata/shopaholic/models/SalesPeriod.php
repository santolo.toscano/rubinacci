<?php namespace Lovata\Shopaholic\Models;

use Model;
use Illuminate\Support\Facades\DB;
use Lovata\CouponsShopaholic\Models\Coupon;
use October\Rain\Database\Traits\Validation;

/**
 * Class SalesPeriod
 * @package Lovata\Shopaholic\Models
 * @author  Santolo Toscano, Vincenzo Salerno, Citel Group
 *
 * @property int                                                     $id
 * @property bool                                                    $enabled
 * @property int                                                     $coupon_id
 * @property \October\Rain\Argon\Argon                               $start_date
 * @property \October\Rain\Argon\Argon                               $end_date
 *
 * @property Coupon                                                  $coupon
 * @method static Coupon|\October\Rain\Database\Relations\BelongsTo  coupon()
 *
 */
class SalesPeriod extends Model {
    use Validation;

    public $rules = [
        'coupon_id' => 'required'
    ];

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'lovata_shopaholic_sales_period';

    public $belongsTo = [
        'coupon' => [
            Coupon::class,
            'key' => 'coupon_id',
        ],
    ];

    public function getCouponIdOptions() {
        $ret = array();

        //retrieve lista coupon con user_id e max_usage a null
        $coupons = Coupon::where(['user_id' => null])
            ->where(['max_usage' => null])
            ->get();

        foreach ($coupons as $coupon){
            //reperisco coupon group associato
            $group = $coupon->coupon_group;

            //scarto se non attivo o se con limitazioni su max_usage/max_usage_per_user
            if(($group->max_usage !== NULL && $group->max_usage !== 0)
            || ($group->max_usage_per_user !== NULL && $group->max_usage_per_user !== 0)
            ) {
                continue;
            }

            //reperisco promo mechanism associata
            $mechanism = $group->mechanism;

            //dal group prendo date inizio e fine coupon
            $dateBegin = date_format($group->date_begin, "d/m/Y");
            $dateEnd = "-";
            if(isset($group->date_end)) {
                $dateEnd = date_format($group->date_end, "d/m/Y");
            }

            //traduzione da nome a simbolo del discount type
            $discount_type = $mechanism->discount_type === "percent" ? "%" : "€";

            //creo il record da mostrare con chiave=coupon_id
            $ret[$coupon->id] = "$coupon->code | -$mechanism->discount_value$discount_type | inizio coupon: $dateBegin, fine coupon: $dateEnd";
        }

        return $ret;
    }
}
