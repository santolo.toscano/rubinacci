<?php namespace Lovata\Shopaholic\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class SalesPeriod extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController',        'Backend\Behaviors\FormController'    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Lovata.Shopaholic', 'shopaholic-menu-promo', 'side-sales-period');
    }
}
