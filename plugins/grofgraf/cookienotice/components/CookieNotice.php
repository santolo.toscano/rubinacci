<?php namespace GrofGraf\CookieNotice\Components;

use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\Input;
use GrofGraf\CookieNotice\Models\Settings;
use Kharanenka\Helper\Result;

class CookieNotice extends ComponentBase
{
    private $cookies = [];
    public function componentDetails()
    {
        return [
            'name'        => 'cookieNotice Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
      return [
        'loadCSS' => [
            'title'       => 'Load CSS',
            'description' => 'Load css required for animation',
            'type'        => 'checkbox',
            'default'     => true,
        ],
      ];
    }

    public function onRun(){
      if(!isset($_COOKIE['confirm_cookie'])){
        $this->page['cookie_notice'] = $this->getCookieNotice();
      }
    }

    public function getCookieNotice(){
      if($this->property('loadCSS') == true){
        $this->addCss('assets/css/main.css');
      }
      $data = [
        "cookie_notice_content" => Settings::instance()->cookie_notice_content ?: "This site uses cookies. By continuing to browse the site, you are agreeing to our use of cookies.",
        "button_text" => Settings::instance()->button_text ?: "Agree!"
      ];

      return $this->renderPartial('::cookie-notice', $data);
    }

    public function onConfirmCookies(){
      $data = post();
      if(!isset($_COOKIE['confirm_cookie'])){
        if($data['check-social']=="false"){
          setcookie('ps_analytics',"",strtotime('+ ' . Settings::get('days_valid') . ' day'));
          setcookie("newsletternotifyclosed", "true", strtotime('+ ' . Settings::get('days_valid') . ' day'));
          setcookie("newsletternotify", "true", strtotime('+ ' . Settings::get('days_valid') . ' day'));
          if(!($data['check-performance']))
            setcookie('TawkConnection',"",strtotime('+ ' . Settings::get('days_valid') . ' day'));
          Result::setMessage("newsletter");
        }
        if($data['check-performance']=="false")
          setcookie('TawkConnection',"",strtotime('+ ' . Settings::get('days_valid') . ' day'));
        setcookie("confirm_cookie", md5("confirm_cookie"), strtotime('+ ' . Settings::get('days_valid') . ' day'));
      }
      return Result::get();
    }
    public function onDenyCookies(){
      // Clear all cookies
      /**
       * TODO HERE: remove all cookies from $_COOKIE
       * 
      */
      return;
    }

    public function siteKey(){
      return Settings::get('site_key');
    }

    public function enableCaptcha(){
      return Settings::get('enable_captcha');
    }
}
