<?php return [
    'plugin' => [
        'name' => 'Stripe для Shopaholic',
        'description' => 'Добавляет возможность оплаты заказов через Stripe',
    ],
    'component'            => [
        'payment_card_form_name'                       => 'Форма платежной карты',
        'payment_card_form_description'                => 'Ввод реквизитов карты для оплаты заказа',
    ],
    'settings' => [
        'api_key_public' => 'API Ключ публичный',
    ],
];