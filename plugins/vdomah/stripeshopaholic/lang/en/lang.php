<?php return [
    'plugin' => [
        'name' => 'Stripe for Shopaholic',
        'description' => 'Adds possibility to pay for orders using Stripe',
    ],
    'component'            => [
        'payment_card_form_name'                       => 'Payment Card Form',
        'payment_card_form_description'                => 'Enter card details to pay for order',
    ],
    'settings' => [
        'api_key_public' => 'API Key public',
        'payment_mode_option_make_order' => 'Payment on order create (using MakeOrder component)',
        'payment_mode_option_order_page' => 'Payment after order is created (using OrderPage component)',
        'payment_mode' => 'Payment Mode',
    ],
];