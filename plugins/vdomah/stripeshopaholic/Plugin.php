<?php namespace Vdomah\StripeShopaholic;

use Event;
use System\Classes\PluginBase;

use Vdomah\StripeShopaholic\Classes\Event\ExtendFieldHandler;
use Vdomah\StripeShopaholic\Classes\Event\ExtendOrderCreateHandler;

/**
 * Class Plugin
 * @package Vdomah\StripeShopaholic
 * @author Artem Rybachuk, alchemistt@ukr.net
 */
class Plugin extends PluginBase
{
    public $require = ['Lovata.Toolbox', 'Lovata.Shopaholic', 'Lovata.OrdersShopaholic', 'Lovata.OmnipayShopaholic'];

    /**
     * Register component plugin method
     * @return array
     */
    public function registerComponents()
    {
        return [
            'Vdomah\StripeShopaholic\Components\PaymentCardForm'     => 'PaymentCardForm',
        ];
    }

    /**
     * Boot plugin method
     */
    public function boot()
    {
        $this->addEventListener();
    }

    /**
     * Add event listeners
     */
    protected function addEventListener()
    {
        Event::subscribe(ExtendFieldHandler::class);
        Event::subscribe(ExtendOrderCreateHandler::class);
    }
}
