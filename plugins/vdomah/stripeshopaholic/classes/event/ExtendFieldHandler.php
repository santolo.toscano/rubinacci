<?php namespace Vdomah\StripeShopaholic\Classes\Event;

use Input;
use Lovata\OrdersShopaholic\Models\PaymentMethod;
use Lovata\OrdersShopaholic\Controllers\PaymentMethods;

/**
 * Class ExtendFieldHandler
 * @package Vdomah\StripeShopaholic\Classes\Event
 * @author Artem Rybachuk, alchemistt@ukr.net
 */
class ExtendFieldHandler
{
    const STRIPE_GATEWAY_ID = 'Stripe';
    const PAYMENT_MODE_OPTION_MAKE_ORDER = 'make_order';
    const PAYMENT_MODE_OPTION_ORDER_PAGE = 'order_page';

    /**
     * Add listeners
     * @param \Illuminate\Events\Dispatcher $obEvent
     */
    public function subscribe($obEvent)
    {
        $obEvent->listen('backend.form.extendFields', function ($obWidget) {
            $this->extendPaymentMethodFields($obWidget);
        });
    }

    /**
     * Extend settings fields
     * @param \Backend\Widgets\Form $obWidget
     */
    protected function extendPaymentMethodFields($obWidget)
    {
        // Only for the Settings controller
        if (!$obWidget->getController() instanceof PaymentMethods || $obWidget->isNested) {
            return;
        }

        // Only for the Settings model
        if (!$obWidget->model instanceof PaymentMethod || empty($obWidget->model->gateway_id) || $obWidget->model->gateway_id != self::STRIPE_GATEWAY_ID) {
            return;
        }

        $this->addGatewayPropertyFields($obWidget->model, $obWidget);
    }

    /**
     * Add gateway property list
     * @param PaymentMethod         $obPaymentMethod
     * @param \Backend\Widgets\Form $obWidget
     */
    protected function addGatewayPropertyFields($obPaymentMethod, $obWidget)
    {
        $obWidget->addTabFields([
            'gateway_property[payment_mode]' => [
                'label' => 'vdomah.stripeshopaholic::lang.settings.payment_mode',
                'tab'   => 'lovata.ordersshopaholic::lang.tab.gateway',
                'type'  => 'dropdown',
                'span'  => 'left',
                'options' => [
                    self::PAYMENT_MODE_OPTION_MAKE_ORDER => 'vdomah.stripeshopaholic::lang.settings.payment_mode_option_make_order',
                    self::PAYMENT_MODE_OPTION_ORDER_PAGE => 'vdomah.stripeshopaholic::lang.settings.payment_mode_option_order_page',
                ],
            ],
            'gateway_property[api_key_public]' => [
                'label' => 'vdomah.stripeshopaholic::lang.settings.api_key_public',
                'tab'   => 'lovata.ordersshopaholic::lang.tab.gateway',
                'type'  => 'text',
                'span'  => 'left',
            ],
        ]);
    }
}
