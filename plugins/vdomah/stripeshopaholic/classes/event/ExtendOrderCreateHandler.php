<?php namespace Vdomah\StripeShopaholic\Classes\Event;

use Input;

use Lovata\OrdersShopaholic\Models\PaymentMethod;
use Lovata\OrdersShopaholic\Classes\Processor\OrderProcessor;

/**
 * Class ExtendOrderCreateHandler
 * @package Vdomah\StripeShopaholic\Classes\Event
 * @author Artem Rybachuk, alchemistt@ukr.net
 */
class ExtendOrderCreateHandler
{
    const STRIPE_TOKEN_PARAM_NAME = 'stripeToken';

    /**
     * Add listeners
     * @param \Illuminate\Events\Dispatcher $obEvent
     */
    public function subscribe($obEvent)
    {
        $obEvent->listen(OrderProcessor::EVENT_UPDATE_ORDER_AFTER_CREATE, function ($obOrder) {
            $this->extendOrderAfterCreate($obOrder);
        });
    }

    /**
     * Handles 'shopaholic.order.after_create' event to assign stripe token to order
     * @param \Lovata\OrdersShopaholic\Models\Order $obOrder
     */
    protected function extendOrderAfterCreate($obOrder)
    {
        $bSave = false;

        $obPaymentMethod = $obOrder->payment_method;

        if (!$obPaymentMethod || $obPaymentMethod->gateway_id != ExtendFieldHandler::STRIPE_GATEWAY_ID)
            return;

        if ($obPaymentMethod->getProperty('payment_mode') == ExtendFieldHandler::PAYMENT_MODE_OPTION_MAKE_ORDER &&
            $sPaymentToken = Input::get(self::STRIPE_TOKEN_PARAM_NAME)
        ) {
            $obOrder->payment_token = $sPaymentToken;
            $bSave = true;
        }

        if ($bSave)
            $obOrder->save();
    }
}