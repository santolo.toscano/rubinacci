<?php

use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class DbSystemAddFrozenFlag extends Migration
{
    public function up()
    {
        if (Schema::hasColumn('system_plugin_versions', 'is_frozen')) {
            return;
        }
        Schema::table('system_plugin_versions', function (Blueprint $table) {
            $table->boolean('is_frozen')->default(0);
        });
    }

    public function down()
    {
        Schema::table('system_plugin_versions', function (Blueprint $table) {
            $table->dropColumn('is_frozen');
        });
    }
}
