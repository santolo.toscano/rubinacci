<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DbDeferredBindingsAddIndexSession extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $index = collect(DB::select("SHOW INDEXES FROM deferred_bindings"))->pluck('session_key');
        if ($index) {
            return;
        }
        Schema::table('deferred_bindings', function (Blueprint $table) {
            $table->index('session_key');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('deferred_bindings', function (Blueprint $table) {
            $table->dropIndex(['session_key']);
        });
    }
}
