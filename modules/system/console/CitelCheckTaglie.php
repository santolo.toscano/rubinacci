<?php namespace System\Console;

use Illuminate\Console\Command;
use Ims\Shophelper\Helpers\Helpers;
use Ims\Shophelper\Controllers\ApiController;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class CitelCheckTaglie extends Command
{
    /**
     * @var string The console command name.
     */
    protected $name = 'citel:checkTaglie';

    /**
     * @var string The console command description.
     */
    protected $description = 'No description provided yet...';

    /**
     * Execute the console command.
     * @return void
     */
    public function handle()
    {
        $help = new Helpers();
        $api = new ApiController($help);
        $help->printStartMessage("citel:checkTaglie");
        print_r($api->checkTaglie());
    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }
}
