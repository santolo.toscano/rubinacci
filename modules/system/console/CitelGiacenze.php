<?php namespace System\Console;

use Illuminate\Console\Command;
use Ims\Shophelper\Helpers\Helpers;
use Ims\Shophelper\Controllers\ApiController;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class CitelGiacenze extends Command
{
    /**
     * @var string The console command name.
     */
    protected $name = 'citel:giacenze';

    /**
     * @var string The console command description.
     */
    protected $description = 'No description provided yet...';

    /**
     * Execute the console command.
     * @return void
     */
    public function handle()
    {
        $help = new Helpers();
        $api = new ApiController($help);
        $help->printStartMessage("citel:giacenze");
        $oldMemory = ini_get('memory_limit');
        $oldInterval = ini_get('max_execution_time');
        ini_set('memory_limit', '4G');
        ini_set('max_execution_time', '30000');
        // print_r($api->giacenzeUpdateCitel());
        print_r($api->giancenzeNavSync());
        ini_set('memory_limit', $oldMemory);
        ini_set('max_execution_time', $oldInterval);
    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }
}
